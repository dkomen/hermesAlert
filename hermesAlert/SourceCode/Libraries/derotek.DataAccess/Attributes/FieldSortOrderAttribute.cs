﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.DataAccess.Attributes
{
    public enum OrderDirection
    {
        Descending = 0,
        Ascending = 1,
        None = 2
    }
    /// <summary>
    /// Define the order on a specific field in which the records must be sorted when retrieved
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class FieldSortOrderAttribute : System.Attribute
    {

        #region Properties
        public OrderDirection Direction { get; set; }
        #endregion

        #region Constructors
        public FieldSortOrderAttribute(OrderDirection direction)
        {
            Direction = direction;
        }
        #endregion

        #region Methods
        public static string GetNameOfFieldToSort<T>(out OrderDirection direction)
        {
            System.Reflection.PropertyInfo[] info = typeof(T).GetProperties();
            direction = OrderDirection.None;
            foreach (System.Reflection.PropertyInfo property in info)
            {
                object[] attribs = property.GetCustomAttributes(typeof(FieldSortOrderAttribute), true);
                if (attribs.Length > 0)
                {
                    direction = ((FieldSortOrderAttribute)attribs[0]).Direction;
                    return property.Name;
                }
            }
            return string.Empty;

        }
        #endregion
    }
}
