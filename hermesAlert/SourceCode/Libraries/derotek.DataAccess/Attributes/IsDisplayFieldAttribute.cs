﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.DataAccess.Attributes
{
    [System.AttributeUsage(AttributeTargets.Property)]
    public class IsDisplayFieldAttribute : System.Attribute
    {
        public static string GetPropertyValue<T>(T objectToQuery)
        {
            System.Reflection.PropertyInfo[] pi = objectToQuery.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo property in pi)
            {
                object[] attributesOfProperty = property.GetCustomAttributes(typeof(Attributes.IsDisplayFieldAttribute), true);
                if (attributesOfProperty.Length > 0)
                {
                    return property.GetValue(objectToQuery, null).ToString();
                }
            }
            return string.Empty;
        }
    }
}