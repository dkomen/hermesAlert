﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.DataAccess
{
    public interface IAuditTrail
    {
        DateTime DateCreated { get; set; }
        DateTime DateLastSaved { get; set; }
    }
}
