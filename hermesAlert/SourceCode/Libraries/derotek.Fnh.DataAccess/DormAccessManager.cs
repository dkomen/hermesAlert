﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess
{
    public class DormAccessManager
    {
        public static DORM.Interfaces.IDataAccessor GetFnhDataAccessor()
        {
            return new DORM.ConcreteAccessors.FluentNhibernate.Accessor<Mapping.PageHitsMap>(derotek.Fnh.DataAccess.Properties.Settings.Default.DBConnectionString, DORM.Enumerations.DatastoreType.MsSql);
        }
        public static void CreateDataStoreDDL()
        {
            new DORM.ConcreteAccessors.FluentNhibernate.Accessor<Mapping.PageHitsMap>(derotek.Fnh.DataAccess.Properties.Settings.Default.DBConnectionString, DORM.Enumerations.DatastoreType.MsSql).CreateSessionFactory(true, derotek.Fnh.DataAccess.Properties.Settings.Default.DBConnectionString, DORM.Enumerations.DatastoreType.MsSql);
        }
    }
}
