﻿/****** Object:  StoredProcedure [dbo].[GetUsersMonitoringGpsLocation]    Script Date: 11/09/2011 06:49:42 ******/
DROP PROCEDURE [dbo].[GetUsersMonitoringGpsLocation]
GO
/****** Object:  StoredProcedure [dbo].[SearchForStolenIdentificationNumber]    Script Date: 11/09/2011 06:49:42 ******/
DROP PROCEDURE [dbo].[SearchForStolenIdentificationNumber]
GO
/****** Object:  StoredProcedure [dbo].[GetEvents]    Script Date: 11/09/2011 06:49:41 ******/
DROP PROCEDURE [dbo].[GetEvents]
GO
/****** Object:  StoredProcedure [dbo].[GetStolenItemsInAreas]    Script Date: 11/09/2011 06:49:42 ******/
DROP PROCEDURE [dbo].[GetStolenItemsInAreas]
GO
/****** Object:  UserDefinedFunction [dbo].[LocationsBeingMonitored]    Script Date: 11/09/2011 06:49:45 ******/
DROP FUNCTION [dbo].[LocationsBeingMonitored]
GO
/****** Object:  UserDefinedFunction [dbo].[LocationsBeingMonitored]    Script Date: 11/09/2011 06:49:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[LocationsBeingMonitored]
(	
	-- Add the parameters for the function here
	@UserId bigint
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Id, Longitude, Latitude, RadiusInMeters FROM MonitoringLocation
	WHERE UserId = COALESCE(@UserId, UserId)
)
GO
/****** Object:  StoredProcedure [dbo].[GetStolenItemsInAreas]    Script Date: 11/09/2011 06:49:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetStolenItemsInAreas]
	-- Add the parameters for the stored procedure here
	@UserId bigint, 
	@PropertyTypeToGet bigint,
	@MaximumRecordsPerLocation int,
	@MaximumLocationsBeingMonitored int,
	@DateWasStolenStart DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF(@UserId=0)
	BEGIN 
		SET @UserId = NULL
	END
	Select Identity(int, 1,1) AS PK, Id, Longitude, Latitude, RadiusInMeters
	Into   #LocationsBeingMonitored
	From   LocationsBeingMonitored(@UserId)

	DECLARE @TempTable TABLE
    ( 
		Id bigint
    ) 

	DECLARE @RecordsMonitoring int
    DECLARE @Record int
    DECLARE @Long float
    DECLARE @Lat float
    DECLARE @RadiusInMeters int
    
    SELECT @RecordsMonitoring = MAX(PK) From #LocationsBeingMonitored    
    SET @Record = 1;
    
    WHILE @Record <= @RecordsMonitoring --For each location being monitored
    BEGIN
		
		SET @Long = (SELECT Longitude FROM #LocationsBeingMonitored WHERE PK = @Record)
		SET @Lat = (SELECT Latitude FROM #LocationsBeingMonitored WHERE PK = @Record)
		SET @RadiusInMeters = (SELECT RadiusInMeters FROM #LocationsBeingMonitored WHERE PK = @Record)
		if(@PropertyTypeToGet=0)
		BEGIN
			INSERT INTO @TempTable SELECT top(@MaximumRecordsPerLocation) Id 
			FROM RegisteredItem 
			WHERE 
				(CONVERT(Geography, SqlGeographyPoint).STDistance(CONVERT(Geography, 'POINT (' + CONVERT(char,@Long) + ' ' + CONVERT(char,@Lat) + ')'))) <= @RadiusInMeters  
				AND IsStolen=1 
				AND IfIsStolenMustNotifyCommunity=1
				AND DateWasStolen >= @DateWasStolenStart
				--ORDER BY DateWasStolen
		END
		ELSE
		BEGIN
			INSERT INTO @TempTable SELECT top(@MaximumRecordsPerLocation) Id 
			FROM RegisteredItem 
			WHERE 
				(CONVERT(Geography, SqlGeographyPoint).STDistance(CONVERT(Geography, 'POINT (' + CONVERT(char,@Long) + ' ' + CONVERT(char,@Lat) + ')'))) <= @RadiusInMeters  
				AND IsStolen=1 
				AND IfIsStolenMustNotifyCommunity=1
				AND PropertyType_Id = @PropertyTypeToGet
				AND DateWasStolen >= @DateWasStolenStart
		END
			SET @Record = @Record + 1
    END
	SELECT * FROM @TempTable	
END
GO
/****** Object:  StoredProcedure [dbo].[GetEvents]    Script Date: 11/09/2011 06:49:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dean Komen
-- Create date: 11 Aug 2011
-- Description:	Get all the incident in the monitoring areas for a user within a date range
-- =============================================
-- EXEC [dbo].[GetEvents] null, null, 200, '1 Aug 2011', '19 Sep 2011'
CREATE PROCEDURE [dbo].[GetEvents]
	-- Add the parameters for the stored procedure here
	@UserId bigint = NULL, 
	@EventTypeToGet bigint = NULL,
	@MaximumRecordsPerLocation int,
	@DateStart DateTime,
	@DateEnd DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempTable TABLE
	( 
		Id bigint
	) 

	if(@UserId IS NOT NULL)
	BEGIN
		-- ==========================
		-- Get all the incidents in the monitoring areas for a specific user within the date range
		-- ==========================
	
		--Get all area monitored by the user=@UserId
		Select Identity(int, 1,1) AS PK, Id, Longitude, Latitude, RadiusInMeters
		Into   #LocationsBeingMonitored
		From   LocationsBeingMonitored(@UserId)

		DECLARE @RecordsMonitoring int
		DECLARE @Record int
		DECLARE @Long float
		DECLARE @Lat float
		DECLARE @RadiusInMeters int
	    
		SELECT @RecordsMonitoring = MAX(PK) From #LocationsBeingMonitored    
		SET @Record = 1;
	    
		WHILE @Record <= @RecordsMonitoring --For each location being monitored
		BEGIN
			SET @Long = (SELECT Longitude FROM #LocationsBeingMonitored WHERE PK = @Record)
			SET @Lat = (SELECT Latitude FROM #LocationsBeingMonitored WHERE PK = @Record)
			SET @RadiusInMeters = (SELECT RadiusInMeters FROM #LocationsBeingMonitored WHERE PK = @Record)
			
			INSERT INTO @TempTable SELECT top(@MaximumRecordsPerLocation) Id 
			FROM Incident 
			WHERE 
				(CONVERT(Geography, SqlGeographyPoint).STDistance(CONVERT(Geography, 'POINT (' + CONVERT(char,@Long) + ' ' + CONVERT(char,@Lat) + ')'))) <= @RadiusInMeters  
				AND Id = COALESCE(@EventTypeToGet,Id)
				AND IncidentDateTime >= @DateStart AND IncidentDateTime <= @DateEnd
				ORDER BY IncidentDateTime
			SET @Record = @Record + 1
		END	
	END
	ELSE
	BEGIN	
		-- ==========================
		-- Get all the incidents within the date range
		-- ==========================
		INSERT INTO @TempTable SELECT Id 
		FROM Incident 
		WHERE 
			Id = COALESCE(@EventTypeToGet,Id)
			AND IncidentDateTime >= @DateStart AND IncidentDateTime <= @DateEnd
			ORDER BY IncidentDateTime	
	END
	
	SELECT * FROM @TempTable
END
GO
/****** Object:  StoredProcedure [dbo].[SearchForStolenIdentificationNumber]    Script Date: 11/09/2011 06:49:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SearchForStolenIdentificationNumber] 
	@SearchTerm varchar(50),
	@MaximumRecords int
AS
BEGIN
	SELECT TOP(@MaximumRecords) a.UserId as Id 
	FROM Attachment a
	JOIN RegisteredItem ri ON ri.Id = a.UserId
	WHERE 
		ri.IsStolen=1
		AND ri.IfIsStolenMustNotifyCommunity=1
		AND a.Description LIKE('%' + @SearchTerm + '%')
END
GO
/****** Object:  StoredProcedure [dbo].[GetUsersMonitoringGpsLocation]    Script Date: 11/09/2011 06:49:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Dean Komen
-- Create date: 29 Oct 2011
-- Description:	Return a list of userIs's of users that are monitoring the area in in which the supplied gps coordinate lies
-- =============================================
CREATE PROCEDURE [dbo].[GetUsersMonitoringGpsLocation]
	@SqlGeographyPoint Geography
AS
BEGIN
	SET NOCOUNT ON; 
	
	SELECT 
		DISTINCT(u.Id) as 'Id'
	FROM 
		MonitoringLocation as ml
	LEFT JOIN [dbo].[User] as u ON u.Id = ml.UserId
	WHERE 
		(CONVERT(Geography, SqlGeographyPoint).STDistance(@SqlGeographyPoint)) <= RadiusInMeters			
	
END
GO
