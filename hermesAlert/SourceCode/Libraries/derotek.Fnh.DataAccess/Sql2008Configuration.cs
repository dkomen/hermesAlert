﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Cfg.Db;
using NHibernate.Driver;
using NHibernate.Spatial.Dialect;

namespace derotek.Fnh.DataAccess
{
    public class Sql2008Configuration
  : PersistenceConfiguration<Sql2008Configuration, MsSqlConnectionStringBuilder>
    {
        public Sql2008Configuration()
        {
            Driver<SqlClientDriver>();
        }

        public static Sql2008Configuration MsSql2008
        {
            get { return new Sql2008Configuration().Dialect<MsSql2008GeographyDialect>(); }
        }
    }

}
