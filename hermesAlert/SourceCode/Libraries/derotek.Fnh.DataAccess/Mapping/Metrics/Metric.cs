﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping.Metrics
{
    public class Metric<T>: DORM.ConcreteAccessors.FluentNhibernate.EntityMap<T>
        where T : derotek.HairyDog.Metrics.BaseSimpleType
    {
        public Metric()
        {
            Map(x => x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.ShortDescription);
            Map(x => x.ExternalReferanceId).Not.Nullable();
            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateLastSaved).Not.Nullable();
        }
    }
}
