﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class RegisteredItemMap : EntityMap<derotek.HairyDog.RegisteredItem>
    {
        public RegisteredItemMap()
        {
            Map(x => x.UserId).Not.Nullable().Index("idx__RegisteredItem_OwnerId");
            Map(x => x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.ShortDescription);
            Map(x => x.IsStolen).Not.Nullable().Index("idx__RegisteredItem_IsStolen");
            Map(x => x.PoliceCaseNumber).Nullable();
            Map(x => x.IfIsStolenMustNotifyCommunity).Not.Nullable().Index("idx__RegisteredItem_IfIsStolenNotifyCommunity");
            Map(x => x.DateWasStolen).Not.Nullable().Index("idx__RegisteredItem_DateWasStolen");
            Map(x => x.Notes).Nullable().Length(derotek.Configuration.FieldLengths.Notes);
            References<derotek.HairyDog.Metrics.PropertyType>(x => x.PropertyType).Not.Nullable().Not.LazyLoad();
            Map(x => x.Latitude).Nullable();
            Map(x => x.Longitude).Nullable();
            Map(x => x.SqlGeographyPoint).Not.Nullable().CustomType(typeof(SqlGeographyUserType));
            References<derotek.HairyDog.Metrics.LocationAccuracy>(x => x.LocationAccuracy).Nullable().Not.LazyLoad();
            HasMany<derotek.HairyDog.Attachment>(x => x.Attachments).Not.LazyLoad().Inverse().KeyColumn("UserId").Cascade.DeleteOrphan();
            HasMany<derotek.HairyDog.ItemMonitoring.ItemDetection>(x => x.ItemDetectionMessages).Not.LazyLoad().Inverse().KeyColumn("UserId").Cascade.DeleteOrphan();
        }
    }
}
