﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class PageHitsMap: EntityMap<derotek.HairyDog.PageHits>
    {
        public PageHitsMap()
        {           
            Map(x => x.Page).Not.Nullable().Length(derotek.Configuration.FieldLengths.Path).Index("idx__PageHits_Page");
            Map(x => x.ReferrerIp).Not.Nullable().Length(15);
            Map(x => x.UserId).Nullable();
        }
    }
}
