﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class TownMap : EntityMap<derotek.HairyDog.Town>
    {
        public TownMap()
        {
            References<derotek.HairyDog.Province>(x => x.Province).Not.Nullable();
            Map(x=>x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.Country);
        }
    }
}
