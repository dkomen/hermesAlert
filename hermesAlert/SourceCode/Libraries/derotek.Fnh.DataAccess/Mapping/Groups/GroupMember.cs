﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping.Groups
{
    public class GroupMember: EntityMap<derotek.HairyDog.Groups.GroupMember>
    {
        public GroupMember()
        {
            //DeanK: must still figure out how to add colums to a UNIQUE idx__GroupMember_Membership
            Map(x => x.GroupId).Not.Nullable().Index("idx__GroupMember_Membership");
            Map(x => x.UserId).Not.Nullable().Index("idx__GroupMember_Membership");
            Map(x => x.IsAdministrator).Not.Nullable();
        }
    }
}
