﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping.Groups
{
    public class Group : EntityMap<derotek.HairyDog.Groups.Group>
    {
        public Group()
        {
            Map(x => x.UserId).Not.Nullable().Index("idx__Group_OwnerId");
            Map(x => x.Name).Not.Nullable().Length(derotek.Configuration.FieldLengths.ShortDescription);
            Map(x => x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.Description);
            //References<derotek.HairyDog.Groups.GroupMember>(x => x.Members).Nullable().Not.LazyLoad().ForeignKey("UserId");            
        }
    }
}
