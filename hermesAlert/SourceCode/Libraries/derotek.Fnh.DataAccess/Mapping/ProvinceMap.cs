﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class ProvinceMap : EntityMap<derotek.HairyDog.Province>
    {
        public ProvinceMap()
        {
            References<derotek.HairyDog.Country>(x => x.Country).Not.Nullable();
            Map(x=>x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.Country);
        }
    }
}
