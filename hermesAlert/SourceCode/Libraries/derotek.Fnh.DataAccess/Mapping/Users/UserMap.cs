﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace derotek.Fnh.DataAccess.Mapping.Users
{
    public class UserMap : EntityMap<derotek.HairyDog.Users.User>
    {
        public UserMap()
        {
            Map(x => x.UserVerificationHashRequirement).Nullable().Length(39);
            Map(x => x.UserRating).Not.Nullable();
            Map(x => x.ContactNumber).Nullable().Length(derotek.Configuration.FieldLengths.TelephoneNo);
            Map(x => x.EMailAddress).Not.Nullable().Length(derotek.Configuration.FieldLengths.Email);
            Map(x => x.Name).Nullable().Length(derotek.Configuration.FieldLengths.PersonName);
            Map(x => x.PasswordHash).Nullable().Length(derotek.Configuration.FieldLengths.PasswordHash);
            Map(x => x.SecurityQuestion).Not.Nullable().Length(derotek.Configuration.FieldLengths.SecurityQuestion);
            Map(x => x.SecurityQuestionAnswer).Not.Nullable().Length(derotek.Configuration.FieldLengths.SecurityQuestion);
            Map(x => x.Surname).Nullable().Length(derotek.Configuration.FieldLengths.PersonSurname);
            Map(x => x.NotificationsDeliveryMethod).Not.Nullable();
            Map(x => x.ReceiveNotificationsFrequency).Not.Nullable();
            Map(x => x.Theme).Not.Nullable().Length(derotek.Configuration.FieldLengths.ThemeName);
            Map(x => x.LastLoginDate);
            Map(x => x.PasswordResetId).Nullable().Length(derotek.Configuration.FieldLengths.PasswordResetId);
            Map(x => x.Username).Nullable().Length(derotek.Configuration.FieldLengths.Email).Index("idx__User_UserName");
            Map(x => x.Longitude).Not.Nullable().Default("28.138055324554443");
            Map(x => x.Latitude).Not.Nullable().Default("-25.941297589887277");
            References<derotek.HairyDog.Province>(x => x.Province).Not.Nullable();
            References<derotek.HairyDog.Town>(x => x.Town).Nullable();
            HasMany<derotek.HairyDog.RemoteLibrary.UploadedFile>(x => x.UploadedFiles).Not.LazyLoad().Inverse().KeyColumn("UserId").Cascade.DeleteOrphan();
            HasMany<derotek.HairyDog.Users.UserRole>(x => x.Roles).Not.LazyLoad().Inverse().KeyColumn("UserId").Cascade.DeleteOrphan();
        }
    }
}
