﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace derotek.Fnh.DataAccess.Mapping.Users
{
    public class UserRoleMap : EntityMap<derotek.HairyDog.Users.UserRole>
    {
        public UserRoleMap()
        {
            References<derotek.HairyDog.Metrics.Role>(x => x.Role).Not.Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Users.User>(x => x.User).Not.Nullable();
        }
    }
}
