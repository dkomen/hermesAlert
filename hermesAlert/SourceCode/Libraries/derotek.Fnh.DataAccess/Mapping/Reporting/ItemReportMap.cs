﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace derotek.Fnh.DataAccess.Mapping.Reporting
{
    public class ItemReportMap: EntityMap<derotek.HairyDog.Reporting.ItemReport>
    {
        public ItemReportMap()
        {
            Map(x => x.HasHeenAttendedTo).Not.Nullable();            
            Map(x=>x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.Description);
            Map(x => x.ReportedItemId).Not.Nullable();
            References<derotek.HairyDog.Users.User>(x => x.UserId).Not.Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Metrics.ItemType>(x => x.ItemType).Not.Nullable().Not.LazyLoad();
        }
    }
}
