﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping.Incidents
{
    public class IncidentMap : EntityMap<derotek.HairyDog.Incidents.Incident>
    {
        public IncidentMap()
        {
            Map(x => x.UserId).Not.Nullable().Index("idx__IncidentType_OwnerId");
            Map(x => x.IncidentDateTime).Not.Nullable().Index("idx__Incident_IncidentDateTime");
            Map(x => x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.MediumDescription); 
            Map(x => x.Longitude).Not.Nullable();
            Map(x => x.Latitude).Not.Nullable();
            Map(x => x.SqlGeographyPoint).Not.Nullable().CustomType(typeof(SqlGeographyUserType));
            References<derotek.HairyDog.Metrics.LocationAccuracy>(x => x.LocationAccuracy).Nullable().Not.LazyLoad(); 
            References<derotek.HairyDog.Metrics.IncidentType>(x => x.IncidentType).Not.Nullable().Not.LazyLoad();
        }
    }
}
