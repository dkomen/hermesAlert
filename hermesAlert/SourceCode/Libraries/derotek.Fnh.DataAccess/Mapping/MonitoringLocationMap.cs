﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class MonitoringLocationMap : EntityMap<derotek.HairyDog.MonitoringLocation>
    {
        public MonitoringLocationMap()
        {
            Map(x => x.UserId).Not.Nullable().Index("idx__MonitoringLocation_OwnerId");
            Map(x => x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.ShortDescription);
            Map(x => x.Longitude).Not.Nullable();
            Map(x => x.Latitude).Not.Nullable();
            Map(x => x.SqlGeographyPoint).Not.Nullable().CustomType(typeof(SqlGeographyUserType));
            Map(x => x.RadiusInMeters).Not.Nullable();
        }
    }
}
