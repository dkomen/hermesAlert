﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class CountryMap : EntityMap<derotek.HairyDog.Country>
    {
        public CountryMap()
        {
            Map(x => x.Code).Not.Nullable().Length(8);
            Map(x=>x.Description).Not.Nullable().Length(derotek.Configuration.FieldLengths.Country);
        }
    }
}
