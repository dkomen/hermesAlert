﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class EntityMap<T> : DORM.ConcreteAccessors.FluentNhibernate.EntityMap<T>
        where T : derotek.HairyDog.Entity
    {
        public EntityMap()
        {
            string obj = this.GetType().ToString();

            Map(x => x.DateCreated).Not.Nullable();
            Map(x => x.DateLastSaved).Not.Nullable().Index("idx__" + obj + "_DateLastSaved");
        }
    }
}