﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping
{
    public class AttachmentMap : EntityMap<derotek.HairyDog.Attachment>
    {
        public AttachmentMap()
        {
            Map(x => x.UserId).Not.Nullable().Index("idx__Attachment_OwnerId"); ;
            Map(x => x.Path).Nullable().Length(derotek.Configuration.FieldLengths.Path);
            Map(x => x.Description).Nullable().Length(derotek.Configuration.FieldLengths.ShortDescription);
            References<derotek.HairyDog.Metrics.IdentificationNumberType>(x => x.PictureShowIdNumberType).Not.Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Metrics.AttachmentType>(x => x.AttachmentType).Not.Nullable().Not.LazyLoad();
        }
    }
}
