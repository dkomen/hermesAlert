﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Mapping;

namespace derotek.Fnh.DataAccess.Mapping.PersonAlert
{
    public class PersonAlertMap : EntityMap<HairyDog.PersonAlert.PersonAlert>
    {
        public PersonAlertMap()
        {
            Map(x => x.UserId).Not.Nullable();
            Map(x => x.AwardAmount).Not.Nullable();
            Map(x => x.DateLastSeen).Not.Nullable();
            Map(x => x.Firstname).Not.Nullable().Length(derotek.Configuration.FieldLengths.PersonName);
            Map(x => x.KnownAs).Not.Nullable().Length(derotek.Configuration.FieldLengths.PersonName);
            Map(x => x.SecondName).Not.Nullable().Length(derotek.Configuration.FieldLengths.PersonName);
            Map(x => x.Surname).Not.Nullable().Length(derotek.Configuration.FieldLengths.PersonSurname);
            Map(x => x.ApproximateAge).Nullable().Length(derotek.Configuration.FieldLengths.Age);
            Map(x => x.DistinguishingFeatures).Nullable().Length(derotek.Configuration.FieldLengths.Description);
            Map(x => x.Notes).Not.Nullable().Length(derotek.Configuration.FieldLengths.Notes);
            Map(x => x.PathToImage).Not.Nullable().Length(derotek.Configuration.FieldLengths.Path);
            Map(x => x.PoliceCaseNumber).Not.Nullable().Length(derotek.Configuration.FieldLengths.PoliceCaseNumber);
            Map(x => x.ContactDetails).Nullable().Length(derotek.Configuration.FieldLengths.ContactDetailsMulti);
            Map(x => x.Solved).Not.Nullable();
            Map(x => x.Latitude).Nullable();
            Map(x => x.Longitude).Nullable();
            Map(x => x.SqlGeographyPoint).Not.Nullable().CustomType(typeof(SqlGeographyUserType));
            References<derotek.HairyDog.Metrics.ItemType>(x => x.ItemType).Not.Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Metrics.ApprovalStatus>(x => x.ApprovalStatus).Not.Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Metrics.LocationAccuracy>(x => x.LocationAccuracy).Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Metrics.EyeColour>(x => x.EyeColour).Not.Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Metrics.HairColour>(x => x.HairColour).Not.Nullable().Not.LazyLoad();
            References<derotek.HairyDog.Metrics.Gender>(x => x.Gender).Not.Nullable().Not.LazyLoad();
            HasMany<derotek.HairyDog.ItemMonitoring.ItemDetection>(x => x.ItemDetectionMessages).Not.LazyLoad().Inverse().KeyColumn("UserId").Cascade.DeleteOrphan();
        }
    }
}
