﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping.RemoteUpload
{
    public class UploadedFileMap : EntityMap<derotek.HairyDog.RemoteLibrary.UploadedFile>
    {
        public UploadedFileMap()
        {
            Map(x => x.UserId).Not.Nullable();
            Map(x => x.Path).Not.Nullable().Length(derotek.Configuration.FieldLengths.Path);
            Map(x => x.AttachmentType).Not.Nullable();
        }
    }
}
