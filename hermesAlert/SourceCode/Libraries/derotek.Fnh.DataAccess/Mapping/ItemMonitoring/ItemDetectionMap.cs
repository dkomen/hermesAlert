﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess.Mapping.ItemMonitoring
{
    public class ItemDetectionMap : EntityMap<derotek.HairyDog.ItemMonitoring.ItemDetection>
    {
        public ItemDetectionMap()
        {
            Map(x => x.OwnerItemId).Not.Nullable().Index("idx__ItemDetection_OwnerItemId");
            Map(x => x.KeepPrivate).Not.Nullable();
            Map(x => x.Notes).Nullable().Length(derotek.Configuration.FieldLengths.Notes);
            Map(x => x.HasBeenRead).Not.Nullable();
            Map(x => x.NotificationWasSent).Not.Nullable().Index("idx__ItemDetection_NotificationWasSent"); ;
            Map(x => x.Direction).Not.Nullable();
            Map(x => x.SecondPartyId).Not.Nullable();
            Map(x => x.Latitude).Nullable();
            Map(x => x.Longitude).Nullable();
            Map(x => x.SqlGeographyPoint).Not.Nullable().CustomType(typeof(SqlGeographyUserType));
            References<derotek.HairyDog.Metrics.LocationAccuracy>(x => x.LocationAccuracy).Nullable().Not.LazyLoad();
        }
    }
}
