﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Fnh.DataAccess
{
    public class SPROC_Functions
    {
        public void Test()
        {                        
            List<derotek.HairyDog.Users.User> list = GetListOfUsersMonitoringGpsPoint(Microsoft.SqlServer.Types.SqlGeography.Point(28.1433625221252, -25.9351111888894, 4326));
        }

        /// <summary>
        /// Call a SPROC that returns a list of all users monitoring the supplied gps coordinate
        /// </summary>
        /// <param name="gpsPoint"></param>
        /// <returns></returns>
        public static List<derotek.HairyDog.Users.User> GetListOfUsersMonitoringGpsPoint(Microsoft.SqlServer.Types.SqlGeography gpsPoint)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.OrderedProcedureParameters orderedParams = new DORM.OrderedProcedureParameters();
                orderedParams.Add(new DORM.ProcedureParameter() { Name = "SqlGeographyPoint", Value = gpsPoint, AlternateDataType = typeof(NHibernate.Spatial.Type.SqlGeographyType) });
                
                return transaction.ProcedureGet<derotek.HairyDog.Users.User>("GetUsersMonitoringGpsLocation", orderedParams);
            }
        }

        public static IList<derotek.HairyDog.RegisteredItem> SearchForStolenIdentificationNumber(string searchTerm, int maximumRecordsToReturn)
        {
            try
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    DORM.OrderedProcedureParameters orderedParams = new DORM.OrderedProcedureParameters();
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "SearchTerm", Value = searchTerm });
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "MaximumRecords", Value = maximumRecordsToReturn });

                    IList<DORM.Entity> stolenItems = transaction.ProcedureGet<DORM.Entity>("SearchForStolenIdentificationNumber", orderedParams);

                    if (stolenItems.Count > 0)
                    {
                        List<object> items = new List<object>();
                        foreach (DORM.Interfaces.IEntity entity in stolenItems)
                        {
                            items.Add(entity.Id);
                        }
                        return transaction.RetrieveWhereIn<derotek.HairyDog.RegisteredItem>("Id", items, new List<DORM.OrderBy>() { new DORM.OrderBy(){FieldName=DORM.HelperMethods.GetPropertyName(() => new derotek.HairyDog.RegisteredItem().DateWasStolen), Ordering=DORM.Enumerations.OrderingDirection.Descending}});
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not 'GetMany' objects of type : " + typeof(derotek.HairyDog.RegisteredItem).ToString(), ex);
            }
        }
        public static IList<derotek.HairyDog.RegisteredItem> GetStolenItemsInAreas(long idOfPersonMonitoring, long propertyTypeToGet, int maximumRecordsPerLocation, int maximumLocationsBeingMonitored, DateTime dateWasStolenStart)
        {
            try
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    DORM.OrderedProcedureParameters orderedParams = new DORM.OrderedProcedureParameters();
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "UserId", Value=idOfPersonMonitoring});
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "PropertyTypeToGet", Value=propertyTypeToGet});
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "MaximumRecordsPerLocation", Value=maximumRecordsPerLocation});
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "MaximumLocationsBeingMonitored", Value=maximumLocationsBeingMonitored});
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "DateWasStolenStart", Value = dateWasStolenStart });
                    IList<DORM.Entity> stolenItems = transaction.ProcedureGet<DORM.Entity>("GetStolenItemsInAreas", orderedParams);

                    if (stolenItems.Count > 0)
                    {
                        List<object> items = new List<object>();
                        foreach (DORM.Interfaces.IEntity entity in stolenItems)
                        {
                            items.Add(entity.Id);
                        }
                        return transaction.RetrieveWhereIn<derotek.HairyDog.RegisteredItem>("Id", items, new List<DORM.OrderBy>() { new DORM.OrderBy(){FieldName="IncidentDateTime", Ordering=DORM.Enumerations.OrderingDirection.Descending}});
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not 'GetMany' objects of type : " + typeof(derotek.HairyDog.RegisteredItem).ToString(), ex);
            }
        }
        public static IList<derotek.HairyDog.Incidents.Incident> GetIncidents(long? idOfUserMonitoring, long? incidentTypeToGet, int maximumRecordsPerLocation, int maximumLocationsBeingMonitored, DateTime dateStart, DateTime dateEnd)
        {
            try
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    DORM.OrderedProcedureParameters orderedParams = new DORM.OrderedProcedureParameters();
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "UserId", Value = idOfUserMonitoring });
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "EventTypeToGet", Value=incidentTypeToGet});
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "MaximumRecordsPerLocation", Value=maximumRecordsPerLocation});
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "DateStart", Value=dateStart});
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "DateEnd", Value=dateEnd});
                    IList<DORM.Entity> stolenItems = transaction.ProcedureGet<DORM.Entity>("GetEvents", orderedParams);

                    if (stolenItems.Count > 0)
                    {
                        List<object> items = new List<object>();
                        foreach (DORM.Interfaces.IEntity entity in stolenItems)
                        {
                            items.Add(entity.Id);
                        }
                        return transaction.RetrieveWhereIn<derotek.HairyDog.Incidents.Incident>("Id", items, new List<DORM.OrderBy>() { new DORM.OrderBy() { FieldName = "IncidentDateTime", Ordering = DORM.Enumerations.OrderingDirection.Descending } });
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not 'GetMany' objects of type : " + typeof(derotek.HairyDog.Incidents.Incident).ToString(), ex);
            }
        }
        public static IList<derotek.HairyDog.Users.User> GetUsersMonitoringGpsLocation(Microsoft.SqlServer.Types.SqlGeography sqlGeographyPoint)
        {
            try
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    DORM.OrderedProcedureParameters orderedParams = new DORM.OrderedProcedureParameters();
                    orderedParams.Add(new DORM.ProcedureParameter() { Name = "SqlGeographyPoint", Value = sqlGeographyPoint, AlternateDataType=typeof(NHibernate.Spatial.Type.SqlGeographyType) }); //command.Parameters[0].UdtTypeName = "Geography";
                    IList<DORM.Interfaces.IEntity> stolenItems = transaction.ProcedureGet<DORM.Interfaces.IEntity>("GetUsersMonitoringGpsLocation", orderedParams);

                    if (stolenItems.Count > 0)
                    {
                        List<object> items = new List<object>();
                        foreach (DORM.Interfaces.IEntity entity in stolenItems)
                        {
                            items.Add(entity.Id);
                        }
                        return transaction.RetrieveWhereIn<derotek.HairyDog.Users.User>("Id", items, null);
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Could not 'GetMany' objects of type : " + typeof(derotek.HairyDog.Incidents.Incident).ToString(), ex);
            }            
        }

    }
}
