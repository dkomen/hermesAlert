﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.SqlTypes;
using NHibernate;
using Microsoft.SqlServer.Types;
using System.Data;

using NHibernate.UserTypes;
using System.Data.SqlTypes;

namespace derotek.Fnh.DataAccess
{
    public class SqlGeographyUserType : IUserType
    {

        public bool Equals(object x, object y)
        {

            if (ReferenceEquals(x, y))

                return true;

            if (x == null || y == null)

                return false;

            return x.Equals(y);

        }

        public int GetHashCode(object x)
        {

            return x.GetHashCode();

        }

        public object NullSafeGet(IDataReader rs, string[] names, object owner)
        {

            object prop1 = NHibernateUtil.String.NullSafeGet(rs, names[0]);

            if (prop1 == null)

                return null;

            SqlGeography geo = SqlGeography.Parse(new SqlString(prop1.ToString()));



            return geo;

        }

        public void NullSafeSet(IDbCommand cmd, object value, int index)
        {

            if (value == null)

                ((IDataParameter)cmd.Parameters[index]).Value = DBNull.Value;

            else

                ((IDataParameter)cmd.Parameters[index]).Value = ((SqlGeography)value).STAsText().Value;

        }

        public object DeepCopy(object value)
        {

            if (value == null)

                return null;

            var sourceTarget = (SqlGeography)value;

            SqlGeography targetGeography = SqlGeography.Point(sourceTarget.Lat.Value, sourceTarget.Long.Value,

                                                              sourceTarget.STSrid.Value);

            return targetGeography;

        }

        public object Replace(object original, object target, object owner)
        {

            return DeepCopy(original);

        }

        public object Assemble(object cached, object owner)
        {

            return DeepCopy(cached);

        }

        public object Disassemble(object value)
        {

            return DeepCopy(value);

        }

        public SqlType[] SqlTypes
        {

            get { return new[] { NHibernateUtil.String.SqlType }; }

        }

        public Type ReturnedType
        {

            get { return typeof(SqlGeography); }

        }

        public bool IsMutable
        {

            get { return true; }

        }

    }
}
