﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Configuration
{
    public class FieldLengths
    {
        public const int Password = 25;
        public const int PasswordHash = 64;

        public const int Path = 255;
        public const int Code = 25;
        public const int Country = 50;
               
        public const int Email = 125;
        public const int TelephoneNo = 25;
        public const int ContactDetailsMulti = 200;

        public const int Username = Email;        
        public const int PersonName = 25;
        public const int PersonSurname = 25;
        public const int PersonFullName = PersonName + PersonSurname + 10;
        public const int Age = 3;

        public const int ShortDescription = 60;
        public const int MediumDescription = 125;
        public const int MediumLongDescription = 175;
        public const int Description = 256;
        public const int Notes = 2048;

        public const int LogFileMessage = 1850;
        public const int ThemeName = 15;
        public const int IdentificationNumber = 40;

        public const int SecurityQuestion = 50;
        public const int PoliceCaseNumber = 20;

        public const int PasswordResetId = 15;

        public const int MonetoryAmount = 8;
    }
}
