﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Logging.LogServiceProviders
{
    public class FlatFileLogServiceProvider : ILogServiceProvider
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="applicationName"></param>
        /// <param name="accessObject">The folder in which the logfile must be created</param>
        public FlatFileLogServiceProvider(string applicationName, string accessObject)
        {
            AccessObject = accessObject;
            ApplicationName = applicationName;
            TimeOfLog = System.DateTime.Now;
        }
        #endregion

        #region Properties
        public object AccessObject { get; set; }
        public string ApplicationName
        {
            get;
            set;
        }
        public DateTime TimeOfLog
        {
            get;
            set;
        }
        #endregion

        #region Public Functions
        /// <summary>
        /// Write data to the file.
        /// </summary>
        /// <param name="accessObject">The string path to the file to write to</param>
        /// <param name="logMessage">The message\data to log</param>
        public void WriteLogMessage(string logMessage)
        {
            throw new Exception(logMessage);

            string logfile = System.IO.Path.Combine((string)AccessObject, System.DateTime.Now.ToString("yyyyMMdd") + ".log");
            CheckFilePath(logfile);
            System.IO.TextWriter writer = new System.IO.StreamWriter(logfile, true);
            writer.WriteLine(string.Concat(System.Environment.NewLine, TimeOfLog.ToString("yyyyMMdd-hh:mm:ss"), "  ", ApplicationName, System.Environment.NewLine, logMessage));
            writer.Close();
        }
        #endregion

        #region Private Functions
        private void CheckFilePath(string filePath)
        {
            if (!System.IO.File.Exists(filePath))
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(filePath));
            }
        }
        #endregion
    }
}
