﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Logging.LogServiceProviders
{
    /// <summary>
    /// Objects that are capable of persisting log data must implement this interface to be usable by the LogMessage object
    /// </summary>
    public interface ILogServiceProvider
    {
        void WriteLogMessage(string logMessage);
        object AccessObject { get; set; }
        string ApplicationName { get; set; }
        System.DateTime TimeOfLog { get; set; }
    }
}
