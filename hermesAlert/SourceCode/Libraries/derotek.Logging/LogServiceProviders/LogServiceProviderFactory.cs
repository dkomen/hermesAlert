﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Logging.LogServiceProviders
{
    #region Public Enums
    public enum LogDestination : int
    {
        Database = 0,
        FlatFile = 1,
        Eventlog = 2
    }
    #endregion
    public static class LogServiceProviderFactory
    {
        #region Fields
        private static string _applicationName = "derotek.Logging";
        #endregion

        public static ILogServiceProvider GetProvider(LogDestination logDestination, object data)
        {
            switch (logDestination)
            {
                case LogDestination.Database:
                case LogDestination.Eventlog:
                    throw new NotImplementedException();
                case LogDestination.FlatFile:
                    return new FlatFileLogServiceProvider(_applicationName, (string)data);
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
