﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Logging
{
    /// <summary>
    /// Log a message
    /// </summary>
    public class LogMessage
    {

        #region Constructors
        public LogMessage(Logging.LogServiceProviders.LogDestination logDestination, object data)
        {
            
            HasBeenLogged = false;
            LogServiceProvider = Logging.LogServiceProviders.LogServiceProviderFactory.GetProvider(logDestination, data);
        }
        #endregion

        #region Properties
        public bool HasBeenLogged { get; private set; }
        public LogServiceProviders.ILogServiceProvider LogServiceProvider { get; set; }
        #endregion

        #region Public Functions
        private void MessageToLog(string message)
        {
            try
            {
                LogServiceProvider.WriteLogMessage(message);
                HasBeenLogged = true;
            }
            catch (Exception ex)
            {
                HasBeenLogged = false;
                throw ex;
            }
        }
        #endregion
    }
}
