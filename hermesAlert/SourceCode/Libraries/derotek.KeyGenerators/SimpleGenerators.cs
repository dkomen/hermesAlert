﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.KeyGenerators
{
    public class SimpleGenerators
    {
        #region Fields
        private static string _fieldCharacters = "123ABCDEFGHIJKLMNOPQRSTUVWXYZ12345678901234567890abcdabcdefghijklmnopqrstuvwxyzzxy1234567890SDFGtred";
        #endregion

        public static string GenerateKey(int keyLength)
        {
            System.Text.StringBuilder key = new StringBuilder();
            for(int i = 0; i<keyLength;i++)
            {
                Random rnd = new Random(System.DateTime.Now.Millisecond + System.DateTime.Now.Day);
                System.Threading.Thread.Sleep(2);
                int indexInField = (int)(rnd.NextDouble() * 100);
                key.Append(_fieldCharacters[indexInField]);
            }

            return key.ToString();
        }
    }
}
