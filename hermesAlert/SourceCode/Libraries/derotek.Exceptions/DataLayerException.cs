﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Exceptions
{
    [Serializable]
    public class DataLayerException : BaseException
    {
        #region Constructors
        public DataLayerException(string message)
            : base(message)
        {
        }
        public DataLayerException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
        #endregion
    }
}
