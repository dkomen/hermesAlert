﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Exceptions
{
    [Serializable]
    public class BusinessRuleException : BaseException
    {
        #region Constructors
        public BusinessRuleException(string message)
            : base(message)
        {
        }
        public BusinessRuleException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
        #endregion
    }
}
