﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Exceptions
{
    [Serializable]
    public class StandardException : BaseException
    {
        #region Constructors
        public StandardException(string message)
            : base(message)
        {
        }
        public StandardException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
        #endregion
    }
}
