﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.Exceptions
{
    [Serializable]
    public abstract class BaseException : System.Exception
    {
        #region Fields
        public static string FlatFileLogPath;
        private Logging.LogMessage _messageLogger = new Logging.LogMessage(Logging.LogServiceProviders.LogDestination.FlatFile, FlatFileLogPath);
        #endregion

        #region Constructors
        public BaseException(string message)
            : base(message)
        {
            //throw new Exception(message);
            //_messageLogger.LogServiceProvider.WriteLogMessage(message);
        }
        public BaseException(string message, System.Exception innerException)
            : base(message, innerException)
        {
            //Do we have an inner exception
            if (this.InnerException != null)
            {
                BaseException innerBaseException = this.InnerException as BaseException;
                if (innerBaseException != null)//Yes we have an inner exception.. but is it of type BaseException
                {
                    if (innerBaseException.HasBeenLogged == false)//If it hasnt been logged yet then log it
                    {
                        throw new Exception(message + " : " + innerException.Message, innerException);
                        //innerBaseException._messageLogger.LogServiceProvider.WriteLogMessage(innerBaseException.ToString());
                    }
                }
                //else
                //{
                //    //THis is not one of out own exceptions so create a new one of our own from it
                //    if (innerBaseException != null)
                //    {
                //        throw new StandardException(innerBaseException.ToString(), innerBaseException.InnerException);
                //    }
                //    else
                //    {
                //        throw new StandardException(innerBaseException.ToString());
                //    }
                //}
            }
            //Send to logger
            _messageLogger.LogServiceProvider.WriteLogMessage(message);
        }       
        #endregion

        #region Properties
        /// <summary>
        /// Has this exception already been logged
        /// </summary>
        public bool HasBeenLogged
        {
            get
            {
                return _messageLogger.HasBeenLogged;
            }
        }
        #endregion

        #region Public Functions

        #endregion

        #region Private Methods
        #endregion
    }
}
