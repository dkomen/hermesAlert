﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model
{
    public class MissingPersons
    {
        /// <summary>
        /// Get all the current missing persons in the datastore
        /// </summary>
        /// <param name="maximumRecordsCount"></param>
        /// <returns></returns>
        public static List<derotek.HairyDog.PersonAlert.PersonAlert>  GetMissingPersons(int maximumRecordsCount)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                IList<derotek.HairyDog.PersonAlert.PersonAlert> missingPersons = transaction.Retrieve<derotek.HairyDog.PersonAlert.PersonAlert>("Solved", false, null);
                missingPersons.Shuffle<derotek.HairyDog.PersonAlert.PersonAlert>();
                return missingPersons as List<derotek.HairyDog.PersonAlert.PersonAlert>;
            }            
        }
    }
}
