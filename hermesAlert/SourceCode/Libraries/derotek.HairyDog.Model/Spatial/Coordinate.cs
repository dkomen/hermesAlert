﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model.Spatial
{
    /// <summary>
    /// Represents a positioning coordinate
    /// </summary>
    public class Coordinate
    {
        #region Properties
        public long Id { get; set; }
        public System.Drawing.PointF Point { get; set; }
        #endregion
    }
}
