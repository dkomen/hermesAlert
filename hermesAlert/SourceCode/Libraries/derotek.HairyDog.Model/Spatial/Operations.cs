﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model.Spatial
{
    public class Operations
    {
        #region Public Functions
        public List<WeightedCoordinate> CalculateProximityWeights(List<Coordinate> coordinates, float range)
        {
            coordinates = new List<Coordinate>();
            coordinates.Add(new Coordinate() { Id = 1, Point = new System.Drawing.PointF(28, -25) });
            coordinates.Add(new Coordinate() { Id = 2, Point = new System.Drawing.PointF((float)28.1, (float)-25.15) });
            coordinates.Add(new Coordinate() { Id = 3, Point = new System.Drawing.PointF((float)28.1, (float)-25.1) });
            coordinates.Add(new Coordinate() { Id = 4, Point = new System.Drawing.PointF((float)28.19, (float)-25.2) });

            coordinates.Add(new Coordinate() { Id = 5, Point = new System.Drawing.PointF((float)28.7, (float)-25.7) });
            coordinates.Add(new Coordinate() { Id = 6, Point = new System.Drawing.PointF((float)28.6, (float)-25.6) });


            Dictionary<long, List<Coordinate>> regionsFound = new Dictionary<long, List<Coordinate>>();

            #region Create groups of in range coordinates
            foreach (Coordinate coordinate in coordinates)
            {
                List<Coordinate> currentRegionList = new List<Coordinate>();
                currentRegionList.Add(coordinate);
                regionsFound.Add(coordinate.Id, currentRegionList);
                foreach (Coordinate coordinateToCompareWith in coordinates)
                {
                    if (coordinate.Id != coordinateToCompareWith.Id)
                    {
                        if (IsInRange(coordinate.Point, coordinateToCompareWith.Point, range))
                        {
                            currentRegionList.Add(coordinateToCompareWith);
                        }
                    }
                }
            }
            #endregion

            #region Remove duplicate coordinates but keeping the one in the bigger group
            Dictionary<long, List<Coordinate>> cleanRegionsOfDuplicates = new Dictionary<long, List<Coordinate>>();
            bool mayKeepItem = false;
            foreach (System.Collections.Generic.KeyValuePair<long, List<Coordinate>> outerItem in regionsFound)
            {
                List<Coordinate> cleanedList = new List<Coordinate>();
                cleanRegionsOfDuplicates.Add(outerItem.Key, cleanedList);
                foreach (Coordinate outerCoordinate in outerItem.Value)
                {
                    foreach (System.Collections.Generic.KeyValuePair<long, List<Coordinate>> otherOuterItem in regionsFound)
                    {
                        if (otherOuterItem.Key != outerItem.Key)
                        {
                            foreach (Coordinate otherCoordinate in otherOuterItem.Value)
                            {
                                if (outerCoordinate.Id == otherCoordinate.Id)
                                {
                                    mayKeepItem = (otherOuterItem.Value.Count >= outerItem.Value.Count); //outer has more items than otherOuter
                                    break;
                                }
                            }
                        }
                        if (mayKeepItem)
                        {
                            mayKeepItem = false;
                            cleanedList.Add(outerCoordinate);
                            break;
                        }
                    }
                }
            }
            #endregion
            bool matchFound = false;
            int outerIndexInList = 0;
            int innerIndexInList = 0;
            Dictionary<long, List<Coordinate>> distinctAreas = new Dictionary<long, List<Coordinate>>();
            foreach (System.Collections.Generic.KeyValuePair<long, List<Coordinate>> item in cleanRegionsOfDuplicates)
            {
                matchFound = false;
                foreach (System.Collections.Generic.KeyValuePair<long, List<Coordinate>> innerItem in cleanRegionsOfDuplicates)
                {
                    //if (item.Value.Count <= innerItem.Value.Count)
                    if (item.Key != innerItem.Key)
                    {
                        foreach (Coordinate outerCoordinate in item.Value)
                        {
                            foreach (Coordinate innerCoordinate in innerItem.Value)
                            {
                                if (innerCoordinate.Id==outerCoordinate.Id)
                                {
                                    matchFound = true;
                                    break;
                                }
                            }
                            if (!matchFound)
                            {
                                break;
                            }
                        }
                    }
                    if (matchFound && item.Value.Count > innerItem.Value.Count || (matchFound && item.Value.Count == innerItem.Value.Count && (outerIndexInList < innerIndexInList)))
                    {
                        distinctAreas.Add(item.Key, item.Value);
                        break;
                    }
                    innerIndexInList++;
                }
                outerIndexInList++;
            }

            #region Get the weighted items
            List<WeightedCoordinate> calculatedWeights = new List<WeightedCoordinate>();
            foreach (System.Collections.Generic.KeyValuePair<long, List<Coordinate>> coordinate in distinctAreas)
            {
                System.Drawing.PointF averagedCoordinate = GetAveragedCoordinate(coordinate.Value);
                calculatedWeights.Add(new WeightedCoordinate() { Coordinate = averagedCoordinate, EnclosingRadius = GetEnclosingRadius(averagedCoordinate, coordinate.Value), Weight = coordinate.Value.Count });
            }
            #endregion



            #region Remove duplicate entries
            List<WeightedCoordinate> calculatedWeightsFiltered = new List<WeightedCoordinate>();
            foreach (WeightedCoordinate calculatedWeight in calculatedWeights)
            {
                bool found = false;

                foreach (WeightedCoordinate item in calculatedWeightsFiltered)
                {
                    if (item.Coordinate.X == calculatedWeight.Coordinate.X && item.Coordinate.Y == calculatedWeight.Coordinate.Y)
                    {
                        found = true;
                        break;
                    }
                }

                if (found == false)
                {
                    calculatedWeightsFiltered.Add(calculatedWeight);
                }
            }
            #endregion

            return calculatedWeightsFiltered;
        }

        /// <summary>
        /// Calculate the distance between two coordinates
        /// </summary>
        /// <param name="coordinateA"></param>
        /// <param name="coordinateB"></param>
        /// <returns></returns>
        public float GetDistance(System.Drawing.PointF coordinateA, System.Drawing.PointF coordinateB)
        {
            float x = coordinateA.X - coordinateB.X;
            float y = coordinateA.Y - coordinateB.Y;
            return (float)Math.Sqrt((x * x) + (y * y));
        }
        public bool IsInRange(System.Drawing.PointF coordinateA, System.Drawing.PointF coordinateB, float range)
        {
            if (System.Math.Abs(coordinateA.X - coordinateB.X) <= System.Math.Abs(range))
            {
                if (System.Math.Abs(coordinateA.Y - coordinateB.Y) <= System.Math.Abs(range))
                {
                    return true;
                }
            }
            return false;
        }
        /// <summary>
        /// Get the average coordinate from a list of coordinates
        /// </summary>
        /// <param name="coordinatesToAverage"></param>
        /// <returns></returns>
        public System.Drawing.PointF GetAveragedCoordinate(List<Coordinate> coordinatesToAverage)
        {
            float sumX = 0;
            float sumY = 0;
            foreach (Coordinate coordinate in coordinatesToAverage)
            {
                sumX += coordinate.Point.X;
                sumY += coordinate.Point.Y;
            }

            float newX = sumX / coordinatesToAverage.Count;
            float newY = sumY / coordinatesToAverage.Count;

            return new System.Drawing.PointF(newX, newY);

        }
        public float GetEnclosingRadius(System.Drawing.PointF averagedCoordinate, List<Coordinate> coordinatesToEnclose)
        {
            float enclosingRadius = 0;
            foreach (Coordinate coordinate in coordinatesToEnclose)
            {
                float currentDistance = GetDistance(coordinate.Point, averagedCoordinate);
                if (enclosingRadius < currentDistance)
                {
                    enclosingRadius = currentDistance;
                }
            }

            return enclosingRadius;
        }
        #endregion
    }
}
