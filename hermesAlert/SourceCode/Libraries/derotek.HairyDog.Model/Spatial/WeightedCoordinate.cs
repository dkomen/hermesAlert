﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model.Spatial
{
    public class WeightedCoordinate
    {
        #region Properties
        public System.Drawing.PointF Coordinate { get; set; }
        public float EnclosingRadius { get; set; }
        public int Weight { get; set; }
        #endregion
    }
}
