﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model
{
    public class Users
    {
        public int UpdateUserRatingForIncident(long userId, bool increment)
        {

            int incrementBy = 0;
            if (increment)
            {
                incrementBy = 5;
            }
            else
            {
                incrementBy = -2;
            }

            return UpdateUserRating(userId, incrementBy);

        }
        public int UpdateUserRatingForMissingPerson(long userId, bool increment)
        {

            int incrementBy = 0;
            if (increment)
            {
                incrementBy = 20;
            }
            else
            {
                incrementBy = -10;
            }

            return UpdateUserRating(userId, incrementBy);

        }        
        public int UpdateUserRatingForRegisteredItem(long userId, bool increment)
        {
            int incrementBy = 0;
            if (increment)
            {
                incrementBy = 10;
            }
            else
            {
                incrementBy = -5;
            }

            return UpdateUserRating(userId, incrementBy);
        }

        public int UpdateUserRatingForSigningIntoTheSystem(long userId)
        {            
            return UpdateUserRating(userId, 1);
        }

        private int UpdateUserRating(long userId, int incrementBy)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                derotek.HairyDog.Users.User user = transaction.Retrieve<derotek.HairyDog.Users.User>(userId);

                user.UserRating = user.UserRating + (incrementBy);

                transaction.AddOrUpdate(user);
                transaction.Commit();

                return user.UserRating;
            }            
        }
    }
}
