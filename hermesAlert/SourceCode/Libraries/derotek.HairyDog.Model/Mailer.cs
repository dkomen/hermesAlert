﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model
{
    public class Mailer
    {
        //External dependancy!
        public static string GetSmtpUserPassword()
        {            
            return System.Configuration.ConfigurationManager.AppSettings["Aut"];
        }

        private Dimension15.MailerCache.Entities.ScheduledMailMessage GetNewMessage()
        {
            return GetNewMessage("notifications@hermesAlert.com", "notifications@hermesAlert.com", GetSmtpUserPassword());
        }

        private Dimension15.MailerCache.Entities.ScheduledMailMessage GetNewMessage(string messageFrom, string SmtpLogonName, string SmtpLogonPass)
        {
            Dimension15.MailerCache.Entities.ScheduledMailMessage message = new Dimension15.MailerCache.Entities.ScheduledMailMessage();
            message.ScheduleTime = System.DateTime.Now;

            message.SmtpServer = "smtp.hermesalert.com";
            message.SmtpServerPort = 25;
            message.MessageFrom = messageFrom;            
            message.SmtpServerAuthorisationName = SmtpLogonName;
            message.SmtpServerAuthorisationPassword = SmtpLogonPass;

            //Always send a message to myself too
            message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient("notifications@hermesAlert.com"));

            return message;
        }

        /// <summary>
        /// Send an email to each user monitoring an area into which the supplied Gps point falls
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="geographyPoint"></param>
        public void SendMailToAllUsers(string subject, string body)
        {
            IList<derotek.HairyDog.Users.User> usersToMail = new List<HairyDog.Users.User>();
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                usersToMail = transaction.Retrieve<derotek.HairyDog.Users.User>();
            }
            if (usersToMail.Count > 0)
            {

                Dimension15.MailerCache.Entities.ScheduledMailMessage message = GetNewMessage();               
                
                body = body.Replace("\r\n", "<br />");
                message.Body = "<html><body>" + body + eMailFooter(true) + "</body></html>";
                message.Subject = subject;
                
                foreach (derotek.HairyDog.Users.User user in usersToMail)
                {
                    if (user.EMailAddress != string.Empty
                        //only send to users that actually want to receive email
                        && user.NotificationsDeliveryMethod.ExternalReferanceId == (long)derotek.HairyDog.Enumerations.DeliveryMethod.eMail
                        && user.ReceiveNotificationsFrequency.ExternalReferanceId != (long)derotek.HairyDog.Enumerations.ReceiveNotificationsFrequency.Never)
                    {
                        //if (user.EMailAddress.ToLower().Contains("dkomen@gmail.com") || user.EMailAddress.ToLower().Contains("dean"))
                        //{
                            message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient(user.EMailAddress));
                        //}
                    }
                }
                Dimension15.MailerCache.Service.Client.WcfClient.AddMessage(message);
            }
        }

        /// <summary>
        /// Send an email to each user monitoring an area into which the supplied Gps point falls
        /// </summary>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="geographyPoint"></param>
        public void SendMailToUsersMonitoringGpsPoint(string subject, string body, Microsoft.SqlServer.Types.SqlGeography gpsPoint)
        {

            string userThatWereMailed = string.Empty;

            List<derotek.HairyDog.Users.User> usersToMail = new Monitoring().GetListOfUsersMonitoringGpsPoint(gpsPoint);
            if (usersToMail.Count > 0)
            {
                string to = ConvertToStringOfUsers(usersToMail);

                Dimension15.MailerCache.Entities.ScheduledMailMessage message = GetNewMessage();
                
                body = body.Replace("\r\n", "<br />");
                message.Body = "<html><body>" + body + eMailFooter(true) + "</body></html>";
                message.Subject = subject;   

                foreach (derotek.HairyDog.Users.User user in usersToMail)
                {
                    //if (user.EMailAddress.ToLower() == "dkomen@gmail.com")
                    //{
                        if (user.EMailAddress != string.Empty
                            //only send to users that actually want to receive email
                            && user.NotificationsDeliveryMethod.ExternalReferanceId == (long)derotek.HairyDog.Enumerations.DeliveryMethod.eMail
                            && user.ReceiveNotificationsFrequency.ExternalReferanceId != (long)derotek.HairyDog.Enumerations.ReceiveNotificationsFrequency.Never)
                        {
                            message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient(user.EMailAddress));
                            userThatWereMailed += user.Surname + ", " + user.Name + "(" + user.EMailAddress + "), ";
                        }
                   // }
                }

                try
                {
                    Dimension15.MailerCache.Service.Client.WcfClient.AddMessage(message);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                SendMessageToEmailAddress("notifications@hermesAlert.com", "dkomen@gmail.com", "Users were mailed incidents", userThatWereMailed, true);
            }
        }

        /// <summary>
        /// Send a email to the client asking then to verify their email so that account creation may be completed
        /// </summary>
        /// <param name="eMailAddress">The email address to send to</param>
        /// <param name="UserVerificationHashRequirement">The unique account verification code</param>
        public void SendAccountActivationEmailToUser(string eMailAddress, string UserVerificationHashRequirement)
        {
            Dimension15.MailerCache.Entities.ScheduledMailMessage message = GetNewMessage();  
            message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient(eMailAddress));

            message.Subject = "hermesAlert account activation";
            message.Body = "<html><body>" + "Someone recently used this email address (" + eMailAddress + ") on the www.hermesAlert.com website to create a new account.<br />" +
                "If it was you then please follow the link below to activate your new hermesAlert account.<br />" +
                "http://www.hermesAlert.com/Pages_Main/activate.aspx?id=" + UserVerificationHashRequirement + "<br /><br />" +
                "Once you are logged in you can create the areas you wish to monitor as well as report any incidents you may come across<br /><br />" +
                eMailFooter(false) + "</body></html>";

            Dimension15.MailerCache.Service.Client.WcfClient.AddMessage(message);
            SendMessageToEmailAddress("notifications@hermesAlert.com", "dkomen@gmail.com", "A user was sent their activation email", eMailAddress, true);
        }

        /// <summary>
        /// Send an email to single recipient
        /// </summary>
        /// <param name="fromAddress"></param>
        /// <param name="toAddress"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="isFromLocalUser">Is this email is from a hermesAlert email address or from an external person</param>
        public void SendMessageToEmailAddress(string fromAddress, string toAddress, string subject, string body, bool isFromLocalUser)
        {
            System.Threading.Thread.Sleep(1500);

            Dimension15.MailerCache.Entities.ScheduledMailMessage message = GetNewMessage(fromAddress, "notifications@hermesAlert.com", GetSmtpUserPassword());                        
           
            message.Subject = subject;
            body = body.Replace("\r\n", "<br />");
            message.Body = "<html><body>" + body + eMailFooter(true) + "</body></html>";

            message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient(toAddress));

            Dimension15.MailerCache.Service.Client.WcfClient.AddMessage(message);
            
        }

        public void SendPasswordResetEmail(string toAddress, string resetId)
        {
            //System.Net.Mail.MailMessage messages = new System.Net.Mail.MailMessage("notifications@hermesAlert.com", View.EMailAddress);
            string subject = "Password reset request from hermesAlert";
            string body = "A request was recently made on the www.hermesAlert.com website to reset your hermesAlert logon password." + System.Environment.NewLine +
                "If this is correct then please follow this link to reset your password : http://www.hermesAlert.com/Pages_User/ResetPassword.aspx?id=" + resetId + System.Environment.NewLine + System.Environment.NewLine +
                "If you did not request a password reset then you may ignore this email." +
                System.Environment.NewLine + System.Environment.NewLine + "From" + System.Environment.NewLine + "The hermesAlert team" + System.Environment.NewLine + "http://www.hermesAlert.com";
                       

            Dimension15.MailerCache.Entities.ScheduledMailMessage message = GetNewMessage("notifications@hermesAlert.com", "notifications@hermesAlert.com", GetSmtpUserPassword());

            message.Subject = subject;
            body = body.Replace("\r\n", "<br />");
            message.Body = "<html><body>" + body + eMailFooter(true) + "</body></html>";

            message.Recipients.Add(new Dimension15.MailerCache.Entities.MailRecipient(toAddress));

            Dimension15.MailerCache.Service.Client.WcfClient.AddMessage(message);

        }

        #region Private Functions

        /// <summary>
        /// Convert a list of users object into a ';' seperated list of email addresses
        /// </summary>
        /// <param name="userList"></param>
        /// <returns></returns>
        public string ConvertToStringOfUsers(List<derotek.HairyDog.Users.User> userList)
        {
            System.Text.StringBuilder builder = new StringBuilder();

            foreach (derotek.HairyDog.Users.User user in userList)
            {
                if (user.EMailAddress != string.Empty)
                {
                    builder.Append(user.EMailAddress);
                    builder.Append(";");
                }
            }

            return builder.ToString();
            
        }

        private string eMailFooter(bool mayUnsubscribe)
        {
            string footer = "<br /><br />Please report any issues to support@hermesAlert.com<br /><br /><font color='#222255'>Regards,<br />The hermesAlert Team</font>";
            if(mayUnsubscribe)
            {
                footer = "<br /><br />You can specify to stop receiving these notifications by logging in to www.hermesAlert.com and changing your profile settings." + footer;
            }

            return footer ;
        }
        #endregion

    }
}
