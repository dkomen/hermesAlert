﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Model
{
    /// <summary>
    /// Any functions relating to monitoring areas
    /// </summary>
    public class Monitoring
    {
        /// <summary>
        /// Return a list of users monitoring a gps point
        /// </summary>
        /// <param name="gpsPoint"></param>
        /// <returns></returns>
        public List<derotek.HairyDog.Users.User> GetListOfUsersMonitoringGpsPoint(Microsoft.SqlServer.Types.SqlGeography gpsPoint)
        {
            return derotek.Fnh.DataAccess.SPROC_Functions.GetListOfUsersMonitoringGpsPoint(gpsPoint);
        }
    }
}
