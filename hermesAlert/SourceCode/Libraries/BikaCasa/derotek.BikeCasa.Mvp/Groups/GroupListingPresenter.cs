﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Groups
{
    /// <summary>
    /// Show all groups for user and allw to select for editing or to add new
    /// </summary>
    public class GroupListingPresenter
    {
        #region Constructors
        public GroupListingPresenter(IGroupListing view)
        {
            View = view;
            PrepareView();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Show all available groups for current user
        /// </summary>
        private IGroupListing View { get; set; }
        #endregion

        #region Public Functions
        public void PrepareView()
        {
            //derotek.Fnh.DataAccess.AccessManager manager = new Fnh.DataAccess.AccessManager();
            //derotek.DataAccess.Filter filterCriteria = new DataAccess.Filter("UserId", View.CurrentUser.Id);    
       
            //DEANK TODO
            //View.CurrentGroupMemberships = (manager.GetMany<derotek.HairyDog.Groups.Group>(filterCriteria, 100)).GetDisplayableList();

            //View.AvailableGroups = derotek.HairyDog.Model.Groups.Groups.GetAllAvailableGroups(View.CurrentUser.Id);

        }
        #endregion
    }
}
