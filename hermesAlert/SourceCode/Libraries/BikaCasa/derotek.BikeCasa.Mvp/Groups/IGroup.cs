﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Groups
{
    /// <summary>
    /// Add or maintain a single group
    /// </summary>
    public interface IGroup : IView
    {
        string Name { get; set; }
        string Description { get; set; }
    }
}
