﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Groups
{
    /// <summary>
    /// Select a group from a list of available groups
    /// </summary>
    public interface IGroupListingMaintenance
    {
        /// <summary>
        /// A list of all available groups
        /// </summary>
        IGroupSelector GroupsAvailableToJoin { get; set; }
        long SelectedToJoinGroupId { get; set; }
    }
}
