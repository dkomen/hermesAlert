﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Groups
{
    /// <summary>
    /// Select a group from a list of available groups
    /// </summary>
    public interface IGroupListing : IView
    {
        /// <summary>
        /// A list of all user is joining groups
        /// </summary>
        List<derotek.HairyDog.DisplayableItems.DisplayableJoinedGroups> CurrentGroupMemberships { get; set; }
        /// <summary>
        /// The id of the curren tgroup select from the current-memberships listing
        /// </summary>
        long SelectedMembershipGroupId { get; set; }

        /// <summary>
        /// A list of all user is joining groups
        /// </summary>
        List<derotek.HairyDog.DisplayableItems.DisplayableGroup> AvailableGroups { get; set; }
        /// <summary>
        /// The id of the curren tgroup select from the current-memberships listing
        /// </summary>
        long SelectedAvailableGroupId { get; set; }
    }
}
