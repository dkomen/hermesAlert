﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Groups
{
    /// <summary>
    /// Select a group from a list of available groups
    /// </summary>
    public interface IGroupSelector: IView
    {
        /// <summary>
        /// A list of all available groups
        /// </summary>
        List<derotek.HairyDog.DisplayableItems.DisplayableGroup> Groups { get; set; }

        long SelectedGroupId { get; set; }
    }
}
