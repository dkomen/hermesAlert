﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Attachments
{
    public interface IAttachmentsView : IView
    {
        long PhotoAlbumId { get; set; }
        string NewImagePath { get; set; }
        IList<derotek.HairyDog.DisplayableItems.DisplayableAttachmentObject> PhotoAlbumEntries { get; set; }
        string AlbumName { get; set; }
        string AlbumDescription { get; set; }
        string NewAlbumEntryDescription { get; set; }
    }
}
