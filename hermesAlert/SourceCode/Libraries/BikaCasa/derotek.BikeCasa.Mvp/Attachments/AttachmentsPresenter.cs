﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Attachments
{
    public class AttachmentsPresenter
    {
        #region Fields
        private derotek.HairyDog.Interfaces.IAttachmentsHost _attachmentsHost;
        #endregion

        #region Constructors
        public AttachmentsPresenter(IAttachmentsView view, derotek.HairyDog.Interfaces.IAttachmentsHost attachmentsHost)
        {
            View = view;
            _attachmentsHost = attachmentsHost;
            
        }
        #endregion

        #region Properties
        public IAttachmentsView View { get; set; }
        #endregion

        #region Public Methods
        public void AddNewAttachmentRedirection(string queryString)
        {
            View.RedirectView("~/Pages_Main/MaintainAttachment.aspx" + queryString);
        }
        public void MaintainAttachmentRedirection(string queryString)
        {
            View.RedirectView("~/Pages_Main/MaintainAttachment.aspx" + queryString);
        }
        public void PrepareViewForUse()
        {
            if (_attachmentsHost != null)
            {                
                View.PhotoAlbumEntries = _attachmentsHost.Attachments.GetDisplayableList();
                View.AlbumName = "AlbumName";
                View.AlbumDescription = "AlbumDescription";
                View.PhotoAlbumId = 0;
            }
            else
            {
                //throw new derotek.Exceptions.StandardException("The photo album could not be found");
            }
        }
        public void AddNewImage()
        {
            derotek.HairyDog.Attachment newEntry = new HairyDog.Attachment();
            newEntry.Description = View.NewAlbumEntryDescription;
            newEntry.Path = View.NewImagePath;
            newEntry.UserId = ((DORM.Interfaces.IEntity)_attachmentsHost).Id;

            //derotek.HairyDog.p AddNewEntry(newEntry);
        }
        public derotek.HairyDog.Attachment GetImage(long attachmentId)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                return (transaction.Retrieve<derotek.HairyDog.Attachment>(attachmentId));
            }
        }
        public void DeleteImage()
        {
        }
        #endregion
    }
}
