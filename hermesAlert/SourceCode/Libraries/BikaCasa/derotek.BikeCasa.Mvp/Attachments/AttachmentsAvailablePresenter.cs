﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Attachments
{
    public class AttachmentsAvailablePresenter
    {
        #region Fields
        private long _hostUserId;
        #endregion

        #region Constructors
        public AttachmentsAvailablePresenter(IAvailableAttachmentsView view, long hostUserId)
        {
            View = view;
            _hostUserId = hostUserId;
            
        }
        #endregion

        #region Properties
        public IAvailableAttachmentsView View { get; set; }
        #endregion

        #region Public Methods        
        public void PrepareViewForUse()
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                if (_hostUserId != 0)
                {
                    derotek.HairyDog.Users.User user = transaction.Retrieve<derotek.HairyDog.Users.User>(_hostUserId);
                    View.PhotoAlbumEntries = user.UploadedFiles.GetDisplayableList();
                    View.AlbumName = "AlbumName";
                    View.AlbumDescription = "AlbumDescription";
                    View.PhotoAlbumId = 0;
                }
                else
                {
                    //throw new derotek.Exceptions.StandardException("The photo album could not be found");
                }
            }
        }        
        #endregion
    }
}
