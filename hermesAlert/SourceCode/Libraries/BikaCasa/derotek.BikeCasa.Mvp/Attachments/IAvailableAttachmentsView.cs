﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Attachments
{
    public interface IAvailableAttachmentsView: IView
    {
        long PhotoAlbumId { get; set; }
        IList<derotek.HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject> PhotoAlbumEntries { get; set; }
        string AlbumName { get; set; }
        string AlbumDescription { get; set; }
    }
}
