﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp
{
    public interface IView
    {
        /// <summary>
        /// The Id of the current views User or RegisteredItem etc etc
        /// </summary>
        derotek.HairyDog.Users.User CurrentUser { get; set; }
        void DisplayMessage(string message, bool isErrorMessage);
        void RedirectView(string path);
        string ReturnToPage { get; set; }
        long ActiveObjectId { get; set; }
    }
}
