﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp
{
    public interface IViewMasterPage : IView
    {
        bool ShowRedirectionOptions {get; set; }
        derotek.HairyDog.Users.User LoggedInUserDetails { get; set; }
        string PageFooter { get; set; }
    }
}
