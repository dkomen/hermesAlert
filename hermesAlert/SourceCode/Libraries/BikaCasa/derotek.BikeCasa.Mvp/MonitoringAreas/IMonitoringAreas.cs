﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.MonitoringAreas
{
    public interface IMonitoringAreas : IView
    {
        List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> StolenRegisteredObjects { get; set; }
        void SetPropertyTypeList(Dictionary<long, string> list);
        long PropertyType { get; set; }      
    }
}
