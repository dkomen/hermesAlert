﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.MonitoringAreas
{
    public class MonitoringAreasPresenter
    {
        #region Properties
        private IMonitoringAreas View { get; set; }
        #endregion

        #region Constructors
        public MonitoringAreasPresenter(IMonitoringAreas view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void PrepareView(long propertyTypeToListReference)
        {
            //Get property types
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                IList<derotek.HairyDog.Metrics.PropertyType> propertyTypes = transaction.Retrieve<derotek.HairyDog.Metrics.PropertyType>(null);
                View.SetPropertyTypeList(propertyTypes.ToDictionary(x => x.Id, yield => yield.Description));
            }

            derotek.HairyDog.Metrics.PropertyType propertyType;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("ExternalReferanceId", DORM.Enumerations.FilterCriteriaComparitor.Equals, propertyTypeToListReference));
                propertyType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.PropertyType>(filter);
            }
            View.PropertyType = propertyType.Id;
            GetStolenItems(propertyType.Id);
        }
        public void ShowItem(long itemId, string returnToPage)
        {
            View.ActiveObjectId = itemId;
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ActiveObjectId, itemId.ToString());
            
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_ItemRegistration/ItemRegistration.aspx" + qs.EncodeQueryString());
        }
        public void GetStolenItems(long propertyTypeToList)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                IList<derotek.HairyDog.RegisteredItem> stolenItemsInAreas = derotek.Fnh.DataAccess.SPROC_Functions.GetStolenItemsInAreas(View.CurrentUser.Id, propertyTypeToList, 30, 10, System.DateTime.Now.AddDays(-90));
                View.StolenRegisteredObjects = stolenItemsInAreas.GetDisplayableList();
            }
        }
        #endregion

    }
}
