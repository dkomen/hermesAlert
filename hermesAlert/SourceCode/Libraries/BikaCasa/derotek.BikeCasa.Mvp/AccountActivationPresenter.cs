﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp
{
    /// <summary>
    /// Activate accounts with the userverifaction requirements string
    /// </summary>
    public class AccountActivationPresenter
    {
        public bool ActivateUserAccount(string userVerificationHashRequirement)
        {            
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("UserVerificationHashRequirement",DORM.Enumerations.FilterCriteriaComparitor.Equals, userVerificationHashRequirement));
                derotek.HairyDog.Users.User userToActivate = transaction.RetrieveFirst<derotek.HairyDog.Users.User>(filter);

                if (userToActivate != null)
                {
                    userToActivate.UserVerificationHashRequirement = string.Empty;
                    
                    transaction.AddOrUpdate(userToActivate);
                    transaction.Commit();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
