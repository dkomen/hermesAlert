﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace derotek.BikeCasa.Mvp
{
    /// <summary>
    /// Any object that wishes to store their state in the state bag must implement this interface
    /// </summary>
    public interface IStatePersistableObject
    {
        /// <summary>
        /// The id of this object.
        /// </summary>
        string ObjectId { get; set; }

        /// <summary>
        /// For which items\pages to keep this state object alive. Any other items\pages should remove this state object
        /// </summary>
        List<string> KeepAliveForPages { get; set; }
    }
}