﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Views
{
    public interface IPasswordResetView : IView
    {
        string Password { get; set; }
        string PasswordVerify { get; set; }
        string PasswordResetId { get; set; }
    }
}
