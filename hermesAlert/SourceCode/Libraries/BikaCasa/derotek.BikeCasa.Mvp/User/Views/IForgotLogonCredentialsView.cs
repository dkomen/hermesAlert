﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Views
{
    public interface IForgotLogonCredentialsView : IView
    {
        string EMailAddress { get; set; }
    }
}
