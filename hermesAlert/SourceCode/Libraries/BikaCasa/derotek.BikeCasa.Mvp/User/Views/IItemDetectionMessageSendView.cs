﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Views
{
    public enum ReadOnlyMode
    {
        CreateNew =1,
        OnlyReply=2,
        ReadOnly=3
    }
    public interface IItemDetectionMessageSendView : IView
    {
        bool KeepPrivate { get; set; }
        string Notes { get; set; }
        ReadOnlyMode ReadOnlyMode { get; set; }
        long LocationAccuracyWhenPublic { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list);
    }
}
