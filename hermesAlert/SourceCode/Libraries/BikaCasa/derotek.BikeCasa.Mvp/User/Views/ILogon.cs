﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Views
{
    /// <summary>
    /// Allows a user to log in to the application
    /// </summary>
    public interface ILogon: IView
    {
        string Username { get; set; }
        string Password { get; set; }      
    }
}
