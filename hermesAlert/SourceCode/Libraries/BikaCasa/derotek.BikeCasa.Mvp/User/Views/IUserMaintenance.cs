﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Views
{
    public interface IUserMaintenance : derotek.HairyDog.Users.IUserCls, IView
    {
        long SelectedProvinceId { get; set; }
        long? SelectedTownId { get; set; }
        void SetProvincesList(Dictionary<long, string> items);
        void SetTownsList(Dictionary<long, string> items);
        bool ProceedToLoginOptionVisible { set; }
        bool EmailAddressEnabled { get;set; }

        void SetThemeList(Dictionary<long, string> items);
        string Theme { get; set; }

        long SelectedNotificationDeliveryMethodId { get; set; }
        void SetNotificationDeliveryMethod(Dictionary<long, string> items);
        long SelectedNotificationFrequencyMethodId { get; set; }
        void SetNotificationFrequencyMethod(Dictionary<long, string> items);

        double Longitude { get; set; }
        double Latitude { get; set; }
    }
}
