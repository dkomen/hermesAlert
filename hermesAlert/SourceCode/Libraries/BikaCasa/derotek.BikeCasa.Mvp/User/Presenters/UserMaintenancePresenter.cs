﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Presenters
{
    public class UserMaintenancePresenter
    {
        #region Properties
        public Views.IUserMaintenance View { get; private set; }
        #endregion

        #region Constructors
        public UserMaintenancePresenter(Views.IUserMaintenance maintenanceView)
        {
            View = maintenanceView;
            View.DisplayMessage(string.Empty,false);
        }
        #endregion

        #region Public Functions
        public void PrepareFieldDefaults(long userId, StateUserProfile storedPageState)
        {
            IList<derotek.HairyDog.Province> provinces;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                provinces = transaction.Retrieve<derotek.HairyDog.Province>(null);
            }
            View.SetProvincesList(provinces.ToDictionary(x=>x.Id, yield=>yield.Description));
            View.SelectedProvinceId = provinces[0].Id;
            SelectedProvinceChanged(provinces[0].Id);       
     
            //DeliveryMethod
            IList<derotek.HairyDog.Metrics.DeliveryMethod> deliverMethods;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                deliverMethods = transaction.Retrieve<derotek.HairyDog.Metrics.DeliveryMethod>(null);
            }
            View.SetNotificationDeliveryMethod(deliverMethods.ToDictionary(x => x.Id, yield => yield.Description));

            //Notification Frequency
            IList<derotek.HairyDog.Metrics.ReceiveNotificationsFrequency> deliveryFrequency;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                deliveryFrequency = transaction.Retrieve<derotek.HairyDog.Metrics.ReceiveNotificationsFrequency>(null);
            }
            View.SetNotificationFrequencyMethod(deliveryFrequency.ToDictionary(x => x.Id, yield => yield.Description));

            //Themes
            Dictionary<long, string> themes = new Dictionary<long, string>();
            themes.Add(1, "Entropy");
            themes.Add(2, "Boson");
            themes.Add(3, "BaseTheme");
            
            View.SetThemeList(themes);


            if (storedPageState != null)
            {
                View.ActiveObjectId = storedPageState.ActiveObjectId;
                View.ContactNumber = storedPageState.ContactNumber;
                View.EMailAddress = storedPageState.EMailAddress;
                View.EmailAddressEnabled = storedPageState.EmailAddressEnabled;
                View.Latitude = storedPageState.Latitude;
                View.Longitude = storedPageState.Longitude;
                View.Name = storedPageState.Name;
                View.Password = storedPageState.Password;
                View.ReturnToPage = storedPageState.ReturnToPage;
                View.SecurityQuestion = storedPageState.SecurityQuestion;
                View.SecurityQuestionAnswer = storedPageState.SecurityQuestionAnswer;
                View.SelectedNotificationDeliveryMethodId = storedPageState.SelectedNotificationDeliveryMethodId;
                View.SelectedNotificationFrequencyMethodId = storedPageState.SelectedNotificationFrequencyMethodId;
                View.SelectedProvinceId = storedPageState.SelectedProvinceId;
                View.SelectedTownId = storedPageState.SelectedTownId;
                View.Surname = storedPageState.Surname;
                View.Theme = storedPageState.Theme;
                //View.Username = storedPageState.Username;
                //View.UserRating = storedPageState.UserRating;
                View.UserVerificationHashRequirement = storedPageState.UserVerificationHashRequirement;
                //View.VerifyPassword = storedPageState.VerifyPassword;
                storedPageState = null;
            }
            else
            {
                if (View.CurrentUser.Id != 0)
                {
                    View.ProceedToLoginOptionVisible = false;
                    derotek.HairyDog.Users.User user;
                    using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                    {
                        user = transaction.Retrieve<derotek.HairyDog.Users.User>(View.CurrentUser.Id);
                    }
                    View.ContactNumber = user.ContactNumber;
                    View.EMailAddress = user.EMailAddress;
                    View.Name = user.Name;
                    View.Surname = user.Surname;
                    View.SelectedProvinceId = user.Province.Id;
                    View.SelectedNotificationFrequencyMethodId = user.ReceiveNotificationsFrequency.Id;
                    View.SelectedNotificationDeliveryMethodId = user.NotificationsDeliveryMethod.Id;
                    View.Theme = user.Theme;
                    View.Latitude = user.Latitude;
                    View.Longitude = user.Longitude;
                    if (user.Town != null)
                    {
                        View.SelectedTownId = user.Town.Id;
                    }
                    View.EmailAddressEnabled = false;
                    View.SecurityQuestion = user.SecurityQuestion;
                    View.SecurityQuestionAnswer = user.SecurityQuestionAnswer;
                }
                else
                {
                    View.EmailAddressEnabled = true;
                    View.ProceedToLoginOptionVisible = true;
                    View.SelectedNotificationDeliveryMethodId = deliverMethods[0].Id;
                    View.SelectedNotificationFrequencyMethodId = deliveryFrequency[0].Id;
                }
            }
        }
        public void SelectedProvinceChanged(long provinceId)
        {
            IList<derotek.HairyDog.Town> towns = new List<HairyDog.Town>();
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filterCriteria = new DORM.Filter();
                filterCriteria.Add(new DORM.FilterItem("Province", DORM.Enumerations.FilterCriteriaComparitor.Equals, new HairyDog.Province(provinceId)));
                towns = transaction.Retrieve<derotek.HairyDog.Town>(filterCriteria, 500);
            }
                       
            View.SetTownsList(towns.ToDictionary(x => x.Id, yield => yield.Description));
        }
        public void RedirectToLogonView()
        {
            View.RedirectView("~/default.aspx");
        }
        public void SaveUser()
        {
            bool verificationPassed = true;

            if (View.EMailAddress == string.Empty)
            {
                View.DisplayMessage("An email address is required", true);
                verificationPassed = false;
            }

            string eMailRegex = @"^(([^<>()[\]\\.,;:\s@\""]+"
                        + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                        + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                        + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                        + @"[a-zA-Z]{2,}))$";
            if(!System.Text.RegularExpressions.Regex.IsMatch(View.EMailAddress, eMailRegex))
            {
                View.DisplayMessage("The email address is not in a valid format", true);
                verificationPassed = false;
            }

            if (View.CurrentUser.Id == 0 && View.Password == string.Empty)
            {
                View.DisplayMessage("A password is required", true);
                verificationPassed = false;
            }

            if (View.Password != string.Empty && View.Password != View.VerifyPassword)
            {
                if (View.Password != View.VerifyPassword)
                {
                    View.DisplayMessage("Passwords do not match", true);
                    verificationPassed = false;
                }
            }
            
            if (View.EmailAddressEnabled && UserEMailExists(View.EMailAddress))
            {
                View.DisplayMessage("A user with this email address already exists", true);
                verificationPassed = false;
            }

            if (View.CurrentUser.Id != 0 && (View.Longitude==0 || View.Latitude == 0))
            {
                View.DisplayMessage("A valid latitude as well as longitude value must be supplied", true);
                verificationPassed = false;
            }
            
            if(verificationPassed)
            {                
                derotek.HairyDog.Users.User user;
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    if (View.CurrentUser.Id != 0)
                    {
                        user = transaction.Retrieve<HairyDog.Users.User>(View.CurrentUser.Id);
                    }
                    else
                    {
                        user = new HairyDog.Users.User();
                        user.UserRating = 50;
                    }
                    user.ContactNumber = View.ContactNumber;
                    user.EMailAddress = View.EMailAddress;
                    user.Name = View.Name;
                    string userVerificationHashRequirement = View.UserVerificationHashRequirement;
                    user.UserVerificationHashRequirement = userVerificationHashRequirement;
                    //Did password actually change?
                    if (View.Password != string.Empty)
                    {
                        user.Password = View.Password;
                    }

                    user.SecurityQuestion = View.SecurityQuestion;
                    user.SecurityQuestionAnswer = View.SecurityQuestionAnswer;
                    user.Theme = View.Theme;
                    user.Province = transaction.Retrieve<derotek.HairyDog.Province>(View.SelectedProvinceId);
                    if (View.SelectedTownId != null)
                    {
                        user.Town = transaction.Retrieve<derotek.HairyDog.Town>(View.SelectedTownId.Value);
                    }
                    else
                    {
                        user.Town = null;
                    }
                    user.Surname = View.Surname;
                    user.NotificationsDeliveryMethod = transaction.Retrieve<derotek.HairyDog.Metrics.DeliveryMethod>(View.SelectedNotificationDeliveryMethodId);
                    user.ReceiveNotificationsFrequency = transaction.Retrieve<derotek.HairyDog.Metrics.ReceiveNotificationsFrequency>(View.SelectedNotificationFrequencyMethodId);
                    user.Latitude = View.Latitude;
                    user.Longitude = View.Longitude;
                    user.LastLoginDate = System.DateTime.Now;
                    try
                    {
                        transaction.AddOrUpdate(user);
                        transaction.Commit();
                        if (View.UserVerificationHashRequirement != null && View.UserVerificationHashRequirement.Trim() != string.Empty)
                        {
                            new derotek.HairyDog.Model.Mailer().SendAccountActivationEmailToUser(View.EMailAddress, userVerificationHashRequirement);
                            View.DisplayMessage("An email has been sent to your email address (" + View.EMailAddress + "), please follow the instructions in it so that your account may be activated." + System.Environment.NewLine + "Please ensure that your email systems' spam filter does not block the email from being delivered.", false);
                        }
                        else
                        {
                            View.CurrentUser.Theme = View.Theme;
                            View.DisplayMessage("Saved successfully", false);
                        }
                        View.ProceedToLoginOptionVisible = View.CurrentUser.Id == 0;
                    }
                    catch (derotek.EntityValidation.ValidationErrorException valEx)
                    {
                        View.ProceedToLoginOptionVisible = false;
                        View.DisplayMessage(valEx.Message, true);
                    }
                    catch (Exception ex)
                    {
                        //View.ProceedToLoginOptionVisible = false;
                        View.DisplayMessage("An unknown error occurred while saving", true);
                    }
                }
            }
        }
        public void RedirectToSelectGpsLocation(string returnToPage)
        {
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_Main/SelectGpsLocation.aspx" + qs.EncodeQueryString());
        }
        #endregion

        #region Private Functions
        /// <summary>
        /// Does a user with this username already exist
        /// </summary>
        /// <param name="eMailAddress"></param>
        /// <returns></returns>
        private bool UserEMailExists(string userName)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("Username", DORM.Enumerations.FilterCriteriaComparitor.Equals, userName));
                return (transaction.Retrieve<derotek.HairyDog.Users.User>(filter, 1).Count == 1);
            }
        }
        #endregion
    }
}
