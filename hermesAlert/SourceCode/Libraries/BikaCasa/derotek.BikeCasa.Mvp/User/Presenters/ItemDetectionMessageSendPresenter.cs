﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Presenters
{
    public class ItemDetectionMessageSendPresenter
    {
        #region Properties
        public Views.IItemDetectionMessageSendView View { get; private set; }
        public long RegistereditemId { get; private set; }
        #endregion

        #region Constructors
        public ItemDetectionMessageSendPresenter(Views.IItemDetectionMessageSendView view, long registereditemId)
        {
            View = view;
            RegistereditemId = registereditemId;
        }
        #endregion

        #region Public Functions
        public void PrepareView(long idOfMessageToOpen)
        {
            //Get Location accuracy types
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                IList<derotek.HairyDog.Metrics.LocationAccuracy> locationAccuraciesWhenPublic = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(null);
                View.SetLocationAccuracyWhenPublicList(locationAccuraciesWhenPublic.ToDictionary(x => x.Id, yield => yield.Description));
            }

            if (idOfMessageToOpen != 0)
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    HairyDog.ItemMonitoring.ItemDetection message = transaction.Retrieve<HairyDog.ItemMonitoring.ItemDetection>(idOfMessageToOpen);
                    View.KeepPrivate = message.KeepPrivate;
                    View.Latitude = message.Latitude;
                    View.Longitude = message.Longitude;
                    View.Notes = message.Notes;
                    View.LocationAccuracyWhenPublic = message.LocationAccuracy.Id;

                    derotek.HairyDog.RegisteredItem item = transaction.Retrieve<derotek.HairyDog.RegisteredItem>(message.OwnerItemId);

                    if ((View.CurrentUser.Id != item.UserId) && (View.CurrentUser.Id != message.SecondPartyId))
                    {
                        View.ReadOnlyMode = Views.ReadOnlyMode.ReadOnly;
                    }
                    else
                    {
                        View.ReadOnlyMode = Views.ReadOnlyMode.OnlyReply;
                        if ((message.Direction == HairyDog.ItemMonitoring.MessagDirection.ToOwner && item.UserId == View.CurrentUser.Id)
                            || (message.Direction == HairyDog.ItemMonitoring.MessagDirection.ToSecondParty && View.CurrentUser.Id == message.SecondPartyId))
                        {
                            message.HasBeenRead = true;
                            transaction.AddOrUpdate(message);
                            transaction.Commit();
                        }
                    }
                }
            }
            else
            {
                View.ReadOnlyMode = Views.ReadOnlyMode.CreateNew;
            }

        }
        public void SendMessage(string returnToPage)
        {
            try
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    HairyDog.ItemMonitoring.ItemDetection itemDetection = new HairyDog.ItemMonitoring.ItemDetection();

                    itemDetection.KeepPrivate = View.KeepPrivate;
                    itemDetection.Latitude = View.Latitude;
                    itemDetection.Longitude = View.Longitude;
                    itemDetection.LocationAccuracy = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(View.LocationAccuracyWhenPublic);
                    itemDetection.Notes = View.Notes;
                    itemDetection.OwnerItemId = RegistereditemId;
                    itemDetection.Direction = HairyDog.ItemMonitoring.MessagDirection.ToOwner;
                    itemDetection.SecondPartyId = View.CurrentUser.Id;
                    itemDetection.HasBeenRead = false;
                    itemDetection.NotificationWasSent = false;
                    transaction.AddOrUpdate(itemDetection);
                    transaction.Commit();
                }
                View.RedirectView(returnToPage);
            }
            catch (derotek.EntityValidation.ValidationErrorException valEx)
            {
                View.DisplayMessage(valEx.Message, true);
            }
            catch (Exception ex)
            {
                View.DisplayMessage("An unknown error orcurred while saving", true);
            }
        }
        #endregion
    }
}
