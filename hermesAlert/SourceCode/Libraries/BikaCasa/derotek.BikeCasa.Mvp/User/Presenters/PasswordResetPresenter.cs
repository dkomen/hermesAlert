﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace derotek.BikeCasa.Mvp.User.Presenters
{
    public class PasswordResetPresenter
    {
        #region Properties
        public Views.IPasswordResetView View { get; private set; }
        #endregion

        #region Constructors
        public PasswordResetPresenter(Views.IPasswordResetView view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void Save()
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("PasswordResetId", DORM.Enumerations.FilterCriteriaComparitor.Equals, View.PasswordResetId));
                derotek.HairyDog.Users.User user = transaction.RetrieveFirst<derotek.HairyDog.Users.User>(filter);

                if (user != null)
                {

                    if (View.Password == string.Empty)
                    {
                        View.DisplayMessage("A password is required", true);
                        return;
                    }
                    else if (View.Password != string.Empty && View.Password != View.PasswordVerify)
                    {
                        if (View.Password != View.PasswordVerify)
                        {
                            View.DisplayMessage("Passwords do not match", true);
                            return;
                        }
                    }
                    user.Password = View.Password;
                    user.PasswordResetId = string.Empty;
                    transaction.AddOrUpdate(user);
                    transaction.Commit();
                    View.DisplayMessage("Your new logon password was saved", true);
                }
                else
                {
                    View.DisplayMessage("There was no request to change this accounts' password!", true);
                }
            }
        }
        #endregion
    }
}
