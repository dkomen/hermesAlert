﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace derotek.BikeCasa.Mvp.User.Presenters
{
    /// <summary>
    /// An object that can be used to persists the state of an IUserMaintenance page
    /// </summary>
    /// <remarks>Example: store to statebag and refresh the page later with this same data</remarks>
    [Serializable]
    public class StateUserProfile : IStatePersistableObject, derotek.BikeCasa.Mvp.User.Views.IUserMaintenance
    {
        #region Constructors
        public StateUserProfile()
        {
            KeepAliveForPages = new List<string>();
        }
        #endregion

        #region IStatePersistableObject
        /// <summary>
        /// The id of this object.
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// For which items\pages to keep this state object alive. Any other items\pages should remove this state object
        /// </summary>
        public List<string> KeepAliveForPages { get; set; }
        #endregion
     
    


        public HairyDog.Users.User CurrentUser
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public void DisplayMessage(string message, bool isErrorMessage)
        {
            throw new NotImplementedException();
        }

        public void RedirectView(string path)
        {
            throw new NotImplementedException();
        }

        public string ReturnToPage { get; set; }

        public long ActiveObjectId { get; set; }

        public long SelectedProvinceId { get; set; }

        public long? SelectedTownId { get; set; }

        public void SetProvincesList(Dictionary<long, string> items)
        {
            throw new NotImplementedException();
        }

        public void SetTownsList(Dictionary<long, string> items)
        {
            throw new NotImplementedException();
        }

        public bool ProceedToLoginOptionVisible
        {
            set { throw new NotImplementedException(); }
        }

        public bool EmailAddressEnabled { get; set; }

        public void SetThemeList(Dictionary<long, string> items)
        {
            throw new NotImplementedException();
        }

        public string Theme { get; set; }

        public long SelectedNotificationDeliveryMethodId { get; set; }

        public void SetNotificationDeliveryMethod(Dictionary<long, string> items)
        {
            throw new NotImplementedException();
        }

        public long SelectedNotificationFrequencyMethodId { get; set; }

        public void SetNotificationFrequencyMethod(Dictionary<long, string> items)
        {
            throw new NotImplementedException();
        }

        public double Longitude { get; set; }

        public double Latitude { get; set; }

        public string VerifyPassword { get; set; }

        public string UserVerificationHashRequirement { get; set; }

        public int UserRating { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string ContactNumber { get; set; }

        public string EMailAddress { get; set; }

        public string Username { get; set; }

        public string PasswordHash { get; set; }

        public string Password { get; set; }

        public string SecurityQuestion { get; set; }

        public string SecurityQuestionAnswer { get; set; }

        public long Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateLastSaved { get; set; }
    }
}