﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace derotek.BikeCasa.Mvp.User.Presenters
{
    public class ForgotLogonCredentialsPresenter
    {
        #region Properties
        public Views.IForgotLogonCredentialsView View { get; private set; }
        #endregion

        #region Constructors
        public ForgotLogonCredentialsPresenter(Views.IForgotLogonCredentialsView view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void SendResetDetails()
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("EMailAddress", DORM.Enumerations.FilterCriteriaComparitor.Equals, View.EMailAddress));
                derotek.HairyDog.Users.User user = transaction.RetrieveFirst<derotek.HairyDog.Users.User>(filter);

                if (user != null)
                {
                    string resetId = derotek.KeyGenerators.SimpleGenerators.GenerateKey(derotek.Configuration.FieldLengths.PasswordResetId);

                    new derotek.HairyDog.Model.Mailer().SendPasswordResetEmail(View.EMailAddress, resetId);
                    
                    user.PasswordResetId = resetId;
                    transaction.AddOrUpdate(user);
                    transaction.Commit();
                    View.DisplayMessage("An email with instructions on how to change your password has been emailed to " + View.EMailAddress, false);
                }
                else
                {
                    View.DisplayMessage("This email address does not exist", true);
                }
            }
        }
        #endregion
    }
}
