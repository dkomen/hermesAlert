﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.User.Presenters
{
    public class LogonPresenter
    {
        #region Properties
        public Views.ILogon View { get; private set; }
        #endregion

        #region Constructors
        public LogonPresenter(Views.ILogon logonView)
        {
            View = logonView;
        }
        #endregion

        #region Public Functions
        public void LogonUser()
        {
            if (derotek.HairyDog.Model.UserAuthentication.AuthenticateUser(View.Username, View.Password) == false)
            {
                View.CurrentUser.Id = 0;     

                View.DisplayMessage("Unknown username or password", true);
            }
            else
            {

                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    DORM.Filter filter = new DORM.Filter();
                    filter.Add( new DORM.FilterItem("Username",DORM.Enumerations.FilterCriteriaComparitor.Equals, View.Username));
                    IList<derotek.HairyDog.Users.User> users = transaction.Retrieve<derotek.HairyDog.Users.User>(filter, 1);
                    if ((users[0].UserVerificationHashRequirement == null) || (users[0].UserVerificationHashRequirement.Trim() == string.Empty))
                    {
                        users[0].LastLoginDate = System.DateTime.Now;

                        transaction.AddOrUpdate(users[0]);
                        transaction.Commit();
                        
                        new derotek.HairyDog.Model.Users().UpdateUserRatingForSigningIntoTheSystem(users[0].Id);
                        View.CurrentUser = users[0];

                        View.RedirectView("~/Pages_Monitoring/ItemsMonitoring.aspx");
                    }
                    else
                    {
                        View.DisplayMessage("User has not been activated yet.", true);
                    }
                }                             
            }
        }
        public void CreateUser()
        {
            View.RedirectView("~/Pages_User/CreateNewUser.aspx");
        }
        public void ForgotLogonCredentials()
        {
            View.RedirectView("~/Pages_User/ForgotLogonDetails.aspx");
        }
        #endregion
    }
}
