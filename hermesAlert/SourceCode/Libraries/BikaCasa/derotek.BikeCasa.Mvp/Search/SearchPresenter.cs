﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Search
{
    public class SearchPresenter
    {
        #region Properties
        private ISearchView View { get; set; }
        #endregion

        #region Constructors
        public SearchPresenter(ISearchView view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void PrepareView(long propertyTypeToListReference)
        {
            
        }
        public void ShowItem(long itemId, string returnToPage)
        {
            View.ActiveObjectId = itemId;
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ActiveObjectId, itemId.ToString());
            
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_ItemRegistration/ItemRegistration.aspx" + qs.EncodeQueryString());
        }
        public void GetStolenItemSearchResults(string searchTerm)
        {
            if (searchTerm.Trim().Length >= 4)
            {
                View.DisplayMessage(string.Empty, false);
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    IList<derotek.HairyDog.RegisteredItem> searchResults =  derotek.Fnh.DataAccess.SPROC_Functions.SearchForStolenIdentificationNumber(searchTerm.Trim(), 50);
                    View.StolenItemSearchResults = searchResults.GetDisplayableList();
                }                
            }
            else
            {
                View.DisplayMessage("A minimum of 4 characters are needed to do a search", true);
            }            
        }
        #endregion
    }
}
