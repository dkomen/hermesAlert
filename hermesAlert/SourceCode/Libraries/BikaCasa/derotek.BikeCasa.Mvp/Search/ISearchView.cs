﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Search
{
    public interface ISearchView : IView
    {
        List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> StolenItemSearchResults { get; set; }   
    }
}
