﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Registrations
{
    public class RegistrationsPresenter
    {
        #region Properties
        private IRegisteredItems View { get; set; }
        #endregion

        #region Constructors
        public RegistrationsPresenter(IRegisteredItems view)
        {
            View = view;
        }

        public void PrepareView()
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("UserId", DORM.Enumerations.FilterCriteriaComparitor.Equals, View.CurrentUser.Id));

                View.RegisteredObjects = (transaction.Retrieve<derotek.HairyDog.RegisteredItem>(filter, 100)).GetDisplayableList();
            }                        
        }


        public void Edit(long idOfToEdit, string returnToPage)
        {
            View.ActiveObjectId = idOfToEdit;
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            qs.Variables.Add(derotek.HairyDog.QueryString.ActiveObjectId, idOfToEdit.ToString());
            View.RedirectView("~/Pages_ItemRegistration/ItemRegistration.aspx" + qs.EncodeQueryString());
        }

        public void RedirectToAddNewRegisteredItem(string returnToPage)
        {
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_ItemRegistration/ItemRegistration.aspx" + qs.EncodeQueryString());
        }
        #endregion       
    }
}
