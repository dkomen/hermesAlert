﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace derotek.BikeCasa.Mvp.Registrations
{
    /// <summary>
    /// An object that can be used to persists the state of an IRegisteredItem page
    /// </summary>
    /// <remarks>Example: store to statebag and refresh the page later with this same data</remarks>
    [Serializable]
    public class StateItemRegistration : IStatePersistableObject, derotek.BikeCasa.Mvp.Registrations.IRegisteredItem
    {
        #region Constructors
        public StateItemRegistration()
        {
            KeepAliveForPages = new List<string>();
        }
        #endregion

        #region IStatePersistableObject
        /// <summary>
        /// The id of this object.
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// For which items\pages to keep this state object alive. Any other items\pages should remove this state object
        /// </summary>
        public List<string> KeepAliveForPages { get; set; }
        #endregion
        
        public string Description { get; set; }
        public string LongDescription { get; set; }
        public long PropertyType { get; set; }
        public bool IsStolen { get; set; }
        public string PoliceCaseNumber { get; set; }
        public bool IfIsStolenMustNotifyCommunity { get; set; }
        public DateTime DateWasStolen { get; set; }
        public long LocationAccuracyWhenPublic { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public void SetPropertyTypeList(Dictionary<long, string> list)
        {
            throw new NotImplementedException();
        }
        public void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list)
        {
            throw new NotImplementedException();
        }
        public HairyDog.Interfaces.IAttachmentsHost AttachmentsHost { get; set; }
        public bool ShowAttachments { get; set; }
        public bool ReadonlyMode { get; set; }
        public List<HairyDog.ItemMonitoring.ItemDetection> Messages { get; set; }
        public derotek.HairyDog.Users.User CurrentUser { get; set; }
        public void DisplayMessage(string message, bool isErrorMessage)
        {
            throw new NotImplementedException();
        }
        public void RedirectView(string path)
        {
            throw new NotImplementedException();
        }
        public string ReturnToPage { get; set; }
        public long ActiveObjectId { get; set; }
    }
}