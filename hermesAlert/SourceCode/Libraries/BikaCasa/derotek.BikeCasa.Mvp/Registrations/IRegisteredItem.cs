﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Registrations
{
    public interface IRegisteredItem: IView
    {
        string Description { get; set; }
        string LongDescription { get; set; }
        long PropertyType { get; set; }
        bool IsStolen { get; set; }
        string PoliceCaseNumber { get; set; }
        bool IfIsStolenMustNotifyCommunity { get; set; }
        DateTime DateWasStolen { get; set;  }
        long LocationAccuracyWhenPublic { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        void SetPropertyTypeList(Dictionary<long, string> list);
        void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list);   
        derotek.HairyDog.Interfaces.IAttachmentsHost AttachmentsHost {get;set;}
        bool ShowAttachments { get; set; }
        bool ReadonlyMode { get; set; }
        List<derotek.HairyDog.ItemMonitoring.ItemDetection> Messages { get; set; }
    }
}
