﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Registrations
{
    public class RegisteredItemPresenter
    {
        #region Constructors
        public RegisteredItemPresenter(derotek.BikeCasa.Mvp.Registrations.IRegisteredItem view)
        {
            View = view;
        }
        #endregion

        #region Properties
        private derotek.BikeCasa.Mvp.Registrations.IRegisteredItem View { get; set; }
        #endregion

        public void PrepareView(long currentObjectId, StateItemRegistration storedPageState)
        {            

            //Get property types
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                IList<derotek.HairyDog.Metrics.PropertyType> propertyTypes = transaction.Retrieve<derotek.HairyDog.Metrics.PropertyType>(null);
                View.SetPropertyTypeList(propertyTypes.ToDictionary(x => x.Id, yield => yield.Description));
            }            

            //Get Location accuracy types
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                IList<derotek.HairyDog.Metrics.LocationAccuracy> locationAccuraciesWhenPublic = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(null);
                View.SetLocationAccuracyWhenPublicList(locationAccuraciesWhenPublic.ToDictionary(x => x.Id, yield => yield.Description));
            }

            if (storedPageState != null)
            {
                View.AttachmentsHost = storedPageState.AttachmentsHost;
                View.ActiveObjectId = storedPageState.ActiveObjectId;
                View.Description = storedPageState.Description;
                View.CurrentUser = storedPageState.CurrentUser;
                View.DateWasStolen = storedPageState.DateWasStolen;
                View.IfIsStolenMustNotifyCommunity = storedPageState.IfIsStolenMustNotifyCommunity;
                View.IsStolen = storedPageState.IsStolen;
                View.LongDescription = storedPageState.LongDescription;
                View.Messages = storedPageState.Messages;
                View.PoliceCaseNumber = storedPageState.PoliceCaseNumber;
                View.PropertyType = storedPageState.PropertyType;
                View.ReadonlyMode = storedPageState.ReadonlyMode;
                View.ReturnToPage = storedPageState.ReturnToPage;
                View.ShowAttachments = storedPageState.ShowAttachments;

                View.Latitude = storedPageState.Latitude;
                View.Longitude = storedPageState.Longitude;

                if (storedPageState.LocationAccuracyWhenPublic != 0)
                {
                    View.LocationAccuracyWhenPublic = storedPageState.LocationAccuracyWhenPublic;
                    View.Latitude = storedPageState.Latitude;
                    View.Longitude = storedPageState.Longitude;
                }
                storedPageState = null;
            }
            else
            {
                if (currentObjectId != 0)
                {
                    derotek.HairyDog.RegisteredItem registeredItem;
                    using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                    {
                        registeredItem = transaction.Retrieve<derotek.HairyDog.RegisteredItem>(currentObjectId);
                    }

                    View.AttachmentsHost = registeredItem;
                    View.Description = registeredItem.Description;
                    View.IsStolen = registeredItem.IsStolen;
                    View.PoliceCaseNumber = registeredItem.PoliceCaseNumber;
                    View.IfIsStolenMustNotifyCommunity = registeredItem.IfIsStolenMustNotifyCommunity;
                    View.DateWasStolen = registeredItem.DateWasStolen;
                    View.LongDescription = registeredItem.Notes;
                    View.PropertyType = registeredItem.PropertyType.Id;
                    if (registeredItem.LocationAccuracy != null)
                    {
                        View.LocationAccuracyWhenPublic = registeredItem.LocationAccuracy.Id;

                        double longitude = registeredItem.Longitude;
                        double latitude = registeredItem.Latitude;
                        if (registeredItem.UserId != View.CurrentUser.Id)
                        {
                            derotek.HairyDog.CoordinateObfuscator.ObfuscateCoordinates(registeredItem.LocationAccuracy, registeredItem.Longitude, registeredItem.Latitude, out longitude, out latitude);
                        }
                        View.Latitude = latitude;
                        View.Longitude = longitude;
                    }
                    View.ReadonlyMode = (registeredItem.UserId != View.CurrentUser.Id);
                    using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                    {
                        DORM.Filter filterCriteria = new DORM.Filter();
                        filterCriteria.Add(new DORM.FilterItem("OwnerItemId",DORM.Enumerations.FilterCriteriaComparitor.Equals, currentObjectId));
                        View.Messages = transaction.Retrieve<derotek.HairyDog.ItemMonitoring.ItemDetection>(filterCriteria, 100) as List<derotek.HairyDog.ItemMonitoring.ItemDetection>;
                    }
                }
                else
                {
                    View.ReadonlyMode = false;
                }
                View.ShowAttachments = currentObjectId != 0;
            }

        }
        public void DeleteRegisteredItem(long currentObjectId, string returnToPage)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                derotek.HairyDog.RegisteredItem registeredItem = new HairyDog.RegisteredItem();
                if (currentObjectId != 0)
                {
                    registeredItem = transaction.Retrieve<derotek.HairyDog.RegisteredItem>(currentObjectId);
                    transaction.Delete(registeredItem);
                    transaction.Commit();                    
                }
                View.CurrentUser.UserRating = new derotek.HairyDog.Model.Users().UpdateUserRatingForRegisteredItem(View.CurrentUser.Id, false);
                RedirectView(returnToPage);
            }           
        }
        public void SaveRegisteredItem(long currentObjectId)
        {

            bool stolenItemDetailsChanged = true;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                derotek.HairyDog.RegisteredItem registeredItem = new HairyDog.RegisteredItem();
                if (currentObjectId != 0)
                {
                    registeredItem = transaction.Retrieve<derotek.HairyDog.RegisteredItem>(currentObjectId);

                    #region Has any pertinent info regarding the stolen item changed?
                    stolenItemDetailsChanged = (Math.Abs(Math.Abs(registeredItem.Latitude) - Math.Abs(View.Latitude)) >= 0.0001)
                        || (Math.Abs(Math.Abs(registeredItem.Longitude) - Math.Abs(View.Longitude)) >= 0.0001)
                        || registeredItem.LocationAccuracy.Id != View.LocationAccuracyWhenPublic
                        || (View.IfIsStolenMustNotifyCommunity != registeredItem.IfIsStolenMustNotifyCommunity)
                        || registeredItem.DateWasStolen != View.DateWasStolen;
                    #endregion
                }

                registeredItem.UserId = View.CurrentUser.Id;
                registeredItem.Description = View.Description;
                registeredItem.IsStolen = View.IsStolen;
                registeredItem.PoliceCaseNumber = View.PoliceCaseNumber;
                registeredItem.IfIsStolenMustNotifyCommunity = View.IfIsStolenMustNotifyCommunity;
                registeredItem.DateWasStolen = View.DateWasStolen != new System.DateTime() ? View.DateWasStolen : System.DateTime.Now;
                registeredItem.Notes = View.LongDescription;
                registeredItem.PropertyType = transaction.Retrieve<derotek.HairyDog.Metrics.PropertyType>(View.PropertyType);
                registeredItem.LocationAccuracy = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(View.LocationAccuracyWhenPublic);
                registeredItem.Latitude = View.Latitude;
                registeredItem.Longitude = View.Longitude;

                try
                {
                    transaction.AddOrUpdate(registeredItem);
                    transaction.Commit();
                    View.AttachmentsHost = registeredItem;

                    View.DisplayMessage("Saved successfully", false);

                    //Update user rating if new RegisteredItem               
                    if (View.ActiveObjectId == 0)
                    {
                        View.CurrentUser.UserRating = new derotek.HairyDog.Model.Users().UpdateUserRatingForRegisteredItem(View.CurrentUser.Id, true);
                    }
                    if (registeredItem.IfIsStolenMustNotifyCommunity && stolenItemDetailsChanged)
                    {
                        double longitude;
                        double latitude;
                        derotek.HairyDog.CoordinateObfuscator.ObfuscateCoordinates(registeredItem.LocationAccuracy, registeredItem.Longitude, registeredItem.Latitude, out longitude, out latitude);

                        #region Twitter
                        string description = "MISSING ITEM ";
                        if (description.Length > 67)
                        {
                            description = description + registeredItem.Description.Substring(0, 67) + "...";
                        }
                        description = "(" + registeredItem.DateWasStolen.ToString("dd MMM yyyy hh:mm") + "): " + description
                            + System.Environment.NewLine + "http://www.hermesAlert.com/Pages_main/showgps.aspx?long=" + longitude + "&lat=" + latitude + "&pt=" + registeredItem.PropertyType.Id;
                        new derotek.HairyDog.Model.Twitter().SendTweet(description);
                        #endregion

                        description = "A missing item was reported in one of the areas you are monitoring<br /><br />"
                                + "Property type : " + Enum.GetName(typeof(derotek.HairyDog.Enumerations.PropertyType), registeredItem.PropertyType.ExternalReferanceId) + "<br />"
                                + "Event date : " + registeredItem.DateWasStolen.ToString("dddd, dd MMM yyyy : hh:mm") + "<br />"
                                + "Gps coordinates (long, lat) : " + longitude.ToString() + ", " + latitude.ToString() + "<br />"
                                + "Description : " + registeredItem.Description + "<br /><br />"
                                + "http://www.hermesAlert.com/Pages_main/showgps.aspx?long=" + longitude + "&lat=" + latitude + "&pt=" + registeredItem.PropertyType.Id;
                        (new derotek.HairyDog.Model.Mailer()).SendMailToUsersMonitoringGpsPoint("Missing item report from hermesAlert area monitor", description, Microsoft.SqlServer.Types.SqlGeography.Point(latitude, longitude, 4326));
                    }
                    View.ActiveObjectId = registeredItem.Id;
                }
                catch (derotek.EntityValidation.ValidationErrorException valEx)
                {
                    View.DisplayMessage(valEx.Message, true);
                }
                catch (Exception ex)
                {
                    View.DisplayMessage("An unknown error occured while saving", true);
                }
                View.ShowAttachments = registeredItem.Id != 0;
            }            
        }
        public void RedirectView(string path)
        {
            View.RedirectView(path);
        }
        public void MessageRead(long activeObjectId, long idToEdit, string returnToPage)         
        {
            derotek.HairyDog.QueryString callingPageDetails = new derotek.HairyDog.QueryString();
            callingPageDetails.Variables.Add(derotek.HairyDog.QueryString.ActiveObjectId, activeObjectId.ToString());
            callingPageDetails.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);

            derotek.HairyDog.QueryString qs = new derotek.HairyDog.QueryString();
            qs.Variables[derotek.HairyDog.QueryString.ReturnToPage] = returnToPage + callingPageDetails.EncodeQueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ChildObjectId, idToEdit.ToString());
            qs.Variables.Add(derotek.HairyDog.QueryString.ActiveObjectId, activeObjectId.ToString());
            View.RedirectView("~/Pages_User/SendMessage.aspx" + qs.EncodeQueryString());
        }
        public void RedirectToSelectGpsLocation(string returnToPage)
        {
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_Main/SelectGpsLocation.aspx" + qs.EncodeQueryString());
        }
    }
}
