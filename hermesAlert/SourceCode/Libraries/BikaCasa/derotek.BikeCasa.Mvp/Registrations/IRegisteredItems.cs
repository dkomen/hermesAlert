﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Registrations
{
    public interface IRegisteredItems : IView
    {
        List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> RegisteredObjects { get; set; }
    }
}
