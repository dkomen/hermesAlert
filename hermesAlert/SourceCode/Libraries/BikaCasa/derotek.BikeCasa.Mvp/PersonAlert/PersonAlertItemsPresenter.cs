﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.PersonAlert
{
    public class PersonAlertItemsPresenter
    {
        #region Properties
        public IPersonAlertItems View { get; private set; }
        #endregion

        #region Constructors
        public PersonAlertItemsPresenter(IPersonAlertItems view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void PrepareView(derotek.HairyDog.Enumerations.ItemType AlertItemTypeToShow)
        {
            derotek.Fnh.DataAccess.AccessManager manager = new Fnh.DataAccess.AccessManager();

            derotek.DataAccess.Filter filter = new DataAccess.Filter();
            derotek.HairyDog.PersonAlert.PersonAlert filterDummy = new HairyDog.PersonAlert.PersonAlert();
            filter.Items.Add(filterDummy.ItemType.GetType().Name, AlertItemTypeToShow);
            filter.Items.Add(filterDummy.IsDeleted.GetType().Name, false);
            filter.Items.Add(filterDummy.ApprovalStatus.GetType().Name, HairyDog.Enumerations.ApprovalStatus.Approved);
            View.AlertItems = manager.GetMany<derotek.HairyDog.PersonAlert.PersonAlert>(filter, 200);
        }
        #endregion
    }
}
