﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.PersonAlert
{
    interface IPersonAlert : IView, HairyDog.Interfaces.ILocation
    {
        public Single AwardAmount { get; set; }
        public string Firstname { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public string KnownAs { get; set; }
        public System.DateTime DateLastSeen { get; set; }
        public string PoliceCaseNumber { get; set; }
        public string PathToImage { get; set; }
        public virtual derotek.HairyDog.Enumerations.ItemType ItemType { get; set; }     
    }
}
