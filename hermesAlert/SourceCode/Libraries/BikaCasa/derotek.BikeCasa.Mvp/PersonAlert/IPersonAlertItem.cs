﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.PersonAlert
{
    public interface IPersonAlertItem: IView
    {

        #region Contact person
        long UserId { get; set; }
        #endregion

        #region Who is missing
        string Firstname { get; set; }
        string SecondName { get; set; }
        string Surname { get; set; }
        string KnownAs { get; set; }
        #endregion

        #region Date last seen
        System.DateTime DateLastSeen
        {
            get;
            set;
        }
        #endregion

        string ContactDetails { get; set; }
        string PoliceCaseNumber { get; set; }
        /// <summary>
        /// The path to the currently persisted image
        /// </summary>
        string PathToImageToDisplay { get; set; }
        /// <summary>
        /// The file name of the newly uploaded image. If isEmpty then no file was uploaded
        /// </summary>
        string NewImageFileName { get; }
        void SetApprovalStatusList(Dictionary<long, string> list);
        void SetGenderList(Dictionary<long, string> list);
        void SetEyeColourList(Dictionary<long, string> list);
        void SetHairColourList(Dictionary<long, string> list);
        long ApprovalStatus { get; set; }
        Single AwardAmount { get; set; }
        bool Solved { get; set; }
        IList<derotek.HairyDog.ItemMonitoring.ItemDetection> ItemDetectionMessages { get; set; }

        string ApproximateAge { get; set; }
        long EyeColour { get; set; }
        long HairColour { get; set; }
        long Gender { get; set; }
        string DistinguishingFeatures { get; set; }
        string Notes { get; set; }

        void SetItemTypeList(Dictionary<long, string> list);
        int ItemType { get; set; }

        long LocationAccuracyWhenPublic { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list);

        bool ReadonlyMode { get; set; }
    }
}
