﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.PersonAlert
{
    /// <summary>
    /// Maintain a person alert item
    /// </summary>
    public class PersonAlertsPresenter
    {
        #region Properties
        public IPersonAlertItems View { get; private set; }
        #endregion

        #region Constructors
        public PersonAlertsPresenter(IPersonAlertItems view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void PrepareView()
        {
            List<derotek.HairyDog.PersonAlert.PersonAlert> personAlertItems = derotek.HairyDog.Model.MissingPersons.GetMissingPersons(150);
            View.AlertItems = personAlertItems.GetDisplayableList(); 
        }
        public void RedirectToMissingPersonItem(string path)
        {
            View.RedirectView(path);
        }
        public void Edit(long idOfToEdit, string returnToPage)
        {
            View.ActiveObjectId = idOfToEdit;
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            qs.Variables.Add(derotek.HairyDog.QueryString.ActiveObjectId, idOfToEdit.ToString());
            View.RedirectView("~/Pages_Reporting/MissingPerson.aspx" + qs.EncodeQueryString());
        }
        #endregion
    }
}
