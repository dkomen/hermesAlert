﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.PersonAlert
{
    public interface IPersonAlertItems: IView
    {
        List<derotek.HairyDog.DisplayableItems.DisplayablePersonAlertObject> AlertItems { set; }
    }
}
