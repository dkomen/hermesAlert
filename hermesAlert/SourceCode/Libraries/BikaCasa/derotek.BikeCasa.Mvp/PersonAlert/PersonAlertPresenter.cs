﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.PersonAlert
{
    /// <summary>
    /// Maintain a person alert item
    /// </summary>
    public class PersonAlertPresenter
    {
        #region Properties
        public IPersonAlertItem View { get; private set; }
        #endregion

        #region Constructors
        public PersonAlertPresenter(IPersonAlertItem view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void PrepareView(long alertId)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                IList<derotek.HairyDog.Metrics.LocationAccuracy> locationAccuraciesWhenPublic = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(null);
                View.SetLocationAccuracyWhenPublicList(locationAccuraciesWhenPublic.ToDictionary(x => x.Id, yield => yield.Description));

                IList<derotek.HairyDog.Metrics.ApprovalStatus> approvalStatusList = transaction.Retrieve<derotek.HairyDog.Metrics.ApprovalStatus>(null);
                View.SetApprovalStatusList(approvalStatusList.ToDictionary(x => x.Id, yield => yield.Description));

                IList<derotek.HairyDog.Metrics.Gender> genderList = transaction.Retrieve<derotek.HairyDog.Metrics.Gender>(null);
                View.SetGenderList(genderList.ToDictionary(x => x.Id, yield => yield.Description));

                IList<derotek.HairyDog.Metrics.EyeColour> eyeColourList = transaction.Retrieve<derotek.HairyDog.Metrics.EyeColour>(null);
                View.SetEyeColourList(eyeColourList.ToDictionary(x => x.Id, yield => yield.Description));

                IList<derotek.HairyDog.Metrics.HairColour> hairColourList = transaction.Retrieve<derotek.HairyDog.Metrics.HairColour>(null);
                View.SetHairColourList(hairColourList.ToDictionary(x => x.Id, yield => yield.Description));
            }

            if (alertId != 0)
            {
                derotek.HairyDog.PersonAlert.PersonAlert personAlert;
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    personAlert = transaction.Retrieve<derotek.HairyDog.PersonAlert.PersonAlert>(alertId);
                }
                if (personAlert != null)
                {
                    View.AwardAmount = personAlert.AwardAmount;
                    View.DateLastSeen = personAlert.DateLastSeen;
                    View.Firstname = personAlert.Firstname;
                    View.KnownAs = personAlert.KnownAs;
                    View.Latitude = personAlert.Latitude;
                    View.Longitude = personAlert.Longitude;
                    View.PoliceCaseNumber = personAlert.PoliceCaseNumber;
                    View.SecondName = personAlert.SecondName;
                    View.Surname = personAlert.Surname;
                    View.ContactDetails = personAlert.ContactDetails;

                    View.ApprovalStatus = personAlert.ApprovalStatus.Id;
                    View.ItemType = (int)personAlert.ItemType.Id;
                    View.ApproximateAge = personAlert.ApproximateAge;
                    View.DistinguishingFeatures = personAlert.DistinguishingFeatures;
                    View.EyeColour = personAlert.EyeColour.Id;
                    View.HairColour = personAlert.HairColour.Id;
                    View.Gender = personAlert.Gender.Id;
                    View.LocationAccuracyWhenPublic = personAlert.LocationAccuracy.Id;
                    View.Notes = personAlert.Notes;
                    View.PathToImageToDisplay = personAlert.PathToImage;

                    View.Gender = personAlert.Gender.Id;

                    //Is the current person viewing this personAlert NOT the same person that created the record?
                    if (View.CurrentUser.Id != personAlert.UserId)
                    {
                        //Yes, so hide persistance etc options, only the original owner may edit this record
                        View.ReadonlyMode = true;
                    }

                }                
            }
            else
            {
                View.DateLastSeen = System.DateTime.Today;
            }
        }
        public void Save(string originalPathToFile, string newFullPathToFile, string newThumbsParthToFile)
        {
            try
            {
            derotek.HairyDog.PersonAlert.PersonAlert personAlert;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                DORM.Filter filter = new DORM.Filter();
                if (View.ActiveObjectId == 0)
                {
                    personAlert = new HairyDog.PersonAlert.PersonAlert();                    
                    filter.Add(new DORM.FilterItem("ExternalReferanceId", DORM.Enumerations.FilterCriteriaComparitor.Equals,(long)HairyDog.Enumerations.ApprovalStatus.Approved));
                    personAlert.ApprovalStatus = transaction.RetrieveFirst<HairyDog.Metrics.ApprovalStatus>(filter);
                    //personAlert.IsDeleted = false;
                    personAlert.UserId = View.CurrentUser.Id;
                }
                else
                {
                    personAlert = transaction.Retrieve<derotek.HairyDog.PersonAlert.PersonAlert>(View.ActiveObjectId);
                }

                personAlert.AwardAmount = View.AwardAmount;
                personAlert.DateLastSeen = View.DateLastSeen;
                personAlert.Firstname = View.Firstname;
                filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("ExternalReferanceId", DORM.Enumerations.FilterCriteriaComparitor.Equals ,(long)derotek.HairyDog.Enumerations.ItemType.MissingPerson));
                personAlert.ItemType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.ItemType>(filter);
                personAlert.KnownAs = View.KnownAs;
                personAlert.Latitude = View.Latitude;
                personAlert.Longitude = View.Longitude;
                personAlert.LocationAccuracy = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(View.LocationAccuracyWhenPublic);
                personAlert.UserId = View.CurrentUser.Id;
                personAlert.ContactDetails = View.ContactDetails;
                #region Save photo only if there is a newly uploaded one
                if (View.NewImageFileName.Trim() != string.Empty) //new image
                {
                    derotek.HairyDog.Graphics.ImageManipulation.ResizeImage(originalPathToFile, newFullPathToFile, derotek.HairyDog.Graphics.ImageManipulation.MaximumAlbumPicturePixelSize, true);
                    derotek.HairyDog.Graphics.ImageManipulation.ResizeImage(originalPathToFile, newThumbsParthToFile, derotek.HairyDog.Graphics.ImageManipulation.MaximumThumbnailImagePixelSize, true);
                    personAlert.PathToImage = View.NewImageFileName;
                    View.PathToImageToDisplay = View.NewImageFileName; ;
                }
                else
                {
                    //dont change the image path if no new file was uploaded
                }
                #endregion
                personAlert.PoliceCaseNumber = View.PoliceCaseNumber;
                personAlert.SecondName = View.SecondName;
                personAlert.Surname = View.Surname;
                personAlert.ApproximateAge = View.ApproximateAge;
                personAlert.DistinguishingFeatures = View.DistinguishingFeatures;
                personAlert.EyeColour = transaction.Retrieve<HairyDog.Metrics.EyeColour>(View.EyeColour);
                personAlert.Gender = transaction.Retrieve<HairyDog.Metrics.Gender>(View.Gender);
                personAlert.HairColour = transaction.Retrieve<HairyDog.Metrics.HairColour>(View.HairColour);
                personAlert.Notes = View.Notes;
                personAlert.Solved = View.Solved;
                transaction.AddOrUpdate(personAlert);
                transaction.Commit();
            }
               
                if (View.ActiveObjectId == 0)
                {
                    if (View.CurrentUser.UserRating >= 200)
                    {
                        double longitude;
                        double latitude;
                        derotek.HairyDog.CoordinateObfuscator.ObfuscateCoordinates(personAlert.LocationAccuracy, personAlert.Longitude, personAlert.Latitude, out longitude, out latitude);

                        #region Twitter
                        string description = "MISSING PERSON ";
                        if (description.Length > 67)
                        {
                            description = description + personAlert.DistinguishingFeatures.Substring(0, 67) + "...";
                        }
                        description = "(" + personAlert.DateLastSeen.ToString("dd MMM yyyy hh:mm") + "): " + description
                            + System.Environment.NewLine + "http://www.hermesAlert.com/Pages_main/showgps.aspx?long=" + longitude + "&lat=" + latitude;
                        new derotek.HairyDog.Model.Twitter().SendTweet(description);
                        #endregion

                        description = "<br>A missing person was reported in one of the areas you are monitoring<br><br>"
                            + "Name : " + personAlert.KnownAs + " (" + personAlert.Firstname + " " + personAlert.Surname + ")" + "<br>"
                            + "Last seen : " + personAlert.DateLastSeen.ToString("dddd, dd MMM yyyy : hh:mm") + "<br>"
                            + "Gps coordinates (long, lat) : " + personAlert.Longitude.ToString() + ", " + personAlert.Latitude.ToString() + "<br>"
                            + "Features : " + personAlert.DistinguishingFeatures + "<br>"
                            + "Notes : " + personAlert.Notes + "<br><br>"
                            + "http://www.hermesAlert.com/Pages_main/showgps.aspx?long=" + longitude + "&lat=" + latitude;
                        (new derotek.HairyDog.Model.Mailer()).SendMailToUsersMonitoringGpsPoint("Missing person report from hermesAlert area monitor", description, personAlert.SqlGeographyPoint);
                    }

                    View.CurrentUser.UserRating = new derotek.HairyDog.Model.Users().UpdateUserRatingForIncident(View.CurrentUser.Id, true);
                }
                View.ActiveObjectId = personAlert.Id;
                View.DisplayMessage("Saved successfully", false);
            }
            catch (derotek.EntityValidation.ValidationErrorException valEx)
            {
                View.DisplayMessage(valEx.Message, true);
            }
            catch (Exception ex)
            {
                View.DisplayMessage("An unknown error occurred while saving", true);
            }
        }        
        public void Delete(string redirectToPage)
        {
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                derotek.HairyDog.PersonAlert.PersonAlert personAlert = new HairyDog.PersonAlert.PersonAlert();
                if (View.ActiveObjectId != 0)
                {
                    personAlert = transaction.Retrieve<derotek.HairyDog.PersonAlert.PersonAlert>(View.ActiveObjectId);
                    transaction.Delete(personAlert);
                    transaction.Commit();
                    //Update user rating
                    View.CurrentUser.UserRating = new derotek.HairyDog.Model.Users().UpdateUserRatingForMissingPerson(View.CurrentUser.Id, false);
                    RedirectView(View.ReturnToPage);
                }
            }
        }
        public void RedirectView(string path)
        {
            View.RedirectView(path);
        }
        #endregion
    }
}
