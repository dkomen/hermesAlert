﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp
{
    public class ViewMasterPagePresenter
    {
        #region Fields
        private IViewMasterPage View {get;set;}
        #endregion

        #region Constructors
        public ViewMasterPagePresenter(IViewMasterPage view)
        {
            View = view;
        }
        #endregion

        #region Public Functions

        #endregion

        #region View
        public void PrepareView(string rootPath)
        {
            View.ShowRedirectionOptions = View.CurrentUser.Id != 0;
            View.PageFooter = "<a href='" + rootPath + "Pages_Main/faq.aspx'>Q&A</a>&nbsp;&nbsp; copyright © DeroTek 2011, 2012 &nbsp;&nbsp;<a href='" + rootPath + "Pages_Main/RecieveMessage.aspx'>contact</a><font size='1'><br />Optimised for: Firefox 12+, Chrome 21+, Opera 10.6+, Safari 5+, Internet Explorer 9+, and on mobile only browsers: Opera Mobile 10.1+, Blackberry OS 6, iPhone 3+, Android 2+</font>";
            View.LoggedInUserDetails = View.CurrentUser;
        }

        public void RedirectView(string path)
        {
            View.RedirectView(path);
        }
        #endregion
    }
}
