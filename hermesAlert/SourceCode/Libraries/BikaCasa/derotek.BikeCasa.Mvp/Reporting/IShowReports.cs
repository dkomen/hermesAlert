﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Reporting
{
    public interface IShowReports : IView
    {
        List<derotek.HairyDog.DisplayableItems.DisplayableQuickReport> QuickReports { get; set; }  
    }
}
