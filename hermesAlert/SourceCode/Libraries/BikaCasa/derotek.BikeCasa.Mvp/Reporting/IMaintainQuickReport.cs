﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Reporting
{
    public interface IMaintainQuickReport: IView
    {
        string Description { get; set; }        
        long IncidentType { get; set; }
        DateTime IncidentDate { get; set; }
        long LocationAccuracyWhenPublic { get; set; }
        double Latitude { get; set; }
        double Longitude { get; set; }
        void SetIncidentTypeList(Dictionary<long, string> list);
        void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list);        
    }
}
