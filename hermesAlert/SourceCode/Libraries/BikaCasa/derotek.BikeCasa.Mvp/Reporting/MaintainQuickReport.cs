﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Reporting
{
    public class MaintainQuickReport
    {
        #region Constructors
        public MaintainQuickReport(derotek.BikeCasa.Mvp.Reporting.IMaintainQuickReport view)
        {
            View = view;
        }
        #endregion

        #region Properties
        private derotek.BikeCasa.Mvp.Reporting.IMaintainQuickReport View { get; set; }
        #endregion

        public void PrepareView(long currentObjectId, StateQuickReportItem storedPageState)
        {            
            //Get property types
            IList<derotek.HairyDog.Metrics.IncidentType> propertyTypes;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                propertyTypes = transaction.Retrieve<derotek.HairyDog.Metrics.IncidentType>(null);
            }
            View.SetIncidentTypeList(propertyTypes.ToDictionary(x => x.Id, yield => yield.Description));

            //Get Location accuracy types
            IList<derotek.HairyDog.Metrics.LocationAccuracy> locationAccuraciesWhenPublic;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                locationAccuraciesWhenPublic = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(null);
            }
            View.SetLocationAccuracyWhenPublicList(locationAccuraciesWhenPublic.ToDictionary(x => x.Id, yield => yield.Description));

            if (storedPageState != null)
            {
                View.Description = storedPageState.Description;
                View.IncidentDate = storedPageState.IncidentDate;
                View.Description = storedPageState.Description;
                View.IncidentType = storedPageState.IncidentType;
                View.ReturnToPage = storedPageState.ReturnToPage;
                View.ActiveObjectId = storedPageState.ActiveObjectId;

                View.IncidentType = storedPageState.IncidentType;
                View.Description = storedPageState.Description;
                View.IncidentDate = storedPageState.IncidentDate;
                View.Latitude = storedPageState.Latitude;
                View.Longitude = storedPageState.Longitude;

                if (storedPageState.LocationAccuracyWhenPublic != 0)
                {
                    View.LocationAccuracyWhenPublic = storedPageState.LocationAccuracyWhenPublic;
                    View.Latitude = storedPageState.Latitude;
                    View.Longitude = storedPageState.Longitude;
                }
                storedPageState = null;
            }
            else
            {
                if (currentObjectId != 0)
                {
                    
                    derotek.HairyDog.Incidents.Incident incident;
                    using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                    {
                        incident = transaction.Retrieve<derotek.HairyDog.Incidents.Incident>(currentObjectId);
                    }

                    View.Description = incident.Description;
                    View.IncidentDate = incident.IncidentDateTime;
                    View.Description = incident.Description;
                    View.IncidentType = incident.IncidentType.Id;
                    if (incident.LocationAccuracy != null)
                    {
                        View.LocationAccuracyWhenPublic = incident.LocationAccuracy.Id;
                        View.Latitude = incident.Latitude;
                        View.Longitude = incident.Longitude;
                    }
                }
            }
            
        }
        public void DeleteIncident(long currentObjectId, string returnToPage)
        {
            derotek.HairyDog.Incidents.Incident incident = new HairyDog.Incidents.Incident();
            if (currentObjectId != 0)
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    incident = transaction.Retrieve<derotek.HairyDog.Incidents.Incident>(currentObjectId);
                    transaction.Delete(incident);
                    transaction.Commit();
                }                
                //Update user rating
                View.CurrentUser.UserRating = new derotek.HairyDog.Model.Users().UpdateUserRatingForIncident(View.CurrentUser.Id, false);
                RedirectView(returnToPage);
            }
        }
        public void SaveIncident()
        {           
            derotek.HairyDog.Incidents.Incident incident = new HairyDog.Incidents.Incident();
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                if (View.ActiveObjectId != 0)
                {
                    incident = transaction.Retrieve<derotek.HairyDog.Incidents.Incident>(View.ActiveObjectId);
                }
                incident.UserId = View.CurrentUser.Id;
                incident.Description = View.Description;
                incident.IncidentDateTime = View.IncidentDate != new System.DateTime() ? View.IncidentDate : System.DateTime.Now;
                incident.IncidentType = transaction.Retrieve<derotek.HairyDog.Metrics.IncidentType>(View.IncidentType);
                incident.LocationAccuracy = transaction.Retrieve<derotek.HairyDog.Metrics.LocationAccuracy>(View.LocationAccuracyWhenPublic);
                incident.Latitude = View.Latitude;
                incident.Longitude = View.Longitude;

                try
                {
                    transaction.AddOrUpdate(incident);
                    transaction.Commit();
                    View.DisplayMessage("Saved successfully", false);

                    //Update user rating if new Incident
                    if (View.ActiveObjectId == 0)
                    {
                        if (View.CurrentUser.UserRating >= 200)
                        {

                            string description = incident.Description;
                            if (description.Length > 67)
                            {
                                description = incident.Description.Substring(0, 67) + "...";
                            }

                            description = "(" + incident.IncidentDateTime.ToString("dd MMM yyyy hh:mm") + ")" + Enum.GetName(typeof(derotek.HairyDog.Enumerations.IncidentType), incident.IncidentType.ExternalReferanceId) + ": " + description
                            + System.Environment.NewLine + "http://www.hermesAlert.com/Pages_main/ShowMapFromGps.aspx?id=" + incident.Id;
                            new derotek.HairyDog.Model.Twitter().SendTweet(description);

                            description = "<html><body><br />An incident occurred in one of the areas you are monitoring<br /><br />"
                                + "Incident type : " + Enum.GetName(typeof(derotek.HairyDog.Enumerations.IncidentType), incident.IncidentType.ExternalReferanceId) + "<br />"
                                + "Event date : " + incident.IncidentDateTime.ToString("dddd, dd MMM yyyy : hh:mm") + "<br />"
                                + "Gps coordinates (long, lat) : " + incident.Longitude.ToString() + ", " + incident.Latitude.ToString() + "<br />"
                                + "Description : " + incident.Description + "<br /><br />"
                                + "http://www.hermesAlert.com/Pages_main/ShowMapFromGps.aspx?id=" + incident.Id
                                + "</body></html>";
                            //+ "http://www.hermesAlert.com/Pages_main/ShowMapFromGps.aspx?long=" + incident.Longitude + "&lat=" + incident.Latitude;
                            (new derotek.HairyDog.Model.Mailer()).SendMailToUsersMonitoringGpsPoint("Incident from hermesAlert area monitor", description, incident.SqlGeographyPoint);


                        }

                        View.CurrentUser.UserRating = new derotek.HairyDog.Model.Users().UpdateUserRatingForIncident(View.CurrentUser.Id, true);
                    }

                    View.ActiveObjectId = incident.Id;
                }
                catch (derotek.EntityValidation.ValidationErrorException valEx)
                {
                    View.DisplayMessage(valEx.Message, true);
                }
                catch (Exception ex)
                {
                    View.DisplayMessage("Incident saved but an unknown error occurred while sending community notifications", true);
                }
            }                        
        }
        public void RedirectToSelectGpsLocation(string returnToPage)
        {
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_Main/SelectGpsLocation.aspx" + qs.EncodeQueryString());
        }
        public void RedirectView(string path)
        {            
            View.RedirectView(path);
        }
    }
}
