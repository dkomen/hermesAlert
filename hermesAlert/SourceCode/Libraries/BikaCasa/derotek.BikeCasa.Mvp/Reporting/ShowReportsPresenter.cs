﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Reporting
{
    public class ShowReportsPresenter
    {
        #region Properties
        private IShowReports View { get; set; }
        #endregion

        #region Constructors
        public ShowReportsPresenter(IShowReports view)
        {
            View = view;
        }
        #endregion

        #region Public Functions
        public void RedirectToAddNewIncident(string returnToPage)
        {
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_Reporting/QuickReportItem.aspx" + qs.EncodeQueryString());
        }
        public void PrepareView()
        {
            GetIncidentReports();
        }
        public void ShowItem(long itemId, string returnToPage)
        {
            View.ActiveObjectId = itemId;
            derotek.HairyDog.QueryString qs = new HairyDog.QueryString();
            qs.Variables.Add(derotek.HairyDog.QueryString.ActiveObjectId, itemId.ToString());
            
            qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage, returnToPage);
            View.RedirectView("~/Pages_Reporting/QuickReportItem.aspx" + qs.EncodeQueryString());
        }
        public void GetIncidentReports()
        {
            View.DisplayMessage(string.Empty, false);

            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                List<DORM.OrderBy> orderingFields = new List<DORM.OrderBy>();
                orderingFields.Add(new DORM.OrderBy() { FieldName="IncidentDateTime", Ordering=DORM.Enumerations.OrderingDirection.Descending});
                IList<derotek.HairyDog.Incidents.Incident> searchResults = transaction.Retrieve<derotek.HairyDog.Incidents.Incident>("UserId", View.CurrentUser.Id, orderingFields);
                View.QuickReports = searchResults.GetDisplayableList();
            }
        }
        #endregion
    }
}
