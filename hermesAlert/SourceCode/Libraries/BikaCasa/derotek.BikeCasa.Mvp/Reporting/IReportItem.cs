﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Reporting
{
    public interface IReportItem: IView
    {
        #region Fields
        /// <summary>
        /// A list of displayable items
        /// </summary>
        List<derotek.HairyDog.DisplayableItems.DisplayableItemTypesList> ItemTypes { get; set; }
        /// <summary>
        /// The type of item being reported
        /// </summary>
        Dictionary<long, string> ItemType { get; set; }
        /// <summary>
        /// The Id of the member that reported this
        /// </summary>
        long UserId { get; set; }
        /// <summary>
        /// The Id of the item being reported on
        /// </summary>
        long ReportedItemId { get; set; }
        /// <summary>
        /// A description of why the item is being reported
        /// </summary>
        string Description { get; set; }
        #endregion
    }
}
