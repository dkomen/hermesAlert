﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace derotek.BikeCasa.Mvp.Reporting
{
    /// <summary>
    /// An object that can be used to persists the state of an IMaintainQuickReport page
    /// </summary>
    /// <remarks>Example: store to statebag and refresh the page later with this same data</remarks>
    [Serializable]
    public class StateQuickReportItem : IStatePersistableObject, derotek.BikeCasa.Mvp.Reporting.IMaintainQuickReport
    {
        #region Constructors
        public StateQuickReportItem()
        {
            KeepAliveForPages = new List<string>();
        }
        #endregion

        #region IStatePersistableObject
        /// <summary>
        /// The id of this object.
        /// </summary>
        public string ObjectId { get; set; }

        /// <summary>
        /// For which items\pages to keep this state object alive. Any other items\pages should remove this state object
        /// </summary>
        public List<string> KeepAliveForPages { get; set; }
        #endregion

        #region IMaintainQuickReport
        public string Description { get; set; }

        public long IncidentType { get; set; }

        public DateTime IncidentDate { get; set; }

        public long LocationAccuracyWhenPublic { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public void SetIncidentTypeList(Dictionary<long, string> list)
        {
            throw new NotImplementedException();
        }

        public void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list)
        {
            throw new NotImplementedException();
        }

        public derotek.HairyDog.Users.User CurrentUser { get; set; }

        public void DisplayMessage(string message, bool isErrorMessage)
        {
            throw new NotImplementedException();
        }

        public void RedirectView(string path)
        {
            throw new NotImplementedException();
        }

        public string ReturnToPage { get; set; }

        public long ActiveObjectId { get; set; }
        #endregion        
    }
}