﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Orphans
{
    public interface IOrphansView
    {
        KeyValuePair<int, Single> OrphanAttachmentsThumbsDeleted { get; set; }
        KeyValuePair<int, Single> OrphanAttachmentsResizedDeleted { get; set; }
        KeyValuePair<int, Single> OrphanAttachmentsOriginalsDeleted { get; set; }
        string Message { get; set; }
    }
}
