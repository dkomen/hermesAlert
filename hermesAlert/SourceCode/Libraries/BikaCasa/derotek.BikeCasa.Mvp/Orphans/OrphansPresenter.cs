﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.BikeCasa.Mvp.Orphans
{
    public class OrphansPresenter
    {
        #region Properties
        public IOrphansView View { get; private set; }
        #endregion

        #region Constructors
        public OrphansPresenter(IOrphansView orphansView)
        {
            View = orphansView;
        }
        #endregion

        public void DeleteOrphans(string pathToOrphanAttachmentsOriginals, string pathToOrphanAttachmentsThumbs, string pathToOrphanAttachmentsResized)
        {
            int filesDeleted = 0;
            Single sizeOfFilesDeleted = 0;
            string message = string.Empty;

            #region Kill all ORIGINAL files
            string[] filesInPath = System.IO.Directory.GetFiles(pathToOrphanAttachmentsOriginals);
            message += "Path : " + pathToOrphanAttachmentsOriginals + ", ";
            message += "filesInPath : " + filesInPath.Length + "<br />";
            foreach (string file in filesInPath)
            {
                try
                {
                    System.IO.FileInfo f = new System.IO.FileInfo(file);
                    long fLength = f.Length;
                    System.IO.File.Delete(file);
                    sizeOfFilesDeleted += fLength;
                    filesDeleted++;
                }
                catch { };
            }
            View.OrphanAttachmentsOriginalsDeleted = new KeyValuePair<int, float>(filesDeleted, sizeOfFilesDeleted);
            #endregion

            #region Kill all orphaned THUMBS
            sizeOfFilesDeleted = 0;
            filesDeleted = 0;
            IList<derotek.HairyDog.Attachment> attachments;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                attachments = transaction.Retrieve<derotek.HairyDog.Attachment>(null);
            }

            filesInPath = System.IO.Directory.GetFiles(pathToOrphanAttachmentsThumbs);
            message += "Path : " + pathToOrphanAttachmentsThumbs + ", ";
            message += "filesInPath : " + filesInPath.Length + "<br /> ";
            foreach (string file in filesInPath)
            {
                try
                {
                    if (attachments.HasPathOf(System.IO.Path.GetFileName(file)) == false)
                    {
                        //This file is not used by any attachment
                        System.IO.FileInfo f = new System.IO.FileInfo(file);
                        TimeSpan daysDifference = System.DateTime.Now - f.CreationTime;
                        if (daysDifference.Days > 14)
                        {
                            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                            {
                                DORM.Filter filter = new DORM.Filter();
                                filter.Add(new DORM.FilterItem("Path",DORM.Enumerations.FilterCriteriaComparitor.Equals ,System.IO.Path.GetFileName(file)));
                                derotek.HairyDog.RemoteLibrary.UploadedFile upload = transaction.RetrieveFirst<derotek.HairyDog.RemoteLibrary.UploadedFile>(filter);
                                if (upload != null)
                                {
                                    transaction.Delete(upload);
                                    transaction.Commit();
                                }
                            }
                            long fLength = f.Length;
                            System.IO.File.Delete(file);
                            sizeOfFilesDeleted += fLength;
                            filesDeleted++;
                        }
                    }
                }
                catch { };
            }
            View.OrphanAttachmentsThumbsDeleted = new KeyValuePair<int, float>(filesDeleted, sizeOfFilesDeleted);

            #endregion

            #region Kill all orphaned RESIZED
            sizeOfFilesDeleted = 0;
            filesDeleted = 0;

            filesInPath = System.IO.Directory.GetFiles(pathToOrphanAttachmentsResized);
            message += "Path : " + pathToOrphanAttachmentsResized + ", ";
            message += "filesInPath : " + filesInPath.Length + "<br />";
            foreach (string file in filesInPath)
            {
                try
                {
                    if (attachments.HasPathOf(System.IO.Path.GetFileName(file)) == false)
                    {
                        //This file is not used by any attachment
                        System.IO.FileInfo f = new System.IO.FileInfo(file);
                        TimeSpan daysDifference = System.DateTime.Now - f.CreationTime;
                        if (daysDifference.Days > 14)
                        {
                            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                            {
                                DORM.Filter filter = new DORM.Filter();
                                filter.Add(new DORM.FilterItem("Path",DORM.Enumerations.FilterCriteriaComparitor.Equals, System.IO.Path.GetFileName(file)));
                                derotek.HairyDog.RemoteLibrary.UploadedFile upload = transaction.RetrieveFirst<derotek.HairyDog.RemoteLibrary.UploadedFile>(filter);
                                if (upload != null)
                                {
                                    transaction.Delete(upload);
                                    transaction.Commit();
                                }
                            }
                            long fLength = f.Length;
                            System.IO.File.Delete(file);
                            sizeOfFilesDeleted += fLength;
                            filesDeleted++;
                        }
                    }
                }
                catch { };
            }
            View.OrphanAttachmentsResizedDeleted = new KeyValuePair<int, float>(filesDeleted, sizeOfFilesDeleted);

            #endregion
            View.Message = message;
        }
    }
}
