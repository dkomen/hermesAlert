﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Shuffle the contents of a generic list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(this IList<T> list)
        {
            Random rng = new Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> GetDisplayableList(this IList<derotek.HairyDog.RegisteredItem> items)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> displayableList = new List<HairyDog.DisplayableItems.DisplayableRegisteredObject>();
            if (items != null)
            {
                foreach (derotek.HairyDog.RegisteredItem item in items)
                {
                    displayableList.Add(new HairyDog.DisplayableItems.DisplayableRegisteredObject(item));
                }
            }
            return displayableList;
        }

        public static List<derotek.HairyDog.DisplayableItems.DisplayableAttachmentObject> GetDisplayableList(this IList<derotek.HairyDog.Attachment> attachments)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayableAttachmentObject> displayableList = new List<HairyDog.DisplayableItems.DisplayableAttachmentObject>();
            foreach (derotek.HairyDog.Attachment attachment in attachments)
            {
                displayableList.Add(new HairyDog.DisplayableItems.DisplayableAttachmentObject(attachment));
            }
            return displayableList;
        }
        public static List<derotek.HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject> GetDisplayableList(this IList<derotek.HairyDog.RemoteLibrary.UploadedFile> attachments)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject> displayableList = new List<HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject>();
            foreach (derotek.HairyDog.RemoteLibrary.UploadedFile attachment in attachments)
            {
                displayableList.Add(new HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject(attachment));
            }
            return displayableList;
        }
        public static List<derotek.HairyDog.DisplayableItems.DisplayableQuickReport> GetDisplayableList(this IList<derotek.HairyDog.Incidents.Incident> incidents)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayableQuickReport> displayableList = new List<HairyDog.DisplayableItems.DisplayableQuickReport>();
            foreach (derotek.HairyDog.Incidents.Incident incident in incidents)
            {
                displayableList.Add(new HairyDog.DisplayableItems.DisplayableQuickReport(incident));
            }
            return displayableList;
        }
        public static List<derotek.HairyDog.DisplayableItems.DisplayablePersonAlertObject> GetDisplayableList(this IList<derotek.HairyDog.PersonAlert.PersonAlert> personAlerts)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayablePersonAlertObject> displayableList = new List<HairyDog.DisplayableItems.DisplayablePersonAlertObject>();
            foreach (derotek.HairyDog.PersonAlert.PersonAlert alert in personAlerts)
            {
                displayableList.Add(new HairyDog.DisplayableItems.DisplayablePersonAlertObject(alert));
            }
            return displayableList;
        }
        public static List<derotek.HairyDog.DisplayableItems.DisplayableApprovalStatusList> GetDisplayableList(this IList<derotek.HairyDog.Metrics.ApprovalStatus> list)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayableApprovalStatusList> displayableList = new List<HairyDog.DisplayableItems.DisplayableApprovalStatusList>();
            foreach (derotek.HairyDog.Metrics.ApprovalStatus status in list)
            {
                displayableList.Add(new HairyDog.DisplayableItems.DisplayableApprovalStatusList(status));
            }
            return displayableList;
        }
        public static List<derotek.HairyDog.DisplayableItems.DisplayableGenderList> GetDisplayableList(this IList<derotek.HairyDog.Metrics.Gender> list)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayableGenderList> displayableList = new List<HairyDog.DisplayableItems.DisplayableGenderList>();
            foreach (derotek.HairyDog.Metrics.Gender gender in list)
            {
                displayableList.Add(new HairyDog.DisplayableItems.DisplayableGenderList(gender));
            }
            return displayableList;
        }
        public static List<derotek.HairyDog.DisplayableItems.DisplayableGroup> GetDisplayableList(this IList<derotek.HairyDog.Groups.Group> list)
        {
            List<derotek.HairyDog.DisplayableItems.DisplayableGroup> displayableList = new List<HairyDog.DisplayableItems.DisplayableGroup>();
            foreach (derotek.HairyDog.Groups.Group group in list)
            {
                displayableList.Add(new HairyDog.DisplayableItems.DisplayableGroup(group));
            }
            return displayableList;
        }

        
        public static bool HasPathOf(this IList<derotek.HairyDog.Attachment> attachments, string path)
        {            
            foreach (derotek.HairyDog.Attachment attachment in attachments)
            {
                if (attachment.Path!=null && (attachment.Path.ToLower() == path.ToLower()))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
