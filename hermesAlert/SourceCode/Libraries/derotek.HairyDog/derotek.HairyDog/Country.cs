﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    public class Country : Entity, Interfaces.ISimpleObject
    {
        #region Constructors
        public Country() { }
        public Country(int code, string description)
        {
            Code = code;
            Description = description;
        }
        public Country(long id)
        {
            Id = id;
        }
        #endregion

        /// <summary>
        /// Country code : eg South Africa = 27
        /// </summary>
        public virtual int Code { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;
            set;
        }
        public static explicit operator Dictionary<long, string>(Country item)
        {
            Dictionary<long, string> dictionary = new Dictionary<long, string>();
            dictionary.Add(item.Id, item.Description);
            return dictionary;
        }        
    }
}
