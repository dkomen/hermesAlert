﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    public class Province : Entity, Interfaces.ISimpleObject
    {

        #region Constructors
        public Province() { }
        public Province(long id)
        {
            Id = id;
        }
        public Province(Country country, string description)
        {
            Country = country;
            Description = description;
        }
        #endregion

        public virtual Country Country { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;
            set;
        }        
    }
}
