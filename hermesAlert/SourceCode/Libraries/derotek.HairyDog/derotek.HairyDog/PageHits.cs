﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace derotek.HairyDog
{
    public class PageHits : Entity, IComparable
    {
        public virtual string ReferrerIp { get; set; }
        public virtual string Page { get; set; }
        public virtual long UserId { get; set; }
        public virtual int CompareTo(object obj)
        {
           PageHits ph=(PageHits)obj;
           return DateTime.Compare(this.DateCreated,ph.DateCreated);

        }
        public class SortByDateCreatedDescending : IComparer<PageHits>
        {
            public int Compare(PageHits hits1, PageHits hits2)
            {
                return (DateTime.Compare(hits2.DateCreated, hits1.DateCreated));
            }
        }
    }
}
