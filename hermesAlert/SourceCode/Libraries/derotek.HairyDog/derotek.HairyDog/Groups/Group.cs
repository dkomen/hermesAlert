﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Groups
{
    public class Group : Entity, derotek.HairyDog.Interfaces.IBaseItem
    {
        /// <summary>
        /// The user that owns this group
        /// </summary>
        public virtual long UserId { get; set; }

        /// <summary>
        /// A name for this group
        /// </summary>
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Name")]
        [derotek.EntityValidation.Attributes.RequiredField("Name")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Name
        {
            get;
            set;
        }

        /// <summary>
        /// A short description of this group
        /// </summary>
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;set;
        }

        /// <summary>
        /// Members of this group
        /// </summary>
        public virtual List<GroupMember> Members { get; set; }
    }
}
