﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Groups
{
    public class GroupMember : Entity
    {
        /// <summary>
        /// The group to which membership is given
        /// </summary>
        public virtual long GroupId { get; set; }
        
        /// <summary>
        /// The user to which memebership is given
        /// </summary>
        public virtual long UserId { get; set; }

        /// <summary>
        /// Can this user administratte this group
        /// </summary>
        public virtual bool IsAdministrator { get; set; }
    }
}
