﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    /// <summary>
    /// All persistable entities must inherit from this interface
    /// </summary>
    [Serializable]
    public abstract class EntityOld : derotek.EntityValidation.BaseEntityValidation, DORM.Interfaces.IEntity
    {
        #region Properties
        public virtual long Id { get; set; }
        [DORM.Attributes.FieldOrder(DORM.Enumerations.OrderingDirection.Descending)]
        public virtual DateTime DateCreated
        {
            get;
            set;
        }

        public virtual DateTime DateLastSaved
        {
            get;
            set;
        }       
        #endregion        
    }
}
