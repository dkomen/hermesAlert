﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    public class OptionState : Interfaces.IBaseItem
    {
        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;
            set;
        }
    }
}
