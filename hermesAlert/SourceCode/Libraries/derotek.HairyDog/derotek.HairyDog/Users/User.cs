﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    public class User : BaseUser, IUser, derotek.HairyDog.Interfaces.IGpsCoordinate
    {
        #region Constructors
        public User()
        {
            UploadedFiles = new List<derotek.HairyDog.RemoteLibrary.UploadedFile>();
        }
        #endregion

        #region Properties
        
        [derotek.EntityValidation.Attributes.RequiredField("eMail address")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Email, "eMail address")]
        public virtual new string EMailAddress
        { 
            get
            {
                return base.EMailAddress;
            } 
            set
            {
                base.EMailAddress = value;
                Username = base.EMailAddress;
            }
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Password, "Verify password")]
        public virtual string VerifyPassword
        {
            get;
            set;
        }

        /// <summary>
        /// How a user wishes to receive hairyDog notifications
        /// </summary>
        [derotek.EntityValidation.Attributes.RequiredField("Notifications delivery method")]
        public virtual Metrics.DeliveryMethod NotificationsDeliveryMethod { get; set; }

        /// <summary>
        /// How often a user will be sent hairyDog notifications
        /// </summary>
        [derotek.EntityValidation.Attributes.RequiredField("Receive notifications frequency")]
        public virtual Metrics.ReceiveNotificationsFrequency ReceiveNotificationsFrequency { get; set; }

        public virtual DateTime LastLoginDate
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("Province")]
        public virtual Province Province
        {
            get;
            set;
        }

        public virtual Town Town
        {
            get;
            set;
        }

        public virtual IList<derotek.HairyDog.RemoteLibrary.UploadedFile> UploadedFiles { get; set; }
        public virtual IList<derotek.HairyDog.Users.UserRole> Roles { get; set; }
        #endregion        
            
        #region Public Functions
        /// <summary>
        /// Does the user have a specified role?
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public virtual bool HasRole(derotek.HairyDog.Enumerations.Role role)
        {
            foreach (derotek.HairyDog.Users.UserRole userRole in Roles)
            {
                if (userRole.Role.ExternalReferanceId == (int)role)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region Map Default location

        /// <summary>
        /// Default longitude for map plotting
        /// </summary>
        public virtual double Longitude
        { get; set; }

        /// <summary>
        /// Default latitude for map plotting
        /// </summary>
        public virtual double Latitude
        { get; set; }
        #endregion
    }
}
