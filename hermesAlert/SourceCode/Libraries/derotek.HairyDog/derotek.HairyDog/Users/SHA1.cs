﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    /// <summary>
    /// Does SHA1 hashing
    /// </summary>
    public class SHA1
    {
        #region Public
        /// <summary>
        /// Generates a Base64 string of a hash from an array of bytes
        /// </summary>
        /// <param name="p_oBytesToHash">The byte array which to hash</param>
        /// <returns>A base64 encoded string of the hashed bytes</returns>
        public static string GenerateHash(byte[] p_oBytesToHash)
        {
            System.Security.Cryptography.SHA1CryptoServiceProvider l_oSHA1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            byte[] l_oBytesToHash = l_oSHA1.ComputeHash(p_oBytesToHash, 0, p_oBytesToHash.Length);
            return System.Convert.ToBase64String(l_oBytesToHash, 0, l_oBytesToHash.Length);
        }
        /// <summary>
        /// Generates a Base64 string of a hash from a string of characters
        /// </summary>
        /// <param name="p_sString">The string to hash</param>
        /// <returns>A base64 encoded string of the hashed bytes</returns>
        public static string GenerateHash(string p_sString)
        {
            byte[] l_oBytesToConvert = new byte[p_sString.Length];
            System.Text.ASCIIEncoding l_oAsc = new System.Text.ASCIIEncoding();
            l_oAsc.GetBytes(p_sString, 0, p_sString.Length, l_oBytesToConvert, 0);
            return GenerateHash(l_oBytesToConvert);
        }
        #endregion
    }
}
