﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    [System.CLSCompliant(true)]
    public interface IUserCls: IBaseUser
    {
        string VerifyPassword { get; set; }
    }
}
