﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    [CLSCompliant(true)]
    public class BaseUser : Entity, IBaseUser
    {

        #region Fields
        private string _username = string.Empty;
        private string _password = string.Empty;
        private string _passwordHash = string.Empty;
        private string _styleSheetTheme = "BaseTheme";
        private string _eMailAddress = string.Empty;
        #endregion

        #region Constructors
        public BaseUser()
        {
            Id = 0;
        }
        public BaseUser(string username, string password)
        {
            Id = 0;
            Username = username;
            Password = password;          
        }
        #endregion

        #region IUser

        [derotek.EntityValidation.Attributes.MaximumFieldLength(39, "User verification hash requirement")]
        public virtual string UserVerificationHashRequirement
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("User rating")]
        public virtual int UserRating
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("First name")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PersonName, "First name")]
        public virtual string Name
        {
            get;
            set;
        }
        
        [derotek.EntityValidation.Attributes.RequiredField("Surname")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PersonSurname, "Surname")]
        public virtual string Surname
        {
            get;
            set;
        }
        
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.TelephoneNo, "Contact number")]
        public virtual string ContactNumber
        {
            get;
            set;
        }
        
        [derotek.EntityValidation.Attributes.RequiredField("eMail address")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Email, "eMail address")]
        public virtual string EMailAddress
        {
            get
            {
                return _eMailAddress;
            }
            set
            {
                _eMailAddress = value.ToLower();
            }
        }
        
        [derotek.EntityValidation.Attributes.RequiredField("Username")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Username, "Username")]
        public virtual string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
                //PasswordHash = GeneratePasswordHash();
            }
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Password, "Password")]
        public virtual string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
                PasswordHash = GeneratePasswordHash();
            }
        }

        [derotek.EntityValidation.Attributes.RequiredField("Password hash")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PasswordHash, "Password hash")]
        public virtual string PasswordHash
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("Security question")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PasswordHash, "Security question")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string SecurityQuestion { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Security question answer")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PasswordHash, "Security question answer")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string SecurityQuestionAnswer { get; set; }

        private string GeneratePasswordHash()
        {
            return SHA1.GenerateHash(Username + Password);
        }

        [derotek.EntityValidation.Attributes.RequiredField("Theme")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ThemeName, "Theme")]
        public virtual string Theme
        {
            get
            {
                if (!string.IsNullOrEmpty(_styleSheetTheme))
                {
                    return _styleSheetTheme;
                }
                else
                {
                    return "BaseTheme";
                }
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _styleSheetTheme = "BaseTheme";
                }
                else
                {
                    _styleSheetTheme = value;
                }
            }
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PasswordResetId, "Password reset Id")]
        public virtual string PasswordResetId
        {
            get;set;
        }
        #endregion        
    }
}
