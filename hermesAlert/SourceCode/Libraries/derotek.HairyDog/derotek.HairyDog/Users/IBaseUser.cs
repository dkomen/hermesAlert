﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    [System.CLSCompliant(true)]
    public interface IBaseUser : DORM.Interfaces.IEntity
    {
        string UserVerificationHashRequirement { get; set; }
        int UserRating { get; set; }
        string Name { get; set; }
        string Surname { get; set; }
        string ContactNumber { get; set; }
        string EMailAddress { get; set; }
        string Username { get; set; }
        string PasswordHash { get; set; }
        string Password { set; get; }
        string SecurityQuestion { get; set; }
        string SecurityQuestionAnswer { get; set; }
    }
}
