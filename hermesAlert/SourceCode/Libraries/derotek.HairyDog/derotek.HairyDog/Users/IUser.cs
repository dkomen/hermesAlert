﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    public interface IUser : IUserCls, IUserNonCls
    {
        DateTime LastLoginDate { get; set; }
    }
}
