﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    public interface IUserNonCls
    {
        Province Province { get; set; }
        Town Town { get; set; }
    }
}
