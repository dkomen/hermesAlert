﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Users
{
    [Serializable]
    public class UserRole : Entity
    {
        #region Properties
        [derotek.EntityValidation.Attributes.RequiredField("User")]
        public virtual Users.User User { get; set; }
        [derotek.EntityValidation.Attributes.RequiredField("Role")]
        public virtual derotek.HairyDog.Metrics.Role Role { get; set; }
        #endregion
    }
}
