﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class IncidentType : BaseSimpleType
    {
        #region Constructors
        public IncidentType():base() 
        { }
        public IncidentType(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
    }
}
