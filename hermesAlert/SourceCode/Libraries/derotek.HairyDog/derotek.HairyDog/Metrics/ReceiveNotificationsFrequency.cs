﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class ReceiveNotificationsFrequency : BaseSimpleType
    {

        #region Constructors
        public ReceiveNotificationsFrequency():base() { }
        public ReceiveNotificationsFrequency(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
        
    }
}
