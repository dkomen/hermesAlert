﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class LocationAccuracy : BaseSimpleType
    {

        #region Constructors
        public LocationAccuracy():base() { }
        public LocationAccuracy(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
    }
}
