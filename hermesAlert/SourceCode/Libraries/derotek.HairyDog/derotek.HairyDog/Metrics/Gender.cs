﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class Gender : BaseSimpleType
    {

        #region Constructors
        public Gender() : base() { }
        public Gender(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
        
    }
}
