﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class BaseSimpleType : Entity, derotek.HairyDog.Interfaces.IBaseItem
    {

        #region Constructors
        public BaseSimpleType() { }
        public BaseSimpleType(string description, long externalReferanceId)
        {
            Description = description;
            ExternalReferanceId = externalReferanceId;
        }
        #endregion
        /// <summary>
        /// Used for Enums
        /// </summary>
        public virtual long ExternalReferanceId { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        [derotek.DataAccess.Attributes.FieldSortOrderAttribute(DataAccess.Attributes.OrderDirection.Ascending)]
        public virtual string Description
        {
            get;
            set;
        }

    }
}
