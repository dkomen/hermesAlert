﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class ApprovalStatus : BaseSimpleType
    {

        #region Constructors
        public ApprovalStatus():base() { }
        public ApprovalStatus(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
        
    }
}
