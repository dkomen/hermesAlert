﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class IdentificationNumberType : BaseSimpleType
    {

        #region Constructors
        public IdentificationNumberType():base() { }
        public IdentificationNumberType(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
    }
}
