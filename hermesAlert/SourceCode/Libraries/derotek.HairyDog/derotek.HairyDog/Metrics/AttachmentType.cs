﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class AttachmentType : BaseSimpleType
    {

        #region Constructors
        public AttachmentType():base() { }
        public AttachmentType(string description, long externalReferanceId):base(description, externalReferanceId)
        { }
        #endregion
        
    }
}
