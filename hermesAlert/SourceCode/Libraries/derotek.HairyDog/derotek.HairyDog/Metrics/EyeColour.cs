﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class EyeColour : BaseSimpleType
    {
        #region Constructors
        public EyeColour():base() { }
        public EyeColour(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion     
    }
}
