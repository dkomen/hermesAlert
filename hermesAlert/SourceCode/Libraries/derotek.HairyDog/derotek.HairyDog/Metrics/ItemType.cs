﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class ItemType : BaseSimpleType
    {

        #region Constructors
        public ItemType():base() { }
        public ItemType(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
        
    }
}
