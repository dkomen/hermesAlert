﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class PropertyType :BaseSimpleType
    {

        #region Constructors
        public PropertyType():base() { }
        public PropertyType(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
    }
}
