﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics 
{
    [Serializable]
    public class Role : BaseSimpleType
    {
         #region Constructors
        public Role():base() { }
        public Role(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
    }
}
