﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Metrics
{
    [Serializable]
    public class DeliveryMethod : BaseSimpleType
    {

        #region Constructors
        public DeliveryMethod():base() { }
        public DeliveryMethod(string description, long externalReferanceId)
            : base(description, externalReferanceId)
        { }
        #endregion
        
    }
}
