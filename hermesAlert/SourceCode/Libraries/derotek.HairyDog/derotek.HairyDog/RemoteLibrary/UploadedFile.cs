﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.RemoteLibrary
{
    public class UploadedFile : Entity
    {
        [derotek.EntityValidation.Attributes.RequiredField("Owner Id")]
        public virtual long UserId {get;set;}
        [derotek.EntityValidation.Attributes.RequiredField("Path")]
        public virtual string Path { get; set; }
        public virtual derotek.HairyDog.Metrics.AttachmentType AttachmentType { get; set; }
    }
}
