﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    public class Town : Entity, Interfaces.ISimpleObject
    {        
        #region Constructors
        public Town() { }
        public Town(long id)
        {
            Id = id;
        }
        public Town(Province province, string description)
        {
            Province = province;
            Description = description;
        }
        #endregion

        public virtual Province Province { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;
            set;
        }
        public static explicit operator KeyValuePair<long, string>(Town item)
        {
            return new KeyValuePair<long, string>(item.Id, item.Description);
        }
    }
}
