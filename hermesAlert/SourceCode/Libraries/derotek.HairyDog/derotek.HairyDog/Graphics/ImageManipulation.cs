﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
namespace derotek.HairyDog.Graphics
{
    public class ImageManipulation
    {
        #region Properties
        /// <summary>
        /// Given a file name create a new name for internal system usage - prevents duplicates
        /// </summary>
        /// <param name="originalFileName"></param>
        /// <returns></returns>
        public static string GetNewSystemFileName(string originalFileName)
        {
            //"~/Images/UserImages/ProfileThumbnails/" + 
            return System.IO.Path.GetFileNameWithoutExtension(originalFileName) + "-" + System.Guid.NewGuid().ToString() + System.IO.Path.GetExtension(originalFileName);
        }

        public static System.Drawing.Size MaximumThumbnailImagePixelSize
        {
            get
            {
                return new System.Drawing.Size(130, 130);
            }
        }
        public static System.Drawing.Size MaximumAlbumPicturePixelSize
        {
            get
            {
                return new System.Drawing.Size(900, 800);
            }
        }
        #endregion

        public static void ResizeImage(string sourceFile, string resizedFile, System.Drawing.Size resizeTo, bool deleteOriginalAfterwards)
        {
            if (System.IO.File.Exists(sourceFile))
            {
                System.Drawing.Bitmap bitmap = new System.Drawing.Bitmap(sourceFile);
                System.Drawing.Bitmap newbitmap = ResizeImage(bitmap, resizeTo);
                //bitmap.Dispose();
                if (System.IO.File.Exists(resizedFile))
                {
                    System.IO.File.Delete(resizedFile);
                }
                long compressionQualityPercentage = GetCompressionLevel(bitmap, resizeTo, sourceFile);
                EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compressionQualityPercentage);
                ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");
                if (jpegCodec == null)
                {
                    throw new Exception("Internal error : Could not create jpg encoder!");
                }

                EncoderParameters encoderParams = new EncoderParameters(1);
                encoderParams.Param[0] = qualityParam;

                resizedFile = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(resizedFile), System.IO.Path.GetFileNameWithoutExtension(resizedFile) + ".jpg");
                newbitmap.Save(resizedFile,jpegCodec, encoderParams);                
                newbitmap.Dispose();
                bitmap.Dispose();
                if (deleteOriginalAfterwards)
                {
                    //System.IO.File.Delete(sourceFile);
                }
            }
            else
            {
                throw new Exception("File '" + sourceFile + "' does not exist");
            }
        }

        private static long GetCompressionLevel(System.Drawing.Bitmap bitmap, System.Drawing.Size resizeTo, string sourceFile)
        {
            long compressionQualityPercentage = 70;
            if ((resizeTo.Width <= 250 && resizeTo.Height <= 250)
                || (bitmap.Width <= MaximumAlbumPicturePixelSize.Width && bitmap.Height <= MaximumAlbumPicturePixelSize.Height && ((new System.IO.FileInfo(sourceFile).Length <= 80 * 1024))))
            {
                compressionQualityPercentage = 85;
            }

            return compressionQualityPercentage;
        }
        private static ImageCodecInfo GetEncoderInfo(string mimeType)
	    {
	       ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
	 
	       for (int i = 0; i <codecs.Length; i++)
	          if (codecs[i].MimeType == mimeType)
	             return codecs[i];
	       return null;
	    }

        public static System.Drawing.Bitmap ResizeImage(System.Drawing.Bitmap bitmapToResize, System.Drawing.Size maximumImagePixelSize)
        {
            float resizeRatioWidth = ((float)maximumImagePixelSize.Width / (float)bitmapToResize.Width);
            float resizeRatioHeight = ((float)maximumImagePixelSize.Height / (float)bitmapToResize.Height);
            float resizeRatio = 0;
            if ((resizeRatioWidth < 1) || (resizeRatioHeight < 1))
            {
                if (resizeRatioWidth > resizeRatioHeight)
                {
                    resizeRatio = resizeRatioHeight;
                }
                else
                {
                    resizeRatio = resizeRatioWidth;
                }

                System.Drawing.Size newSize = new System.Drawing.Size((int)(bitmapToResize.Width * resizeRatio), (int)(bitmapToResize.Height * resizeRatio));
                System.Drawing.Bitmap resizedBitmap = new System.Drawing.Bitmap(bitmapToResize, newSize);
                return resizedBitmap;
            }
            else
            {
                return bitmapToResize;
            }
        }
        public static System.Drawing.Bitmap ResizeImage(System.Drawing.Bitmap bitmapToResize, System.Drawing.Size maximumImagePixelSize, string saveToPath)
        {
            System.Drawing.Bitmap resizedBitmap = ResizeImage(bitmapToResize, maximumImagePixelSize);

            long compressionQualityPercentage = GetCompressionLevel(bitmapToResize, maximumImagePixelSize, saveToPath);
            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, compressionQualityPercentage);
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;

            resizedBitmap.Save(saveToPath, jpegCodec, encoderParams);          

            return resizedBitmap;
        }
    }
}
