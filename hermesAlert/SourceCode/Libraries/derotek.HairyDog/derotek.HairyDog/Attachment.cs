﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    [Serializable]
    public class Attachment : Entity, derotek.HairyDog.Interfaces.IBaseItem
    {
        #region Properties
        public virtual long UserId { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;
            set;
        }
        public virtual derotek.HairyDog.Metrics.AttachmentType AttachmentType { get; set; }
        
        /// <summary>
        /// Physical path to the attachment
        /// </summary>
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Path, "Path")]
        public virtual string Path { get; set; }
        public virtual derotek.HairyDog.Metrics.IdentificationNumberType PictureShowIdNumberType {get;set;}
        #endregion        
    }
}
