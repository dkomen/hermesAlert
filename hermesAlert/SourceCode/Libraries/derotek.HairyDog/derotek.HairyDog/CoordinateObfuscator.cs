﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    public class CoordinateObfuscator
    {
        public static void ObfuscateCoordinates(derotek.HairyDog.Metrics.LocationAccuracy locationAccuracy, double lng, double lat, out double lngObfuscated, out double latObfuscated)
        {
            latObfuscated = 0;
            lngObfuscated = 0;

            if (locationAccuracy.ExternalReferanceId == (long)derotek.HairyDog.Enumerations.LocationAccuracy.Exact)
            {
                lngObfuscated = lng;
                latObfuscated = lat;
            }
            else
            {
                Random rnd = new Random(System.DateTime.Now.Millisecond);
                double seed = rnd.NextDouble();
                if (seed > 0.5)
                {
                    seed = rnd.NextDouble();
                    lngObfuscated = lng + (0.003 * seed);
                    seed = rnd.NextDouble();
                    latObfuscated = lat + (0.003 * seed);
                }
                else
                {
                    seed = rnd.NextDouble();
                    lngObfuscated = lng - (0.003 * seed);
                    seed = rnd.NextDouble();
                    latObfuscated = lat - (0.003 * seed);
                }
                
            }
        }
    }
}
