﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;

namespace derotek.HairyDog
{
    public class MonitoringLocation : Entity,Interfaces.IGpsCoordinate, Interfaces.IBaseItem
    {
        #region Fields
        private double _longitude = 0;
        private double _latitude = 0;
        #endregion

        public virtual long UserId
        {
            get;
            set;
        }

        public virtual int RadiusInMeters
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;
            set;
        }

        public virtual double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
                SqlGeographyPoint = SqlGeography.Point(_longitude, _latitude, 4326);
            }
        }

        public virtual double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }

        public virtual Microsoft.SqlServer.Types.SqlGeography SqlGeographyPoint
        {
            get;
            set;
        }
    }
}
