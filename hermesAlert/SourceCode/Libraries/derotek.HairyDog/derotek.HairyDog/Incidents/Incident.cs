﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;

namespace derotek.HairyDog.Incidents
{
    public class Incident : Entity, derotek.HairyDog.Interfaces.ILocation
    {
        #region Fields
        private double _longitude;
        private double _latitude;
        #endregion

        [derotek.EntityValidation.Attributes.RequiredField("Incident owner")]
        public virtual long UserId { get; set; }
        
        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.MediumLongDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description { get; set; }
        [derotek.DataAccess.Attributes.FieldSortOrder(DataAccess.Attributes.OrderDirection.Descending)]
        public virtual DateTime IncidentDateTime { get; set; }

        public virtual Metrics.IncidentType IncidentType { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Longitude")]
        public virtual double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }

        [derotek.EntityValidation.Attributes.RequiredField("Latitude")]
        public virtual double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }
     
        public virtual Microsoft.SqlServer.Types.SqlGeography SqlGeographyPoint
        {
            get;
            set;
        }
        
        [derotek.EntityValidation.Attributes.RequiredField("Location accuracy")]
        public virtual Metrics.LocationAccuracy LocationAccuracy
        {
            get;
            set;
        }
        
    }
}
