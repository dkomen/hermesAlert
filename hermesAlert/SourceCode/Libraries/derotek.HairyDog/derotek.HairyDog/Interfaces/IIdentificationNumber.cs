﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Interfaces
{
    public interface IIdentificationNumber
    {
        Metrics.IdentificationNumberType IdentificationNumberType { get; set; }
        string IdentificationNumber { get; set; }
    }
}
