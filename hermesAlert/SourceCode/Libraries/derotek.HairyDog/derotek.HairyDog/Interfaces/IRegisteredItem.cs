﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Interfaces
{
    public interface IRegisteredItem : Interfaces.IBaseItem, Interfaces.INotes, Interfaces.ILocation, Interfaces.IAttachmentsHost//, IIdentificationNumber
    {
        bool IsStolen { get; set; }
        bool IfIsStolenMustNotifyCommunity { get; set; }
        DateTime DateWasStolen { get; set; }
        derotek.HairyDog.Metrics.PropertyType PropertyType { get; set; }
        IList<derotek.HairyDog.ItemMonitoring.ItemDetection> ItemDetectionMessages { get; set; }
    }
}
