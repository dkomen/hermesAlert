﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Interfaces.Inflictions
{
    public interface IPersonalTheftInfliction : IAttachments
    {
        /// <summary>
        /// Serial number, registration number etc
        /// </summary>
        string IdentifyingCode { get; set; }
        INotes Description { get; set; }
        /// <summary>
        /// Set to PersonalTheft in constructor
        /// </summary>
        Enumerations.Infliction InflictionType { get; }
    }
}
