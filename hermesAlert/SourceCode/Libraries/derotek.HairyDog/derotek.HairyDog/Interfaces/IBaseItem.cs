﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Interfaces
{
    public interface IBaseItem
    {
        /// <summary>
        /// A description of the item
        /// </summary>
        String Description { get; set; }
    }
}
