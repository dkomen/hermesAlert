﻿using System;
using System.Collections.Generic;

namespace derotek.HairyDog.Interfaces
{
    public interface IAttachments
    {
        //void AddOrUpdate(Attachment attachment);
        //void Delete(long attachmentId);
        //Attachment GetAttachment(long attachmentId);
        //IList<Attachment> GetAllAttachments();
        IList<Attachment> Attachments { get; set; }
    }
}
