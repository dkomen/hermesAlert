﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Interfaces
{
    public interface IGpsCoordinate 
    {
        double Longitude { get; set; }
        double Latitude { get; set; }
    }
}
