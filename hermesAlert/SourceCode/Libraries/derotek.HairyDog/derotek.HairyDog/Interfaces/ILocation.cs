﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Interfaces
{
    public interface ILocation : IGpsCoordinate
    {
        Metrics.LocationAccuracy LocationAccuracy { get; set; }
        Microsoft.SqlServer.Types.SqlGeography SqlGeographyPoint { get; set; }
    }
}
