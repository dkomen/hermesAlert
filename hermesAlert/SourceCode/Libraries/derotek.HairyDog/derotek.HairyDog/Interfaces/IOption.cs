﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Interfaces
{
    public interface IOption: DORM.Interfaces.IEntity, INotes, IAttachmentsHost, ILocation
    {        
        #region Public Properties
        long UserID { get; set; }
        OptionState State { get; set; }
        DateTime IncidentDate { get; set; }
        bool WasLocalPolicingNotified { get; set; }
        IList<OptionInfliction> Inflictions { get; set; }
        DateTime UpdateTimeStamp { get; set; }
        decimal RewardAmountInRands { get; set; }
        string QuickDescription { get; set; }      
        #endregion        
    }
}
