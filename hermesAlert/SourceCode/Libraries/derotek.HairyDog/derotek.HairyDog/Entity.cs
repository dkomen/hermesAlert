﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    /// <summary>
    /// All persistable entities must inherit from this interface
    /// </summary>
    [Serializable]
    public abstract class Entity : derotek.EntityValidation.BaseEntityValidation, DORM.Interfaces.IEntity
    {
        #region Constructors
        public Entity()
        {
            DateCreated = System.DateTime.Now;
            DateLastSaved = System.DateTime.Now;
        }
        #endregion

        #region Properties
        [DORM.Attributes.FieldOrder(DORM.Enumerations.OrderingDirection.Descending)]
        public virtual DateTime DateCreated
        {
            get;
            set;
        }
        public virtual DateTime DateLastSaved
        {
            get;
            set;
        }       
        #endregion                
        public virtual long Id
        {
            get;
            set;
        }
    }
}
