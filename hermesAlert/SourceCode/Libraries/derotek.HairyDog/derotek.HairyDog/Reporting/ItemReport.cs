﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Reporting
{
    /// <summary>
    /// Repoting of an item
    /// </summary>
    /// <example>Perhaps when needing to report an traffic accident incident which was reported in the completely wrong city</example>
    public class ItemReport : Entity, derotek.HairyDog.Interfaces.IBaseItem
    {
        /// <summary>
        /// The type of item being reported
        /// </summary>
        public virtual derotek.HairyDog.Metrics.ItemType ItemType { get; set; }
        /// <summary>
        /// The Id of the member that reported this
        /// </summary>
        public virtual derotek.HairyDog.Users.User UserId { get; set; }
        /// <summary>
        /// The Id of the item being reported on
        /// </summary>
        public virtual long ReportedItemId { get; set; }
        /// <summary>
        /// Has a hermesAlert employee attended to this reported item?
        /// </summary>
        public virtual bool HasHeenAttendedTo { get; set; }
        /// <summary>
        /// A description of why the item is being reported
        /// </summary>
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Notes, "Notes")]
        [derotek.EntityValidation.Attributes.RequiredField("Notes")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description { get; set; }
    }
}
