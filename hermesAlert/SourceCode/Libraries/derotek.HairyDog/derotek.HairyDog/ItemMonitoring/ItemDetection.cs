﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;

namespace derotek.HairyDog.ItemMonitoring
{
    public enum MessagDirection
    {
        ToOwner=1,
        ToSecondParty=2
    }

    [Serializable]
    public class ItemDetection : Entity, Interfaces.ILocation, Interfaces.IAttachmentsHost
    {
        #region Fields
        private double _longitude = 0;
        private double _latitude = 0;
        #endregion

        #region Constructors
        public ItemDetection()
        {
            Attachments = new List<Attachment>();
        }
        #endregion
        
        /// <summary>
        /// The item that was detected
        /// </summary>
        [derotek.EntityValidation.Attributes.RequiredField("Parent item id")]
        public virtual long OwnerItemId { get; set; }
        /// <summary>
        /// Keep this message for yourself and the owner of the item that has been detected and not the public community ?
        /// </summary>
        public virtual bool KeepPrivate { get; set; }    
        [derotek.EntityValidation.Attributes.RequiredField("Notes")]
        public virtual string Notes { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Message direction")]
        public virtual MessagDirection Direction { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Second party Id")]
        public virtual long SecondPartyId { get; set; }

        public virtual bool NotificationWasSent { get; set; }

        public virtual bool HasBeenRead { get; set; }

        public virtual IList<Attachment> Attachments
        {
            get;
            set;
        }
        /// <summary>
        /// Where was this item detected
        /// </summary>
        public virtual double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }
        /// <summary>
        /// Where was this item detected
        /// </summary>
        public virtual double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }
        public virtual Metrics.LocationAccuracy LocationAccuracy
        {
            get;
            set;
        }

        public virtual Microsoft.SqlServer.Types.SqlGeography SqlGeographyPoint
        {
            get;set;
        }
    }
}
