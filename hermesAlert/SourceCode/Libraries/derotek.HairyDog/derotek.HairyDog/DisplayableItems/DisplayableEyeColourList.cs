﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableEyeColourList : Entity
    {
        #region Constructors
        public DisplayableEyeColourList(Metrics.EyeColour eyeColour)
        {
            Id = eyeColour.Id;
            Description = eyeColour.Description;            
        }
        #endregion

        #region Properties
        string Description { get; set; }
        #endregion
    }    
}
