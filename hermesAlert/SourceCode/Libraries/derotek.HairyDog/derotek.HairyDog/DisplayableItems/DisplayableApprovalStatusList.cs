﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableApprovalStatusList : Entity
    {
        #region Constructors
        public DisplayableApprovalStatusList(Metrics.ApprovalStatus approvalStatus)
        {
            Id = approvalStatus.Id;
            Description = approvalStatus.Description;            
        }
        #endregion

        #region Properties
        string Description { get; set; }
        #endregion
    }    
}
