﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableItemTypesList : Entity
    {
        #region Constructors
        public DisplayableItemTypesList(Metrics.ItemType gender)
        {
            Id = gender.Id;
            Description = gender.Description;            
        }
        #endregion

        #region Properties
        string Description { get; set; }
        #endregion
    }    
}
