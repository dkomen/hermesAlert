﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableAttachmentObject: Entity
    {
        #region Constructors
        public DisplayableAttachmentObject(Attachment attachment)
        {
            Id = attachment.Id;
            Description = attachment.Description;
            Path = attachment.Path;
            AttachmentType = attachment.AttachmentType.Description;
            IdentificationNumberType = attachment.PictureShowIdNumberType.Description;
            IsPicture = (attachment.AttachmentType.ExternalReferanceId == (long)derotek.HairyDog.Enumerations.AttachmentType.Picture);
        }
        #endregion

        #region Properties
        public string AttachmentType { get; private set; }
        public bool IsPicture { get; set; }
        public string Description { get; private set; }
        public string Path { get; private set; }
        public string IdentificationNumberType { get; set; }
        #endregion
    }    
}
