﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableGroup: Entity
    {
        #region Constructors
        public DisplayableGroup(Groups.Group group)
        {
            Id = group.Id;
            Description = group.Description;
            Name = group.Name;
        }
        #endregion

        #region Properties
        public string Name { get; private set; }
        public string Description { get; private set; }
        #endregion
    }
}
