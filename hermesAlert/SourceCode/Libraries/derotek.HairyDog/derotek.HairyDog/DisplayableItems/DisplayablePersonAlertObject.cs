﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayablePersonAlertObject : Entity
    {
        #region Constructors
        public DisplayablePersonAlertObject(PersonAlert.PersonAlert alertItem)
        {
            Id = alertItem.Id;
            FullName = alertItem.Firstname + " " + alertItem.SecondName + (alertItem.SecondName.Trim().Length>=1?" ":string.Empty) + alertItem.Surname;
            KnownAs = alertItem.KnownAs;
            PictureUrl = alertItem.PathToImage;            
            Reward = alertItem.AwardAmount;
            DateLastSeen = alertItem.DateLastSeen;
            PoliceCaseNumber = alertItem.PoliceCaseNumber;
            Longitude = alertItem.Longitude;
            Latitude = alertItem.Latitude;
            ApproximateAge = alertItem.ApproximateAge;
            EyeColour = alertItem.EyeColour;
            HairColour = alertItem.HairColour;
            DistinguishingFeatures = alertItem.DistinguishingFeatures;
            Notes = alertItem.Notes;
        }
        #endregion

        #region Properties
        public string PictureUrl { get; private set; }
        public string FullName { get; private set; }
        public string KnownAs { get; private set; }
        public string PoliceCaseNumber { get; private set; }
        public Single Reward { get; private set; }
        public DateTime DateLastSeen { get; private set; }
        double Longitude { get; set; }
        double Latitude { get; set; }
        string ApproximateAge { get; set; }
        Metrics.EyeColour EyeColour { get; set; }
        Metrics.HairColour HairColour { get; set; }
        string DistinguishingFeatures { get; set; }
        string Notes { get; set; }
        #endregion
    }
}
