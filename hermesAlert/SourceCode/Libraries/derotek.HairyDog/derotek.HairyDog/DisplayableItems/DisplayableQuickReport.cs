﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableQuickReport: Entity
    {
        #region Constructors
        public DisplayableQuickReport(Incidents.Incident incident)
        {
            Id = incident.Id;
            Description = incident.Description;
            IncidentType = incident.IncidentType.Description;
            IncidentDateTime = incident.IncidentDateTime;
        }
        #endregion

        #region Properties
        public string IncidentType { get; private set; }
        public string Description { get; private set; }
        public DateTime IncidentDateTime {get; private set;}
        #endregion
    }    
}
