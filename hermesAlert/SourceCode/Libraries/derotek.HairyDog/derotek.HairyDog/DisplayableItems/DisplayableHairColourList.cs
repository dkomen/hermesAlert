﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableHairColourList : Entity
    {
        #region Constructors
        public DisplayableHairColourList(Metrics.HairColour hairColour)
        {
            Id = hairColour.Id;
            Description = hairColour.Description;            
        }
        #endregion

        #region Properties
        string Description { get; set; }
        #endregion
    }    
}
