﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableGenderList : Entity
    {
        #region Constructors
        public DisplayableGenderList(Metrics.Gender gender)
        {
            Id = gender.Id;
            Description = gender.Description;            
        }
        #endregion

        #region Properties
        string Description { get; set; }
        #endregion
    }    
}
