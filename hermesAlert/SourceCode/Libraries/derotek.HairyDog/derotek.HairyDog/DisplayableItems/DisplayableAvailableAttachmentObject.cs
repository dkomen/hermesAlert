﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableAvailableAttachmentObject: Entity
    {
        #region Constructors
        public DisplayableAvailableAttachmentObject(RemoteLibrary.UploadedFile attachment)
        {
            Id = attachment.Id;
            Path = attachment.Path;
            AttachmentType = attachment.AttachmentType.Description;
            IsPicture = (attachment.AttachmentType.ExternalReferanceId == (long)derotek.HairyDog.Enumerations.AttachmentType.Picture);
        }
        #endregion

        #region Properties
        public string AttachmentType { get; private set; }
        public bool IsPicture { get; set; }
        public string Path { get; private set; }
        #endregion
    }    
}
