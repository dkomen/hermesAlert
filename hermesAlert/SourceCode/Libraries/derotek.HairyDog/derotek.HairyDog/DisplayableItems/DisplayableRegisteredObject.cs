﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.DisplayableItems
{
    public class DisplayableRegisteredObject: Entity
    {
        #region Constructors
        public DisplayableRegisteredObject(RegisteredItem registeredItem)
        {
            Id = registeredItem.Id; 
            Description = registeredItem.Description;
            PropertyType = registeredItem.PropertyType.Description;
            HasAttachments = registeredItem.Attachments.Count > 0 ? "Yes" : "No";
            if (registeredItem.Attachments.Count > 0)
            {
                PictureUrl = registeredItem.Attachments[0].Path;
            }
            IsStolen = registeredItem.IsStolen?"Yes":"No";
            DateWasStolen = registeredItem.DateWasStolen;
        }
        #endregion

        #region Properties
        public string PictureUrl { get; private set; }
        public string PropertyType { get; private set; }
        public string Description { get; private set; }
        public string IdentificationNumberType { get; private set; }
        public string IdentificationNumber { get; private set; }
        public string HasAttachments { get; private set; }
        public string IsStolen { get; private set; }
        public DateTime DateWasStolen { get; private set; }        
        #endregion
    }
}
