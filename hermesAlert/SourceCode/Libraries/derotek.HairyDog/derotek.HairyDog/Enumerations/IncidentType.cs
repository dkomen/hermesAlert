﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum IncidentType
    {
        Other=1,
        PhysicalAssualt=2,
        Murder=5,
        AttemptedMurder=6,
        Rape=10,
        AttemptedRape=11,
        VehicleHijack=20,
        AttemptedVehicleHijack = 21,
        TrafficAttack = 22,
        TrafficObstruction = 23,
        PublicViolence=30,
        TrafficAccident=40,
        Mugging=50,
        Robbery=60,
        PoliceActivity = 70,
        Fire=80,
        Drugs = 90,
        PoorRoadSurface=100,
        PoorRoadMarkingLighting=105,
        Construction=110,
        Roadworks=115,
        FarmAttack=130
    }
}
