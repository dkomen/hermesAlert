﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum ApprovalStatus : int
    {
        Pending=1,
        Approved=2,
        Declined=3
    }
}
