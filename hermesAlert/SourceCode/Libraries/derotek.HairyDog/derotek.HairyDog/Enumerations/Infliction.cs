﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum Infliction
    {
        AnimalCruelty=1,
        PysicalViolence=2,
        PhysicalViolenceRape=4,
        PersonalTheft=8,
        CommercialTheft=16
    }
}
