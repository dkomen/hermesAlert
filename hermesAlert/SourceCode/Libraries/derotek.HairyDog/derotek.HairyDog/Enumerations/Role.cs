﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    /// <summary>
    /// The role a a member may have
    /// </summary>
    public enum Role
    {
        User=1,
        Administrator=100
    }
}
