﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum IdentificationNumberType: long
    {
        Unknown=1,
        SerialNumber=2,
        ModelNumber=3,
        RegistrationNumber=4,
        PersonsIdentityNumber=5,
        CellPhoneImeiNumber=6,
        EngineNumber=7
    }
}
