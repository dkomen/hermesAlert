﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum ItemType
    {
        RegisteredItem=1,
        MissingPerson=2,
        WantedPerson=3,
        MissingPet=4,
        Incident=5
    }
}
