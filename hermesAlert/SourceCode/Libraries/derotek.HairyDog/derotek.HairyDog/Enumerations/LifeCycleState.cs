﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum LifeCycleState
    {
        Open=1,
        Closed=2,
        Cancelled=3,
        Resolved=4
    }
}
