﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum PropertyType
    {
        Other=1,
        PassengerVehicleSuv=10,
        Sport=11,
        MPV=12,
        HomeRobbery=13,
        Truck=14,
        HeavyIndustrialVehicle=15,
        PerformanceVehicle=20,
        RacingCar = 21,
        OffroadVehicle=22,
        CellPhone = 30,
        Motorcycle = 31,
        Bicycle=40,
        ElectronicEquipment=50,
        ClothingAndLinen=60,
        GardeningEquipment=70,
        Aviation = 80,
        BuildingMaterials = 90,
        Art = 100,
        PhotographicEquipment = 110,
        FirearmsExplosives = 120
    }
}
