﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    public enum AttachmentType
    {
        None=1,
        Audio=2,
        Video=3,
        Picture=4,
        Document=5
    }
}
