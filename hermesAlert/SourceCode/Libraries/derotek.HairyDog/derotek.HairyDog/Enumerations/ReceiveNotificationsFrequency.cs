﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog.Enumerations
{
    /// <summary>
    /// How often to receive notifications
    /// </summary>
    public enum ReceiveNotificationsFrequency
    {
        Immediate=1,
        Never=10
    }
}
