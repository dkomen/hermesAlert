﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;

namespace derotek.HairyDog
{
    [Serializable]
    public class RegisteredItem : Entity, Interfaces.IRegisteredItem
    {
        #region Fields
        private double _longitude = 0;
        private double _latitude = 0;
        #endregion

        #region Constructors
        public RegisteredItem()
        {
            Attachments = new List<Attachment>();             
        }
        #endregion

        public virtual long UserId { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Description")]
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ShortDescription, "Description")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Description
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Notes, "Notes")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Notes
        {
            get;
            set;
        }

        public virtual bool IsStolen
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PoliceCaseNumber, "Police case number")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string PoliceCaseNumber { get; set; }

        public virtual bool IfIsStolenMustNotifyCommunity { get; set; }

        [derotek.DataAccess.Attributes.FieldSortOrder(DataAccess.Attributes.OrderDirection.Ascending)]
        public virtual DateTime DateWasStolen { get; set; }

        [derotek.EntityValidation.Attributes.RequiredField("Property type")]
        public virtual derotek.HairyDog.Metrics.PropertyType PropertyType
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("Location accuracy")]
        public virtual Metrics.LocationAccuracy LocationAccuracy
        {
            get;
            set;
        }

        public virtual IList<Attachment> Attachments
        {
            get;
            set;
        }

        public virtual IList<derotek.HairyDog.ItemMonitoring.ItemDetection> ItemDetectionMessages
        {
            get;
            set;
        }

        public virtual double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }

        public virtual double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }

        public virtual Microsoft.SqlServer.Types.SqlGeography SqlGeographyPoint
        {
            get;
            set;
        }
    }
}
