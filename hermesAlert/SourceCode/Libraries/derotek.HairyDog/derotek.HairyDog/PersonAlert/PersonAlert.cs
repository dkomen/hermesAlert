﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Types;

namespace derotek.HairyDog.PersonAlert
{
    public class PersonAlert : Entity, HairyDog.Interfaces.ILocation
    {
        #region Fields
        private double _longitude = 0;
        private double _latitude = 0;
        #endregion

        #region Contact person
        [derotek.EntityValidation.Attributes.RequiredField("Contact community member")]
        public virtual long UserId
        {
            get;
            set;
        }
        #endregion

        #region Who is missing
        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PersonName, "First name")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Firstname
        {
            get;set;
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PersonName, "Second name")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string SecondName
        {
            get;set;
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PersonSurname, "Surname")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Surname
        {
            get;set;
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PersonName, "Known as")]
        [derotek.EntityValidation.Attributes.RequiredField("Known as")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string KnownAs
        {
            get;set;
        }

        public virtual string ApproximateAge { get; set; }
        public virtual Metrics.EyeColour EyeColour { get; set; }
        public virtual Metrics.HairColour HairColour { get; set; }

        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string DistinguishingFeatures { get; set; }
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string Notes { get; set; }
        public virtual Metrics.Gender Gender { get; set; }
        #endregion

        #region Date last seen
        [derotek.DataAccess.Attributes.FieldSortOrder(DataAccess.Attributes.OrderDirection.Descending)]
        [derotek.EntityValidation.Attributes.RequiredField("Date last seen")]
        public virtual System.DateTime DateLastSeen
        {
            get;
            set;
        }
        #endregion

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.PoliceCaseNumber, "Police case number")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string PoliceCaseNumber
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.ContactDetailsMulti, "Contact details")]
        [derotek.EntityValidation.Attributes.NoSwearing(false)]
        public virtual string ContactDetails {get;set;}

        [derotek.EntityValidation.Attributes.MaximumFieldLength(derotek.Configuration.FieldLengths.Path, "Photo of person")]
        [derotek.EntityValidation.Attributes.RequiredField("Photo of person")]
        public virtual string PathToImage
        {
            get;
            set;
        }
        public virtual bool Solved { get; set; }
        public virtual Metrics.ApprovalStatus ApprovalStatus
        {
            get;
            set;
        }
        public virtual Single AwardAmount
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("Location accuracy")]
        public virtual Metrics.LocationAccuracy LocationAccuracy
        {
            get;
            set;
        }
        public virtual IList<derotek.HairyDog.ItemMonitoring.ItemDetection> ItemDetectionMessages
        {
            get;
            set;
        }
        public virtual double Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                _longitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }
        public virtual double Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                _latitude = value;
                SqlGeographyPoint = SqlGeography.Point(_latitude, _longitude, 4326);
            }
        }
        public virtual Microsoft.SqlServer.Types.SqlGeography SqlGeographyPoint
        {
            get;
            set;
        }

        [derotek.EntityValidation.Attributes.RequiredField("Item type")]
        public virtual Metrics.ItemType ItemType
        {
            get;
            set;
        }

    }
}
