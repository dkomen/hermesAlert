﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.HairyDog
{
    public class QueryString
    {
        #region Fields
        public static string ActiveObjectId = "ActiveObjectId";
        public static string ChildObjectId = "ChildObjectId";
        public static string ReturnToPage = "ReturnToPage"; 
        #endregion

        public Dictionary<string, string> Variables = new Dictionary<string, string>();

        public string EncodeQueryString()
        {
            string variablesString = string.Empty;

            foreach (KeyValuePair<string, string> variable in Variables)
            {
                if (variablesString.Length != 0)                
                {
                    variablesString += "&";
                }
                
                variablesString += variable.Key + "=" + variable.Value.ToString();
            }

            string encodedString = string.Empty;
            if (!string.IsNullOrEmpty(variablesString))
            {
                encodedString = System.Convert.ToBase64String(System.Text.Encoding.ASCII.GetBytes(variablesString));
            }
            if (!string.IsNullOrEmpty(encodedString))
            {
                encodedString = "?ref=" + encodedString;
            }
            
            return encodedString;
        }

        public void DecodeQueryString(string queryStringRefVariable)
        {
            Variables = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(queryStringRefVariable))
            {
                string decodedString = System.Text.Encoding.ASCII.GetString(System.Convert.FromBase64String(queryStringRefVariable));
                string[] variables = decodedString.Split('&');

                foreach (string variable in variables)
                {
                    string[] variableComponents = variable.Split("=".ToCharArray(),2);
                    Variables.Add(variableComponents[0], variableComponents[1]);
                }

            }
        }
    }
}
