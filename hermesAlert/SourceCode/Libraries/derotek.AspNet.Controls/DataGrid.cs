﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace derotek.AspNet.Controls
{
    /// <summary>
    /// Summary description for Grid
    /// </summary>
    [ToolboxData(@"<{0}:DataGrid runat=""server"" \>")]
    public class DataGrid : GridView
    {
        #region Constructors
        public DataGrid()
        {
            this.RowStyle.CssClass = "RowStyle";
            this.EmptyDataRowStyle.CssClass = "EmptyRowStyle";
            this.PagerStyle.CssClass = "PagerStyle";
            this.SelectedRowStyle.CssClass = "SelectedRowStyle";
            this.HeaderStyle.CssClass = "HeaderStyle";
            this.EditRowStyle.CssClass = "EditRowStyle";
            this.AlternatingRowStyle.CssClass = "AltRowStyle";
            this.GridLines = System.Web.UI.WebControls.GridLines.None;
            this.AllowPaging = true;
            this.AllowSorting = false;
            this.CssClass = "GridViewStyle";            

        }
        #endregion

        #region Overrides
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            string script = "<script type='text/javascript'> " +
            "    var originalRowClassName;" +
            "    function AmHovering(row, state) {" +
            "        if (state == true) {" +
            "            originalRowClassName = row.className;" +
            "            row.className = 'SelectedRowStyle';" +
            "        }" +
            "        else {" +
            "            row.className = originalRowClassName;" +
            "        }" +
            "    }" +
            "</script>";

            Page.ClientScript.RegisterStartupScript(this.GetType(), "HoveringScript", script);
        }
        protected override void OnRowCreated(GridViewRowEventArgs e)
        {
            base.OnRowCreated(e);
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onclick", "javascript:__doPostBack('" + this.UniqueID + "','Edit$" + e.Row.RowIndex + "');return false;");
                e.Row.Attributes.Add("onmouseover", "AmHovering(this, true);");
                e.Row.Attributes.Add("onmouseout", "AmHovering(this, false);");
            }
        }


        bool _ignoreEvent = false;
        protected override void OnDataBound(EventArgs e)
        {
            //if (_ignoreEvent == false)
            //{
            //    base.OnDataBound(e);
            //    if (this.Rows.Count == 0)
            //    {
            //        this.Columns.Clear();
            //        this.DataKeyNames = null;
            //        System.Data.DataTable tempTable = new System.Data.DataTable();
            //        tempTable.Columns.Add("Results");
            //        tempTable.Rows.Add(new object[] { "No data was found!" });
            //        this.DataSource = tempTable;
            //        this.AutoGenerateColumns = true;
            //        _ignoreEvent = true;
            //        this.DataBind();
            //        _ignoreEvent = false;
            //    }
            //}
        }
        #endregion

        #region Private Functions

        #endregion
    }
}