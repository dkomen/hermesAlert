﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.EntityValidation
{
    [Serializable]
    public abstract class BaseEntityValidation : IEntityValidation
    {
        #region Fields
        private bool _isValid = false;
        private List<string> _validationErrors = new List<string>();
        #endregion

        #region Properties
        /// <summary>
        /// Has all business rules been passed for this entity
        /// </summary>
        public virtual bool IsValid
        {
            get
            {
                TestForValidationErrors(true);
                return ValidationErrors.Count == 0;
            }
            set
            {
                _isValid = value;
            }
        }
        /// <summary>
        /// A list containing all (if any) validation errors
        /// </summary>
        public virtual List<string> ValidationErrors
        {
            get { return _validationErrors; }
            set { _validationErrors = value; }
        }
        #endregion

        #region Public Functions
        public virtual void TestForValidationErrors(bool throwExceptionOnFailed)
        {
            TestForValidationErrors(this, throwExceptionOnFailed);
        }
        #endregion

        public virtual void TestForValidationErrors(object objectToCheck, bool throwExceptionOnFailed)
        {
            _isValid = true;
            _validationErrors = new List<string>();

            System.Reflection.PropertyInfo[] pi = objectToCheck.GetType().GetProperties();
            foreach (System.Reflection.PropertyInfo propertyInfo in pi)
            {
                _isValid = true;
                //Required field
                _isValid = Check_RequiredFieldAttribute(propertyInfo, objectToCheck, ref _validationErrors);
                //Maximum field length
                if (_isValid) _isValid = Check_MaximumFieldLengthAttribute(propertyInfo, objectToCheck, ref _validationErrors);
                //Swearing etc
                if (_isValid) _isValid = Check_NoSwearingAttribute(propertyInfo, objectToCheck, ref _validationErrors);
            }
            _isValid = _validationErrors.Count == 0;
            if (throwExceptionOnFailed && !_isValid)
            {
                throw new ValidationErrorException(_validationErrors);
            }
        }

        #region Private Functions
        private bool Check_RequiredFieldAttribute(System.Reflection.PropertyInfo propertyInfo, object objectToCheck, ref List<string> validationErrors)
        {
            bool isValid = true;
            object[] requiredFields = propertyInfo.GetCustomAttributes(typeof(Attributes.RequiredFieldAttribute), true);
            if (requiredFields.Length > 0)
            {
                object value = propertyInfo.GetValue(objectToCheck, null);
                if (value is System.ValueType)
                {
                    if (Convert.ChangeType(value, propertyInfo.PropertyType).Equals(System.Activator.CreateInstance(propertyInfo.PropertyType)))
                    {
                        isValid = false;
                    }
                }
                else if (value is System.String)
                {
                    isValid = !string.IsNullOrEmpty((string)value);
                }
                else
                {
                    isValid = value != null;
                }
                if (!isValid)
                {
                    validationErrors.Add(((Attributes.RequiredFieldAttribute)requiredFields[0]).ErrorMessage);
                }
            }
            return isValid;
        }
        private bool Check_MaximumFieldLengthAttribute(System.Reflection.PropertyInfo propertyInfo, object objectToCheck, ref List<string> validationErrors)
        {
            bool isValid = true;
            int maximumFieldLength;
            if (derotek.EntityValidation.Attributes.MaximumFieldLength.HasMaximumFieldLengthSet(propertyInfo, out maximumFieldLength))
            {
                object value = propertyInfo.GetValue(objectToCheck, null);
                if (value == null)
                {
                    isValid = true;
                }
                else if (value is System.String)
                {
                    isValid = ((string)value).Length <= maximumFieldLength;
                }
                if (!isValid)
                {
                    validationErrors.Add(derotek.EntityValidation.Attributes.MaximumFieldLength.GetErrorMessage(propertyInfo));
                }
            }
            else
            {
                isValid = true;
            }            
            return isValid;
        }
        private bool Check_NoSwearingAttribute(System.Reflection.PropertyInfo propertyInfo, object objectToCheck, ref List<string> validationErrors)
        {
            bool isValid = true;
            object[] noSwearWords = propertyInfo.GetCustomAttributes(typeof(Attributes.NoSwearingAttribute), true);
            if (noSwearWords.Length > 0)
            {
                object value = propertyInfo.GetValue(objectToCheck, null);
                if (value is System.String)
                {
                    isValid = !HasSwearWords((string)value);
                }                
                if (!isValid)
                {
                    validationErrors.Add(((Attributes.NoSwearingAttribute)noSwearWords[0]).ErrorMessage);
                }
            }
            return isValid;
        }
        private bool HasSwearWords(string stringToCheck)
        {
            List<string> illegalWords = new List<string>() {"cunt", "doos", "dutchman", "fuck", "kafer", "kaffer", "kak", "kont", "masturba", "masturbe", "nigger", "platneus", "plat neus","poes", "pussy", "rooinek", "rooi nek", "shit", "soutie", "soutpiel", "sout piel", "vagina", "vok", "vokker"};            

            bool swearWordFound = false;

            foreach (string searchWord in illegalWords)
            {
                if (stringToCheck.ToLower().Contains(searchWord))
                {
                    return true;
                }
            }
            
            return swearWordFound;
        }
        #endregion
    }
}
