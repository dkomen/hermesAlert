﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.EntityValidation
{
    [Serializable]
    public class ValidationErrorException: derotek.Exceptions.BaseException
    {
        public ValidationErrorException(List<string> validationErrors)
            : base(ValidationErrorException.CreateSingleMessage(validationErrors))
        {
        }
        public static string CreateSingleMessage(List<string> messages)
        {
            System.Text.StringBuilder messagesConcatenated = new StringBuilder();
            foreach (string message in messages)
            {
                messagesConcatenated.AppendLine(message);
            }
            return messagesConcatenated.ToString();
        }
    }
}
