﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.EntityValidation.Attributes
{
    /// <summary>
    /// A property is required, there must be valid data in it
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class MaximumFieldLength : System.Attribute
    {
        public MaximumFieldLength(int maximumFieldLength, string fieldName)
        {
            FieldLength = maximumFieldLength;
            FieldName = fieldName;
        }
        public int FieldLength { get; set; }
        public string FieldName { get; set; }
        public string ErrorMessage
        {
            get
            {
                return "'" + FieldName + "' may not be longer than " + FieldLength + " characters";
            }
        }
        public static bool HasMaximumFieldLengthSet(System.Reflection.PropertyInfo propertyInfo, out int fieldMaximumLength)
        {

            fieldMaximumLength = 0;

            object[] maximumFieldLengths = propertyInfo.GetCustomAttributes(typeof(Attributes.MaximumFieldLength), true);

            if (maximumFieldLengths.Length > 0)
            {
                //Attributes.MaximumFieldLength attr = (Attributes.MaximumFieldLength)maximumFieldLengths[0];
                fieldMaximumLength = GetMaximumFieldLength(propertyInfo);
                return true;
            }
            else
            {
                return false;
            }
        }
        public static int GetMaximumFieldLength(System.Reflection.PropertyInfo propertyInfo)
        {

            object[] maximumFieldLengths = propertyInfo.GetCustomAttributes(typeof(Attributes.MaximumFieldLength), true);

            if (maximumFieldLengths.Length > 0)
            {
                Attributes.MaximumFieldLength attr = (Attributes.MaximumFieldLength)maximumFieldLengths[0];
                return attr.FieldLength;
            }
            else
            {
                throw new derotek.Exceptions.StandardException("'" + propertyInfo.Name + "' does not have the MaximumFieldLengthAttribute attribute on it");
            }
        }
        public static string GetErrorMessage(System.Reflection.PropertyInfo propertyInfo)
        {

            object[] maximumFieldLengths = propertyInfo.GetCustomAttributes(typeof(Attributes.MaximumFieldLength), true);

            if (maximumFieldLengths.Length > 0)
            {
                Attributes.MaximumFieldLength attr = (Attributes.MaximumFieldLength)maximumFieldLengths[0];
                return attr.ErrorMessage;
            }
            else
            {
                throw new derotek.Exceptions.StandardException("'" + propertyInfo.Name + "' does not have the MaximumFieldLengthAttribute attribute on it");
            }
        }
    }
}
