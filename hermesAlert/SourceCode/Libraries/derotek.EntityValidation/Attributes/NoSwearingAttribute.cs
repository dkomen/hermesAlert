﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.EntityValidation.Attributes
{
    /// <summary>
    /// A property is required, there must be valid data in it
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class NoSwearingAttribute : System.Attribute
    {
        public NoSwearingAttribute(bool swearingAllowed)
        {
            SwearingAllowed = swearingAllowed;
        }
        public bool SwearingAllowed { get; set; }
        public string ErrorMessage
        {
            get
            {
                return "Possible rude or derogatory found";
            }
        }
    }
}
