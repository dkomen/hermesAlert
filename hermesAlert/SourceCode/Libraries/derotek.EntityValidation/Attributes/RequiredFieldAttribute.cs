﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.EntityValidation.Attributes
{
    /// <summary>
    /// A property is required, there must be valid data in it
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property)]
    public class RequiredFieldAttribute : System.Attribute
    {
        public RequiredFieldAttribute(string friendlyName)
        {
            FriendlyName = friendlyName;
        }
        public string FriendlyName { get; set; }
        public string ErrorMessage
        {
            get
            {
                return "A valid '" + FriendlyName + "' is required";
            }
        }
    }
}
