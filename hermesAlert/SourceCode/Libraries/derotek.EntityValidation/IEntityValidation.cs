﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace derotek.EntityValidation
{
    public interface IEntityValidation
    {
        /// <summary>
        /// Is this a valid object with all validations checked successfully
        /// </summary>
        bool IsValid { get; set; }
        /// <summary>
        /// A list of any validation checks that this item has failed
        /// </summary>
        List<string> ValidationErrors { get; set; }
        /// <summary>
        /// Check for validation errors on an object
        /// </summary>
        /// <param name="objectToCheck">Object to check validation for</param>
        /// <param name="throwExceptionOnFailed">Should an exception with validation erros be thown?</param>
        void TestForValidationErrors(object objectToCheck, bool throwExceptionOnFailed);
        /// <summary>
        /// Check for errors on the current object
        /// </summary>
        /// <param name="throwExceptionOnFailed">Should an exception with validation erros be thown?</param>
        void TestForValidationErrors(bool throwExceptionOnFailed);
    }
}
