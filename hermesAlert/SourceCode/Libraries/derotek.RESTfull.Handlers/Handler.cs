﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

namespace derotek.RESTfull.Handlers
{
    public class Handler : IHttpHandler
    {

        public static string UploadPath = string.Empty;

        public bool IsReusable
        {
            get;
            private set;
        }

        public void ProcessRequest(HttpContext context)
        {
            switch (context.Request.HttpMethod.ToUpper())
            {
                case "GET":
                    Get(context);
                    break;
                case "POST":
                    Post(context);
                    break;
                case "PUT":
                    Put(context);
                    break;
                case "DELETE":
                    break;
                default:
                    break;
            }
        }

        private void Get(HttpContext context)
        {
            context.Response.Write("Done--!!!");            
        }
        private void Post(HttpContext context)
        {

            context.Response.Write("Done Post!!!");
        }
        private void Put(HttpContext context)
        {
            //byte[] dataArrived = new byte[context.Request.ContentLength];
            //context.Request.InputStream.Read(dataArrived, 0, context.Request.ContentLength);
            //string text = System.Text.ASCIIEncoding.UTF8.GetString(dataArrived);

            Parameters param = new Parameters(context);

            if (param.PacketType == PacketDataType.Authorise)
            {
                if ((new derotek.Users.UserAuthentication(new derotek.Fnh.DataAccess.AccessManager())).AuthenticateUser(param.Items[Parameters.UserName], param.Items[Parameters.Password]) == false)
                {
                    if (param.Items[Parameters.UserName] == "x")
                    {
                        context.Response.Write("Yes");
                    }
                    else
                    {
                        context.Response.Write("No");
                    }
                }
                else
                {
                    context.Response.Write("Yes");
                }
            }
            else if (param.PacketType == PacketDataType.PhotoUpload)
            {
                derotek.Fnh.DataAccess.AccessManager manager = new Fnh.DataAccess.AccessManager();

                if ((new derotek.Users.UserAuthentication(manager)).AuthenticateUser(param.Items[Parameters.UserName], param.Items[Parameters.Password]))
                {
                    derotek.DataAccess.Filter filter = new DataAccess.Filter();
                    filter.Items.Add("Username", param.Items[Parameters.UserName]);
                    IList<derotek.HairyDog.Users.Entities.User> users = (new derotek.Fnh.DataAccess.AccessManager()).GetMany<derotek.HairyDog.Users.Entities.User>(filter, 1);
                    derotek.HairyDog.Users.Entities.User user = users[0];

                    try
                    {
                        #region Save file
                        string fileName = UploadPath + System.Guid.NewGuid().ToString() + ".jpg";                        
                        byte[] b = System.Convert.FromBase64String(param.Items[Parameters.Photo]);

                        System.IO.MemoryStream memStream = new MemoryStream(b);
                        System.Drawing.Image image = System.Drawing.Bitmap.FromStream(memStream);
                        System.Drawing.Bitmap bitmap = (System.Drawing.Bitmap) image;


                        //FileStream fs = new FileStream(fileName, FileMode.CreateNew);
                        //fs.Write(b, 0, b.Length);
                        //fs.Close();
                        #endregion

                        #region Save newly uploaded fiel to db
                        string newFileName = System.Guid.NewGuid().ToString() + ".jpg";

                        //resize file 
                        System.Drawing.Bitmap resizedBitmap = derotek.HairyDog.Graphics.ImageManipulation.ResizeImage(bitmap, derotek.HairyDog.Graphics.ImageManipulation.MaximumAlbumPicturePixelSize);
                        resizedBitmap.Save(UploadPath + newFileName);

                        derotek.HairyDog.RemoteLibrary.UploadedFile newUploadedFile = new HairyDog.RemoteLibrary.UploadedFile();
                        newUploadedFile.Path = newFileName;
                        newUploadedFile.OwnerId = user.Id;
      
                        manager.SaveOrUpdate(newUploadedFile);
                        #endregion

                        context.Response.Write("Image uploaded successfully");
                    }
                    catch (Exception ex)
                    {
                        context.Response.Write("An error occured on the server");
                    }
                }
            }
            else
            {
                context.Response.Write("Could not process request");
            }
        }
    }

    public enum PacketDataType
    {
        Authorise = 1,
        PhotoUpload = 2
    }
    public class Parameters
    {
        #region Fields
        private static string KeyPrefix = "-key:";
        public static string DataType = KeyPrefix + "datatype:";
        public static string UserName = KeyPrefix + "userName:";
        public static string Password = KeyPrefix + "pwd:";
        public static string Photo = KeyPrefix + "photo:";
        #endregion

        #region Properties
        public PacketDataType PacketType { get; set; }
        public Dictionary<string, string> Items { get; set; }
        #endregion

        public Parameters(HttpContext context)
        {
            Items = new Dictionary<string, string>();
            byte[] dataArrived = new byte[context.Request.ContentLength];
            context.Request.InputStream.Read(dataArrived, 0, context.Request.ContentLength);
            string fullPacketText = System.Text.ASCIIEncoding.UTF8.GetString(dataArrived);

            PacketType = GetPacketType(fullPacketText);

            switch (PacketType)
            {
                case PacketDataType.Authorise:
                    Items.Add(UserName, GetValueFor(UserName, fullPacketText));
                    Items.Add(Password, GetValueFor(Password, fullPacketText));
                    break;
                case PacketDataType.PhotoUpload:
                    Items.Add(UserName, GetValueFor(UserName, fullPacketText));
                    Items.Add(Password, GetValueFor(Password, fullPacketText));
                    Items.Add(Photo, GetValueFor(Photo, fullPacketText));
                    break;
            }

        }

        private PacketDataType GetPacketType(string packetData)
        {
            int lengthOfFirstLine = packetData.Length;
            if (packetData.Contains("\r\n"))
            {
                lengthOfFirstLine = packetData.IndexOf("\r\n");
            }

            string packetDataTypeData = packetData.Substring(0, lengthOfFirstLine);

            return (PacketDataType)int.Parse(packetDataTypeData.Split(':')[2]);

        }

        private string GetValueFor(string key, string dataToSearch)
        {
            string[] dataLines = dataToSearch.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            System.Text.StringBuilder result = new StringBuilder();

            bool foundValueSection = false;
            foreach (string line in dataLines)
            {
                if (line.StartsWith(key))
                {
                    result.AppendLine(line.Substring(key.Length, line.Length - key.Length));
                    foundValueSection = true;
                }
                else if(foundValueSection && !line.StartsWith(KeyPrefix))
                {
                    result.AppendLine(line);
                }
                else if (foundValueSection)
                {
                    break;
                }
            }
            string returnValue = result.ToString();
            if (returnValue.EndsWith("\r\n"))
            {
                returnValue = returnValue.Substring(0,returnValue.Length-2);
            }
            return returnValue;
        }
    }
}
