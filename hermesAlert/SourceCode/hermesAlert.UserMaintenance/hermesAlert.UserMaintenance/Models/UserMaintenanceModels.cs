﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using derotek.Fnh.DataAccess;
using derotek.HairyDog;

namespace hermesAlert.UserMaintenance.Models
{
    public class UserMaintenanceModels
    {
        public List<derotek.HairyDog.Users.Entities.User> Users
        {
            get
            {
                return new derotek.Fnh.DataAccess.AccessManager().GetAll<derotek.HairyDog.Users.Entities.User>();
            }
        }
    }
}