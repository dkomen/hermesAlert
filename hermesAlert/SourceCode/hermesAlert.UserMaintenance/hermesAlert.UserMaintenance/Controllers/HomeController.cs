﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace hermesAlert.UserMaintenance.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "PayM8 Message GateWay";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Users()
        {


            ViewData["users"] = new UserMaintenance.Models.UserMaintenanceModels().Users;
            return View();

        }
    }
}
