package derotek.HairyDogUploader;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HairyDogUploaderActivity extends Activity {
    
	/////Fields
	private ProgressDialog _dialog;
	private final Handler _handler = new Handler();
	private String _message;
	/////End Fields
	
	
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        Button signIn = (Button)findViewById(R.id.SignIn);
        signIn.setOnClickListener(new OnClickListener() {
			
        	final Runnable _showMessage = new Runnable() {
        	    public void run() {
        	        Toast.makeText(getApplicationContext(), _message, Toast.LENGTH_SHORT).show();
        	    }
        	};
			@Override
			public void onClick(View v) {
				
				EditText userName = (EditText)findViewById(R.id.userName);
				EditText password = (EditText)findViewById(R.id.password);
				
				Communicator.UserName = userName.getText().toString();
				Communicator.Password = password.getText().toString();
				_dialog = ProgressDialog.show(HairyDogUploaderActivity.this, "Signing in", "Please wait...", true);
				
				new Thread(new Runnable() {
			        public void run() {
			        	String result = Communicator.AuthenticateUser();
			    		_dialog.dismiss();
			    		
			    		if(result.length()==3)
			    		{			    			
			    			TakePhotoAndSendActivity.AppContext = getApplicationContext();
			    			Intent i = new Intent(getApplicationContext(), TakePhotoAndSendActivity.class);
			    			startActivity(i);
			    		}
			    		else
			    		{
			    			_message = result;
			    			_handler.post(_showMessage);
			    		}
			        }

			}).start(); 
				
			}
		});
    }
    
    @Override
    public void onBackPressed() {
    	finish();
    	return;
    }


}