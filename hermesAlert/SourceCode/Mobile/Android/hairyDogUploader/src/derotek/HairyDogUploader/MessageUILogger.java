package derotek.HairyDogUploader;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class MessageUILogger {

	///Show messages - types are:
    // T : Trace - message will always be sent to DDMS
    // E : Error - message will always be sent to DDMS
    // UI : Show message on UI  
	// D : Send to DDMS
    public static void IndicateMessage(String message, int uiDisplayLength, String messageType, Context context, String Tag)
    {
    	Boolean allowTraceMessages = false;
    	
    	if(messageType=="T" && !allowTraceMessages)
    	{
    		return;
    	}
    	
    	if(messageType=="T")
    	{
    		message = "Trace : " + message;
    	}
    	if(messageType=="E")
    	{
    		message = "Error : " + message;
    	}
    	
    	if(messageType=="T" || messageType=="E" || messageType=="D")
    	{
    		Log.d(Tag,message);
    	}
    	
    	if(messageType!="D")
    	{
    		Toast.makeText(context, message, uiDisplayLength).show();
    	}
    	
    }
}
