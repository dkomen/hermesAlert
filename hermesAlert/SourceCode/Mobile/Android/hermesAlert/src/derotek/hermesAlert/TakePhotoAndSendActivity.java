package derotek.hermesAlert;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.nio.ByteBuffer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

import android.R.bool;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TakePhotoAndSendActivity extends Activity {
	// ///Fields
	private ProgressDialog _dialog;
	private final Handler _handler = new Handler();
	private String _message;
	private String _result;
	private Context _context;
	private static final int CAMERA_PIC_REQUEST = 1337;
	private static final int TAKE_PHOTO_CODE = 1;
	private Intent _cameraIntent;
	private Bitmap _bitmapToSend;
	private Bitmap _bitmapThumbnail;
	private File _photoTempFile;
	public static Context AppContext;
	private static String TAG = "TakePhotoAndSendActivity";

	// ///End Fields

	@Override
	public void onSaveInstanceState(Bundle outState) {
		MessageUILogger.IndicateMessage("Here", Toast.LENGTH_LONG, "T",
				getApplicationContext(), TAG);
		if (_bitmapThumbnail != null) {
			MessageUILogger.IndicateMessage("Here", Toast.LENGTH_LONG, "T",
					getApplicationContext(), TAG);
			ByteBuffer dst = ByteBuffer.allocate(_bitmapThumbnail.getWidth()
					* _bitmapThumbnail.getHeight());
			_bitmapThumbnail.copyPixelsToBuffer(dst);
			outState.putByteArray("ThumbnailImage", dst.array());
		}
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.takephotoandsend);

		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

		final Runnable _showMessage = new Runnable() {
			public void run() {
				Toast.makeText(getApplicationContext(), _message,
						Toast.LENGTH_SHORT).show();
			}
		};

		_cameraIntent = new Intent(
				android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		_context = getApplicationContext();
		// ///Create temp file
		try {
			_photoTempFile = GetTempFile(AppContext);
		} catch (Exception ex) {
			MessageUILogger.IndicateMessage("Could not create temp file :  "
					+ ex.getMessage(), Toast.LENGTH_LONG, "E",
					getApplicationContext(), TAG);
			return;
		}
		// ///END Create temp file

		MessageUILogger.IndicateMessage(
				"Path of photo temp file : " + Uri.fromFile(_photoTempFile),
				Toast.LENGTH_LONG, "T", getApplicationContext(), TAG);
		_cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(_photoTempFile));

		// ///take the photo button click
		Button takeThePhotoButton = (Button) findViewById(R.id.takePhoto);
		takeThePhotoButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivityForResult(_cameraIntent, CAMERA_PIC_REQUEST);
			}
		});
		// ///END take the photo button click

		// ///Send the photo button click
		Button sendThePhotoButton = (Button) findViewById(R.id.sendPhoto);
		sendThePhotoButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					MessageUILogger.IndicateMessage(
							"Sending picture to uploader - original width: "
									+ _bitmapToSend.getWidth() + " height:"
									+ _bitmapToSend.getHeight(),
							Toast.LENGTH_LONG, "T", getApplicationContext(),
							TAG);

					if (_bitmapToSend != null) {
						MessageUILogger.IndicateMessage(
								"Sending the photo now", Toast.LENGTH_LONG,
								"T", getApplicationContext(), TAG);
						_dialog = ProgressDialog.show(
								TakePhotoAndSendActivity.this,
								"Uploading file", "Please wait...", true);
						new Thread(new Runnable() {
							public void run() {
								_result = Communicator.SendPhoto(_bitmapToSend,
										getApplicationContext());
								_message = _result;
								_handler.post(_showMessage);
								_dialog.dismiss();
							}

						}).start();

					} else {
						_message = "No file to send";
						_handler.post(_showMessage);
						MessageUILogger.IndicateMessage("No file to send",
								Toast.LENGTH_LONG, "E",
								getApplicationContext(), TAG);
					}
				} catch (Exception ex) {
					MessageUILogger.IndicateMessage("Could not upload file : "
							+ ex.getMessage(), Toast.LENGTH_LONG, "E",
							getApplicationContext(), TAG);
				}
			}
		});
		// ///END Send the photo button click
	}

	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try {
			if (requestCode == CAMERA_PIC_REQUEST) {
				switch (requestCode) {
				case CAMERA_PIC_REQUEST:
					final File file = _photoTempFile;
					try {
						MessageUILogger.IndicateMessage(
								"Get the image from local media store",
								Toast.LENGTH_LONG, "T",
								getApplicationContext(), TAG);
						_bitmapToSend = android.provider.MediaStore.Images.Media
								.getBitmap(getContentResolver(),
										Uri.fromFile(file));

						ImageView imageToSend = (ImageView) findViewById(R.id.imageToSend);

						MessageUILogger.IndicateMessage(
								"Set the UI thumbnail image",
								Toast.LENGTH_LONG, "T",
								getApplicationContext(), TAG);
						imageToSend.setImageBitmap(_bitmapToSend);
						if (_bitmapToSend != null) {
							MessageUILogger.IndicateMessage(
									"We have a bitmap to send!",
									Toast.LENGTH_LONG, "T",
									getApplicationContext(), TAG);
						} else {
							MessageUILogger.IndicateMessage(
									"We cant send a bitmap, it was null",
									Toast.LENGTH_LONG, "T",
									getApplicationContext(), TAG);
						}
					} catch (FileNotFoundException e) {

						MessageUILogger.IndicateMessage(
								"The bitmap file was not found",
								Toast.LENGTH_LONG, "T",
								getApplicationContext(), TAG);
					} catch (IOException e) {
						MessageUILogger.IndicateMessage(
								"There was an unknown IO exception when dealing with the bitmap\\camera : "
										+ e.getMessage(), Toast.LENGTH_LONG,
								"T", getApplicationContext(), TAG);
					}
					break;
				}
			} else {
				MessageUILogger.IndicateMessage(
						"The camera returned an unexpected result : "
								+ resultCode, Toast.LENGTH_LONG, "T",
						getApplicationContext(), TAG);
			}
		} catch (Exception ex) {
			MessageUILogger.IndicateMessage("An unhandled exception occured : "
					+ ex.getMessage(), Toast.LENGTH_LONG, "T",
					getApplicationContext(), TAG);
		}
	}

	private File GetTempFile(Context context) throws Exception {

		final File path = new File(Environment.getExternalStorageDirectory(),
				context.getPackageName());
		if (!path.exists()) {
			path.mkdir();
		}

		return new File(path, "image.tmp");
	}

}
