package derotek.hermesAlert;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

public class Communicator {
	public static String UserName = "";
	public static String Password = "";
	private static String TAG = "Communicator";
	private static String _url = "http://www.hermesalert.com/upload.rest";
	//private static String _url = "http://192.168.0.200:8081/upload.rest";

	// private static String _url =
	// "http://192.168.0.200/hairydogdev/upload.rest";

	public static String SendPhoto(Bitmap picture, Context context) {
		int maxWidth = 1150;
		int maxHeight = 1150;

		try {
			// Log.d(TAG, "Doing HttpPut for SendPhoto method");
			MessageUILogger.IndicateMessage(
					"Doing HttpPut for SendPhoto method", Toast.LENGTH_SHORT,
					"D", context, TAG);
			HttpClient httpClient = new DefaultHttpClient();
			HttpPut httpPut = new HttpPut(_url);
			java.io.ByteArrayOutputStream stream = new ByteArrayOutputStream();

			Bitmap scaledPicture = picture;
			int newWidth;
			int newHeight;
			if (picture.getWidth() > maxWidth
					|| picture.getHeight() > maxHeight) {
				MessageUILogger.IndicateMessage(
						"We need to resize the picture from width:"
								+ picture.getWidth() + " height:"
								+ picture.getHeight(), Toast.LENGTH_LONG, "D",
						context, TAG);
				if (picture.getWidth() >= picture.getHeight()) {
					newWidth = maxWidth;
					newHeight = (int) ((double) picture.getHeight() / ((double) picture
							.getWidth() / (double) newWidth));
				} else {
					newHeight = maxHeight;
					newWidth = (int) ((double) picture.getWidth() / ((double) picture
							.getHeight() / (double) newHeight));
				}

				MessageUILogger.IndicateMessage("ScaledPicture to width:"
						+ newWidth + " height:" + newHeight, Toast.LENGTH_LONG,
						"D", context, TAG);
				scaledPicture = Bitmap.createScaledBitmap(picture, newWidth,
						newHeight, false);
				MessageUILogger.IndicateMessage(
						"ScaledPicture is now width:"
								+ scaledPicture.getWidth() + " height:"
								+ scaledPicture.getHeight(), Toast.LENGTH_LONG,
						"D", context, TAG);
			}

			scaledPicture.compress(CompressFormat.JPEG, 70, stream);

			MessageUILogger.IndicateMessage("Build the Http packet",
					Toast.LENGTH_LONG, "D", context, TAG);
			byte[] bitmapData = stream.toByteArray();

			String encodedString = Base64.encodeToString(bitmapData, 0,
					bitmapData.length, Base64.DEFAULT);

			String dataPacket = "-key:datatype:2" + "\r\n" + "-key:userName:"
					+ UserName + "\r\n" + "-key:pwd:" + Password + "\r\n"
					+ "-key:photo:" + encodedString;
			HttpEntity dataToSend = new StringEntity(dataPacket);
			httpPut.setEntity(dataToSend);

			MessageUILogger.IndicateMessage("Now send the picture over Http",
					Toast.LENGTH_LONG, "D", context, TAG);
			return GetResponseText(httpClient.execute(httpPut));
		} catch (Exception ex) {
			Log.d(TAG, "Could not send photo : " + ex.getMessage());
			return ex.getMessage();
		}
	}

	public static String AuthenticateUser() {
		try {
			Log.d(TAG, "Doing Http	Put for AuthenticateUser method");
			HttpClient httpClient = new DefaultHttpClient();
			HttpPut httpPut = new HttpPut(_url);
//
			String dataPacket = "-key:datatype:1" + "\r\n" + "-key:userName:"
					+ UserName + "\r\n" + "-key:pwd:" + Password;
			HttpEntity dataToSend = new StringEntity(dataPacket);
			httpPut.setEntity(dataToSend);

			return GetResponseText(httpClient.execute(httpPut));
		} catch (Exception ex) {
			Log.d(TAG, "Could not authenticate user : " + ex.getMessage());
			return "ERROR";// ex.getMessage();
		}
	}

	private static String GetResponseText(HttpResponse response)
			throws IllegalStateException, IOException {
		HttpEntity resultStream = response.getEntity();

		if (resultStream != null) {
			InputStream inStream = resultStream.getContent();

			String line = "";
			StringBuilder total = new StringBuilder();

			// Wrap a BufferedReader around the InputStream
			BufferedReader rd = new BufferedReader(new InputStreamReader(
					inStream));

			// Read response until the end
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}

			String returnString = total.toString();
			Log.d(TAG, "returnString : " + returnString);
			return returnString;
		} else {
			return "False";
		}

	}
}
