﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Monitoring_ItemsMonitoring : PageBase, derotek.BikeCasa.Mvp.MonitoringAreas.IMonitoringAreas
{
    #region Fields
    private derotek.BikeCasa.Mvp.MonitoringAreas.MonitoringAreasPresenter _presenterMonitoringAreas;
    #endregion
    
    #region Form Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Monitoring";
        _presenterMonitoringAreas = new derotek.BikeCasa.Mvp.MonitoringAreas.MonitoringAreasPresenter(this); 
        if (!IsPostBack)
        {
            uxGoogleMultiMap.UserId = SessionVariables.CurrentUserGet(Session).Id;            
            _presenterMonitoringAreas.PrepareView((long)derotek.HairyDog.Enumerations.PropertyType.ElectronicEquipment);

            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                derotek.HairyDog.Metrics.PropertyType propertyType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.PropertyType>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.PropertyType.ElectronicEquipment);
                uxGoogleMultiMap.PropertyTypeToShow = propertyType.Id;
            }
        }
    }

    /// <summary>
    /// Click on row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uxMonitoringEvents_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {
            long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            string returnToPage = this.Page.AppRelativeVirtualPath;
            (new derotek.BikeCasa.Mvp.MonitoringAreas.MonitoringAreasPresenter(this)).ShowItem(id, Page.AppRelativeVirtualPath);            
        }
    }
    protected void uxMonitoringEvents_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _presenterMonitoringAreas.GetStolenItems(long.Parse(uxPropertyTypeList.SelectedValue));
        ((GridView)sender).PageIndex = e.NewPageIndex;
        ((GridView)sender).DataBind();
    }    
    protected void uxMaintainAreasMonitoring_Click(object sender, EventArgs e)
    {
        derotek.HairyDog.QueryString qs = new derotek.HairyDog.QueryString();
        qs.Variables.Add(derotek.HairyDog.QueryString.ReturnToPage,Page.AppRelativeVirtualPath);
        Response.Redirect("~/Pages_Main/MaintainMultipleGpsMarkerAreas.aspx"+ qs.EncodeQueryString());
    }
    
    protected void uxGetPropertyDataInMonitoringAreas_Click(object sender, EventArgs e)
    {
        _presenterMonitoringAreas.GetStolenItems(long.Parse(uxPropertyTypeList.SelectedValue));
        uxGoogleMultiMap.PropertyTypeToShow = long.Parse(uxPropertyTypeList.SelectedValue);
    }
    #endregion

    #region View IMonitoringAreas
    List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> derotek.BikeCasa.Mvp.MonitoringAreas.IMonitoringAreas.StolenRegisteredObjects
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxMonitoringEvents.DataKeyNames = new string[] { "Id" };
            uxMonitoringEvents.DataSource = value;
            uxMonitoringEvents.DataBind();
        }
    }
    public void SetPropertyTypeList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxPropertyTypeList, list);
    }
    public long PropertyType
    {
        get
        {
            return long.Parse(uxPropertyTypeList.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxPropertyTypeList, value);
        }
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    void derotek.BikeCasa.Mvp.IView.DisplayMessage(string message, bool isErrorMessage)
    {
        
    }
    void derotek.BikeCasa.Mvp.IView.RedirectView(string path)
    {
        Response.Redirect(path);
    }
    #endregion
}