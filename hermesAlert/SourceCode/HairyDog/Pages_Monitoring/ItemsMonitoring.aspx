﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ItemsMonitoring.aspx.cs" Inherits="Pages_Monitoring_ItemsMonitoring" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="GoogleMultiMap" Src="~/WebControls/GoogleMultiMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register Assembly="derotek.AspNet.Controls" Namespace="derotek.AspNet.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
    <div id="Div1" runat="server" class="Section">
        <uc:PageSectionHeading ID="PageSectionHeading2" runat="server" SectionHeadingId="EventsInMonitoringAreas" BreakBefore="true"/>
        <div id="Div44" runat="server" style="width:1185px;position:relative;left:5px">       
            <asp:Label ID="Label2" runat="server" SkinID="NormalNoWidthGiven">Select the type of items to retrieve</asp:Label>
                <asp:DropDownList SkinID="Normal" ID="uxPropertyTypeList" runat="server" AutoPostBack="false"  />
                &nbsp;<uc:Button id="uxGetPropertyDataInMonitoringAreas" runat="server" Text="Search" onclicked="uxGetPropertyDataInMonitoringAreas_Click" /><br /><br />
            <uc:Button id="uxMaintainAreasMonitoring" runat="server" Text="Edit the geographic areas you wish to monitor" onclicked="uxMaintainAreasMonitoring_Click" />
            <br />
            <uc:GoogleMultiMap ID="uxGoogleMultiMap" runat="server" UserMode="DisplayZoneHits" />
            <br />             
            <cc1:DataGrid ID="uxMonitoringEvents"  runat="server" AutoGenerateColumns="False" onrowcommand="uxMonitoringEvents_RowCommand" OnPageIndexChanging="uxMonitoringEvents_PageIndexChanging"> 
                <Columns>
                    <asp:ImageField DataImageUrlField="PictureUrl" DataImageUrlFormatString="~/Uploads/Thumbs/{0}" AlternateText="Edit"  ControlStyle-Height = "50px">                
                    </asp:ImageField>
                    <asp:BoundField DataField="PropertyType" HeaderText="Property type" SortExpression="PropertyType" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="HasAttachments" HeaderText="Attachments?" />
                    <asp:BoundField DataField="DateWasStolen" HeaderText="Date went missing" DataFormatString="{0:dd MMMM yyyy}"/>                
                </Columns>
                           
            </cc1:DataGrid>
            <br />            
        </div>
    </div>
</asp:Content>
