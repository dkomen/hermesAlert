﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mvpUser = derotek.BikeCasa.Mvp.User;

public partial class PagesUser_Logon : PageBase, mvpUser.Views.ILogon
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert for South Africa : Report crimes and theft for free";
        derotek.Exceptions.StandardException.FlatFileLogPath = SessionVariables.RootPath(false);
        SessionVariables.CurrentUserSet(this.Session, new derotek.HairyDog.Users.User());
        uxGoogleMultiMap.UserId = 0;
        if (!IsPostBack)
        {
            uxDisplayMessage.Visible = false;
            uxGoogleMultiMap.PropertyTypeToShow = 0;
        }
    }
    protected void ButtonSignIn_Click(object sender, EventArgs e)
    {
        mvpUser.Presenters.LogonPresenter logonPresenter = new mvpUser.Presenters.LogonPresenter(this);
        logonPresenter.LogonUser();
    }
    protected void uxButtonCreateAccount_Click(object sender, EventArgs e)
    {
        mvpUser.Presenters.LogonPresenter logonPresenter = new mvpUser.Presenters.LogonPresenter(this);
        logonPresenter.CreateUser();
    }
    protected void uxForgotLogonCredentials_Click(object sender, EventArgs e)
    {        
        new derotek.BikeCasa.Mvp.User.Presenters.LogonPresenter(this).ForgotLogonCredentials();
    }
    #region View
    public string Username
    {
        get
        {
            return uxUserName.Text;
        }
        set
        {
            uxUserName.Text = value;
        }
    }
    public string Password
    {
        get
        {
            return uxPassword.Text;
        }
        set
        {
            uxPassword.Text = value;
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.isErrorMessage = isErrorMessage;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.Message = message;
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
    #endregion

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
}