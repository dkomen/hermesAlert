﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ItemRegistration.aspx.cs" Inherits="Pages_ItemRegistration_ItemRegistration" %>
<%@ Register Assembly="derotek.AspNet.Controls" Namespace="derotek.AspNet.Controls" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Attachments" Src="~/WebControls/Attachments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"> 

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js" type="text/javascript"></script>
	<script type="text/javascript">
	    $(document).ready(function ()
        {
            if('<%=GetStolenValue()%>'=='False')
            {                
	            $(".slidingDiv").hide();
            }
	        $(".show_hide").show();

	        $('.show_hide').click(function ()
            {
	            $(".slidingDiv").slideToggle();
	        });
	    });
	</script>
    

    <div id="Div1" runat="server" class="Section" >                
        <uc:PageSectionHeading ID="PageSectionHeading1" SectionHeadingId="RegisterItem" runat="server" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">    
            <asp:Table ID="Table1" runat="server" Width="125px" SkinID="Normal">
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Right">
                            <asp:Label ID="Label6" runat="server" SkinID="Normal">Item type</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:DropDownList SkinID="Normal" ID="uxPropertyType" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label1" runat="server" SkinID="Normal" Text="Item description" />
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:TextBox SkinID="NormalWideText" ID="uxDescription" runat="server" />
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                        <asp:Label ID="Label2" runat="server" SkinID="Normal">Notes</asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:TextBox SkinID="NormalTextBlock" ID="uxLongDescription" runat="server" TextMode="MultiLine" Rows="5" />
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label5" runat="server" SkinID="NormalWideText">Is stolen\Missing?</asp:Label>
                    </asp:TableCell><asp:TableCell>
                        <asp:CheckBox SkinID="Normal" class="show_hide" ID="uxIsStolen" runat="server" Checked="false" OnClick="javascript:ShowStolenSpecificInfo();" />
                    </asp:TableCell></asp:TableRow></asp:Table><asp:Table runat="server">
                <asp:TableRow>   
                    <asp:TableCell HorizontalAlign="Left"  >
                        <div id="uxIsStolenDetailsHost" runat="server" class="slidingDiv cornersButton">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="top">
                                        <asp:Label ID="Label12" runat="server" SkinID="NormalNoWidthGiven">Date item was last seen</asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Calendar ID="uxDateStolen" runat="server" BackColor="White" BorderColor="#333333" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" 
                                            Font-Size="8pt" ForeColor="Black" Height="180px" NextPrevFormat="ShortMonth" Width="230px">
                                                <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="6pt" />
                                                <DayStyle BackColor="#dedede" ForeColor="#1212bb" />
                                                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                                <OtherMonthDayStyle ForeColor="#999999" />
                                                <SelectedDayStyle BackColor="#333399" ForeColor="White" />
                                                <TitleStyle BackColor="#222299" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="White" Height="10pt" />
                                                <TodayDayStyle BackColor="#999999" ForeColor="White" />
                                        </asp:Calendar>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Label ID="Label3" runat="server" SkinID="NormalNoWidthGiven" Text="Police case number" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                        <asp:TextBox SkinID="Normal" ID="uxPoliceCaseNumber" runat="server" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Label ID="Label11" runat="server" SkinID="NormalWideText">Notify the community</asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox SkinID="Normal" ID="uxNotifyCommunity" runat="server" Checked="true" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Label ID="Label9" runat="server" SkinID="NormalWideText">Last known location : </asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>                           
                                        <asp:Label ID="Label7" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Longitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLongitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;
                                        <asp:Label ID="Label8" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Latitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLatitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <%--<uc:Button ID="uxSelectGpslocation" runat="server" Text="Select gps location" OnClientClick="javascript:showList();" AutoPostBack="false" />--%>
                                        <uc:Button ID="uxSelectGpslocation" runat="server" Text="Select gps location" OnClicked="uxSelectGpslocation_Click" AutoPostBack="false" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right" Width="225px">
                                        <asp:Label ID="Label10" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Hide coordinates from public</asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:DropDownList ID="uxLocationAccuracyWhenPublic" runat="server" SkinID='Normal' />  
                                    </asp:TableCell>
                                </asp:TableRow>                                              
                            </asp:Table>
                        </div>
                    </asp:TableCell></asp:TableRow></asp:Table><br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />
            <asp:Label ID="Label4" runat="server" SkinID="SmallWarningText">Remember that what you add here is private and will only be shared with the community if you indicate that the item<br /> is stolen and that the community must be notified</asp:Label>
            <br /><br />
            <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click" /> 
            <uc:Button ID="uxDelete" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this item?');" OnClicked="uxDelete_Click" />   
            <uc:Button ID="uxProceedReturn" runat="server" Text="Return..." OnClicked="uxProceedToRegisteredItemsPage_Click" />
            <br />
            <br />
        </div>
    </div>
    <br />
    
    <div id="uxAttachmentsHost" runat="server" >
        <uc:Attachments ID="uxAttachments" runat="server" />
    </div>
    <br />
    
    <div id="Div3" runat="server" class="Section" >        
        <uc:PageSectionHeading ID="PageSectionHeading2" runat="server" SectionHeadingId="RegisteredItemMessages" BreakBefore="true"/>
        <div id="Div4" runat="server" style="width:1185px;position:relative;left:5px">
            <cc1:DataGrid ID="uxRegisteredItemsMessages"  runat="server" AutoGenerateColumns="False"  
                onrowcommand="uxRegisteredItemsMessages_RowCommand" OnPageIndexChanging="uxRegisteredItemsMessages_PageIndexChanging"> 
            <Columns>                
                <asp:BoundField DataField="Id" Visible="False" />
                <asp:BoundField DataField="DateLastSaved" DataFormatString="{0: dd MMM yyyh HH:mm}" HeaderText="Date" SortExpression="DateLastSaved">
                     <ItemStyle Width="125px" />
                </asp:BoundField>
                <asp:BoundField DataField="Notes" HeaderText="Message" />
                <asp:TemplateField HeaderText="Read?" ItemStyle-Width="50px">
                    <ItemTemplate>
                        <%# (Boolean.Parse(Eval("HasBeenRead").ToString())) ? "Yes" : "No" %>
                    </ItemTemplate>  
                </asp:TemplateField>                               
            </Columns>                
            </cc1:DataGrid>
            <%--<br />
            <uc:Button runat="server" ID="uxAddNewRegisteredItem" OnClicked="uxAddNewRegisteredItem_Clicked" Text="Add new"  />  --%>
            <br />
            <uc:Button ID="uxSendPrivateMessage" runat="server" Text="Create new message" OnClicked="uxSendMessage_Click" />
            <br />
        </div>   
        <br />      
    </div>

    <script type="text/javascript">        
        ShowStolenSpecificInfo();

        function ShowStolenSpecificInfo() {
      
            var uxIsStolen = document.getElementById('<%=uxIsStolen.ClientID %>');
            var uxIsStolenDetails = document.getElementById('<%=uxIsStolenDetailsHost.ClientID %>');
            
            if (uxIsStolen.checked == true) {
                uxIsStolenDetails.style.visibility = "visible";
            }
            else {
                uxIsStolenDetails.style.visibility = "hidden";
            }
        }        
    </script>
</asp:Content>

