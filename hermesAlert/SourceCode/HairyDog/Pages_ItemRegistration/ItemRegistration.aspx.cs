﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using derotek.HairyDog;

public partial class Pages_ItemRegistration_ItemRegistration : PageBase, derotek.BikeCasa.Mvp.Registrations.IRegisteredItem
{
    #region Fields
    private derotek.BikeCasa.Mvp.Registrations.RegisteredItemPresenter _presenter;
    #endregion

    #region Form Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Maintain registered item"; 
        _presenter = new derotek.BikeCasa.Mvp.Registrations.RegisteredItemPresenter(this);

        uxDescription.MaxLength = derotek.Configuration.FieldLengths.ShortDescription;
        uxLongDescription.MaxLength = derotek.Configuration.FieldLengths.Notes;
        uxPoliceCaseNumber.MaxLength = derotek.Configuration.FieldLengths.PoliceCaseNumber;
        #region JavaScript
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GpsScript", JavaScript.ShowGpsSelectionFormScript(uxGpsLongitudeSelected.ClientID, uxGpsLatitudeSelected.ClientID));
        #endregion

        if (!IsPostBack)
        {
            derotek.BikeCasa.Mvp.Registrations.StateItemRegistration sessionItem = (derotek.BikeCasa.Mvp.Registrations.StateItemRegistration)Session[this.AppRelativeVirtualPath];
            if (sessionItem != null && Session["Longitude"] != null && Session["Longitude"] != null)
            {
                sessionItem.Latitude = double.Parse(Session["Latitude"].ToString());
                sessionItem.Longitude = double.Parse(Session["Longitude"].ToString());
            }
            _presenter.PrepareView(base.ActiveObjectId, sessionItem);
            uxDisplayMessage.Visible = false;
        }        
    }
    protected void uxSave_Click(object sender, EventArgs e)
    {
        _presenter.SaveRegisteredItem(base.ActiveObjectId);
    }
    protected void uxDelete_Click(object sender, EventArgs e)
    {
        _presenter.DeleteRegisteredItem(base.ActiveObjectId, SessionVariables.RootPath(true) + "Pages_User/UserRegisteredItems.aspx");
    }
    protected void uxProceedToRegisteredItemsPage_Click(object sender, EventArgs e)
    {
        _presenter.RedirectView(base.ReturnToPage);
    }
    protected void uxSendMessage_Click(object sender, EventArgs e)
    {
        //Return to parent
        QueryString callingPagedetails = new QueryString();
        callingPagedetails.Variables.Add(SessionVariables.ActiveObjectId, base.ActiveObjectId.ToString());
        callingPagedetails.Variables.Add(SessionVariables.ReturnToPage, base.ReturnToPage);

        //Return to current
        QueryString qs = new QueryString();
        qs.Variables[SessionVariables.ReturnToPage] = Page.AppRelativeVirtualPath + callingPagedetails.EncodeQueryString();
        qs.Variables.Add(SessionVariables.ActiveObjectId, base.ActiveObjectId.ToString());        
        RedirectView("~/Pages_User/SendMessage.aspx" + qs.EncodeQueryString());
    }

    protected void uxRegisteredItemsMessages_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {
            long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            _presenter.MessageRead(base.ActiveObjectId, id, Page.AppRelativeVirtualPath);
        }
    }
    protected void uxRegisteredItemsMessages_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _presenter.PrepareView(base.ActiveObjectId, null);
        ((GridView)sender).PageIndex = e.NewPageIndex;
        ((GridView)sender).DataBind();
    }
    protected void uxSelectGpslocation_Click(object sender, EventArgs e)
    {
        CallGpsPage("~/Pages_Main/SelectGpsLocation.aspx");
    }
    #endregion

    #region View
    public string Description
    {
        get
        {
            return uxDescription.Text;
        }
        set
        {
            uxDescription.Text = value;
        }
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.Message = message;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.isErrorMessage = isErrorMessage;
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
    public string LongDescription
    {
        get
        {
            return uxLongDescription.Text;
        }
        set
        {
            uxLongDescription.Text = value;
        }
    }
    public bool IsStolen
    {
        get
        {
            return uxIsStolen.Checked;
        }
        set
        {
            uxIsStolen.Checked = value;           
        }
    }
    public string PoliceCaseNumber
    {
        get
        {
            return uxPoliceCaseNumber.Text;
        }
        set
        {
            uxPoliceCaseNumber.Text = value;
        }
    }
    public bool IfIsStolenMustNotifyCommunity
    {
        get
        {
            return uxNotifyCommunity.Checked;
        }
        set
        {
            uxNotifyCommunity.Checked = value;
        }
    }
    public DateTime DateWasStolen
    {
        get
        {
            return uxDateStolen.SelectedDate;
        }
        set
        {
            uxDateStolen.SelectedDate = value;
        }
    }
    public long PropertyType
    {
        get
        {
            return long.Parse(uxPropertyType.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxPropertyType, value);
        }
    }
    public void SetPropertyTypeList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxPropertyType, list);
    }
    public long LocationAccuracyWhenPublic
    {
        get
        {
            return long.Parse(uxLocationAccuracyWhenPublic.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxLocationAccuracyWhenPublic, value);
        }
    }
    public void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxLocationAccuracyWhenPublic, list);
    }
    public double Latitude
    {
        get
        {
            return double.Parse(uxGpsLatitudeSelected.Text != string.Empty ? uxGpsLatitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLatitudeSelected.Text = value.ToString();
        }
    }
    public double Longitude
    {
        get
        {
            return double.Parse(uxGpsLongitudeSelected.Text != string.Empty ? uxGpsLongitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLongitudeSelected.Text = value.ToString();
        }
    }
    public bool ShowAttachments
    {
        get
        {
            return uxAttachmentsHost.Visible;
        }
        set
        {
            uxAttachmentsHost.Visible = value;
        }
    }
    public derotek.HairyDog.Interfaces.IAttachmentsHost AttachmentsHost
    {
        get
        {
            return uxAttachments.AttachmentsHost;
            
        }
        set
        {            
            uxAttachments.AttachmentsHost = value;
        }
    }
    bool _readonlyMode = false;
    public bool ReadonlyMode
    {
        get
        {
            return _readonlyMode;
        }
        set
        {
            _readonlyMode = value;
            uxSave.Visible = !value;
            if (value)
            {
                uxSelectGpslocation.Text = "View gps location";
            }
            uxDelete.Visible = !value;
            uxSendPrivateMessage.Visible = base.ActiveObjectId == 0 ? false : value;
            uxAttachments.ReadOnlyMode = !value;

            uxPropertyType.Enabled = !value;
            uxDescription.Enabled = !value;
            uxLongDescription.Enabled = !value;
            uxIsStolen.Enabled = !value;
            uxNotifyCommunity.Enabled = !value;
            uxDateStolen.Enabled = !value;
            uxPoliceCaseNumber.Enabled = !value;
            uxGpsLongitudeSelected.Enabled = !value;
            uxGpsLatitudeSelected.Enabled = !value;
            uxLocationAccuracyWhenPublic.Enabled = !value;
        }
    }    
    public List<derotek.HairyDog.ItemMonitoring.ItemDetection> Messages 
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxRegisteredItemsMessages.DataKeyNames = new string[] { "Id" };
            uxRegisteredItemsMessages.DataSource = value;
            uxRegisteredItemsMessages.DataBind();
        }
    }
    #endregion   

    #region Public Functions
    public bool GetStolenValue()
    {
        return IsStolen;
    }
    #endregion

    #region Private Functions
    private void CallGpsPage(string pageToCallForSelectingGpsData)
    {
        Session["Longitude"] = Longitude;
        Session["Latitude"] = Latitude;

        derotek.BikeCasa.Mvp.Registrations.StateItemRegistration state = new derotek.BikeCasa.Mvp.Registrations.StateItemRegistration();
        state.AttachmentsHost = this.AttachmentsHost;
        state.ActiveObjectId = this.ActiveObjectId;
        state.Description = this.Description;
        state.CurrentUser = this.CurrentUser;
        state.DateWasStolen = this.DateWasStolen;
        state.IfIsStolenMustNotifyCommunity = this.IfIsStolenMustNotifyCommunity;
        state.IsStolen = this.IsStolen;
        state.LongDescription = this.LongDescription;
        //state.Messages = this.Messages;
        state.PoliceCaseNumber = this.PoliceCaseNumber;
        state.PropertyType = this.PropertyType;
        state.ReadonlyMode = this.ReadonlyMode;
        state.ReturnToPage = this.ReturnToPage;
        state.ShowAttachments = this.ShowAttachments;

        state.Latitude = this.Latitude;
        state.Longitude = this.Longitude;
        state.LocationAccuracyWhenPublic = this.LocationAccuracyWhenPublic;
        state.ObjectId = this.AppRelativeVirtualPath;

        state.KeepAliveForPages.Add(this.AppRelativeVirtualPath);
        state.KeepAliveForPages.Add("~/Pages_Main/SelectGpsLocation.aspx");

        Session[state.ObjectId] = state;

        _presenter.RedirectToSelectGpsLocation(this.AppRelativeVirtualPath);
    }
    #endregion
}