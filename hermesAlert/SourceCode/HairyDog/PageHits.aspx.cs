﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class PageHits : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
        {
            IList<derotek.HairyDog.PageHits> hits = transaction.Retrieve<derotek.HairyDog.PageHits>(null,250);
            (hits as List<derotek.HairyDog.PageHits>).Sort((IComparer<derotek.HairyDog.PageHits>)(new derotek.HairyDog.PageHits.SortByDateCreatedDescending()));
            uxPageHitsView.DataSource = hits;
        }
        uxPageHitsView.AutoGenerateColumns = false;

        uxPageHitsView.DataBind();
    }
}