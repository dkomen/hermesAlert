﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="PagesUser_Logon" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="GoogleMultiMap" Src="~/WebControls/GoogleMultiMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="Slideshow" Src="~/WebControls/Slideshow.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    
    <style>
        .table
        {
            display:inline-table;
        }
        .rowLeft
        {
            width:625px;
            float: left; 
        }
        .rowRight
        {
            width:520px;
            float:right;
        }        
    </style>
    
    <div runat="server" style="height:630px;position:relative;top:-18px;overflow:hidden">
        <div class="table" id="uiLeft">
            <div class="rowLeft">                
                <uc:GoogleMultiMap ID="uxGoogleMultiMap" runat="server" ZoomLevel="5" MapType="HYBRID" Longitude="24.5" Latitude="-24.4" UserMode="DisplayZoneHits" CanvasWidth="600" CanvasHeight="550" />
            </div>
            <div class="rowRight" id="uiRightTop" style="position:relative;top:15px;left:22px;">
                <uc:Slideshow ID="Slideshow1" runat="server" />
            </div>
            <div class="rowRight" id="uiRight" style="position:relative; top:150px">
                <table>
                    <tr>
                        <td> 
                            <br />                            
                            <uc:MessageBox runat="server" ID="uxDisplayMessage"  />  
                            <asp:Table ID="uiLogin" runat="server" SkinId="Logon">
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Label ID="Label1" runat="server" SkinID="NormalNoWidthGiven" Text="eMail" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left">
                                        <asp:TextBox SkinID="Normal" ID="uxUserName" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Right">
                                        <asp:Label ID="Label2" runat="server" SkinID="NormalNoWidthGiven">Password</asp:Label>
                                    </asp:TableCell><asp:TableCell HorizontalAlign="Left">
                                        <asp:TextBox SkinID="Normal" ID="uxPassword" runat="server" TextMode="Password" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <uc:Button ID="ButtonSignIn" runat="server" Text="Sign in" OnClicked="ButtonSignIn_Click" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                            <div id="Divx" runat="server" style="position:relative;top:6px;left:75px">
                                <uc:Button ID="uxButtonCreateAccount" runat="server" Text="Create account" OnClicked="uxButtonCreateAccount_Click" />&nbsp;<uc:Button ID="uxForgotPassword" runat="server" Text="Forgot password" OnClicked="uxForgotLogonCredentials_Click" />
                            </div>                                     
                        </td>
                    </tr>
                </table>  
                <div style="overflow:hidden;height:570px;position:relative;left:-10px">
                    <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/FrontScreen.png" style="position:relative;left:0px;top:24px" />                            
                    <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/JoinFree.png" style="position:relative;top:-30px;left:360px" />                    
                    <%--<a href="../Mobile/hermesAlert.zip"><asp:Image ID="Image2" runat="server" border="0px" ImageUrl="~/Images/WithAndroid.png" style="position:relative;top:50px;left:-65px;height:160px" />--%>
                    <%--</a> --%>
                </div>      
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/NowWith.png" style="position:absolute;top:270px;left:-156px;height:114px"/>                                 
            </div>
        </div>                        
    </div>

</asp:Content>
    

