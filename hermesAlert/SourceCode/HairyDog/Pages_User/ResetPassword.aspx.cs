﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_User_ResetPassword : PageBase, derotek.BikeCasa.Mvp.User.Views.IPasswordResetView
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Reset Password";
        if (!IsPostBack)
        {
            uxDisplayMessage.Visible = false;
        }
    }

    protected void uxSave_Click(object sender, EventArgs e)
    {
        new derotek.BikeCasa.Mvp.User.Presenters.PasswordResetPresenter(this).Save();
    }
    protected void uxProceedToLogin_Click(object sender, EventArgs e)
    {
        RedirectView("~/default.aspx");
    }

    public string Password
    {
        get
        {
            return uxPassword.Text;
        }
        set
        {
            uxPassword.Text = value;
        }
    }

    public string PasswordVerify
    {
        get
        {
            return uxRetypePassword.Text;
        }
        set
        {
            uxRetypePassword.Text = value;
        }
    }

    public string PasswordResetId
    {
        get
        {
            return Request.QueryString["id"];
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.isErrorMessage = isErrorMessage;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.Message = message;
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
}