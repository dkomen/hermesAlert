﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_User_SendMessage : PageBase, derotek.BikeCasa.Mvp.User.Views.IItemDetectionMessageSendView
{
    #region Fields
    private derotek.BikeCasa.Mvp.User.Views.ReadOnlyMode _readOnlyLevel = derotek.BikeCasa.Mvp.User.Views.ReadOnlyMode.ReadOnly;
    #endregion

    #region Ui Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Send Message"; 
        uxDisplayMessage.Visible = false;
        uxNotes.MaxLength = derotek.Configuration.FieldLengths.Description;
        #region JavaScript        
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GpsScript", JavaScript.ShowGpsSelectionFormScript(uxGpsLongitudeSelected.ClientID, uxGpsLatitudeSelected.ClientID));
        #endregion

        if (!IsPostBack)
        {
            (new derotek.BikeCasa.Mvp.User.Presenters.ItemDetectionMessageSendPresenter(this, base.ActiveObjectId)).PrepareView(base.ChildObjectId);
        }
    }
    public void uxSave_Click(object sender, EventArgs e)
    {
        string returnToPage = base.ReturnToPage;
        (new derotek.BikeCasa.Mvp.User.Presenters.ItemDetectionMessageSendPresenter(this, base.ActiveObjectId)).SendMessage(returnToPage);
    }
    public void uxReply_Click(object sender, EventArgs e)
    {
        
    }
    protected void uxReturn_Click(object sender, EventArgs e)
    {
        RedirectView(base.ReturnToPage);
    }
    #endregion

    #region View
    public bool KeepPrivate
    {
        get
        {
            return uxKeepMessagePrivate.Checked;
        }
        set
        {
            uxKeepMessagePrivate.Checked = value; ;
        }
    }
    public string Notes
    {
        get
        {
            return uxNotes.Text;
        }
        set
        {
            uxNotes.Text = value;
        }
    }
    public long LocationAccuracyWhenPublic
    {
        get
        {
            return long.Parse(uxLocationAccuracyWhenPublic.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxLocationAccuracyWhenPublic, value);
        }
    }
    public void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxLocationAccuracyWhenPublic, list);
    }
    public double Latitude
    {
        get
        {
            return double.Parse(uxGpsLatitudeSelected.Text != string.Empty ? uxGpsLatitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLatitudeSelected.Text = value.ToString();
        }
    }
    public double Longitude
    {
        get
        {
            return double.Parse(uxGpsLongitudeSelected.Text != string.Empty ? uxGpsLongitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLongitudeSelected.Text = value.ToString();
        }
    }
    public derotek.BikeCasa.Mvp.User.Views.ReadOnlyMode ReadOnlyMode 
    {
        get
        {
            return _readOnlyLevel;
        }
        set
        {
            uxKeepMessagePrivate.Enabled = false;
            switch (value)
            {
                case derotek.BikeCasa.Mvp.User.Views.ReadOnlyMode.CreateNew: //creating new
                    uxSave.Visible = true;
                    uxReply.Visible = false;
                    uxKeepMessagePrivate.Checked = false;
                    break;
                case derotek.BikeCasa.Mvp.User.Views.ReadOnlyMode.OnlyReply: //may only reply
                    uxSave.Visible = false;
                    uxReply.Visible = true;
                    break;
                case derotek.BikeCasa.Mvp.User.Views.ReadOnlyMode.ReadOnly: //Read only
                    uxSave.Visible = false;
                    uxReply.Visible = false;
                    break;
            }            
        }
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.Message = message;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.isErrorMessage = isErrorMessage;
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
    #endregion
}