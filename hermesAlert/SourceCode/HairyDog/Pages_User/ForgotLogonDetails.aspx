﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ForgotLogonDetails.aspx.cs" Inherits="Pages_User_ForgotLogonDetails" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">                
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="ForgotLogonDetails" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">
            <asp:Table ID="Table1" runat="server" SkinID="Normal">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" ColumnSpan="2">
                        <asp:Label ID="Label2" runat="server" SkinID="NormalNoWidthGiven" Text="Send password reset information to my email address" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label1" runat="server" SkinID="Normal" Text="eMail Address" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxEmailAddress" runat="server" />                        
                    </asp:TableCell>
                </asp:TableRow>                                   
            </asp:Table>
            <br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />           
            <uc:Button ID="uxSendEmail" runat="server" Text="Send" OnClicked="uxSendEMail_Click" />
            <uc:Button ID="uxProceedToLogin" runat="server" Text="Return to login..." OnClicked="uxProceedToLogin_Click" />
            <br />
            <br />
        </div>
    </div>
</asp:Content>

