﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using mvpUser = derotek.BikeCasa.Mvp.User;

public partial class Pages_User_CreateNewUser : PageBase, derotek.BikeCasa.Mvp.User.Views.IUserMaintenance
{
    #region Fields
    private derotek.BikeCasa.Mvp.User.Presenters.UserMaintenancePresenter _presenter;
    #endregion

    #region Form Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Create new account";
        _presenter = new derotek.BikeCasa.Mvp.User.Presenters.UserMaintenancePresenter(this);

        if (!IsPostBack)
        {
            uxEmailAddress.MaxLength = derotek.Configuration.FieldLengths.Email;
            uxFistName.MaxLength = derotek.Configuration.FieldLengths.PersonName;
            uxSurname.MaxLength = derotek.Configuration.FieldLengths.PersonSurname;
            uxPassword.MaxLength = derotek.Configuration.FieldLengths.Password;
            uxRetypePassword.MaxLength = derotek.Configuration.FieldLengths.Password;
            uxSecurityQuestion.MaxLength = derotek.Configuration.FieldLengths.SecurityQuestion;
            uxSecurityQuestionAnswer.MaxLength = derotek.Configuration.FieldLengths.SecurityQuestion;

            derotek.HairyDog.Users.User user = SessionVariables.CurrentUserGet(this.Session);
            _presenter.PrepareFieldDefaults(user.Id, null);
            //uxDisplayMessage.Visible = false;            
        }
    }
    protected void uxSave_Click(object sender, EventArgs e)
    {
        _presenter.SaveUser();
    }
    protected void uxProceedToLogin_Click(object sender, EventArgs e)
    {
        _presenter.RedirectToLogonView();
    }
    protected void uxProvinceList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _presenter.SelectedProvinceChanged(long.Parse(uxProvinceList.SelectedValue));
    }
    #endregion

    #region View
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    public long Id
    {
        get;
        set;
    }

    int derotek.HairyDog.Users.IBaseUser.UserRating
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public string UserVerificationHashRequirement
    {
        get
        {
            return Guid.NewGuid().ToString();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public string VerifyPassword
    {
        get
        {
            return uxRetypePassword.Text;
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public long SelectedProvinceId
    {
        get
        {
            return long.Parse(uxProvinceList.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxProvinceList, value);
        }
    }
    public long? SelectedTownId
    {
        get
        {
            return null;
        }
        set
        {

        }
    }
    public void SetProvincesList(Dictionary<long, string> items)
    {
        HelperClassDataElements.PopulateDropdownList(uxProvinceList, items);
    }
    public void SetTownsList(Dictionary<long, string> items)
    {

    }
    public string Name
    {
        get
        {
            return uxFistName.Text;
        }
        set
        {
            uxFistName.Text = value;
        }
    }
    public string Surname
    {
        get
        {
            return uxSurname.Text;
        }
        set
        {
            uxSurname.Text = value;
        }
    }
    public string ContactNumber
    {
        get
        {
            return string.Empty;
        }
        set
        {
            //uxContactNumber.Text = value;
        }
    }
    public string EMailAddress
    {
        get
        {
            return uxEmailAddress.Text;
        }
        set
        {
            uxEmailAddress.Text = value;
        }
    }
    public string Username
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public string PasswordHash
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public string Password
    {
        set
        {
        }
        get { return uxPassword.Text; }
    }
    public string SecurityQuestion
    {
        get
        {
            return uxSecurityQuestion.Text;
        }
        set
        {
            uxSecurityQuestion.Text = value;
        }
    }
    public string SecurityQuestionAnswer
    {
        get
        {
            return uxSecurityQuestionAnswer.Text;
        }
        set
        {
            uxSecurityQuestionAnswer.Text = value;
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.isErrorMessage = isErrorMessage;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.Message = message;
    }
    public bool ProceedToLoginOptionVisible
    {
        set
        {
            uxProceedToLogin.Visible = value;
        }
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
    public bool EmailAddressEnabled
    {
        get
        {
            return uxEmailAddress.Enabled;
        }
        set
        {
            uxEmailAddress.Enabled = value;
        }
    }
    public DateTime DateCreated
    {
        get;
        set;
    }
    public DateTime DateLastSaved
    {
        get;
        set;
    }
    public long SelectedNotificationDeliveryMethodId
    {
        get
        {
            return long.Parse(uxNotificationDeliveryMethod.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxNotificationDeliveryMethod, value);
        }
    }

    public void SetNotificationDeliveryMethod(Dictionary<long, string> items)
    {
        HelperClassDataElements.PopulateDropdownList(uxNotificationDeliveryMethod, items);
    }

    public long SelectedNotificationFrequencyMethodId
    {
        get
        {
            return long.Parse(uxNotificationFrequency.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxNotificationFrequency, value);
        }
    }

    public void SetNotificationFrequencyMethod(Dictionary<long, string> items)
    {
        HelperClassDataElements.PopulateDropdownList(uxNotificationFrequency, items);
    }

    /// <summary>
    /// Add the themes to the dropdown
    /// </summary>
    /// <param name="items"></param>
    public void SetThemeList(Dictionary<long, string> items)
    {
    }

    public double Latitude
    {
        get;
        set;
    }
    public double Longitude
    {
        get;
        set;
    }
    #endregion

    public void uxLoginRedirect_Click(object sender, EventArgs e)
    {

    }    
}