﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="UserRegisteredItems.aspx.cs" Inherits="Pages_User_UserHome" %>
<%@ Register Assembly="derotek.AspNet.Controls" Namespace="derotek.AspNet.Controls" TagPrefix="cc1" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">        
        <uc:PageSectionHeading runat="server" SectionHeadingId="RegisteredItems" BreakBefore="true"/>
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">
            <cc1:DataGrid ID="uxRegisteredItems"  runat="server" AutoGenerateColumns="False" 
                onrowcommand="uxRegisteredItems_RowCommand" OnPageIndexChanging="uxRegisteredItems_PageIndexChanging"> 
            <Columns>
                <asp:ImageField DataImageUrlField="PictureUrl" DataImageUrlFormatString="~/Uploads/Thumbs/{0}" AlternateText="Edit" ControlStyle-Height = "50px">                              
                </asp:ImageField>                
                <asp:BoundField DataField="PropertyType" HeaderText="Property type" SortExpression="PropertyType" />
                <asp:BoundField DataField="Description" HeaderText="Description" />
                <asp:BoundField DataField="HasAttachments" HeaderText="Attachments?" />
                <asp:BoundField DataField="IsStolen" HeaderText="Stolen\Missing?" />                
            </Columns>                
            </cc1:DataGrid>
            <br />
            <uc:Button runat="server" ID="uxAddNewRegisteredItem" OnClicked="uxAddNewRegisteredItem_Clicked" Text="Add new"  />  
            <br />
            <br />
        </div> 
    </div> 

</asp:Content>


