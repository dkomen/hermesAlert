﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ResetPassword.aspx.cs" Inherits="Pages_User_ResetPassword" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="asptk" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">                
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="ResetPassword" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">
            <asp:Table ID="Table1" runat="server" SkinID="Normal">
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label2" runat="server" SkinID="NormalNoWidthGiven">New Password</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxPassword" runat="server" TextMode="Password" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label6" runat="server" SkinID="NormalNoWidthGiven">Retype new password</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxRetypePassword" runat="server" TextMode="Password" />
                        <asptk:PasswordStrength ID="ps1" runat="server" TargetControlID="uxRetypePassword" DisplayPosition="RightSide"
                            PreferredPasswordLength="12" RequiresUpperAndLowerCaseCharacters="true" MinimumNumericCharacters="2" MinimumSymbolCharacters="2"
                            StrengthIndicatorType="BarIndicator" BarBorderCssClass="barBorder" BarIndicatorCssClass="barInternal"
                        />
                    </asp:TableCell>
                </asp:TableRow>                      
            </asp:Table>
            <br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />           
            <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click" />
            <uc:Button ID="uxProceedToLogin" runat="server" Text="Return to login..." OnClicked="uxProceedToLogin_Click" />
            <br />
            <br />
        </div>
    </div>
</asp:Content>

