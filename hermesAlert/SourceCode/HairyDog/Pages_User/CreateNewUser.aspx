﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CreateNewUser.aspx.cs" Inherits="Pages_User_CreateNewUser" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="asptk" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <style>
        .barBorder
        {
            border: solid 1px red;
            width: 150px;
            height:8px;
        }

        .barInternal
        {
            background: yellow;
        }
        .barInternalGreen
        {
            background: green;
        }
    </style>
    <div id="Div1" runat="server" class="Section" >                
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="NewAccount" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">
            <asp:Table ID="Table1" runat="server" SkinID="Normal" style="position:relative;top:30px;left:30px">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" >
                        <asp:Label ID="Label3" runat="server" SkinID="Normal">First name</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxFistName" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label4" runat="server" SkinID="Normal">Surname</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxSurname" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label1" runat="server" SkinID="Normal" Text="eMail Address" />
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxEmailAddress" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label2" runat="server" SkinID="Normal">Password</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxPassword" runat="server" TextMode="Password" />
                        <asp:pa
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label6" runat="server" SkinID="Normal">Retype Password</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxRetypePassword" runat="server" TextMode="Password" />
                        <asptk:PasswordStrength ID="ps1" runat="server" TargetControlID="uxRetypePassword" DisplayPosition="RightSide"
                            PreferredPasswordLength="12" RequiresUpperAndLowerCaseCharacters="true" MinimumNumericCharacters="2" MinimumSymbolCharacters="2"
                            StrengthIndicatorType="BarIndicator" BarBorderCssClass="barBorder" BarIndicatorCssClass="barInternal"
                            Enabled="true"
                        />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label5" runat="server" SkinID="Normal" Text="Security question" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxSecurityQuestion" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="Label9" runat="server" SkinID="SmallNoWidthGiven">e.g. What is my dogs name</asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label7" runat="server" SkinID="NormalNoWidthGiven" Text="Security question answer" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxSecurityQuestionAnswer" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="Label10" runat="server" SkinID="SmallNoWidthGiven">e.g. Georgie</asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow style="visibility:hidden">
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label8" runat="server" SkinID="Normal">Province</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:DropDownList SkinID="Normal" ID="uxProvinceList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="uxProvinceList_SelectedIndexChanged" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow style="visibility:hidden">
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label11" runat="server" SkinID="NormalWideText">How should hermesAlert send you incident notifications?</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:DropDownList SkinID="Normal" ID="uxNotificationDeliveryMethod" runat="server" AutoPostBack="false" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow style="visibility:hidden">
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label12" runat="server" SkinID="NormalWideText">How often do you want incident notifications?</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:DropDownList SkinID="Normal" ID="uxNotificationFrequency" runat="server" AutoPostBack="false" />
                    </asp:TableCell>
                </asp:TableRow>                
            </asp:Table><br />            
            <asp:Table runat="server" style="text-align:center;position:relative;top:-40px;left:25px">
                 <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="Label13" runat="server" SkinID="NormalHeading">Terms of service</asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell >









                    <div style="overflow:auto; height:175px; width:750px;text-align:left;background-color:#fdfdff;">
                        <b>IN SHORT :</b> You use this site fully at your own risk and agree that you will not defame any members of the public through its use, you also 
agree that hermesAlert will in no way be held liable in any manner whatsoever for what you or any member of the public decide to expose by using the hermesAlert site 
or the hermesAlert software engine.<br/>
<br/>

<b>PLEASE READ THE TERMS AND CONDITIONS OF USE CAREFULLY BEFORE USING THIS SITE. </b><br/>
<br/>
The hermesAlert website is free to use by our registered visitors.
By using this site, you the user are agreeing to comply with and be bound by the following terms and conditions. 
After reviewing the following terms of conditions thoroughly, if you do not agree to the terms and conditions then you are not allowed to 
use this site.<br/>
<br/>

<b>Acceptance of Agreement.</b> <br/>
You agree to the terms and conditions outlined in this Terms and Conditions of use Agreement (Agreement) with respect to our site (the Site, 
specifically the hermesAlert website, as well as the software engine on which it is based). 
This Agreement constitutes the entire and only agreement between us and you, and supersedes all prior or contemporaneous agreements, 
representations, warranties and understandings with respect to the Site, the content, free product samples or freebie offers or 
services provided by or listed on the Site, and the subject matter of this Agreement. 
This Agreement may be amended by us at any time and at any frequency without specific notice to you. The latest Agreement will be posted on 
the Site, and you should review this Agreement prior to using the Site.<br/><br/>

<b>Copyright.</b><br/>
The content, organization, graphics, design, and other matters related to the Site are protected under applicable copyrights and other proprietary 
laws, including but not limited to intellectual property laws. The copying, reproduction, use, modification or publication by you of any such 
matters or any part of the Site is strictly prohibited, without our express prior written permission.
<br/><br/>
<b>Deleting and Modification.</b><br/>
We reserve the right in our sole discretion, without any obligation and without any notice requirement to you, to edit or delete any documents, 
information or other content appearing on the Site, including this Agreement.
<br/><br/>
<b>Indemnification.</b><br/>
You agree to indemnify, defend and hold us, our officers, our share holders, our partners, attorneys and employees harmless from any and all 
liability, loss, damages, claim and expense, including reasonable attorney's fees, related to your violation of this Agreement or use of the Site.
<br/><br/>
<b>Disclaimer.</b><br/> 
THE CONTENT, SERVICES, FREE PRODUCT SAMPLES AND FREEBIE OFFERS FROM OR LISTED THROUGH THE SITE ARE PROVIDED "AS-IS," "AS AVAILABLE," AND ALL 
WARRANTIES, EXPRESS OR IMPLIED, ARE DISCLAIMED, INCLUDING BUT NOT LIMITED TO THE DISCLAIMER OF ANY IMPLIED WARRANTIES OF TITLE, NON-INFRINGEMENT, 
MERCHANTABILITY, QUALITY AND FITNESS FOR A PARTICULAR PURPOSE, WITH RESPECT TO THIS SITE AND ANY WEBSITE WITH WHICH IT IS LINKED. 
THE INFORMATION AND SERVICES MAY CONTAIN BUGS, ERRORS, PROBLEMS OR OTHER LIMITATIONS. WE HAVE NO LIABILITY WHATSOEVER FOR YOUR USE OF ANY 
INFORMATION OR SERVICE. IN PARTICULAR, BUT NOT AS A LIMITATION, WE ARE NOT LIABLE FOR ANY INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES (INCLUDING 
DAMAGES FOR LOSS OF BUSINESS, LOSS OF PROFITS, LOSS OF MONEY, LITIGATION, OR THE LIKE), WHETHER BASED ON BREACH OF CONTRACT, BREACH OF WARRANTY, 
NEGLIGENCE, PRODUCT LIABILITY OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE NEGATION OF DAMAGES SET FORTH ABOVE ARE FUNDAMENTAL 
ELEMENTS OF THE BASIS OF THE BARGAIN BETWEEN US AND YOU THE USER. 
THIS SITE AND THE INFORMATION WOULD NOT BE PROVIDED WITHOUT SUCH LIMITATIONS. NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU 
FROM US THROUGH THE SITE SHALL CREATE ANY WARRANTY, REPRESENTATION OR GUARANTEE NOT EXPRESSLY STATED IN THIS AGREEMENT. 
THE INFORMATION AND ALL OTHER MATERIALS ON THE SITE ARE PROVIDED FOR GENERAL INFORMATION PURPOSES ONLY AND DO NOT CONSTITUTE PROFESSIONAL ADVICE. 
IT IS YOUR RESPONSIBILITY TO EVALUATE THE ACCURACY AND COMPLETENESS OF ALL INFORMATION AVAILABLE ON THIS SITE OR ANY WEBSITE WITH WHICH IT IS LINKED.
<br/><br/>
<b>Limits.</b><br/>
All responsibility or liability for any damages caused by viruses contained within the electronic file containing the form or document is disclaimed. 
We will not be liable to you for any incidental, special or consequential damages of any kind that may result from use of or inability to use the site.
<br/><br/>
<b>Third-Party Website.</b><br/> 
All rules, terms and conditions, other policies (including privacy policies) and operating procedures of third-party linked websites will apply to you 
while on such websites. We are not responsible for the content, accuracy or opinions express in such Websites, and such Websites are not 
investigated, monitored or checked for accuracy or completeness by us. Inclusion of any linked Website on our Site does not imply approval or 
endorsement of the linked Website by us. This Site and the third-party linked websites are independent entities and neither party has authority 
to make any representations or commitments on behalf of the other. If you decide to leave our Site and access these third-party linked sites, you do 
so at your own risk.
<br /><br />
<b>Third-Party Products and Services.</b><br /> 
We advertise third-party linked websites from which you may purchase or otherwise obtain certain sample goods, freebie offerings or free trial services. 
You understand that we do not operate or control the products, free offerings or services offered by third-party linked websites. 
Third-party linked websites are responsible for all aspects of order processing, fulfillment, billing and customer service. We are not a party to the 
transactions entered into between you and third-party linked websites. You agree that use of such third-party linked websites is at your sole risk and 
is without warranties of any kind by us, expressed, implied or otherwise. Under no circumstances are we liable for any damages arising from the 
transactions between you and third-party linked websites or for any information appearing on third-party linked websites or any other site linked to 
or from our site.
<br/><br/>
<b>Submissions.</b><br /> 
All suggestions, ideas, notes, images, concepts and other information you may send to us (collectively, "Submissions") shall be deemed and shall remain 
our sole property and shall not be subject to any obligation of confidence on our part from the moment and at any instant in time which such suggestions, 
ideas, notes, images, concepts and other information were indicated by you to be public to the other website users. 
Without limiting the foregoing, we shall be deemed to own all known and hereafter existing rights of every kind and nature regarding the Submissions 
and shall be entitled to unrestricted use of the Submissions for any purpose, without compensation to the provider of the Submissions.
No submissions of any kind should defame any member of the public nor include hate-speach, racism nor derogatory opinions.
<br /><br />
<b>General.</b><br />
You agree that all actions or proceedings arising directly or indirectly out of this agreement, or your use of the site or any sample products, freebie 
offers or services obtained by you through such use, shall be litigated in the legal courts of the country of South Africa. 
<br /><br />
You are expressly submitting and consenting in advance to such jurisdiction in any action or proceeding in any of such courts, and are waiving any 
claim that that the legal courts in the country of South Africa is an inconvenient forum or an improper forum based on lack of venue. 
<br /><br />
This site is controlled by Dean Komen in the country of South Africa. As such, the laws of South Africa will govern the terms and conditions contained 
in this Agreement and elsewhere throughout the Site, without giving effect to any principles of conflicts of laws.                    
                    </div> 
                    
                    
                    
                    
                    
                    
                    
                    
                                                                                     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>                        
                        <asp:CheckBox runat="server" ID="uxAcceptTOS" skinid="Normal" onclick="javascript:enableSaveButton();" Text="To continue you must accept the Terms of Service" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br /> 
            <uc:Button ID="uxProceedToLogin" runat="server" Text="Return to login..." OnClicked="uxProceedToLogin_Click" />          
            <span id="uiButtonHider" style="visibility:visible;">
                <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click"  />
            </span>            
            <br />
            <br />
        </div>
    </div>
    <script type="text/javascript">

        function enableSaveButton() {
            saver = document.getElementById('uiButtonHider');
            checkBox = document.getElementById('MainContent_ContentPlaceHolder1_uxAcceptTOS');

            if (checkBox.checked == true) {
                saver.style.visibility = "visible";
            }
            else {
                saver.style.visibility = "hidden";
            }
        }
    </script>
</asp:Content>
