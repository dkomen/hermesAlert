﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RegisterUser.aspx.cs" Inherits="Pages_User_RegisterUser" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div id="Div1" runat="server" class="Section">                
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="Profile" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">
            <asp:Table ID="Table1" runat="server" SkinID="Normal">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label3" runat="server" SkinID="Normal">First name</asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxFistName" runat="server" />
                    </asp:TableCell></asp:TableRow><asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label4" runat="server" SkinID="Normal">Surname</asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxSurname" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label1" runat="server" SkinID="Normal" Text="eMail Address" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxEmailAddress" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label2" runat="server" SkinID="Normal">Password</asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxPassword" runat="server" TextMode="Password" />
                    </asp:TableCell><asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label6" runat="server" SkinID="Normal">Retype Password</asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxRetypePassword" runat="server" TextMode="Password" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label5" runat="server" SkinID="Normal" Text="Security question" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxSecurityQuestion" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="Label9" runat="server" SkinID="SmallNoWidthGiven">e.g. What is my cats name</asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label7" runat="server" SkinID="NormalNoWidthGiven" Text="Security question answer" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="Normal" ID="uxSecurityQuestionAnswer" runat="server" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:Label ID="Label10" runat="server" SkinID="SmallNoWidthGiven">e.g. Tinkie</asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label8" runat="server" SkinID="Normal">Province</asp:Label>
                    </asp:TableCell><asp:TableCell HorizontalAlign="Left">
                        <asp:DropDownList SkinID="Normal" ID="uxProvinceList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="uxProvinceList_SelectedIndexChanged" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label11" runat="server" SkinID="NormalNoWidthGiven">How should hermesAlert send you incident notifications?</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:DropDownList SkinID="Normal" ID="uxNotificationDeliveryMethod" runat="server" AutoPostBack="false" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label12" runat="server" SkinID="NormalNoWidthGiven">How often do you want incident notifications?</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:DropDownList SkinID="Normal" ID="uxNotificationFrequency" runat="server" AutoPostBack="false" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="middle">
                        <asp:Label ID="Label14" runat="server" SkinID="NormalWideText" style="position:relative;top:2px">Incident location : </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="3">                           
                        <asp:Label ID="Label15" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Longitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLongitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;                        
                        <asp:Label ID="Label16" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Latitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLatitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;
                        <uc:Button ID="uxSelectGpslocation" runat="server" Text="Select gps location" OnClicked="uxSelectGpslocation_Click" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label13" runat="server" SkinID="NormalNoWidthGiven">Theme</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:DropDownList SkinID="Normal" ID="uxUiTheme" runat="server" AutoPostBack="false" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table><br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />            
            <asp:Label ID="Label17" runat="server" SkinID="SmallWarningText">Some changes will require you to logout and then sign back in again</asp:Label>
            <br /><br />      
            <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click" />
            <uc:Button ID="uxProceedToLogin" runat="server" Text="login..." OnClicked="uxProceedToLogin_Click" />
            <br />
            <br />
        </div>
    </div>
</asp:Content>
