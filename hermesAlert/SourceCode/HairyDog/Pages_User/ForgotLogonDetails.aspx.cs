﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_User_ForgotLogonDetails : PageBase, derotek.BikeCasa.Mvp.User.Views.IForgotLogonCredentialsView
{
    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Forgot Logon Details";
        if (!IsPostBack)
        {
            uxDisplayMessage.Visible = false;
        }
    }
    protected void uxSendEMail_Click(object sender, EventArgs e)
    {
        new derotek.BikeCasa.Mvp.User.Presenters.ForgotLogonCredentialsPresenter(this).SendResetDetails();
    }
    protected void uxProceedToLogin_Click(object sender, EventArgs e)
    {
        RedirectView("~/default.aspx");
    }

    #endregion

    public string EMailAddress
    {
        get
        {
            return uxEmailAddress.Text;
        }
        set
        {
            uxEmailAddress.Text = value;
        }
    }

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.isErrorMessage = isErrorMessage;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.Message = message;
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
}