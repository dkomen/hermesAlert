﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_User_RegisterUser : PageBaseLocationTransfers, derotek.BikeCasa.Mvp.User.Views.IUserMaintenance
{
    #region Fields
    private derotek.BikeCasa.Mvp.User.Presenters.UserMaintenancePresenter _presenter;    
    #endregion   

    #region Form Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Profile";
        _presenter = new derotek.BikeCasa.Mvp.User.Presenters.UserMaintenancePresenter(this);
        if (!IsPostBack)
        {
            uxEmailAddress.MaxLength = derotek.Configuration.FieldLengths.Email;
            uxFistName.MaxLength = derotek.Configuration.FieldLengths.PersonName;
            uxSurname.MaxLength = derotek.Configuration.FieldLengths.PersonSurname;
            uxPassword.MaxLength = derotek.Configuration.FieldLengths.Password;
            uxRetypePassword.MaxLength = derotek.Configuration.FieldLengths.Password;
            uxSecurityQuestion.MaxLength = derotek.Configuration.FieldLengths.SecurityQuestion;
            uxSecurityQuestionAnswer.MaxLength = derotek.Configuration.FieldLengths.SecurityQuestion;

            derotek.HairyDog.Users.User user = SessionVariables.CurrentUserGet(this.Session);

            derotek.BikeCasa.Mvp.User.Presenters.StateUserProfile sessionItem = (derotek.BikeCasa.Mvp.User.Presenters.StateUserProfile)Session[this.AppRelativeVirtualPath];
            if (sessionItem != null && Session["Longitude"] != null && Session["Longitude"] != null)
            {
                sessionItem.Latitude = double.Parse(Session["Latitude"].ToString());
                sessionItem.Longitude = double.Parse(Session["Longitude"].ToString());
            }

            _presenter.PrepareFieldDefaults(user.Id, sessionItem);
            Session[this.AppRelativeVirtualPath] = null;



            uxDisplayMessage.Visible = false;
        }
    }
    protected void uxSave_Click(object sender, EventArgs e)
    {
        _presenter.SaveUser();
    }
    protected void uxProceedToLogin_Click(object sender, EventArgs e)
    {       
        _presenter.RedirectToLogonView();
    }
    protected void uxProvinceList_SelectedIndexChanged(object sender, EventArgs e)
    {
        _presenter.SelectedProvinceChanged(long.Parse(uxProvinceList.SelectedValue));
    }
    protected void uxSelectGpslocation_Click(object sender, EventArgs e)
    {
        CallGpsPage("~/Pages_Main/SelectGpsLocation.aspx");
    }
    #endregion

    #region View
    public override double Latitude
    {
        get
        {
            return double.Parse(uxGpsLatitudeSelected.Text != string.Empty ? uxGpsLatitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLatitudeSelected.Text = value.ToString();
            
        }
    }
    public override double Longitude
    {
        get
        {
            return double.Parse(uxGpsLongitudeSelected.Text != string.Empty ? uxGpsLongitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLongitudeSelected.Text = value.ToString();
        }
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    public long Id
    {
        get;
        set;
    }
    public int UserRating
    {
        get
        {
            return 0;
        }
        set
        {
           
        }
    }
    public string VerifyPassword
    {
        get
        {
            return uxRetypePassword.Text;
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public long SelectedProvinceId
    {
        get
        {
            return long.Parse(uxProvinceList.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxProvinceList, value);
        }
    }
    public long? SelectedTownId
    {
        get
        {
            return null;
        }
        set
        {
            
        }
    }
    public void SetProvincesList(Dictionary<long, string> items)
    {
        HelperClassDataElements.PopulateDropdownList(uxProvinceList, items);
    }
    public void SetTownsList(Dictionary<long, string> items)
    {
        
    }
    public string Name
    {
        get
        {
            return uxFistName.Text;
        }
        set
        {
            uxFistName.Text=value;
        }
    }
    public string Surname
    {
        get
        {
            return uxSurname.Text;
        }
        set
        {
            uxSurname.Text = value;
        }
    }
    public string ContactNumber
    {
        get
        {
            return string.Empty;
        }
        set
        {
            //uxContactNumber.Text = value;
        }
    }
    public string EMailAddress
    {
        get
        {
            return uxEmailAddress.Text;
        }
        set
        {
            uxEmailAddress.Text = value;
        }
    }
    public string Username
    {
        get
        {
            return string.Empty;
        }
        set
        {
            
        }
    }
    public string PasswordHash
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public string Password
    {
        set
        {
        }
        get { return uxPassword.Text; }
    }
    public string SecurityQuestion
    {
        get
        {
            return uxSecurityQuestion.Text;
        }
        set
        {
            uxSecurityQuestion.Text = value;
        }
    }
    public string SecurityQuestionAnswer
    {
        get
        {
            return uxSecurityQuestionAnswer.Text;
        }
        set
        {
            uxSecurityQuestionAnswer.Text = value;
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.isErrorMessage = isErrorMessage;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.Message = message;
    }
    public bool ProceedToLoginOptionVisible
    {
        set
        {
            uxProceedToLogin.Visible = value;
        }
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
    public bool EmailAddressEnabled 
    {
        get
        {
            return uxEmailAddress.Enabled;
        }
        set
        {
            uxEmailAddress.Enabled = value;
        }
    }
    public DateTime DateCreated
    {
        get;
        set;
    }
    public DateTime DateLastSaved
    {
        get;
        set;
    }
    public long SelectedNotificationDeliveryMethodId
    {
        get
        {
            return long.Parse(uxNotificationDeliveryMethod.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxNotificationDeliveryMethod, value);
        }
    }
    public string UserVerificationHashRequirement
    {
        get;
        set;
    }
    
    public void SetNotificationDeliveryMethod(Dictionary<long, string> items)
    {
        HelperClassDataElements.PopulateDropdownList(uxNotificationDeliveryMethod, items);
    }
    public long SelectedNotificationFrequencyMethodId
    {
        get
        {
            return long.Parse(uxNotificationFrequency.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxNotificationFrequency, value);
        }
    }
    public void SetNotificationFrequencyMethod(Dictionary<long, string> items)
    {
        HelperClassDataElements.PopulateDropdownList(uxNotificationFrequency, items);
    }

    public string Theme
    {
        get
        {
            return uxUiTheme.SelectedItem.Text;
        }
        set
        {
            foreach (ListItem item in uxUiTheme.Items)
            {
                if (item.Text.ToLower() == value.ToLower())
                {
                    uxUiTheme.SelectedValue = item.Value;
                }
            }
        }
    }
    /// <summary>
    /// Add the themes to the dropdown
    /// </summary>
    /// <param name="items"></param>
    public void SetThemeList(Dictionary<long, string> items)
    {
        HelperClassDataElements.PopulateDropdownList(uxUiTheme, items);
    }


    #endregion           

    public override void RePopulatePageFields(string pageName)
    {
        uxGpsLongitudeSelected.Text = this.ViewState["Longitude"].ToString();
        uxGpsLatitudeSelected.Text = this.ViewState["Longitude"].ToString();
    }

    public override void CallGpsPage(string pageToCallForSelectingGpsData)
    {
        Session["Longitude"] = Longitude;
        Session["Latitude"] = Latitude;

        derotek.BikeCasa.Mvp.User.Presenters.StateUserProfile state = new derotek.BikeCasa.Mvp.User.Presenters.StateUserProfile();
        state.ActiveObjectId = this.ActiveObjectId;
        state.ContactNumber = this.ContactNumber;
        state.EMailAddress = this.EMailAddress;
        state.EmailAddressEnabled = this.EmailAddressEnabled;
        state.Latitude = this.Latitude;
        state.Longitude = this.Longitude;
        state.Name = this.Name;
        state.ObjectId = this.AppRelativeVirtualPath;
        state.Password = string.Empty;
        state.ReturnToPage = this.ReturnToPage;
        state.SecurityQuestion = this.SecurityQuestion;
        state.SecurityQuestionAnswer = this.SecurityQuestionAnswer;
        state.SelectedNotificationDeliveryMethodId = this.SelectedNotificationDeliveryMethodId;
        state.SelectedNotificationFrequencyMethodId = this.SelectedNotificationFrequencyMethodId;
        state.SelectedProvinceId = this.SelectedProvinceId;
        state.SelectedTownId = this.SelectedTownId;
        state.Surname = this.Surname;
        state.Theme = this.Theme;
        state.Username = this.Username;
        state.UserRating = this.UserRating;
        state.UserVerificationHashRequirement = this.UserVerificationHashRequirement;
        state.VerifyPassword = this.VerifyPassword;

        state.KeepAliveForPages.Add(this.AppRelativeVirtualPath);
        state.KeepAliveForPages.Add("~/Pages_Main/SelectGpsLocation.aspx");

        Session[state.ObjectId] = state;

        _presenter.RedirectToSelectGpsLocation(this.AppRelativeVirtualPath);
    }

    public void uxLoginRedirect_Click(object sender, EventArgs e)
    {

    }    
}