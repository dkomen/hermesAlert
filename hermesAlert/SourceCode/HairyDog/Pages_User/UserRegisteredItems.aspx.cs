﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_User_UserHome : PageBase, derotek.BikeCasa.Mvp.Registrations.IRegisteredItems
{
    #region Fields
    private derotek.BikeCasa.Mvp.Registrations.RegistrationsPresenter _presenterRegistereditems;
    #endregion

    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Registrations"; 
        _presenterRegistereditems = new derotek.BikeCasa.Mvp.Registrations.RegistrationsPresenter(this);
        if (!IsPostBack)
        {
            _presenterRegistereditems.PrepareView();            
        }
    }
    protected void uxRegisteredItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {
            
            long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            _presenterRegistereditems.Edit(id, Page.AppRelativeVirtualPath);
        }
    }
    protected void uxAddNewRegisteredItem_Clicked(object sender, EventArgs e)
    {
        _presenterRegistereditems.RedirectToAddNewRegisteredItem(Page.AppRelativeVirtualPath);
    }
    protected void uxRegisteredItems_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _presenterRegistereditems.PrepareView();
        ((GridView)sender).PageIndex = e.NewPageIndex;
        ((GridView)sender).DataBind();
    }  
    #endregion

    #region View IRegisteredItems
    public List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> RegisteredObjects
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxRegisteredItems.DataKeyNames = new string[] { "Id" };
            uxRegisteredItems.DataSource = value;
            uxRegisteredItems.DataBind();
        }
    }
    public long CurrentUserId
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session).Id;
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        throw new NotImplementedException();
    }
    public void RedirectView(string path)
    {
        Server.Transfer(path,true);
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    #endregion
}
            
            