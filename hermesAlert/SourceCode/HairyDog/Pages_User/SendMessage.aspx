﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SendMessage.aspx.cs" Inherits="Pages_User_SendMessage" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section" >                
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="NewItemMessage" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">
            <asp:Table ID="Table1" runat="server" SkinID="Normal">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right"> 
                        <asp:Label ID="Label3" runat="server" SkinID="NormalNoWidthGiven">Private message</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:CheckBox SkinID="Normal" ID="uxKeepMessagePrivate" runat="server" Checked="True" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                        <asp:Label ID="Label4" runat="server" SkinID="Normal">Notes</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="NormalTextBlock" ID="uxNotes" runat="server" TextMode="MultiLine" Rows="5" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="top">
                        <asp:Label ID="Label9" runat="server" SkinID="NormalNoWidthGiven">Location information</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="2">                           
                        <asp:Label ID="Label7" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Longitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLongitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;                        
                        <asp:Label ID="Label8" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Latitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLatitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;
                        <uc:Button ID="uxSelectGpslocation" runat="server" Text="Select gps location" OnClientClick="javascript:showList();" AutoPostBack="false" />                                                                                                          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label10" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Hide coordinates from public</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="uxLocationAccuracyWhenPublic" runat="server" SkinID='Normal' />  
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            <br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />           
            <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click" />
            <uc:Button ID="uxReply" runat="server" Text="Reply" OnClientClick="javascript:alert('Sorry, this will only work in a few days time'); return false;" OnClicked="uxReply_Click" />
            <uc:Button ID="uxReturn" runat="server" Text="Return..." OnClicked="uxReturn_Click" />
            <br />
            <br />
        </div>
    </div>
</asp:Content>

