﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BaseMasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (SessionVariables.CurrentUserGet(Session).Id == 0
             && (!Request.PhysicalPath.ToLower().EndsWith("default.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("logon.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("createnewuser.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("faq.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("forgotlogondetails.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("activate.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("recievemessage.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("showgps.aspx")
             && !Request.PhysicalPath.ToLower().EndsWith("showmapfromgps.aspx")   
             && !Request.PhysicalPath.ToLower().EndsWith("resetpassword.aspx")))
        {
            //Response.Redirect("~/Pages_User/Logon.aspx");
            Response.Redirect("~/default.aspx");
        }
    }

    public string RootOfApplication()
    {
        return SessionVariables.RootPath(true); 
    }
}
