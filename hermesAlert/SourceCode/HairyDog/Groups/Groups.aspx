﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Groups.aspx.cs" Inherits="Groups_Groups" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="GoogleMultiMap" Src="~/WebControls/GoogleMultiMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register Assembly="derotek.AspNet.Controls" Namespace="derotek.AspNet.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">
        <uc:PageSectionHeading ID="PageSectionHeading2" runat="server" SectionHeadingId="GroupMembership" BreakBefore="true"/>
        <div id="Div44" runat="server" style="width:1185px;position:relative;left:5px">       
            <cc1:DataGrid ID="uxCurrentGroups"  runat="server" AutoGenerateColumns="False" onrowcommand="uxCurrentGroups_RowCommand" OnPageIndexChanging="uxCurrentGroups_PageIndexChanging"> 
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />              
                </Columns>  
            </cc1:DataGrid>
            <br />        
            <uc:Button id="uxCreateNewGroup" runat="server" Text="Create new group" onclicked="uxCreateNewGroup_Click" />            
        </div>
        <br />
    </div>
    <br />
    <div id="Div3" runat="server" class="Section">
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="AvailableGroups" BreakBefore="true"/>
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">       
            <cc1:DataGrid ID="uxAvailableGroups"  runat="server" AutoGenerateColumns="False" onrowcommand="uxAvailableGroups_RowCommand" OnPageIndexChanging="uxAvailableGroups_PageIndexChanging"> 
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />              
                </Columns>  
            </cc1:DataGrid>
            <div id="searchButton" style="position:relative;left:844px">
                 <asp:TextBox SkinID="NormalWideText" ID="uxDescription" runat="server" />
                 <uc:Button id="uxSearch" runat="server" Text="Search" onclicked="uxSearchAvailableGroups_Click" />  
            </div>
            <br />         
        </div>
        <br/>
    </div>
</asp:Content>

