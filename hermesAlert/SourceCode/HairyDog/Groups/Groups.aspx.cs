﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Groups_Groups : PageBase, derotek.BikeCasa.Mvp.Groups.IGroupListing
{
    #region Form Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            new derotek.BikeCasa.Mvp.Groups.GroupListingPresenter(this);
        }
        
    }

    protected void uxCurrentGroups_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {
            //long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            //string returnToPage = this.Page.AppRelativeVirtualPath;
            //(new derotek.BikeCasa.Mvp.MonitoringAreas.MonitoringAreasPresenter(this)).ShowItem(id, Page.AppRelativeVirtualPath);
        }
    }
    protected void uxCurrentGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //_presenterMonitoringAreas.GetStolenItems(long.Parse(uxPropertyTypeList.SelectedValue));
        //((GridView)sender).PageIndex = e.NewPageIndex;
        //((GridView)sender).DataBind();
    }
    protected void uxCreateNewGroup_Click(object sender, EventArgs e)
    {
    }
    
    protected void uxAvailableGroups_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {
            //long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            //string returnToPage = this.Page.AppRelativeVirtualPath;
            //(new derotek.BikeCasa.Mvp.MonitoringAreas.MonitoringAreasPresenter(this)).ShowItem(id, Page.AppRelativeVirtualPath);
        }
    }
    protected void uxAvailableGroups_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //_presenterMonitoringAreas.GetStolenItems(long.Parse(uxPropertyTypeList.SelectedValue));
        //((GridView)sender).PageIndex = e.NewPageIndex;
        //((GridView)sender).DataBind();
    }
    protected void uxSearchAvailableGroups_Click(object sender, EventArgs e)
    {
    }
    #endregion



    public derotek.HairyDog.Users.User CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public void DisplayMessage(string message, bool isErrorMessage)
    {
        throw new NotImplementedException();
    }

    public void RedirectView(string path)
    {
        throw new NotImplementedException();
    }

    public List<derotek.HairyDog.DisplayableItems.DisplayableGroup> CurrentGroupMemberships
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxCurrentGroups.DataKeyNames = new string[] { "Id" };
            uxCurrentGroups.DataSource = value;
            uxCurrentGroups.DataBind();
        }
    }

    public long SelectedMembershipGroupId
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public List<derotek.HairyDog.DisplayableItems.DisplayableGroup> AvailableGroups
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxAvailableGroups.DataKeyNames = new string[] { "Id" };
            uxAvailableGroups.DataSource = value;
            uxAvailableGroups.DataBind();
        }
    }

    public long SelectedAvailableGroupId
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    List<derotek.HairyDog.DisplayableItems.DisplayableJoinedGroups> derotek.BikeCasa.Mvp.Groups.IGroupListing.CurrentGroupMemberships
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
}