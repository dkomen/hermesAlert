﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Faq.aspx.cs" Inherits="Pages_Main_Faq" %>
<%@ Register Src="~/WebControls/PageSectionHeading.ascx" TagName="PageSectionHeading" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">              
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="FaqHeading" BreakBefore="true" />
        <br />
        <div id="Div3" runat="server" style="position:relative;left:15px;width:1135px">            
            <a href="#WhatIshermesAlert"><asp:Label runat="server" SkinID="NormalParagraph" Text="1.What is hermesAlert?" /></a>
            <br /><a href="#HowRegisterItem"><asp:Label ID="Label2" runat="server" SkinID="NormalParagraph" Text="2.How to register a personal item" /></a>
            <br /><a href="#HowMonitorItems"><asp:Label ID="Label3" runat="server" SkinID="NormalParagraph" Text="3.How to monitor missing/stolen items in your area" /></a>
            <br /><a href="#HowSearchDatabase"><asp:Label ID="Label4" runat="server" SkinID="NormalParagraph" Text="4.How to search the entire community database for missing/stolen items" /></a>
            <br /><a href="#HowReportIncidents"><asp:Label ID="Label13" runat="server" SkinID="NormalParagraph" Text="5.How to report incidents" /></a>
            <br /><a href="#ActivityPoints"><asp:Label ID="Label14" runat="server" SkinID="NormalParagraph" Text="6.What are activity points" /></a>

            <br /><br />
            <a id="WhatIshermesAlert">
                <asp:Label runat="server" SkinID="NormalHeading" Text="1.What is hermesAlert?" />
            </a>
            <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/ShowIt.jpg" class="corners" style="padding-left:50px;padding-bottom:15px;float:right;" />
            <br />
            <asp:Label ID="Label1" runat="server" SkinID="NormalParagraph" style="position:relative;left:35px">
                hermesAlert is a South African social network through which you may keep up to date with community reported events such as traffic accidents, vehicle hijackings, muggings, assualts etc. As well as 
                the theft of fellow community members' property and missing persons.
                <br />
                <br />
                 Here you may also maintain a list of all your valuable personal belongings by registering them together with any photos, serial numbers and the like that you may have.
                 These items are all private and visible to only you, however, if any of your registered items disappear or are stolen you may change the status of the specific item
                  effected to a new status and indicate on a map where it was last seen, additionaly you have the option of informing the community about its new status.
                <br />
                The community will now be able to see all the items details... and send you a message if they have seen the item in question.
                <br />
                <br />
                Community members have the option of searching a database of all the publicly listed stolen or missing items. If you happen to know the
                 whereabouts of a community members' item you may send them an anonymous message informing them of pertinent 
                 information such as where it was seen.
                <br />
                <br />
                Your privacy is important therefore events you report and messages you send are all anynomous and your personal item registrations are all private,
                 we will not inform the community of who you are. It is important that you do not include self identifying information in your descriptions and
                  pictures as at some point the community may view it.           
                <br />
                <br />
                The success of hermesAlert is dependant on a large community, the larger the community the more eyes we have to help find missing items and
                to report events. Every feasible attempt will be made to keep your identity and personal items private. The use of the hermesAlert system is at your own risk.
            </asp:Label>
            <br />
            <br />
            
            <a id="HowRegisterItem">
                <asp:Label ID="Label6" runat="server" SkinID="NormalHeading" Text="2.How to register a personal item" />
            </a>
            <br />
            <asp:Label ID="Label7" runat="server" SkinID="NormalParagraph" style="position:relative;left:30px">
                Click on the main menu option labelled 'Registrations'.<br />
                <br />
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/RegisteredItems.jpg" class="corners" style="padding-left:50px;padding-bottom:15px;float:right;" />
                Add all your personal items of value (motor vehicles, motorcycles, cameras, televisions, bicycles, cell phones etc) to the system, these items are all
                 private to you, however if they disappear you can inform the community about it at which point the specific item in question becomes publicly viewable.
                <br />
                <br />                
                During the process of registering an item, and after its initial saving to the database, you have the ability to add picture attachments to
                 the item. These attachments may be indicated to be of a specific type (serial number, model number, registration number etc) and it is these
                 attachments that will be searchable if the item has been reported missing or stolen and has also been made viewable to the community.
                <br />
                <br />
                When uploading a new picture file please be patient and wait for the upload to complete as this may take some time due to the possible speed
                 constraints of the network you are on. If you have an Android 2.2 based mobile phone you may also install our hermesAlert mobile uploader so that
                 you have the ability to take photos from you phone and upload them directly to your hermesAlert account.
            </asp:Label>
            <br />
            <br />

            <a id="HowMonitorItems">
                <asp:Label ID="Label8" runat="server" SkinID="NormalHeading" Text="3.How to monitor missing/stolen items in your area" />
            </a>
            <br />
            <asp:Label ID="Label9" runat="server" SkinID="NormalParagraph" style="position:relative;left:30px">
                Click on the main menu option labelled 'Monitoring'.<br />
                <br />
                In order for you to see the community items that have been publicly listed as having been stolen or missing in a specific area (perhaps Sandton
                 in Johannesburg or even Washington DC) you will need to specify, on a map, the geographic area (or areas) you are interested in monitoring.
                <br />
                Once you have specified the areas you are interested in you can perform small searches based on the type of item (bicycle, electonics, cell phone,
                 sport equipment etc.) to retrieve and view on a map the items which were found in your monitoring areas.
            </asp:Label>
            <br />
            <br />

            <a id="HowSearchDatabase">
                <asp:Label ID="Label5" runat="server" SkinID="NormalHeading" Text="4.How to search the entire community database for missing/stolen items" />
            </a>
            <br />
            <asp:Label ID="Label10" runat="server" SkinID="NormalParagraph" style="position:relative;left:30px">
                Click on the main menu option labelled 'Search'.<br />
                <br />
                You can search the database of all the stolen or missing items that have been reported by the community and which has been specified
                 as community viewable by the items owner.
                 <br />
                 <br />
                 Enter a few characters to search for and see the results when you select the 'Search' button. The search is performed on all the attachments 
                 that have been added to a registered item.
                 <br />
                 Example : If you have found a bicycle with a serial number 'WX 1234A0987' then you can search for that exact number, but if
                 only a few characters are clearly legible, like perhaps 'WX 123' or '0987' then you may enter only those characters and still find a match
                 in the database.
            </asp:Label>
            <br />
            <br />

            <a id="HowReportIncidents">
                <asp:Label ID="Label11" runat="server" SkinID="NormalHeading" Text="5.How to report incidents" />
            </a>
            <br />
            <asp:Label ID="Label12" runat="server" SkinID="NormalParagraph" style="position:relative;left:30px">
                Click on the main menu option labelled 'Incidents'.<br />
                <br />
                Incident reporting allows you to quicly inform not only the hermesAlert community but also the public at large (via a map of incidents on the logon page)
                 about incidents that have taken place.
                <br />
                Reporting includes indicating what type of incident took place (assualt, attempted vehicle hijack etc), a very quick description, the date it occured as 
                well as its location by selecting, on a map, the gps coordinates where it took place.
            </asp:Label>
            <br />
            <br />

            <a id="ActivityPoints">
                <asp:Label ID="Label15" runat="server" SkinID="NormalHeading" Text="6.What are activity points" />
            </a>
            <br />
            <asp:Label ID="Label16" runat="server" SkinID="NormalParagraph" style="position:relative;left:30px">
                You receive activity points for reporting incidents and for registering personal items with the hermesAlert system.
                <br /><br />10 points for every item you register... and lose 5 points for every item you delete.
                <br />5 points for every incident you report... and lose 2 points for every deletion.
                <br />20 points for reporting a missing person... and lose 10 points for every deletion.
                <br><br />You also receive 50 points for signing up to join the system and 1 point everytime you sign into the system.
            </asp:Label>
            <br />
            <br />
        </div>
        <br />
        <br />

    </div>   
</asp:Content>

