﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Main_ShowMapFromGps : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            if (Request.QueryString["long"] != null && Request.QueryString["lat"]!=null)
            {
                uxGoogleMultiMap.Longitude = double.Parse(Request.QueryString["long"]);
                uxGoogleMultiMap.Latitude = double.Parse(Request.QueryString["lat"]);
                if (Request.QueryString["pt"] != null)
                {
                    uxGoogleMultiMap.PropertyTypeToShow = long.Parse(Request.QueryString["pt"]);
                }
                else
                {
                    uxGoogleMultiMap.PropertyTypeToShow = 0;
                }
            }
            else if (Request.QueryString["id"] != null)
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    derotek.HairyDog.Incidents.Incident incident = transaction.Retrieve<derotek.HairyDog.Incidents.Incident>(long.Parse(Request.QueryString["id"]));
                    uxGoogleMultiMap.Longitude = incident.Longitude;
                    uxGoogleMultiMap.Latitude = incident.Latitude;
                }
                if (Request.QueryString["pt"] != null)
                {
                    uxGoogleMultiMap.PropertyTypeToShow = long.Parse(Request.QueryString["pt"]);
                }
                else
                {
                    uxGoogleMultiMap.PropertyTypeToShow = 0;
                }
            }
        }
    }
}