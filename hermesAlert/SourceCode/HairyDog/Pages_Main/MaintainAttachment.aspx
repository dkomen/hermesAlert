﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainAttachment.aspx.cs" Inherits="Pages_Main_MaintainAttachment" %>
<%@ Register Src="~/WebControls/ImageUploader.ascx" TagPrefix="uc" TagName="ImageUploader" %>
<%@ Register Src="~/WebControls/PageSectionHeading.ascx" TagPrefix="uc" TagName="PageSectionHeading" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="AvailableAttachments" Src="~/WebControls/AvailableAttachments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section" >              
        <uc:PageSectionHeading runat="server" SectionHeadingId="AttachmentMaintenance" BreakBefore="true" />
        <br />
        <asp:Table ID="Table1" runat="server" style="width:1185; " > 
            <asp:TableRow>
                <asp:TableCell style="width:340px;padding:5px;vertical-align:top">
                    <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="UploadedAttachment" BreakBefore="true" />
                    <uc:ImageUploader id="uxImageUploader" runat="server" />    
                </asp:TableCell>
                <asp:TableCell style="width:828px;padding:5px;vertical-align:top">
                    <uc:AvailableAttachments ID="uxAvailableAttachments" runat="server" />        
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
         <br /> <br />
        <div id="Div3" runat="server" style="position:relative;left:5px">
            <asp:Table ID="Table2" runat="server" SkinID="Normal" >    
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="right" Width="250px" Height="34px" VerticalAlign="top">                        
                        <asp:Label runat='server' ID='Label3' SkinID='NormalNoWidthGiven' Text='Which image?'/>                                                                            
                    </asp:TableCell>
                    <asp:TableCell>                
                        <asp:RadioButton GroupName="SelectedImage" ID="uxUseNewUploadedImage" runat="server" SkinID="Normal" Text="Newly uploaded image" />
                        <br />
                        <asp:RadioButton GroupName="SelectedImage" ID="uxUseSelectedAvailableImage" runat="server" SkinID="Normal" Text="Selected image from 'Available Attachments'"/>
                        <br />
                        <asp:RadioButton Checked="true" GroupName="SelectedImage" ID="uxUseNoImage" runat="server" SkinID="Normal" Text="Don't use any image" />
                    </asp:TableCell>
                </asp:TableRow>  
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="right">                        
                        <asp:Label runat='server' ID='Label1' SkinID='NormalNoWidthGiven' Text='Short description'/>                                                                            
                    </asp:TableCell>
                    <asp:TableCell>                
                        <asp:TextBox ID="uxDescription" runat="server" SkinID="NormalWideText"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>        
                <asp:TableRow>                    
                    <asp:TableCell HorizontalAlign="right" Height="34px">                        
                        <asp:Label runat='server' ID='Label2' SkinID='NormalNoWidthGiven' Text='What does this image represent?'/>                                                                            
                    </asp:TableCell>
                    <asp:TableCell>                
                        <asp:DropDownList ID="uxIdentificationNumberType" runat="server" SkinID="Normal" EnableViewState="true" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
            
        
            <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click" /> 
            <uc:Button ID="uxDelete" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this attachment?');" OnClicked="uxDelete_Click" /> 
            <uc:Button ID="uxReturn" runat="server" Text="Return..." OnClicked="uxReturn_Click" /> 
        </div>
        <br /> <br />
       
    </div>
</asp:Content>

