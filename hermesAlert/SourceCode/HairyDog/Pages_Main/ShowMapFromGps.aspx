﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ShowMapFromGps.aspx.cs" Inherits="Pages_Main_ShowMapFromGps" %>
<%@ Register Src="~/WebControls/GoogleMultiMap.ascx" TagName="GoogleMultiMap" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">
        <div id="Div4" runat="server" style="width:1175px;position:relative;left:5px">           
            <uc:GoogleMultiMap ID="uxGoogleMultiMap" runat="server" ZoomLevel="16" UserMode="DisplayZoneHits" />
        </div>
        <br />
    </div>
</asp:Content>

