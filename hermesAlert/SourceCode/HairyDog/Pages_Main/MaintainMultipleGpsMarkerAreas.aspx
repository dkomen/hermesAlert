﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MaintainMultipleGpsMarkerAreas.aspx.cs" Inherits="Pages_Main_MaintainMultipleGpsMarkerAreas" %>
<%@ Register Src="~/WebControls/GoogleMultiMap.ascx" TagName="GoogleMultiMap" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">
        <uc:PageSectionHeading ID="PageSectionHeading2" runat="server" SectionHeadingId="Multimap" BreakBefore="true"/>
        <div id="Div4" runat="server" style="width:1175px;position:relative;left:5px">
            <uc:GoogleMultiMap id="uxGoogleMultiMap" runat="server" ZoomLevel="6" Longitude="24.5" Latitude="-29.4"/>
        </div>
        <br />
    </div>
</asp:Content>

