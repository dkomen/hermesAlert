﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="RecieveMessage.aspx.cs" Inherits="Pages_Main_RecieveMessage" %>
<%@ Register Src="~/WebControls/PageSectionHeading.ascx" TagName="PageSectionHeading" TagPrefix="uc" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section" >                
        <uc:PageSectionHeading ID="PageSectionHeading1" SectionHeadingId="SendMessage" runat="server" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">    
            <asp:Table ID="Table1" runat="server" Width="125px" SkinID="Normal">
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Right">
                            <asp:Label ID="Label3" runat="server" SkinID="Normal">Your email address</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:TextBox SkinID="Normal" ID="uxEmailAddress" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Right">
                            <asp:Label ID="Label6" runat="server" SkinID="Normal">Department</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:DropDownList SkinID="Normal" ID="uxDepartment" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label1" runat="server" SkinID="NormalWideText" Text="Short description" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:TextBox SkinID="NormalWideText" ID="uxShortDescription" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                        <asp:Label ID="Label2" runat="server" SkinID="NormalWideText">Notes</asp:Label>
                    </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox SkinID="NormalTextBlock" ID="uxLongDescription" runat="server" TextMode="MultiLine" Rows="5" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table><br />
            <br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />
            <uc:Button ID="uxSave" runat="server" Text="Send" OnClicked="uxSend_Click" /> 
            <br />
            <br />
        </div>
    </div>
</asp:Content>

