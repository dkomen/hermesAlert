﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Main_RecieveMessage : PageBase, derotek.BikeCasa.Mvp.IView
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Contact";
        if (!IsPostBack)
        {
            uxDisplayMessage.Visible = false;
            uxDepartment.Items.Add(new ListItem("General Support","support"));
            uxDepartment.Items.Add(new ListItem("Advertising", "advertising"));
            uxDepartment.Items.Add(new ListItem("Report a bug", "support"));
            
            ///if this is an administrator then allow for sending messages to all members
            if (SessionVariables.CurrentUserGet(Session).Id != 0)
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    if (transaction.Retrieve<derotek.HairyDog.Users.User>(SessionVariables.CurrentUserGet(Session).Id).HasRole(derotek.HairyDog.Enumerations.Role.Administrator))
                    {
                        uxDepartment.Items.Add(new ListItem("Send an email to all community members", "community"));
                    }
                }
            }
        }
    }

    protected void uxSend_Click(object sender, EventArgs e)
    {
        if (uxDepartment.SelectedValue == "community")
        {
            if (uxShortDescription.Text == string.Empty)
            {
                DisplayMessage("'Short description' is a required field", true);
                return;
            }
            if (uxLongDescription.Text == string.Empty)
            {
                DisplayMessage("'Notes' is a required field", true);
                return;
            }

            try
            {
                new derotek.HairyDog.Model.Mailer().SendMailToAllUsers(uxShortDescription.Text, uxLongDescription.Text);
                uxShortDescription.Text = string.Empty;
                uxLongDescription.Text = string.Empty;
                DisplayMessage("Thank you, your message will be sent to all the community members", false);
            }
            catch (Exception ex)
            {
                DisplayMessage("An unexpected error occurred: " + ex.ToString(), true);
            }
        }
        else
        {
            if (uxEmailAddress.Text == string.Empty)
            {
                DisplayMessage("'Your email address' is a required field", true);
                return;
            }
            string eMailRegex = @"^(([^<>()[\]\\.,;:\s@\""]+"
                            + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                            + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                            + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                            + @"[a-zA-Z]{2,}))$";
            if (!System.Text.RegularExpressions.Regex.IsMatch(uxEmailAddress.Text, eMailRegex))
            {
                DisplayMessage("The email address is not in a valid format", true);
                return;
            }
            if (uxShortDescription.Text == string.Empty)
            {
                DisplayMessage("'Short description' is a required field", true);
                return;
            }
            if (uxLongDescription.Text == string.Empty)
            {
                DisplayMessage("'Notes' is a required field", true);
                return;
            }

            try
            {
                new derotek.HairyDog.Model.Mailer().SendMessageToEmailAddress(uxEmailAddress.Text, uxDepartment.SelectedValue + "@hermesAlert.com", uxShortDescription.Text, uxLongDescription.Text, false);
                uxShortDescription.Text = string.Empty;
                uxLongDescription.Text = string.Empty;
                DisplayMessage("Thank you, your message will be responded to by a hermesAlert representative", false);
            }
            catch (Exception ex)
            {
                DisplayMessage("An unexpected error occurred", true);
            }
        }
    }

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.Message = message;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.isErrorMessage = isErrorMessage;
    }

    public void RedirectView(string path)
    {
        throw new NotImplementedException();
    }
}