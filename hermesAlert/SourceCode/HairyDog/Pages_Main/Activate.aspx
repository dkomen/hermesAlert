﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Activate.aspx.cs" Inherits="Pages_Main_Activate" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<uc:MessageBox runat="server" ID="uxDisplayMessage" />
<br/><br/>
<uc:Button ID="uxProceedToLogin" runat="server" Text="Proceed to login page..." OnClicked="uxProceedToLogin_Click" />

</asp:Content>

