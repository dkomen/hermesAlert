﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Main_MaintainAttachment : PageBase
{
    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Attachment";
        if (!IsPostBack)
        {
            uxDescription.MaxLength = derotek.Configuration.FieldLengths.ShortDescription;
            if (uxIdentificationNumberType.Items.Count == 0)
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    IList<derotek.HairyDog.Metrics.IdentificationNumberType> identificationNumberTypes = transaction.Retrieve<derotek.HairyDog.Metrics.IdentificationNumberType>(null);
                    HelperClassDataElements.PopulateDropdownList(uxIdentificationNumberType, identificationNumberTypes.ToDictionary(x => x.Id, yield => yield.Description));
                }
            }
            AttachmentId = base.ChildObjectId;
            uxImageUploader.AlbumId = base.ActiveObjectId.ToString();
            string imageToShow = SessionVariables.RootPath(true) + "Images/NoAttachment.png";
            if (AttachmentId != 0)
            {
                derotek.HairyDog.Attachment attachment;
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    attachment = transaction.Retrieve<derotek.HairyDog.Attachment>(AttachmentId);
                }
                FileDescription = attachment.Description;
                IndentificationNumberType = attachment.PictureShowIdNumberType.Id;
                if (!string.IsNullOrEmpty(attachment.Path))
                {
                    imageToShow = SessionVariables.RootPath(true) + "Uploads/Resized/" + attachment.Path;
                }
            }
            
            uxImageUploader.PathToImage = imageToShow;
        }
    }
    public void uxSave_Click(object sender, EventArgs e)
    {
        try
        {
            string albumId = uxImageUploader.AlbumId;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                derotek.HairyDog.Attachment attachment;
                if (AttachmentId != 0)
                {
                    //Retrieve attachment from datastore for editing
                    attachment = transaction.Retrieve<derotek.HairyDog.Attachment>(AttachmentId);
                }
                else
                {
                    //Create a new attachment
                    attachment = new derotek.HairyDog.Attachment();
                    attachment.UserId = long.Parse(albumId);
                    attachment.AttachmentType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.AttachmentType>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.AttachmentType.None);
                }

                if (uxUseNewUploadedImage.Checked && uxImageUploader.PathSavedTo != string.Empty)
                {
                    string savedFileName = System.IO.Path.GetFileName(uxImageUploader.PathSavedTo);
                    if (savedFileName.Trim() != string.Empty) //Check that we have a file
                    {
                        string originalPathToFile = SessionVariables.RootPathToFiles + savedFileName;
                        string newFullPathToFile = SessionVariables.PathToResized + savedFileName;//imagePath.ToString().Substring(0, imagePath.ToString().Length - imageName.ToString().Length) + savedFileName;
                        string newThumbsParthToFile = SessionVariables.PathToThumbs + savedFileName;

                        derotek.HairyDog.Graphics.ImageManipulation.ResizeImage(originalPathToFile, newFullPathToFile, derotek.HairyDog.Graphics.ImageManipulation.MaximumAlbumPicturePixelSize, true);
                        derotek.HairyDog.Graphics.ImageManipulation.ResizeImage(originalPathToFile, newThumbsParthToFile, derotek.HairyDog.Graphics.ImageManipulation.MaximumThumbnailImagePixelSize, true);
                    }
                    attachment.Path = savedFileName;
                    if (attachment.Path.ToLower().EndsWith("jpg") || attachment.Path.ToLower().EndsWith("jpeg")
                        || attachment.Path.ToLower().EndsWith("png")
                        || attachment.Path.ToLower().EndsWith("gif")
                        || attachment.Path.ToLower().EndsWith("png"))
                    {                        
                        attachment.AttachmentType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.AttachmentType>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.AttachmentType.Picture);
                    }
                }
                else if (uxUseSelectedAvailableImage.Checked && uxAvailableAttachments.SelectedImage != string.Empty)
                {
                    derotek.HairyDog.RemoteLibrary.UploadedFile remoteFile = transaction.Retrieve<derotek.HairyDog.RemoteLibrary.UploadedFile>(long.Parse(uxAvailableAttachments.SelectedImage));
                    attachment.AttachmentType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.AttachmentType>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.AttachmentType.Picture);
                    attachment.Path = remoteFile.Path;
                    transaction.Delete(remoteFile);
                }
                attachment.PictureShowIdNumberType = transaction.Retrieve<derotek.HairyDog.Metrics.IdentificationNumberType>(IndentificationNumberType);
                attachment.Description = FileDescription;
                transaction.AddOrUpdate(attachment);
                transaction.Commit();
            }
            uxReturn_Click(null, null);
        }
        catch (Exception ex)
        {
            string m = ex.Message;
            throw new Exception("Dean, I know youre a seriously tired.. but, hey, come fix this at some point!",ex);
        }
    }
    public void uxDelete_Click(object sender, EventArgs e)
    {
        if (AttachmentId != 0)
        {
            derotek.HairyDog.Attachment attachment;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                attachment = transaction.Retrieve<derotek.HairyDog.Attachment>(AttachmentId);
                try
                {
                    string fileToDelete = string.Empty;
                    try
                    {
                        fileToDelete = System.IO.Path.Combine(SessionVariables.PathToThumbs, attachment.Path);
                        System.IO.File.Delete(fileToDelete);
                    }
                    catch { }; //Ignore this error                
                    try
                    {
                        fileToDelete = System.IO.Path.Combine(SessionVariables.PathToResized, attachment.Path);
                        System.IO.File.Delete(fileToDelete);
                    }
                    catch { }; //Ignore this error
                    transaction.Delete(attachment);
                    transaction.Commit();
                    uxReturn_Click(null, null);
                }
                catch (Exception ex)
                {
                    throw new derotek.Exceptions.StandardException("Could not remove attachment : " + AttachmentId, ex);
                }
            }
        }
        else
        {
            uxReturn_Click(null, null);
        }
    }
    public  void uxReturn_Click(object sender, EventArgs e)
    {
        Response.Redirect(base.ReturnToPage, false);
    }
    #endregion

    #region Properties
    public long AttachmentId
    {
        get
        {
            if (ViewState["AttachmentId"]==null)
            {
                return 0;
            }
            else
            {
                return long.Parse(ViewState["AttachmentId"].ToString());
            }
        }
        set
        {
            ViewState["AttachmentId"] = value;
        }
    }
    public string FileDescription
    {
        get
        {
            return uxDescription.Text;
        }
        set
        {
            uxDescription.Text = value;
        }
    }
    public long IndentificationNumberType
    {
        get
        {
            return long.Parse(uxIdentificationNumberType.SelectedValue);
        }
        set
        {
            if (uxIdentificationNumberType.Items.Count == 0)
            {
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    IList<derotek.HairyDog.Metrics.IdentificationNumberType> identificationNumberTypes = transaction.Retrieve<derotek.HairyDog.Metrics.IdentificationNumberType>(null);
                    HelperClassDataElements.PopulateDropdownList(uxIdentificationNumberType, identificationNumberTypes.ToDictionary(x => x.Id, yield => yield.Description));
                }
            }
            HelperClassDataElements.SetDropdownListSelecteditem(uxIdentificationNumberType, value);
        }
    }
    #endregion
}