﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Main_Activate : PageBase, derotek.BikeCasa.Mvp.IView
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Activate Account";
        
        if (!IsPostBack)
        {
            string error = "The id of this account activation request is not in a valid format. Please note that the url string is case sensistive";
            if (Request.QueryString["id"] != null)
            {
                if (new derotek.BikeCasa.Mvp.AccountActivationPresenter().ActivateUserAccount(Request.QueryString["id"]))
                {
                    DisplayMessage("Thank you, your hermesAlert account is now activated and you may login from the Login page", false);
                }
                else
                {
                    error = "It appears that this account activation request has already been processed";
                    DisplayMessage(error, true);
                }
            }
            else
            {
                DisplayMessage(error, true);
            }
        }
    }

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.isErrorMessage = isErrorMessage;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.Message = message;
    }

    public void RedirectView(string path)
    {
        throw new NotImplementedException();
    }

    protected void uxProceedToLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("http://www.hermesAlert.com");
    }
}