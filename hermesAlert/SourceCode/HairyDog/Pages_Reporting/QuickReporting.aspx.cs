﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Reporting_QuickReporting : PageBase, derotek.BikeCasa.Mvp.Reporting.IShowReports
{
    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Incidents";
        uxGoogleMultiMap.PropertyTypeToShow = 0;
        if (!IsPostBack)
        {
            uxGoogleMultiMap.UserId = SessionVariables.CurrentUserGet(Session).Id;
            (new derotek.BikeCasa.Mvp.Reporting.ShowReportsPresenter(this)).PrepareView();            
        }
    }
    protected void uxCreateNewQuickIncident_Click(object sender, EventArgs e)
    {
        (new derotek.BikeCasa.Mvp.Reporting.ShowReportsPresenter(this)).RedirectToAddNewIncident(Page.AppRelativeVirtualPath);
    }

    protected void uxQuickReports_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {
            long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            string returnToPage = this.Page.AppRelativeVirtualPath;
            (new derotek.BikeCasa.Mvp.Reporting.ShowReportsPresenter(this)).ShowItem(id, Page.AppRelativeVirtualPath);
        }
    }
    protected void uxQuickReports_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        (new derotek.BikeCasa.Mvp.Reporting.ShowReportsPresenter(this)).GetIncidentReports();
        ((GridView)sender).PageIndex = e.NewPageIndex;
        ((GridView)sender).DataBind();
    }    
    #endregion

    public List<derotek.HairyDog.DisplayableItems.DisplayableQuickReport> QuickReports
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxQuickReports.DataKeyNames = new string[] { "Id" };
            uxQuickReports.DataSource = value;
            uxQuickReports.DataBind();
        }
    }

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }    

    public long CurrentUserId
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session).Id;
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
       
    }
    public void RedirectView(string path)
    {
        Server.Transfer(path, true);
    }
}