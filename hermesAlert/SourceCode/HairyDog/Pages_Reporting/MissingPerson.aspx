﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MissingPerson.aspx.cs" Inherits="Pages_Reporting_MissingPerson" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register TagPrefix="uc" TagName="ImageUploader" Src="~/WebControls/ImageUploader.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section" >                
        <uc:PageSectionHeading ID="PageSectionHeading1" SectionHeadingId="MissingPersons" runat="server" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">    
            <asp:Table id="uxPanels" runat="server" style="width:100%;">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table ID="Table1" runat="server" style="width:770px;padding:0;vertical-align:top" SkinID="NormalStandOutMore">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="2">
                                    <uc:PageSectionHeading ID="PageSectionHeading3" runat="server" SectionHeadingId="MissingPersonDetails" BreakBefore="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label6" runat="server" SkinID="Normal">First name</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:TextBox ID="uxFirstName" runat="server" SkinID='Normal'></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow> 
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label1" runat="server" SkinID="Normal">Second name</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:TextBox ID="uxSecondName" runat="server" SkinID='Normal'></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow> 
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label2" runat="server" SkinID="Normal">Surname</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:TextBox ID="uxSurname" runat="server" SkinID='Normal'></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>          
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label3" runat="server" SkinID="Normal">Known as</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:TextBox ID="uxKnownAs" runat="server" SkinID='Normal'></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>     
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label5" runat="server" SkinID="Normal">Eye colour</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" >
                                    <asp:DropDownList SkinID="Normal" ID="uxEyeColourList" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label7" runat="server" SkinID="Normal">Hair colour</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:DropDownList SkinID="Normal" ID="uxHairColourList" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label8" runat="server" SkinID="Normal">Gender</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:DropDownList SkinID="Normal" ID="uxGenderList" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label17" runat="server" SkinID="Normal">Approximate age</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:TextBox SkinID="Normal" ID="uxApproximateAge" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                                        <asp:Label ID="Label9" runat="server" SkinID="Normal">Distinguishing features</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:TextBox SkinID="NormalWideText" ID="uxDistinguishingFeatures" runat="server" TextMode="MultiLine" Rows="4" width="510px" Height="75px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right">
                                        <asp:Label ID="Label16" runat="server" SkinID="Normal">Award amount(ZAR)</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:TextBox ID="uxAwardAmount" runat="server" SkinID='Normal'></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>   
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right">
                                        <asp:Label ID="Label4" runat="server" SkinID="Normal">Police case number</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:TextBox ID="uxPoliceCaseNumber" runat="server" SkinID='Normal'></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>   
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right" VerticalAlign="top">
                                    <asp:Label ID="Label12" runat="server" SkinID="NormalNoWidthGiven">Date last seen</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Calendar ID="uxDateLastSeen" runat="server" BackColor="White" BorderColor="#333333" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" 
                                        Font-Size="8pt" ForeColor="Black" Height="180px" NextPrevFormat="ShortMonth" Width="230px">
                                            <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="6pt" />
                                            <DayStyle BackColor="#dedede" ForeColor="#1212bb" />
                                            <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                            <OtherMonthDayStyle ForeColor="#999999" />
                                            <SelectedDayStyle BackColor="#999999" ForeColor="White" />
                                            <TitleStyle BackColor="#222299" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="White" Height="10pt" />
                                            <TodayDayStyle BackColor="#999999" ForeColor="White" />
                                    </asp:Calendar>
                                </asp:TableCell>                    
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                                        <asp:Label ID="Label10" runat="server" SkinID="NormalWideText">Notes</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:TextBox SkinID="Normal" ID="uxNotes" runat="server" TextMode="MultiLine" Rows="5" width="510px" Height="120px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Label ID="Label11" runat="server" SkinID="NormalWideText">Last known location : </asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>                           
                                    <asp:Label ID="Label13" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Longitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLongitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;
                                    <asp:Label ID="Label14" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Latitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLatitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell />
                                <asp:TableCell>
                                    <uc:Button ID="uxSelectGpslocation" runat="server" Text="Select gps location" OnClientClick="javascript:showList();" AutoPostBack="false" />                                                                                                         
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow><asp:TableCell></asp:TableCell></asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Label ID="Label15" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Hide coordinates from public</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:DropDownList ID="uxLocationAccuracyWhenPublic" runat="server" SkinID='Normal' />  
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                                        <asp:Label ID="Label18" runat="server" SkinID="Normal">Contact details</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                                    <asp:TextBox SkinID="NormalWideText" ID="uxContactDetails" runat="server" TextMode="MultiLine" Rows="4" width="300px" Height="75px" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                 <asp:TableCell HorizontalAlign="Right" style="width:225px;">
                                        <asp:Label ID="Label19" runat="server" SkinID="Normal">Person was found?</asp:Label>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Left">
                                    <asp:CheckBox SkinID="Normal" ID="uxSolved" runat="server" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>            
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Table runat="server" style="width:400px;padding:0;vertical-align:top" SkinId="NormalStandOutMore">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <uc:PageSectionHeading ID="PageSectionHeading2" runat="server" SectionHeadingId="UploadedAttachment" BreakBefore="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell style="width:390px">
                                    <uc:ImageUploader id="uxImageUploader" runat="server" />    
                                </asp:TableCell>
                            </asp:TableRow>                        
                        </asp:Table>                  
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>           
            <br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />
            <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click" /> 
            <uc:Button ID="uxDelete" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this missing person report?');" OnClicked="uxDelete_Click" />   
            <uc:Button ID="uxProceedReturn" runat="server" Text="Return..." OnClicked="uxProceedToMissingPersonListingPage_Click" />
            <br />
            <br />
 
            
        </div>
    </div>
    <br />
</asp:Content>

