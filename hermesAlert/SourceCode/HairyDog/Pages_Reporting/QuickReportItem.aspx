﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="QuickReportItem.aspx.cs" Inherits="Pages_Reporting_QuickReportItem" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.js" type="text/javascript"></script>   

    <div id="Div1" runat="server" class="Section" >                
        <uc:PageSectionHeading ID="PageSectionHeading1" SectionHeadingId="QuickIncidentReport" runat="server" BreakBefore="true" />
        <div id="Div2" runat="server" style="width:1185px;position:relative;left:5px">    
            <asp:Table ID="Table1" runat="server" style="width:900px" SkinID="Normal">
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Right" style="width:225px;">                           
                        <asp:Label ID="Label6" runat="server" SkinID="NormalWideText" style="position:relative;top:2px">Incident type</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:DropDownList SkinID="Normal" ID="uxIncidentType" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">                    
                        <asp:Label ID="Label1" runat="server" SkinID="NormalWideText" style="position:relative;top:2px;">Description</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="2">
                        <asp:TextBox SkinID="NormalWideText" ID="uxDescription" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="top">                   
                        <asp:Label ID="Label2" runat="server" SkinID="NormalWideText" style="position:relative;top:2px">Time of incident</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left" VerticalAlign="top">
                        <asp:DropDownList ID="uxIncidentTimeHour" runat="server">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>01</asp:ListItem>
                            <asp:ListItem>02</asp:ListItem>
                            <asp:ListItem>03</asp:ListItem>
                            <asp:ListItem>04</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>06</asp:ListItem>
                            <asp:ListItem>07</asp:ListItem>
                            <asp:ListItem>08</asp:ListItem>
                            <asp:ListItem>09</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>11</asp:ListItem>
                            <asp:ListItem>12</asp:ListItem>
                            <asp:ListItem>13</asp:ListItem>
                            <asp:ListItem>14</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>16</asp:ListItem>
                            <asp:ListItem>17</asp:ListItem>
                            <asp:ListItem>18</asp:ListItem>
                            <asp:ListItem>19</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>21</asp:ListItem>
                            <asp:ListItem>22</asp:ListItem>
                            <asp:ListItem>23</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="uxIncidentTimeMinutes" runat="server">
                            <asp:ListItem>00</asp:ListItem>
                            <asp:ListItem>05</asp:ListItem>
                            <asp:ListItem>10</asp:ListItem>
                            <asp:ListItem>15</asp:ListItem>
                            <asp:ListItem>20</asp:ListItem>
                            <asp:ListItem>25</asp:ListItem>
                            <asp:ListItem>30</asp:ListItem>
                            <asp:ListItem>35</asp:ListItem>
                            <asp:ListItem>40</asp:ListItem>
                            <asp:ListItem>45</asp:ListItem>
                            <asp:ListItem>50</asp:ListItem>                                                        
                            <asp:ListItem>55</asp:ListItem>                                                        
                        </asp:DropDownList><asp:Label ID="Label3" runat="server" SkinID="NormalNoWidthGiven">hh:mm</asp:Label>   
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="top">
                        <asp:Label ID="Label12" runat="server" SkinID="NormalNoWidthGiven">Date of incident</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Calendar ID="uxIncidentDate" runat="server" BackColor="White" BorderColor="#333333" BorderStyle="Solid" CellSpacing="1" Font-Names="Verdana" 
                            Font-Size="8pt" ForeColor="Black" Height="180px" NextPrevFormat="ShortMonth" Width="230px">
                                <DayHeaderStyle Font-Bold="True" Font-Size="8pt" ForeColor="#333333" Height="6pt" />
                                <DayStyle BackColor="#dedede" ForeColor="#1212bb" />
                                <NextPrevStyle Font-Bold="True" Font-Size="8pt" ForeColor="White" />
                                <OtherMonthDayStyle ForeColor="#999999" />
                                <SelectedDayStyle BackColor="#999999" ForeColor="White" />
                                <TitleStyle BackColor="#222299" BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" Font-Size="10pt" ForeColor="White" Height="10pt" />
                                <TodayDayStyle BackColor="#999999" ForeColor="White" />
                        </asp:Calendar>
                    </asp:TableCell>                    
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="middle">
                        <asp:Label ID="Label9" runat="server" SkinID="NormalWideText" style="position:relative;top:2px">Incident location : </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell ColumnSpan="3">                           
                        <asp:Label ID="Label7" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Longitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLongitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;                        
                        <asp:Label ID="Label8" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Latitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLatitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;
                        <uc:Button ID="uxSelectGpslocation" runat="server" Text="Select gps location" OnClicked="uxSelectGpslocation_Click" />
                        <%--<uc:Button ID="uxSelectGpslocation" runat="server" Text="Select gps location" OnClientClick="javascript:showList();" AutoPostBack="false" />--%>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:Label ID="Label10" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Hide coordinates from public</asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:DropDownList ID="uxLocationAccuracyWhenPublic" runat="server" SkinID='Normal' />  
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>            
            <br />
            <uc:MessageBox runat="server" ID="uxDisplayMessage" />
            <br />            
            <asp:Label ID="Label4" runat="server" SkinID="SmallWarningText">If you have more than 200 activity points then community members monitoring the area where the incident took place will be notified via email</asp:Label>
            <br /><br />
            <uc:Button ID="uxSave" runat="server" Text="Save" OnClicked="uxSave_Click" /> 
            <uc:Button ID="uxDelete" runat="server" Text="Delete" OnClientClick="return confirm('Are you sure you want to delete this quick report item?');" OnClicked="uxDelete_Click" />   
            <uc:Button ID="uxProceedReturn" runat="server" Text="Return..." OnClicked="uxProceedToRegisteredItemsPage_Click" />
            <br />
            <br />
        </div>
    </div>
    <br />
    
</asp:Content>

