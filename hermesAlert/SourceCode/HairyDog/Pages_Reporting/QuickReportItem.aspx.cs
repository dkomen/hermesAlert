﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Reporting_QuickReportItem : PageBaseLocationTransfers, derotek.BikeCasa.Mvp.Reporting.IMaintainQuickReport
{

    #region Fields
    private derotek.BikeCasa.Mvp.Reporting.MaintainQuickReport _presenter;
    #endregion

    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Maintain quick incident report"; 
        _presenter = new derotek.BikeCasa.Mvp.Reporting.MaintainQuickReport(this);        
        #region JavaScript
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GpsScript", JavaScript.ShowGpsSelectionFormScript(uxGpsLongitudeSelected.ClientID, uxGpsLatitudeSelected.ClientID));
        #endregion
        if (!IsPostBack)
        {
            uxIncidentTimeHour.SelectedIndex = System.DateTime.Now.Hour;
            uxIncidentTimeMinutes.SelectedIndex = ((int)(System.DateTime.Now.Minute / 5));
            uxIncidentDate.SelectedDate = System.DateTime.Now;

            uxDisplayMessage.Visible = false;

            derotek.BikeCasa.Mvp.Reporting.StateQuickReportItem sessionItem = (derotek.BikeCasa.Mvp.Reporting.StateQuickReportItem)Session[this.AppRelativeVirtualPath];
            if (sessionItem != null && Session["Longitude"] != null && Session["Longitude"] != null)
            {
                sessionItem.Latitude = double.Parse(Session["Latitude"].ToString());
                sessionItem.Longitude = double.Parse(Session["Longitude"].ToString());
            }

            _presenter.PrepareView(base.ActiveObjectId, sessionItem);
            Session[this.AppRelativeVirtualPath] = null;
            uxDescription.MaxLength = 100;
        }
    }

    protected void uxSave_Click(object sender, EventArgs e)
    {
       _presenter.SaveIncident();
    }
    protected void uxDelete_Click(object sender, EventArgs e)
    {
        _presenter.DeleteIncident(base.ActiveObjectId, SessionVariables.RootPath(true) + "Pages_Reporting/QuickReporting.aspx");
    }
    protected void uxProceedToRegisteredItemsPage_Click(object sender, EventArgs e)
    {
        _presenter.RedirectView(base.ReturnToPage);
    }
    protected void uxSelectGpslocation_Click (object sender, EventArgs e)
    {
        CallGpsPage("~/Pages_Main/SelectGpsLocation.aspx");
    }
    #endregion


    public string Description
    {
        get
        {
            return uxDescription.Text;
        }
        set
        {
            uxDescription.Text = value;
        }
    }
    public DateTime IncidentDate
    {
        get
        {
            System.DateTime stamp = new DateTime(uxIncidentDate.SelectedDate.Year, uxIncidentDate.SelectedDate.Month, uxIncidentDate.SelectedDate.Day, uxIncidentTimeHour.SelectedIndex, int.Parse(uxIncidentTimeMinutes.SelectedValue), 0);
            return stamp;
        }
        set
        {
            uxIncidentDate.SelectedDate = new System.DateTime(value.Year, value.Month, value.Day);
            uxIncidentDate.VisibleDate = uxIncidentDate.SelectedDate;

            uxIncidentTimeHour.SelectedIndex = value.Hour;
            uxIncidentTimeMinutes.SelectedIndex = ((int)(value.Minute / 5));
        }
    }
    public long IncidentType
    {
        get
        {
            return long.Parse(uxIncidentType.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxIncidentType, value);
        }
    }

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.Message = message;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.isErrorMessage = isErrorMessage;
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
    public void SetIncidentTypeList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxIncidentType, list);
    }
    public long LocationAccuracyWhenPublic
    {
        get
        {
            return long.Parse(uxLocationAccuracyWhenPublic.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxLocationAccuracyWhenPublic, value);
        }
    }
    public void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxLocationAccuracyWhenPublic, list);
    }
    public override double Latitude
    {
        get
        {
            return double.Parse(uxGpsLatitudeSelected.Text != string.Empty ? uxGpsLatitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLatitudeSelected.Text = value.ToString();
        }
    }
    public override double Longitude
    {
        get
        {
            return double.Parse(uxGpsLongitudeSelected.Text != string.Empty ? uxGpsLongitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLongitudeSelected.Text = value.ToString();
        }
    }

    public override void RePopulatePageFields(string pageName)
    {
        uxGpsLongitudeSelected.Text = this.ViewState["Longitude"].ToString();
        uxGpsLatitudeSelected.Text = this.ViewState["Longitude"].ToString();
    }

    public override void CallGpsPage(string pageToCallForSelectingGpsData)
    {
        Session["Longitude"]= Longitude;
        Session["Latitude"] = Latitude;

        derotek.BikeCasa.Mvp.Reporting.StateQuickReportItem state = new derotek.BikeCasa.Mvp.Reporting.StateQuickReportItem();
        state.IncidentType = this.IncidentType;
        state.Description = this.Description;
        state.IncidentDate = this.IncidentDate;
        state.Latitude = this.Latitude;
        state.Longitude = this.Longitude;
        state.LocationAccuracyWhenPublic = this.LocationAccuracyWhenPublic;
        state.ObjectId = this.AppRelativeVirtualPath;
        state.ActiveObjectId = this.ActiveObjectId;
        state.ReturnToPage = this.ReturnToPage;

        state.KeepAliveForPages.Add(this.AppRelativeVirtualPath);
        state.KeepAliveForPages.Add("~/Pages_Main/SelectGpsLocation.aspx");

        Session[state.ObjectId] = state;

        _presenter.RedirectToSelectGpsLocation(this.AppRelativeVirtualPath);
    }
}