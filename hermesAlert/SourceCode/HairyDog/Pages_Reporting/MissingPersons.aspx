﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="MissingPersons.aspx.cs" Inherits="Pages_Reporting_MissingPersons" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="GoogleMultiMap" Src="~/WebControls/GoogleMultiMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register Assembly="derotek.AspNet.Controls" Namespace="derotek.AspNet.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">
        <uc:PageSectionHeading ID="PageSectionHeading2" runat="server" SectionHeadingId="MissingPersons" BreakBefore="true"/>
        <div id="Div44" runat="server" style="width:1185px;position:relative;left:5px">                   
            <uc:GoogleMultiMap ID="uxGoogleMultiMap" runat="server" ZoomLevel="6" UserMode="DisplayZoneHits" Longitude="24.5" Latitude="-29.4"/>
            <br />
            <uc:Button id="uxCreateNewMissingItemReport" runat="server" Text="Add new missing person report" onclicked="uxCreateNewMissingItemReport_Click" /><br /><br />            
            <cc1:DataGrid ID="uxAlerts"  runat="server" AutoGenerateColumns="False" onrowcommand="uxAlerts_RowCommand" OnPageIndexChanging="uxAlerts_PageIndexChanging"> 
            <Columns>
                <asp:ImageField DataImageUrlField="PictureUrl" DataImageUrlFormatString="~/Uploads/Thumbs/{0}" AlternateText="Edit" ControlStyle-Height = "50px">                              
                </asp:ImageField>
                <asp:BoundField DataField="FullName" HeaderText="Name" SortExpression="PropertyType" />
                <asp:BoundField DataField="KnownAs" HeaderText="Known as" SortExpression="PropertyType" />
                <asp:BoundField DataField="PoliceCaseNumber" HeaderText="Police case number" />                
                <asp:BoundField DataField="DateLastSeen" HeaderText="Date last seen" DataFormatString="{0:dd MMMM yyyy, HH:mm}" />
            </Columns> 
            </cc1:DataGrid>
            <br />            
        </div>
    </div>
</asp:Content>

