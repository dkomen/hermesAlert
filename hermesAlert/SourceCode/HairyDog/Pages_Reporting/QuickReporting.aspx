﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="QuickReporting.aspx.cs" Inherits="Pages_Reporting_QuickReporting" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="GoogleMultiMap" Src="~/WebControls/GoogleMultiMap.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register Assembly="derotek.AspNet.Controls" Namespace="derotek.AspNet.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div1" runat="server" class="Section">
        <uc:PageSectionHeading ID="PageSectionHeading2" runat="server" SectionHeadingId="QuickIncidents" BreakBefore="true"/>
        <div id="Div44" runat="server" style="width:1185px;position:relative;left:5px">                   
            <uc:GoogleMultiMap ID="uxGoogleMultiMap" runat="server" ZoomLevel="6" UserMode="DisplayZoneHits" Longitude="24.5" Latitude="-29.4"/>
            <br />
            <uc:Button id="uxCreateNewQuickIncident" runat="server" Text="Add new incident report" onclicked="uxCreateNewQuickIncident_Click" /><br /><br />            
            <cc1:DataGrid ID="uxQuickReports"  runat="server" AutoGenerateColumns="False" onrowcommand="uxQuickReports_RowCommand" OnPageIndexChanging="uxQuickReports_PageIndexChanging"> 
            <Columns>
                <asp:BoundField DataField="IncidentType" HeaderText="Incident type" SortExpression="PropertyType" />
                <asp:BoundField DataField="Description" HeaderText="Description" />                
                <asp:BoundField DataField="IncidentDateTime" HeaderText="Date" DataFormatString="{0:dd MMMM yyyy, HH:mm}" />
            </Columns> 
            </cc1:DataGrid>
            <br />            
        </div>
    </div>
</asp:Content>

