﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Reporting_MissingPersons : PageBase, derotek.BikeCasa.Mvp.PersonAlert.IPersonAlertItems
{

    #region Fields
    private derotek.BikeCasa.Mvp.PersonAlert.PersonAlertsPresenter _presenterMissingPersonAlert;
    #endregion

    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Missing persons";
        _presenterMissingPersonAlert = new derotek.BikeCasa.Mvp.PersonAlert.PersonAlertsPresenter(this);
        if (!IsPostBack)
        {
            _presenterMissingPersonAlert.PrepareView();
        }
    }
    protected void uxCreateNewMissingItemReport_Click(object sender, EventArgs e)
    {
        _presenterMissingPersonAlert.RedirectToMissingPersonItem("~/Pages_Reporting/MissingPerson.aspx");
    }
    protected void uxAlerts_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {

            long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            _presenterMissingPersonAlert.Edit(id, Page.AppRelativeVirtualPath);
        }
    }    
    protected void uxAlerts_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _presenterMissingPersonAlert.PrepareView();
        ((GridView)sender).PageIndex = e.NewPageIndex;
        ((GridView)sender).DataBind();
    }  
    #endregion

    #region View
    public List<derotek.HairyDog.DisplayableItems.DisplayablePersonAlertObject> AlertItems
    {
        set
        {
            uxAlerts.DataKeyNames = new string[] { "Id" };
            uxAlerts.DataSource = value;
            uxAlerts.DataBind();
        }
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            //((PageBase)Page).ActiveObjectId = value;
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        throw new NotImplementedException();
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }
    #endregion
}