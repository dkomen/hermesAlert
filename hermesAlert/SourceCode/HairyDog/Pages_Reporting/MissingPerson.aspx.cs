﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Reporting_MissingPerson : PageBase, derotek.BikeCasa.Mvp.PersonAlert.IPersonAlertItem
{
    #region Fields
    public derotek.BikeCasa.Mvp.PersonAlert.PersonAlertPresenter _presenter;
    #endregion

    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Title = "hermesAlert : Maintain missing person alert";
        _presenter = new derotek.BikeCasa.Mvp.PersonAlert.PersonAlertPresenter(this);
        if (!IsPostBack)
        {
            uxApproximateAge.MaxLength = derotek.Configuration.FieldLengths.Age;
            uxAwardAmount.MaxLength = derotek.Configuration.FieldLengths.MonetoryAmount;
            uxDistinguishingFeatures.MaxLength = derotek.Configuration.FieldLengths.Description;
            uxFirstName.MaxLength = derotek.Configuration.FieldLengths.PersonName;
            uxSecondName.MaxLength = derotek.Configuration.FieldLengths.PersonName;
            uxSurname.MaxLength = derotek.Configuration.FieldLengths.PersonSurname;
            uxKnownAs.MaxLength = derotek.Configuration.FieldLengths.PersonName;
            uxNotes.MaxLength = derotek.Configuration.FieldLengths.Notes;
            uxPoliceCaseNumber.MaxLength = derotek.Configuration.FieldLengths.PoliceCaseNumber;
            uxContactDetails.MaxLength = derotek.Configuration.FieldLengths.ContactDetailsMulti;

            uxImageUploader.AlbumId = this.CurrentUser.Id.ToString();
            Title = "hermesAlert : Maintain missing person item"; 
            uxDisplayMessage.Visible = false;
            _presenter.PrepareView(base.ActiveObjectId);
        }
        #region JavaScript
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "GpsScript", JavaScript.ShowGpsSelectionFormScript(uxGpsLongitudeSelected.ClientID, uxGpsLatitudeSelected.ClientID));
        #endregion
    }
    protected void uxSave_Click(object sender, EventArgs e)
    {

        string savedFileName = string.Empty;

        string originalPathToFile = string.Empty;
        string newFullPathToFile = string.Empty;
        string newThumbsParthToFile = string.Empty;

        //Was a new image uploaded?
        if (NewImageFileName != string.Empty)
        {
            //Yes... so resize it etc
            savedFileName = System.IO.Path.GetFileName(uxImageUploader.PathSavedTo);

            originalPathToFile = SessionVariables.RootPathToFiles + savedFileName;
            newFullPathToFile = SessionVariables.PathToResized + savedFileName;
            newThumbsParthToFile = SessionVariables.PathToThumbs + savedFileName;
                
        }
        _presenter.Save(originalPathToFile, newFullPathToFile, newThumbsParthToFile);

    }
    protected void uxDelete_Click(object sender, EventArgs e)
    {
        _presenter.Delete(base.ReturnToPage);//SessionVariables.RootPath(true) + "Pages_Reporting/MissingPersons.aspx"
    }
    protected void uxProceedToMissingPersonListingPage_Click(object sender, EventArgs e)
    {
        _presenter.RedirectView("MissingPersons.aspx");
    }
    #endregion

    #region View
    public long UserId
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public string Firstname
    {
        get
        {
            return uxFirstName.Text;
        }
        set
        {
            uxFirstName.Text = value;
        }
    }//
    public string SecondName
    {
        get
        {
            return uxSecondName.Text;
        }
        set
        {
            uxSecondName.Text = value;
        }
    }//
    public string Surname
    {
        get
        {
            return uxSurname.Text;
        }
        set
        {
            uxSurname.Text = value;
        }
    }//
    public string KnownAs
    {
        get
        {
            return uxKnownAs.Text;
        }
        set
        {
            uxKnownAs.Text = value;
        }
    }//
    public DateTime DateLastSeen
    {
        get
        {
            return uxDateLastSeen.SelectedDate;
        }
        set
        {
            uxDateLastSeen.SelectedDate = new System.DateTime(value.Year, value.Month, value.Day);
            uxDateLastSeen.VisibleDate = uxDateLastSeen.SelectedDate;
        }
    }//
    public string PoliceCaseNumber
    {
        get
        {
           return uxPoliceCaseNumber.Text;
        }
        set
        {
            uxPoliceCaseNumber.Text = value;
        }
    }//
    public string PathToImageToDisplay
    {
        get
        {
            return uxImageUploader.PathToImage;
        }
        set
        {
            uxImageUploader.PathToImage = SessionVariables.RootPath(true) + "Uploads/Resized/" + value;
        }
    }
    /// <summary>
    /// Was this person found?
    /// </summary>
    public bool Solved
    {
        get
        {
            return uxSolved.Checked;
        }
        set
        {
            uxSolved.Checked = value;
        }
    }
    public string NewImageFileName 
    {
        get
        {
            return uxImageUploader.PathSavedTo;
        }
    }
    public void SetApprovalStatusList(Dictionary<long, string> list)
    {
       
    }//
    public void SetGenderList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxGenderList, list);
    }//
    public void SetEyeColourList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxEyeColourList, list);
    }//
    public void SetHairColourList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxHairColourList, list);
    }//
    public long ApprovalStatus
    {
        get;
        set;
    }
    public float AwardAmount
    {
        get
        {
            return float.Parse(uxAwardAmount.Text.Trim() == string.Empty ? "0" : uxAwardAmount.Text);
        }
        set
        {
            uxAwardAmount.Text = value.ToString();
        }
    }
    public string ContactDetails
    {
        get
        {
            return uxContactDetails.Text;
        }
        set
        {
            uxContactDetails.Text = value;
        }
    }
    public IList<derotek.HairyDog.ItemMonitoring.ItemDetection> ItemDetectionMessages
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public string ApproximateAge
    {
        get
        {
            return uxApproximateAge.Text;
        }
        set
        {
            uxApproximateAge.Text = value;
        }
    }//
    public long EyeColour
    {
        get
        {
           return long.Parse(uxEyeColourList.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxEyeColourList, value);
        }
    }//
    public long HairColour
    {
        get
        {
            return long.Parse(uxHairColourList.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxHairColourList, value);
        }
    }//
    public long Gender
    {
        get
        {
            return long.Parse(uxGenderList.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxGenderList, value);
        }
    }//
    public string DistinguishingFeatures
    {
        get
        {
            return uxDistinguishingFeatures.Text;
        }
        set
        {
            uxDistinguishingFeatures.Text = value;
        }
    }//
    public string Notes
    {
        get
        {
            return uxNotes.Text;
        }
        set
        {
            uxNotes.Text = value;
        }
    }//
    public void SetItemTypeList(Dictionary<long, string> list)
    {
        throw new NotImplementedException();
    }
    public int ItemType
    {
        get;
        set;
    }
    /// <summary>
    /// Diable persistance
    /// </summary>
    public bool ReadonlyMode
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxSave.Visible = !value;
            uxDelete.Visible = !value;
            uxImageUploader.Enabled = false;
            //uxSendPrivateMessage.Visible = base.ActiveObjectId == 0 ? false : value;
        }
    }    
    public long LocationAccuracyWhenPublic
    {
        get
        {
            return long.Parse(uxLocationAccuracyWhenPublic.SelectedValue);
        }
        set
        {
            HelperClassDataElements.SetDropdownListSelecteditem(uxLocationAccuracyWhenPublic, value);
        }
    }//
    public void SetLocationAccuracyWhenPublicList(Dictionary<long, string> list)
    {
        HelperClassDataElements.PopulateDropdownList(uxLocationAccuracyWhenPublic, list);
    }//
    public double Latitude
    {
        get
        {
            return double.Parse(uxGpsLatitudeSelected.Text != string.Empty ? uxGpsLatitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLatitudeSelected.Text = value.ToString();
        }
    }//
    public double Longitude
    {
        get
        {
            return double.Parse(uxGpsLongitudeSelected.Text != string.Empty ? uxGpsLongitudeSelected.Text : "0");
        }
        set
        {
            uxGpsLongitudeSelected.Text = value.ToString();
        }
    }//
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }//
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.Message = message;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.isErrorMessage = isErrorMessage;
    }//
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }//
    #endregion
}