﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_Search_Search : PageBase, derotek.BikeCasa.Mvp.Search.ISearchView
{
    #region Fields
    private derotek.BikeCasa.Mvp.Search.SearchPresenter _presenterSearch;
    #endregion

    #region Form Events
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "hermesAlert : Search";
        _presenterSearch = new derotek.BikeCasa.Mvp.Search.SearchPresenter(this);
        if (!IsPostBack)
        {
            uxDisplayMessage.Visible = false;
        }
    }

    /// <summary>
    /// Click on row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uxMonitoringEvents_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.ToUpper() == "EDIT")
        {
            long id = long.Parse(((GridView)sender).DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString());
            string returnToPage = this.Page.AppRelativeVirtualPath;
            _presenterSearch.ShowItem(id, Page.AppRelativeVirtualPath);
        }
    }

    /// <summary>
    /// Click on row
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void uxSearchDatabase_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        uxMonitoringEvents_RowCommand(sender, e);
    }
    protected void uxSearchResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        _presenterSearch.GetStolenItemSearchResults(uxDatabaseSearchText.Text);
        ((GridView)sender).PageIndex = e.NewPageIndex;
        ((GridView)sender).DataBind();
    }
    protected void uxSearchDatabase_Click(object sender, EventArgs e)
    {
        _presenterSearch.GetStolenItemSearchResults(uxDatabaseSearchText.Text);
    }

    #endregion

    #region View ISearchView

    List<derotek.HairyDog.DisplayableItems.DisplayableRegisteredObject> derotek.BikeCasa.Mvp.Search.ISearchView.StolenItemSearchResults
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxSearchResults.DataKeyNames = new string[] { "Id" };
            uxSearchResults.DataSource = value;
            uxSearchResults.DataBind();
        }
    }

    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    void derotek.BikeCasa.Mvp.IView.DisplayMessage(string message, bool isErrorMessage)
    {
        uxDisplayMessage.Message = message;
        uxDisplayMessage.Visible = message != string.Empty;
        uxDisplayMessage.isErrorMessage = isErrorMessage;
    }
    void derotek.BikeCasa.Mvp.IView.RedirectView(string path)
    {
        Response.Redirect(path);
    }
    #endregion
}