﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Search.aspx.cs" Inherits="Pages_Search_Search" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<%@ Register TagPrefix="uc" TagName="MessageBox" Src="~/WebControls/MessageBox.ascx" %>
<%@ Register Assembly="derotek.AspNet.Controls" Namespace="derotek.AspNet.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="Div2" runat="server" class="Section" >
        <uc:PageSectionHeading ID="PageSectionHeading3" runat="server" SectionHeadingId="SearchDatabase" BreakBefore="true"/>
        <div id="Div4" runat="server" style="width:1185px;position:relative;left:5px">
            <asp:Label ID="Label1" runat="server" Text="Search for a publicly listed identification number" SkinID = "NormalNoWidthGiven"  />
            <asp:TextBox ID= "uxDatabaseSearchText" SkinID="Normal" runat="server" />
            &nbsp;<uc:Button id="uxSearchDatabase" runat="server" Text="Search" onclicked="uxSearchDatabase_Click" />
            <uc:MessageBox runat="server" ID="uxDisplayMessage"  />
            <br />
            <br />
                <cc1:DataGrid ID="uxSearchResults"  runat="server" AutoGenerateColumns="False" onrowcommand="uxSearchDatabase_RowCommand" OnPageIndexChanging="uxSearchResults_PageIndexChanging"> 
                <Columns>
                    <asp:ImageField DataImageUrlField="PictureUrl" DataImageUrlFormatString="~/Uploads/Thumbs/{0}" AlternateText="Edit"  ControlStyle-Height = "50px">                    
                    </asp:ImageField>
                    <asp:BoundField DataField="PropertyType" HeaderText="Property type" SortExpression="PropertyType" />
                    <asp:BoundField DataField="Description" HeaderText="Description" />
                    <asp:BoundField DataField="HasAttachments" HeaderText="Attachments?" />
                    <asp:BoundField DataField="DateWasStolen" HeaderText="Date went missing" DataFormatString="{0:dd MMMM yyyy}" />
                </Columns>
                </cc1:DataGrid>            
                <br />
        </div>
    </div>
</asp:Content>

