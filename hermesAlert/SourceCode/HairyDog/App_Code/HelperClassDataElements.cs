﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using attr = derotek.DataAccess.Attributes;

/// <summary>
/// Summary description for HelperClassDataElements
/// </summary>
public class HelperClassDataElements
{

    public static int MaximumAreasToRetrieve = 8;
    public static int MaximumitemsPerAreaToRetrieve = 25;

    public static void SetDropdownListSelecteditem(DropDownList listToSetIn, long itemToSelect)
    {
        foreach (ListItem item in listToSetIn.Items)
        {
            if (long.Parse(item.Value) == itemToSelect)
            {
                item.Selected = true;
                break;
            }
        }
    }

    public static void PopulateDropdownList(DropDownList listToPopulate, Dictionary<long, string> data)
    {
        listToPopulate.Items.Clear();
        foreach (System.Collections.Generic.KeyValuePair<long, string> item in data)
        {
            ListItem newItem = new ListItem(item.Value, item.Key.ToString());
            listToPopulate.Items.Add(newItem);
        }
    }
    public static void PopulateDropdownList<T>(DropDownList listToPopulate, List<T> dataOfT)
        where T : DORM.Interfaces.IEntity
    {
        listToPopulate.Items.Clear();
        foreach (T entry in dataOfT)
        {
            ListItem item = new ListItem(attr.IsDisplayFieldAttribute.GetPropertyValue<T>((T)entry), entry.Id.ToString());
            listToPopulate.Items.Add(item);
        }
    }    
}