﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for JavaScript
/// </summary>
public class JavaScript
{
	public static string ShowGpsSelectionFormScript(string longitudeClientId, string latitudeClientId)
    {
        return "<script type='text/javascript'>" +
        "    function RemoteFunction(latitude, longitude) {" +
        "       document.getElementById('" + latitudeClientId + "').value = latitude;" +
        "       document.getElementById('" + longitudeClientId + "').value = longitude;" +
        "    }" +
        "" +
        "    function showList() {" +
        "       var latitude = document.getElementById('" + latitudeClientId + "').value;" +
        "       var longitude = document.getElementById('" + longitudeClientId + "').value;" +
        "       url = '../Pages_Main/SelectGpsLocation.aspx?lat=' + latitude + '&long=' + longitude;" +
        "       returnValue = window.showModalDialog(url , window, 'dialogWidth=1233px;dialogHeight=745px;resizable=yes;help=no;unadorned=yes');" + //Fix for IE9: Pass in the [window] object
        "    }" +
        "</script>";            
    }	
}