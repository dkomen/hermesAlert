﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public enum SectionInfoId
{
    RegisteredItems, 
    RegisterItem,
    ItemAttachments,
    AvailableAttachments,
    UploadedAttachment,
    EventsInMonitoringAreas,
    AttachmentMaintenance,
    MultiMap,
    SearchDatabase,
    QuickIncidents,
    QuickIncidentReport,
    Profile,
    NewAccount,
    NewItemMessage,
    RegisteredItemMessages,
    FaqHeading,
    ForgotLogonDetails,
    ResetPassword,
    SendMessage,
    MissingPersons,
    MissingPersonDetails,
    GroupMembership,
    AvailableGroups
}

/// <summary>
/// Summary description for SectionInfo
/// </summary>
public class SectionInfo
{
    #region Constructors
    public SectionInfo(SectionInfoId id, string title, string data)
    {
        Id = id;
        Title = title;
        Data = data;
    }
    #endregion

    #region Properties
    public SectionInfoId Id { get; private set; }
    public string Title { get; private set; }
    public string Data { get; private set; }
    #endregion
}