﻿using System;
using System.Collections.Generic;
using System.Web;


/// <summary>
/// Summary description for SessionVariables
/// </summary>
public static class SessionVariables
{
    #region Fields
    public static string UserId = "UserId";
    public static string StyleThemeId = "StyleThemeId";
    public static string LoggedOnUser = "LoggedOnUser";
    public static string ActiveObjectId = "ActiveObjectId";
    public static string ChildObjectId = "ChildObjectId";
    public static string ReturnToPage = "ReturnToPage";
    #endregion


    #region Constructors
    static SessionVariables()
    {
        RootPathToFiles = SessionVariables.RootPath(false) + @"Uploads\";
        PathToResized = RootPathToFiles + @"Resized\";
        PathToThumbs = RootPathToFiles + @"Thumbs\";
    }
    #endregion


    #region Properties
    public static void CurrentUserSet(System.Web.SessionState.HttpSessionState session, derotek.HairyDog.Users.User user)
    {
        session[SessionVariables.LoggedOnUser] = user;
    }
    public static derotek.HairyDog.Users.User CurrentUserGet(System.Web.SessionState.HttpSessionState session)
    {
        if (session[SessionVariables.LoggedOnUser] == null || ((derotek.HairyDog.Users.User)session[SessionVariables.LoggedOnUser]).Id == 0)
        {
            return new derotek.HairyDog.Users.User();
        }
        else
        {
            return (derotek.HairyDog.Users.User)session[SessionVariables.LoggedOnUser];
        }
    }
    public static string RootPath(bool getVirtualPath)
    {
        string virtualPath = HttpContext.Current.Request.ApplicationPath;
        if (getVirtualPath)
        {
            if (!virtualPath.EndsWith("/"))
            {
                virtualPath += "/";
            }
            return virtualPath;
        }
        else
        {
            string path = HttpContext.Current.Server.MapPath(virtualPath);
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }
            return path;
        }
    }

    public static string RootPathToFiles
    {
        get;
        private set;
    }
    public static string PathToResized
    {
        get;
        private set;
    }
    public static string PathToThumbs
    {
        get;
        private set;
    }   
    #endregion
}