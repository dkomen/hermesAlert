﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// A class that will enable the passing of location information between upto two pages. Both pages must inherit from this class
/// </summary>
public abstract class PageBaseLocationTransfers: PageBase, derotek.HairyDog.Interfaces.IGpsCoordinate
{
    #region Constructors
    public PageBaseLocationTransfers()
	{
        if (Page.AppRelativeVirtualPath == ReturnToPage)
        {
            //Repopulate the page fields
            RePopulatePageFields(Page.AppRelativeVirtualPath);
        }
    }
    #endregion

    #region Properties
    #region IGpsCoordinate
    public abstract double Longitude
    {
        get;
        set;
    }
    public abstract double Latitude
    {
        get;
        set;
    }
    #endregion
    #endregion

    #region Public abstract methods
    public abstract void CallGpsPage(string pageToCallForSelectingGpsData);
    /// <summary>
    /// Override this call on the inheriting page and place in it all the required field mapping methods
    /// </summary>
    /// <param name="pageName"></param>
    public abstract void RePopulatePageFields(string pageName);
    #endregion
}