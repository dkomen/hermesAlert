﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.IO;

/// <summary>
/// Summary description for Handler
/// </summary>
public class Handler : IHttpHandler
{

    public static string UploadPath = string.Empty;

    public bool IsReusable
    {
        get;
        private set;
    }

    public void ProcessRequest(HttpContext context)
    {
        Handler.UploadPath = SessionVariables.RootPath(false) + @"Uploads\";
        switch (context.Request.HttpMethod.ToUpper())
        {
            case "GET":
                Get(context);
                break;
            case "POST":
                Post(context);
                break;
            case "PUT":
                Put(context);
                break;
            case "DELETE":
                break;
            default:
                break;
        }
    }

    private void Get(HttpContext context)
    {
        context.Response.Write("Done a Get!");
    }
    private void Post(HttpContext context)
    {

        context.Response.Write("Done a Post!");
    }
    private void Put(HttpContext context)
    {
        //byte[] dataArrived = new byte[context.Request.ContentLength];
        //context.Request.InputStream.Read(dataArrived, 0, context.Request.ContentLength);
        //string text = System.Text.ASCIIEncoding.UTF8.GetString(dataArrived);

        Parameters param = new Parameters(context);

        #region Check version
        if (param.Items[Parameters.VersionMajor]==string.Empty || int.Parse(param.Items[Parameters.VersionMajor]) != 0)
        {
            context.Response.Write("Wrong version, please upgrade hermesAlert uploader!");
            return;
        }
        #endregion

        if (param.PacketType == PacketDataType.Authorise)
        {
            if (derotek.HairyDog.Model.UserAuthentication.AuthenticateUser(param.Items[Parameters.UserName], param.Items[Parameters.Password]) == false)
            {
                if (param.Items[Parameters.Password] == "x")
                {
                    context.Response.Write("Yes");
                }
                else
                {
                    context.Response.Write("Unknown username or password");
                }
            }
            else
            {
                context.Response.Write("Yes");
            }
        }
        else if (param.PacketType == PacketDataType.PhotoUpload)
        {

            if (derotek.HairyDog.Model.UserAuthentication.AuthenticateUser(param.Items[Parameters.UserName], param.Items[Parameters.Password]))
            {
                DORM.Filter filter = new DORM.Filter();
                filter.Add(new DORM.FilterItem("Username",DORM.Enumerations.FilterCriteriaComparitor.Equals, param.Items[Parameters.UserName]));
                derotek.HairyDog.Users.User user;
                using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                {
                    user = transaction.RetrieveFirst<derotek.HairyDog.Users.User>(filter);
                }
                try
                {
                    #region Save file
                    string fileName = UploadPath + System.Guid.NewGuid().ToString() + ".jpg";
                    byte[] b = System.Convert.FromBase64String(param.Items[Parameters.Photo]);

                    System.IO.MemoryStream memStream = new MemoryStream(b);
                    System.Drawing.Image image = System.Drawing.Bitmap.FromStream(memStream);
                    System.Drawing.Bitmap bitmap = (System.Drawing.Bitmap)image;

                    #endregion

                    #region Save newly uploaded fiel to db
                    string newFileName = System.Guid.NewGuid().ToString() + ".jpg";

                    //resize file                     
                    System.Drawing.Bitmap resizedBitmap = derotek.HairyDog.Graphics.ImageManipulation.ResizeImage(bitmap, derotek.HairyDog.Graphics.ImageManipulation.MaximumAlbumPicturePixelSize, UploadPath + @"Resized\" + newFileName);
                    //resizedBitmap.Save();
                    System.Drawing.Bitmap thumbBitmap = derotek.HairyDog.Graphics.ImageManipulation.ResizeImage(bitmap, derotek.HairyDog.Graphics.ImageManipulation.MaximumThumbnailImagePixelSize, UploadPath + @"Thumbs\" + newFileName);
                    //thumbBitmap.Save();

                    using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                    {
                        filter = new DORM.Filter();
                        filter.Add(new DORM.FilterItem("ExternalReferanceId",DORM.Enumerations.FilterCriteriaComparitor.Equals, (long)derotek.HairyDog.Enumerations.AttachmentType.Picture));
                        derotek.HairyDog.Metrics.AttachmentType attachmentType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.AttachmentType>(filter);

                        derotek.HairyDog.RemoteLibrary.UploadedFile newUploadedFile = new derotek.HairyDog.RemoteLibrary.UploadedFile();
                        newUploadedFile.Path = newFileName;
                        newUploadedFile.UserId = user.Id;
                        newUploadedFile.AttachmentType = attachmentType;
                        transaction.AddOrUpdate(newUploadedFile);
                        transaction.Commit();
                    }
                    #endregion

                    context.Response.Write("Image uploaded successfully");
                }
                catch (Exception ex)
                {
                    context.Response.Write("An error occurred on the server");
                }
            }
            else
            {
                context.Response.Write("Unknown username or password");
            }
        }
        else
        {
            context.Response.Write("Could not process request");
        }
    }
}

public enum PacketDataType
{
    Authorise = 1,
    PhotoUpload = 2
}
public class Parameters
{
    #region Fields
    private static string KeyPrefix = "-key:";
    public static string DataType = KeyPrefix + "datatype:";
    public static string UserName = KeyPrefix + "userName:";
    public static string Password = KeyPrefix + "pwd:";
    public static string Photo = KeyPrefix + "photo:";
    public static string VersionMajor = KeyPrefix + "versionmajor:";
    public static string VersionMinor = KeyPrefix + "versionminor:";
    #endregion

    #region Properties
    public PacketDataType PacketType { get; set; }
    public Dictionary<string, string> Items { get; set; }
    #endregion

    public Parameters(HttpContext context)
    {
        Items = new Dictionary<string, string>();
        byte[] dataArrived = new byte[context.Request.ContentLength];
        context.Request.InputStream.Read(dataArrived, 0, context.Request.ContentLength);
        string fullPacketText = System.Text.ASCIIEncoding.UTF8.GetString(dataArrived);

        PacketType = GetPacketType(fullPacketText);


        Items.Add(UserName, GetValueFor(UserName, fullPacketText));
        Items.Add(Password, GetValueFor(Password, fullPacketText));
        Items.Add(VersionMajor, GetValueFor(VersionMajor, fullPacketText));
        Items.Add(VersionMinor, GetValueFor(VersionMinor, fullPacketText));
        switch (PacketType)
        {
            case PacketDataType.Authorise:
                break;
            case PacketDataType.PhotoUpload:
                Items.Add(Photo, GetValueFor(Photo, fullPacketText));
                break;
        }

    }

    private PacketDataType GetPacketType(string packetData)
    {
        int lengthOfFirstLine = packetData.Length;
        if (packetData.Contains("\r\n"))
        {
            lengthOfFirstLine = packetData.IndexOf("\r\n");
        }

        string packetDataTypeData = packetData.Substring(0, lengthOfFirstLine);

        return (PacketDataType)int.Parse(packetDataTypeData.Split(':')[2]);

    }

    private string GetValueFor(string key, string dataToSearch)
    {
        string[] dataLines = dataToSearch.Split("\r\n".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        System.Text.StringBuilder result = new StringBuilder();

        bool foundValueSection = false;
        foreach (string line in dataLines)
        {
            if (line.StartsWith(key))
            {
                result.AppendLine(line.Substring(key.Length, line.Length - key.Length));
                foundValueSection = true;
            }
            else if (foundValueSection && !line.StartsWith(KeyPrefix))
            {
                result.AppendLine(line);
            }
            else if (foundValueSection)
            {
                break;
            }
        }
        string returnValue = result.ToString();
        if (returnValue.EndsWith("\r\n"))
        {
            returnValue = returnValue.Substring(0, returnValue.Length - 2);
        }
        return returnValue;
    }
}