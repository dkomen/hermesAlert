﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for Class1
/// </summary>
public class PageBase : System.Web.UI.Page
{
    protected override void OnLoad(EventArgs e)
    {
        string g = derotek.HairyDog.Model.Mailer.GetSmtpUserPassword();
        for (int i = 0; i < Session.Contents.Count;i++ )
        {
            derotek.BikeCasa.Mvp.IStatePersistableObject stateItem = Session.Contents[i] as derotek.BikeCasa.Mvp.IStatePersistableObject;
            if (stateItem != null)
            {
                if (!stateItem.KeepAliveForPages.Contains(this.AppRelativeVirtualPath))
                {
                    Session[i] = null;
                }
            }
        }

        string thisPage = this.Page.AppRelativeVirtualPath;
        if (!IsPostBack)
        {            
            derotek.Exceptions.StandardException.FlatFileLogPath = SessionVariables.RootPath(false);
            derotek.HairyDog.QueryString qs = new derotek.HairyDog.QueryString();
            string refVar = Request.QueryString["ref"];
           
            if (!string.IsNullOrEmpty(refVar))
            {
                qs.DecodeQueryString(refVar);
                if (qs.Variables.ContainsKey(SessionVariables.ReturnToPage))
                {
                    ReturnToPage = qs.Variables[SessionVariables.ReturnToPage];
                }
                if (qs.Variables.ContainsKey(SessionVariables.ActiveObjectId))
                {
                    ActiveObjectId = long.Parse(qs.Variables[SessionVariables.ActiveObjectId]);
                }
                if (qs.Variables.ContainsKey(SessionVariables.ChildObjectId))
                {
                    ChildObjectId = long.Parse(qs.Variables[SessionVariables.ChildObjectId]);
                }
            }

            //try
            //{
                //derotek.HairyDog.PageHits hit = new derotek.HairyDog.PageHits();
                //hit.ReferrerIp = this.Request.UserHostAddress;
                //hit.Page = this.Request.FilePath.Replace("\\", "/");
                //hit.UserId = SessionVariables.CurrentUserGet(Session).Id;
                //using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
                //{
                 //   transaction.AddOrUpdate(hit);
                //    transaction.Commit();
                //}
            //}
            //catch (Exception ex)
            //{
            //    new derotek.Exceptions.StandardException("Could not update hit counter : " + ex.InnerException.Message + " : " + ex.InnerException.InnerException.Message, ex);
            //}

        }
        
        base.OnLoad(e);
    }
    public override string StyleSheetTheme
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session).Theme;
        }
        set
        {

        }
    }
    /// <summary>
    /// The id of the root object being maintained by this page. 
    /// </summary>
    public long ActiveObjectId
    {
        get
        {
            if (ViewState[SessionVariables.ActiveObjectId] == null)
            {
                return 0;
            }
            else
            {
                return long.Parse(ViewState[SessionVariables.ActiveObjectId].ToString());
            }
        }
        set
        {
            ViewState[SessionVariables.ActiveObjectId] = value;
        }
    }
    /// <summary>
    /// The id of being maintained by this page IF the ActiveObjectId actually refers to the id of the object being maintained on the ReturnToPage 
    /// </summary>
    public long ChildObjectId
    {
        get
        {
            if (ViewState[SessionVariables.ChildObjectId] == null)
            {
                return 0;
            }
            else
            {
                return long.Parse(ViewState[SessionVariables.ChildObjectId].ToString());
            }
        }
        set
        {
            ViewState[SessionVariables.ChildObjectId] = value;
        }
    }
    /// <summary>
    /// The calling page\page to return to
    /// </summary>
    public string ReturnToPage
    {
        get
        {
            if (ViewState[SessionVariables.ReturnToPage] == null)
            {
                return string.Empty;
            }
            else
            {
                return ViewState[SessionVariables.ReturnToPage].ToString();
            }
        }
        set
        {
            ViewState[SessionVariables.ReturnToPage] = value;
        }
    }
}