﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SectionInfoEntries
/// </summary>
public static class SectionInfoEntries
{
    #region Fields
    public static List<SectionInfo> _sectionInfoEntries = new List<SectionInfo>();
    #endregion

    #region public Constructors
    static SectionInfoEntries()
    {
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.RegisteredItems, "Your registered items", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.RegisterItem, "Item maintenance", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.ItemAttachments, "Searchable attachments (pictures, serial numbers etc.)", ""));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.UploadedAttachment, "Current attachment file", ""));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.AvailableAttachments, "Available attachments", ""));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.EventsInMonitoringAreas, "Events in the areas you are monitoring", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.AttachmentMaintenance, "Attachment maintenance", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.MultiMap, "Edit the areas you wish to monitor", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.SearchDatabase, "Search the public database for stolen\\missing items", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.QuickIncidents, "Quick incident reports", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.QuickIncidentReport, "Maintain incident", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.Profile, "Profile details", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.NewAccount, "Create a new account", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.NewItemMessage, "Item message", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.RegisteredItemMessages, "Community messages", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.FaqHeading, "Frequently asked questions", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.ForgotLogonDetails, "Forgot logon details", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.ResetPassword, "Reset logon password", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.SendMessage, "Send us a message", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.MissingPersons, "Missing persons alert!", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.MissingPersonDetails, "Missing person details", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.GroupMembership, "Current group memberships", "Some data"));
        _sectionInfoEntries.Add(new SectionInfo(SectionInfoId.AvailableGroups, "Groups available to join", "Some data"));     
    }
    #endregion

    #region Properties
    public static List<SectionInfo> Entries
    {
        get
        {
            return _sectionInfoEntries;
        }
    }
    #endregion

    #region Public Properties
    public static SectionInfo GetWithTextName(string textName)
    {
        foreach (SectionInfo info in _sectionInfoEntries)
        {
            if (info.Id.ToString().ToLower() == textName.ToLower())
            {
                return info;
            }            
        }
        return null;
    }
    #endregion
}