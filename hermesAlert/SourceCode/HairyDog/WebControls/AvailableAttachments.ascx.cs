﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using derotek.HairyDog;

public partial class WebControls_AvailableAttachments : System.Web.UI.UserControl, derotek.BikeCasa.Mvp.Attachments.IAvailableAttachmentsView
{
    #region Fields
    public enum ImageSelectionMode
    {
        NoSelection = 1,
        SingleImage = 2,
        ManyImages = 3
    }
    private string _uploadFileName = string.Empty;
    private derotek.BikeCasa.Mvp.Attachments.AttachmentsAvailablePresenter _presenter;
    #endregion

    #region Properties
    private ImageSelectionMode _ImageSelectionModeOption;
    /// <summary>
    /// How are images in this gallery to be selected
    /// </summary>
    public ImageSelectionMode ImageSelectionModeOption
    {
        get
        {
            return _ImageSelectionModeOption;
        }
        set
        {
            _ImageSelectionModeOption = value;
        }
    }
    public derotek.HairyDog.Interfaces.IAttachmentsHost AttachmentsHost
    {
        get
        {
            return (derotek.HairyDog.Interfaces.IAttachmentsHost)ViewState["AttachmentsHost"];
        }
        set
        {
            ViewState["AttachmentsHost"] = value;
        }
    }
    #endregion

    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        ImageSelectionModeOption = ImageSelectionMode.SingleImage;
        string p = Page.AppRelativeVirtualPath;
        _presenter = new derotek.BikeCasa.Mvp.Attachments.AttachmentsAvailablePresenter(this, SessionVariables.CurrentUserGet(Session).Id);
        if (!IsPostBack)
        {
            _presenter.PrepareViewForUse();
            this.Session["UploadInfo"] = new UploadInfo { IsReady = false };
        }
    }
    #endregion

    #region Public Functions
    public void LoadAlbumImages()
    {
        _presenter.PrepareViewForUse();
    }
    public string GetHiddenControlId()
    {
        return uxHiddenFieldForSelectedItems.ClientID;
    }
    public string SelectedImage
    {
        get
        {
            return uxHiddenFieldForSelectedItems.Value;
        }
    }
    #endregion

    #region View
    public long PhotoAlbumId
    {
        get;
        set;
    }
    public string NewImagePath
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public IList<derotek.HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject> PhotoAlbumEntries
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            #region New
            string html = string.Empty;
            string checkedValue = "true";
            html = "<div style='display:block;'>";
            foreach (derotek.HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject entry in value)
            {
                string selectionObject = string.Empty;
                switch (ImageSelectionModeOption)
                {
                    case WebControls_AvailableAttachments.ImageSelectionMode.SingleImage:
                        selectionObject = "<input type='radio' id='" + entry.Id + "' checked='" + checkedValue + "' name='selectASingleImageGroup' value='" + entry.Id + "' onclick=\"toggleSet(this," + (int)WebControls_AvailableAttachments.ImageSelectionMode.SingleImage + ", '" + entry.Id + "')\">";
                        uxHiddenFieldForSelectedItems.Value = entry.Id.ToString();
                        checkedValue = "false";
                        break;
                }
                string pictureHtml = string.Empty;
                if (entry.IsPicture)
                {
                    pictureHtml = "<a href='" + SessionVariables.RootPath(true) + "Uploads/Resized/" + entry.Path + "' rel='prettyPhoto[gallery1]'title=''>" +
                        "<img style='margin:3px' class='cornersButton' height='116px' src='" + SessionVariables.RootPath(true) + "Uploads/Thumbs/" + entry.Path + "' alt='' /></a>";
                }
                else
                {
                    pictureHtml = "<img style='margin:3px' class='corners' src='" + SessionVariables.RootPath(true) + "Images/NoAttachment.png' alt='' />";
                }
                string imageEntry = "<div style='display:inline-block;background-color:#f4efe5;margin:3px;text-align:center' class='cornersButton'>"
                    + pictureHtml + "<br />"
                    + selectionObject + "</div>";
                html += imageEntry;
            }
            html += "</div>";
            uxFF.InnerHtml = html;
            #endregion
            return;
            #region old
            //string html = string.Empty;
            //string checkedValue = "true";
            //html = "<table><tr valign='bottom'>";
            //foreach (derotek.HairyDog.DisplayableItems.DisplayableAvailableAttachmentObject entry in value)
            //{
            //    string selectionObject = string.Empty;
            //    switch (ImageSelectionModeOption)
            //    {
            //        case WebControls_AvailableAttachments.ImageSelectionMode.SingleImage:
            //            selectionObject = "<input type='radio' id='" + entry.Id + "' checked='" + checkedValue + "' name='selectASingleImageGroup' value='" + entry.Id + "' onclick=\"toggleSet(this," + (int)WebControls_AvailableAttachments.ImageSelectionMode.SingleImage + ", '" + entry.Id + "')\">";
            //            uxHiddenFieldForSelectedItems.Value = entry.Id.ToString();
            //            checkedValue = "false";
            //            break;
            //    }
            //    string pictureHtml = string.Empty;
            //    if (entry.IsPicture)
            //    {
            //        pictureHtml = "<a href='" + SessionVariables.RootPath(true) + "Uploads/Resized/" + entry.Path + "' rel='prettyPhoto[gallery1]'title=''><img class='corners' height='120px' src='" + SessionVariables.RootPath(true) + "Uploads/Thumbs/" + entry.Path + "' alt='' /></a><br />";
            //    }
            //    else
            //    {
            //        pictureHtml = "<img class='corners' src='" + SessionVariables.RootPath(true) + "Images/NoAttachment.png' alt='' /><br />";
            //    }
            //    string imageEntry = "<table style='background-color:#dadaff' class='corners' ><tr><td align='center'>"
            //        + pictureHtml
            //        + selectionObject + "</td></tr></table>";
            //    html += "<td>" + imageEntry + "</td>";
            //}
            //html += "</tr></table>";
            //uxFF.InnerHtml = html;
            #endregion
        }
    }
    public string AlbumName
    {
        get;
        set;
    }
    public string AlbumDescription
    {
        get;
        set;
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        throw new NotImplementedException();
    }
    public void RedirectView(string path)
    {
        Server.Transfer(path, false);
    }
    public long ActiveObjectId
    {
        get
        {
            return ((PageBase)Page).ActiveObjectId;
        }
        set
        {
            ((PageBase)Page).ActiveObjectId = value;
        }
    }
    public string ReturnToPage
    {
        get
        {
            return ((PageBase)Page).ReturnToPage;
        }
        set
        {
            ((PageBase)Page).ReturnToPage = value;
        }
    }
    #endregion
}