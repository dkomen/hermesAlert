﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class WebControls_ImageUploader : System.Web.UI.UserControl
{
    public derotek.HairyDog.Interfaces.IAttachmentsHost AttachmentsHost
    {
        get
        {
            return (derotek.HairyDog.Interfaces.IAttachmentsHost)ViewState["AttachmentsHost"];
        }
        set
        {
            ViewState["AttachmentsHost"] = value;
        }
    }
    /// <summary>
    /// The path which the NEWLY UPLOADED file was saved to 
    /// </summary>
    public string PathSavedTo
    {
        get
        {
            return uxPathSavedTo.Value;
        }
    }
    public string AlbumId
    {
        get
        {
            return ViewState["AlbumId"].ToString();
        }
        set
        {
            ViewState["AlbumId"] = value;
        }
    }    
    /// <summary>
    /// The file name of the image to display to the user
    /// </summary>
    public string PathToImage
    {
        get
        {
            return uxUploadedImage.ImageUrl;
        }
        set
        {
            uxUploadedImage.ImageUrl = value;
        }
    }
    public string GetPathSavedToHiddenFieldId()
    {
        return uxPathSavedTo.ClientID;
    }
    /// <summary>
    /// Disable the uloading capability of the control, and only display the image (if there is one)
    /// </summary>
    public bool Enabled
    {
        get
        {
            return true;// file_upload.Visible;
        }
        set
        {
            //file_upload.Visible = value;
            uxUploadCaption.Visible = false;
        }
    }
    
}