﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="MessageBox.ascx.cs" Inherits="WebControls_MessageBox" %>
<br />
<asp:Table ID="uxMessageBox" runat="server" CellPadding="8" BorderWidth="1" SkinID="MessageBoxError">
    <asp:TableRow>
        <asp:TableCell>            
            <asp:Repeater ID="uxMessageRepeater" runat="server" >
                <ItemTemplate>
                    <asp:Label runat="server" ID="uxMessage" SkinID="ErrorMessage" Text='<%# Container.DataItem %>' /><br />
                </ItemTemplate>
            </asp:Repeater>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
<br />
