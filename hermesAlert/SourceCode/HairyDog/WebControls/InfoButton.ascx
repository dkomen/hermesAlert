﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="InfoButton.ascx.cs" Inherits="WebControls_InfoButton" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<style type="text/css">
    .modalBackground {
    background-color: #000;
    filter: alpha(opacity=70);
    opacity: 0.7;
    }
    .modalPopup
    {
      background-color: #ededfd;
      border-width: 5px;
      padding: 3px;
      width: 575px;
      height:350px;
      text-align: center;
      border-radius: 20px;          
    }
</style>

<asp:Button ID="uxButton" runat="server" SkinID="InfoButton" Width="50px" Height="16px" />

<div runat="server" ID="uxPopupView" style="visibility:visible">
    <asp:panel ID="Panel1" runat="server" cssClass="modalPopup" class="cornersButton" style="border-color:Red;border-width:3px">
        <div runat="server" ID="Div3" style="height:348px;padding:1px;background-color:#fafadd;border-radius:18px">
            <asp:Label ID="uxTitle" runat="server" />
            <br />
            <asp:Button runat="server" ID="uxClosePopup" Text="Ok" />
        </div>
    </asp:panel>
</div>
<asp:ModalPopupExtender TargetControlID="uxButton" BackgroundCssClass="modalBackground" 
    OkControlID="uxClosePopup" ID="PopupControlExtender1" runat="server" BehaviorID='showInfoPopup'>    
</asp:ModalPopupExtender>