﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ImageUploader.ascx.cs" Inherits="WebControls_ImageUploader" %>
<%@ Register TagName="Button" TagPrefix="uc" Src="~/WebControls/Button.ascx"  %>

    <%--uploadify--%>
    <script type="text/javascript" src='<%= SessionVariables.RootPath(true) %>Scripts/jquery-1.6.1.min.js'></script>
    <link href="<%=SessionVariables.RootPath(true) %>Scripts/uploadify/uploadify.css" type="text/css" rel="stylesheet" />    
    <script type="text/javascript" src="<%=SessionVariables.RootPath(true) %>Scripts/uploadify/swfobject.js"></script>    
    <script type="text/javascript" src="<%=SessionVariables.RootPath(true) %>Scripts/uploadify/jquery.uploadify.v2.1.4.js"></script>
        <script type="text/javascript">
            var savePath = "Uploads";
            $(document).ready(function () {
                $('#file_upload').uploadify({
                    'uploader': '<%=SessionVariables.RootPath(true) %>Scripts/uploadify/uploadify.swf',
                    'script': '<%=SessionVariables.RootPath(true) %>Scripts/uploadify/Upload.ashx',
                    'scriptData': { 'savePath': savePath },
                    'queuesizelimit': 11,
                    'sizelimit': 524288,
                    'buttonText': 'Upload new file',
                    'cancelImg': '<%=SessionVariables.RootPath(true) %>Scripts/uploadify/cancel.png',
                    'fileExt': '*.jpg;*.png;*.gif',
                    'fileDesc': 'Image Files (.JPG, .GIF, .PNG)',
                    'folder': 'Uploads',
                    'multi': false,
                    'auto': false,
                    onProgress: function () {
                        $('#loader').show();
                    },
                    onComplete:
                    function (event, ID, fileObj, response, data) {
                        var uxPathSaved = document.getElementById('<%=GetPathSavedToHiddenFieldId() %>');
                        uxPathSaved.value = response.toString();
                        $.ajax(
                        {
                            type: "POST",
                            url: "<%=SessionVariables.RootPath(true) %>WebControls/Upload.aspx/UploadComplete",
                            data: "{'albumId': '<%= AlbumId %>', 'imagePath': '" + fileObj.filePath + "', 'imageName': '" + fileObj.name.toString() + "', 'savedFullPath': '" + response.toString() + "', caption: 'The caption'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                msgbox.html(msg.d);
                            }
                        });
                        uploadedImage = document.getElementById('<%=uxUploadedImage.ClientID %>');
                        uploadedImage.src = "<%=SessionVariables.RootPath(true) %>Uploads/" + response.toString();
                    },
                    onError: function (a, b, c, d) {
                        if (d.info == 404)
                           // alert('Could not find upload script. Use a path relative to: ' + '<?= getcwd() ?>');
                            alert('An error occured on upload. Make sure this is a valid image and is less than 512Kb in size');
                        //else
                        //alert('error ' + d.type + ": " + d.info);
                    },
                    'auto': true
                });
            });
    </script>
    <%--end uploadify--%>    
                                
    <asp:Table ID="Table1" runat="server" SkinID="Normal" BorderWidth="1px" BorderColor="#dddddd" SkindId="Normal" class="cornersButton" BackColor="#f4efe5">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="Table3" BackColor="#f4efe5" class="cornersButton" runat="server">
                    <asp:TableRow>
                        <asp:TableCell Width="335px">
                            <asp:Image runat="server" id="uxUploadedImage" style="width:100%" alt='' class="cornersButton"/>
                        </asp:TableCell>
                    </asp:TableRow> 
                </asp:Table>               
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ColumnSpan="2">                                       
                <div class="upload">
                    <div>
                        <input id="file_upload" name="file_upload" type="file" />
                        <br /><asp:Label runat='server' ID='uxUploadCaption' SkinID='SmallNoWidthGiven' Text='Max. file size allowed is 512Kb. Uploads will be converted to jpg format'/> 
                    </div>
                    <br />
                </div> 
            </asp:TableCell>
        </asp:TableRow>        
    </asp:Table>
    
    <%--uxPathSavedTo = Path the NEWLY UPLOADED file was saved to--%>
    <asp:HiddenField ID="uxPathSavedTo" runat="server" />
