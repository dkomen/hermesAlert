﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Slideshow.ascx.cs" Inherits="WebControls_Slideshow" %>
<div style="text-align:center;padding:0px">               
    <span style="position:absolute;top:0px;left:0px;background-color:#998866;width:420px;height:158px;border:2px solid #776644" class="corners" >
        <span id='uxSlideshowImageCaption' style="font-family:verdana;font-size:10pt;font-weight:bold;color:white;position:relative;top:1px;left:-60px"></span>
        <span id="uxSlideshowImageText" style="background-color:#aa9977;position:absolute;top:23px;left:0px;width:270px;height:102px;padding: 10px 18px 8px 8px;text-align:left;
            font-family:verdana;color:White;font-size:x-small"></span>
        <span style="background-color:#ad9c7a;position:absolute;top:0px;left:328px;height:158px;width:70px">
            <img class="cornersButtonTight" id='uxSlideshowImage' src="" alt='Slideshow image' style="position:absolute;top:10px;left:-46px" /> <%--position:absolute;top:28px;left:298px--%>
        </span>
    </span>                
            
    <span style="position:relative;top:138px;left:32px;visibility:hidden">
        <font style="font-family:verdana;font-style:italic;font-weight:bold;font-size:12px;color:#554422">pause</font> 
        <input style="position:relative;top:3px" type="checkbox" id="uxPauseControl" />
    </span>
</div>


<script type="text/javascript">
    var _firstLoad = true;

    ///The index of the current slideshow item to display
    var _currentSlideSHowIndex = 0;

    /// How delay, in milliseconds, to when the slideshow should swith to a new item
    var _slideshowTimer = 15000;

    /// The array of slideshow items that will be displayed
    var _imagesArray = new Array(<%= GetSlideshowItems()%>);

    StartSlideshow();

    //Start the slideshow
    function StartSlideshow() {
        ///Check that the slideshow has not been paused.. also check if this is perhaps the VERY FIRST time the slideshow executes
        if (document.getElementById("uxPauseControl").checked == false || _firstLoad) {
            _firstLoad = false;
            var uxSlideshowImage = document.getElementById("uxSlideshowImage");
            var uxSlideshowImageCaption = document.getElementById("uxSlideshowImageCaption");
            var uxSlideshowImageText = document.getElementById("uxSlideshowImageText");
            uxSlideshowImage.src = _imagesArray[_currentSlideSHowIndex].Image;
            uxSlideshowImageCaption.innerHTML = _imagesArray[_currentSlideSHowIndex].Caption;
            uxSlideshowImageText.innerHTML = _imagesArray[_currentSlideSHowIndex].Text;
            _currentSlideSHowIndex++;
            if (_currentSlideSHowIndex > (_imagesArray.length - 1)) {
                ///If we are at the end of the slideshow then return to the start again                
                _currentSlideSHowIndex = 0;
            }
        }

        //Set the slideshow timer to recursivly call back to this same method
        setTimeout("StartSlideshow();", _slideshowTimer);
    }

    //The slideshow array object
    function SlideshowItem(imagePath, caption, text) {
        this.Image = imagePath;
        this.Caption = caption;
        this.Text = text;
    }
</script>