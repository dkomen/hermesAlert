﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebControls_PageSectionHeading : System.Web.UI.UserControl
{
    private bool _breakBefore = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!BreakBefore)
        {
            uxDivBefore.Style.Add("height", "20px");
        }
        
    }

    public bool BreakBefore
    {
        get
        {
            return _breakBefore;
        }
        set
        {
            _breakBefore = value;
        }
    }

    public string SectionHeadingId
    {
        get
        {
            return uxText.Text;
        }
        set
        {
            uxText.Text = (SectionInfoEntries.GetWithTextName(value)).Title;
            uxInfoButton.SectionInfoId = value;
        }
    }
}