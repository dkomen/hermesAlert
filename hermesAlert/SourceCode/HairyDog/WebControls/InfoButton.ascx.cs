﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public delegate void OnInfoClickDelegate(object sender, EventArgs e);
public partial class WebControls_InfoButton : System.Web.UI.UserControl
{
    #region Fields
    public event OnInfoClickDelegate Clicked;
    private string _clientSideScript = string.Empty;
    #endregion

    #region Properties
    public string SectionInfoId
    {
        get
        {
            return null;
        }
        set
        {
            uxTitle.Text = (SectionInfoEntries.GetWithTextName(value)).Title;
        }
    }
    public string Text
    {
        get
        {
            return uxButton.Text;
        }
        set
        {
            if (value.Length <= 10)
            {
                int length = value.Length;
                int charsToAppend = (12 - value.Length)/2;
                string newValue = value;
                newValue = newValue.PadLeft(length + charsToAppend, ' ');
                newValue = newValue.PadRight(length + charsToAppend + charsToAppend, ' ');
                uxButton.Text = newValue;
            }
            else
            {
                uxButton.Text = " " + value + " ";
            }
        }
    }
    public string OnClientClick
    {
        set
        {
            _clientSideScript = value;           
        }
    }
    public bool LargeButton
    {
        get;
        set;
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        uxPopupView.ID = this.Parent.ID + "PopupView";
        PopupControlExtender1.PopupControlID = uxPopupView.ID; 
        if (!IsPostBack)
        {
            string javaScript = "<script type='text/javascript'>" +
            "    function SetPopupVisible(elementId) {" +
            "        var uxPopup = document.getElementById(elementId);" +
            "        uxPopup.style.visibility = 'visible';" +
            "    }" +
            "</script>";

            Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "PopupScript", javaScript);

            uxButton.Attributes.Remove("OnClick");
            uxButton.Attributes.Add("OnClick", "SetPopupVisible('" + uxPopupView.ClientID + "');" + (Clicked == null ? "return false;" : string.Empty));

            uxButton.Attributes.Remove("onmouseover");
            uxButton.Attributes.Remove("onmouseout");
            uxButton.Attributes.Add("onmouseover", "this.style.backgroundColor='#444477'");
            uxButton.Attributes.Add("onmouseout", "this.style.backgroundColor='#666666'");
        }
    }
    public void uxButton_Click(object sender, EventArgs e)
    {
        if (Clicked != null)
        {
            Clicked(sender, e);
        }
    }
}