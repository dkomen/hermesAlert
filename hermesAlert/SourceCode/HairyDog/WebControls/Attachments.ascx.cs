﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using derotek.HairyDog;

public partial class WebControls_Attachments : System.Web.UI.UserControl, derotek.BikeCasa.Mvp.Attachments.IAttachmentsView
{
    #region Fields
    public enum ImageSelectionMode
    {
        NoSelection = 1,
        SingleImage = 2,
        ManyImages = 3
    }
    private string _uploadFileName = string.Empty;
    private derotek.BikeCasa.Mvp.Attachments.AttachmentsPresenter _presenter;
    #endregion

    #region Properties
    private ImageSelectionMode _ImageSelectionModeOption;
    /// <summary>
    /// How are images in this gallery to be selected
    /// </summary>
    public ImageSelectionMode ImageSelectionModeOption
    {
        get
        {
            return _ImageSelectionModeOption;
        }
        set
        {
            _ImageSelectionModeOption = value;
        }
    }
    public derotek.HairyDog.Interfaces.IAttachmentsHost AttachmentsHost
    {
        get
        {
            return (derotek.HairyDog.Interfaces.IAttachmentsHost)ViewState["AttachmentsHost"];
        }
        set
        {
            ViewState["AttachmentsHost"] = value;
        }
    }
    public bool ReadOnlyMode
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxAddNewAttachment.Visible = value;
            uxModifySelectedAttachment.Visible = value;
        }
    }
    #endregion

    #region UI Events
    protected void Page_Load(object sender, EventArgs e)
    {
        ImageSelectionModeOption = ImageSelectionMode.SingleImage;
        string p = Page.AppRelativeVirtualPath;
        _presenter = new derotek.BikeCasa.Mvp.Attachments.AttachmentsPresenter(this, AttachmentsHost);
        if (!IsPostBack)
        {            
            _presenter.PrepareViewForUse();
            this.Session["UploadInfo"] = new UploadInfo { IsReady = false };
        }
    }

    [System.Web.Services.WebMethod]
    [System.Web.Script.Services.ScriptMethod]
    public static object GetUploadStatus()
    {
        //  get the length of the file on disk and divide that
        //  by the length of the stream ...
        UploadInfo info = HttpContext.Current.Session["UploadInfo"] as UploadInfo;

        if (info != null && info.IsReady)
        {
            int soFar = info.UploadedLength;
            int total = info.ContentLength;

            int percentComplete = (int)Math.Ceiling((double)soFar / (double)total * 100);
            string message = string.Format("Uploading {0} ... {1} of {2} Bytes", info.FileName, soFar, total);

            //  return the percentage
            return new { percentComplete = percentComplete, message = message };
        }

        //  not ready yet ...
        return null;
    }

    protected void SaveImage_Clicked(object sender, EventArgs args)
    {
        ////////if (uxNewImagePath.HasFile)
        ////////{
        ////////    string uploadAndCheckSizeFileName = memeOgram.Libraries.Business.Graphics.ImageManipulation.GetNewSystemFileName(uxNewImagePath.FileName);
        ////////    _uploadFileName = memeOgram.Libraries.Business.Graphics.ImageManipulation.GetNewSystemFileName(uxNewImagePath.FileName);
        ////////    uxNewImagePath.SaveAs(Server.MapPath(uploadAndCheckSizeFileName));
        ////////    memeOgram.Libraries.Business.Graphics.ImageManipulation.ResizeImage(Server.MapPath(uploadAndCheckSizeFileName), Server.MapPath(_uploadFileName), memeOgram.Libraries.Business.Graphics.ImageManipulation.MaximumThumbnailImagePixelSize);
        ////////    new memeOgram.Libraries.Mvp.PhotoAlbums.PhotoAlbumPresenter(this).AddNewImage();
        ////////    uxNewAlbumEntryDescription.Text = string.Empty;
        ////////    new memeOgram.Libraries.Mvp.PhotoAlbums.PhotoAlbumPresenter(this).PrepareViewForUse();
        ////////}
    }
    protected void uxAddNewAttachment_Click(object sender, EventArgs e)
    {
        QueryString qs = new QueryString();
        qs.Variables.Add(SessionVariables.ActiveObjectId, ((PageBase)this.Page).ActiveObjectId.ToString());
        qs.Variables.Add(SessionVariables.ReturnToPage, ((PageBase)this.Page).ReturnToPage);
        qs.Variables[SessionVariables.ReturnToPage] = Page.AppRelativeVirtualPath + qs.EncodeQueryString();
        _presenter.AddNewAttachmentRedirection(qs.EncodeQueryString());
    }
    protected void uxModifySelectedAttachment_Click(object sender, EventArgs e)
    {
        long selecteditem = long.Parse(uxHiddenFieldForSelectedItems.Value);

        QueryString callingPagedetails = new QueryString();
        callingPagedetails.Variables.Add(SessionVariables.ActiveObjectId, ((PageBase)this.Page).ActiveObjectId.ToString());
        callingPagedetails.Variables.Add(SessionVariables.ReturnToPage, ((PageBase)this.Page).ReturnToPage);
        
        QueryString qs = new QueryString();
        qs.Variables[SessionVariables.ReturnToPage] = Page.AppRelativeVirtualPath + callingPagedetails.EncodeQueryString();
        qs.Variables.Add(SessionVariables.ChildObjectId, selecteditem.ToString());
        _presenter.MaintainAttachmentRedirection(qs.EncodeQueryString());

    }
    #endregion

    #region Public Functions
    public void LoadAlbumImages()
    {
        //memeOgram.Libraries.Mvp.PhotoAlbums.PhotoAlbumPresenter presenter = new mvp.PhotoAlbums.PhotoAlbumPresenter(this);
        _presenter.PrepareViewForUse();
    }
    public string GetHiddenControlId()
    {
        return uxHiddenFieldForSelectedItems.ClientID;
    }
    public string SelectedImage
    {
        get
        {
            return uxHiddenFieldForSelectedItems.Value;
        }
    }
    #endregion    

    #region View
    public long PhotoAlbumId
    {
        get;
        set;
    }
    public string NewImagePath
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }
    public IList<derotek.HairyDog.DisplayableItems.DisplayableAttachmentObject> PhotoAlbumEntries
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            string html = string.Empty;
            string checkedValue = "true";
            html = "<table><tr valign='bottom'>";
            foreach (derotek.HairyDog.DisplayableItems.DisplayableAttachmentObject entry in value)
            {    
                string selectionObject = string.Empty;
                switch (ImageSelectionModeOption)
                {
                    case WebControls_Attachments.ImageSelectionMode.SingleImage:
                        selectionObject = "<input type='radio' id='" + entry.Id + "' checked='" + checkedValue + "' name='selectASingleImageGroup' value='" + entry.Id + "' onclick=\"toggleSet(this," + (int)WebControls_Attachments.ImageSelectionMode.SingleImage + ", '" + entry.Id + "')\">";
                        uxHiddenFieldForSelectedItems.Value = entry.Id.ToString();
                        checkedValue = "false";
                        break;
                }
                string pictureHtml = string.Empty;
                if (entry.IsPicture)
                {
                    pictureHtml = "<a href='" + SessionVariables.RootPath(true) + "Uploads/Resized/" + entry.Path + "' rel='prettyPhoto[gallery1]'title='" + entry.Description + "'><img class='corners' src='" + SessionVariables.RootPath(true) + "Uploads/Thumbs/" + entry.Path + "' alt='" + entry.IdentificationNumberType + "' /></a><br />";
                }
                else
                {
                    pictureHtml = "<img class='corners' src='" + SessionVariables.RootPath(true) + "Images/NoAttachment.png' alt='" + entry.IdentificationNumberType + "' /><br />";
                }
                string imageEntry = "<table style='background-color:#f4efe5' class='cornersButton' ><tr><td align='center'>" 
                    + pictureHtml 
                    + selectionObject + "</td><td valign='Top' height='90px' width='125px'><br />" + entry.IdentificationNumberType + "<br />" + entry.Description + "</td></tr></table>";
                html += "<td>" + imageEntry + "</td>";
            }
            html += "</tr></table>";
            uxFF.InnerHtml = html;
            if (uxModifySelectedAttachment.Visible == true)
            {
                uxModifySelectedAttachment.Visible = value.Count > 0;
            }
        }
    }
    public string AlbumName
    {
        get;
        set;
    }
    public string AlbumDescription
    {
        get;
        set;
    }
    public string NewAlbumEntryDescription
    {
        get;
        set;
    }
    public derotek.HairyDog.Users.User  CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }
    public void DisplayMessage(string message, bool isErrorMessage)
    {
        throw new NotImplementedException();
    }
    public void RedirectView(string path)
    {
        Server.Transfer(path, false);
    }
    public long ActiveObjectId
    {
        get
        {
            return ((PageBase)Page).ActiveObjectId;
        }
        set
        {
            ((PageBase)Page).ActiveObjectId = value;
        }
    }
    public string ReturnToPage
    {
        get
        {
            return ((PageBase)Page).ReturnToPage;
        }
        set
        {
            ((PageBase)Page).ReturnToPage = value;
        }
    }
    #endregion    
}