﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebControls_GoogleMap : System.Web.UI.UserControl
{
    public static bool IsMobileBrowser()
    {
        //GETS THE CURRENT USER CONTEXT
        HttpContext context = HttpContext.Current;

        //FIRST TRY BUILT IN ASP.NT CHECK
        if (context.Request.Browser.IsMobileDevice)
        {
            return true;
        }
        //THEN TRY CHECKING FOR THE HTTP_X_WAP_PROFILE HEADER
        if (context.Request.ServerVariables["HTTP_X_WAP_PROFILE"] != null)
        {
            return true;
        }
        //THEN TRY CHECKING THAT HTTP_ACCEPT EXISTS AND CONTAINS WAP
        if (context.Request.ServerVariables["HTTP_ACCEPT"] != null &&
            context.Request.ServerVariables["HTTP_ACCEPT"].ToLower().Contains("wap"))
        {
            return true;
        }
        //AND FINALLY CHECK THE HTTP_USER_AGENT 
        //HEADER VARIABLE FOR ANY ONE OF THE FOLLOWING
        if (context.Request.ServerVariables["HTTP_USER_AGENT"] != null)
        {
            //Create a list of all mobile types
            string[] mobiles =
                new[]
                {
                    "midp", "j2me", "avant", "docomo", 
                    "novarra", "palmos", "palmsource", 
                    "240x320", "opwv", "chtml",
                    "pda", "windows ce", "mmp/", 
                    "blackberry", "mib/", "symbian", 
                    "wireless", "nokia", "hand", "mobi",
                    "phone", "cdm", "up.b", "audio", 
                    "SIE-", "SEC-", "samsung", "HTC", 
                    "mot-", "mitsu", "sagem", "sony"
                    , "alcatel", "lg", "eric", "vx", 
                    "NEC", "philips", "mmm", "xx", 
                    "panasonic", "sharp", "wap", "sch",
                    "rover", "pocket", "benq", "java", 
                    "pt", "pg", "vox", "amoi", 
                    "bird", "compal", "kg", "voda",
                    "sany", "kdd", "dbt", "sendo", 
                    "sgh", "gradi", "jb", "dddi", 
                    "moto", "iphone"
                };

            //Loop through each item in the list created above 
            //and check if the header contains that text
            foreach (string s in mobiles)
            {
                if (context.Request.ServerVariables["HTTP_USER_AGENT"].
                                                    ToLower().Contains(s.ToLower()))
                {
                    return true;
                }
            }
        }

        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            string mapWidthAndHeightStyle = string.Empty;
            if (IsMobileBrowser())
            {
                mapWidthAndHeightStyle = "document.getElementById('map_canvas').style.height = '350px';document.getElementById('map_canvas').style.width = '875px';";
            }            

            string getGeolocationInfo = string.Empty;
            if (string.IsNullOrEmpty(Session["Latitude"].ToString()) || string.IsNullOrEmpty(Session["Longitude"].ToString()) || Session["Latitude"].ToString() == "0")
            //if (string.IsNullOrEmpty(Request.QueryString["lat"]) || string.IsNullOrEmpty(Request.QueryString["long"]) ||Request.QueryString["lat"]=="0")
            {               
                 getGeolocationInfo =
                     "if (navigator.geolocation) {"+
                     "    navigator.geolocation.getCurrentPosition(gotPosition, errorGettingPosition, { 'enableHighAccuracy': true, 'timeout': 25000, 'maximumAge': 0 });" +
                     "}" +
                     "else {" +
                     "initialize(xlatitude, xlongitude);" +
                     "}";

                 Latitude = SessionVariables.CurrentUserGet(Session).Latitude;
                 Longitude = SessionVariables.CurrentUserGet(Session).Longitude;
            }
            else
            {
                Latitude = double.Parse(Session["Latitude"].ToString());
                Longitude = double.Parse(Session["Longitude"].ToString());
                getGeolocationInfo = "initialize(xlatitude, xlongitude);";
            }

            string googleMapsScript =
            "<script type='text/javascript'>" +
                mapWidthAndHeightStyle + 
                "var xlatitude=" + Latitude + ";" +
                "var xlongitude=" + Longitude + ";" +
                getGeolocationInfo + " " + 
                " " + 
                 "function gotPosition(pos) {" +
                    "latitude = pos.coords.latitude;" +
                    "longitude = pos.coords.longitude;" +
                    "if(pos.coords.accuracy<=3000)" +
                    "{" +
                    "        initialize(pos.coords.latitude, pos.coords.longitude);" +
                    "}" + 
                    "else {" +
                    "   alert('Your current location could not be retrieved with sufficient accuracy, using your default profile coordinates instead');" +
                    "   initialize(xlatitude, xlongitude);" +
                    "}" +
                  "}" +

                   "    function errorGettingPosition(err) " +
                   "    { " +
                   "        alert('Error getting gps location');" +
                   "        initialize(xlatitude, xlongitude);" +
                   "    }" +


                "var geocoder = new google.maps.Geocoder();" +
                "var _map;" +
                "var _marker;" +
                "    " +
                "function initialize(latitude, longitude) " +
                "{ " +
                "  var latlng = new google.maps.LatLng(latitude, longitude);" +
                "   var mapOptions = {" +
                "       zoom: 14," +
                "       center: latlng," +
                "       mapTypeId: google.maps.MapTypeId.ROADMAP" +
                "   };" +
                "   _map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);" +
                "       " +
                "   var image = new google.maps.MarkerImage('" + SessionVariables.RootPath(true) + "Images/GlobeSpotterRed.png'," +
                "       new google.maps.Size(40,35)," +
                "       new google.maps.Point(0.0)," +
                "       new google.maps.Point(20,35)" +
                "       );" +
                "   _marker = new google.maps.Marker(" +
                "           {" +
                "               position: latlng," +
                "               draggable: true," +
                "               map: _map," +
                "               icon: image" +
                "           });" +
                "    google.maps.event.addListener(_marker, 'dragend', function (event) {" +
                "    if (event.latLng != null) {" +
                "        var latitude = event.latLng.lat();" +
                "        var longitude = event.latLng.lng();" +
                "        document.getElementById('" + uxGpsLatitudeSelected.ClientID + "').value = latitude;" +
                "        document.getElementById('" + uxGpsLongitudeSelected.ClientID + "').value = longitude; " +
                "       }" +
                "   }); " +

                "   circle = new google.maps.Circle({" +
                "     map: _map," +
                "     radius: 200,    " +
                "     fillColor: '#6666FF'," +
                "     strokeColor: '#8888FF'," +
                "     strokeWeight: 1" +
                "   });" +
                "   circle.bindTo('center', _marker, 'position'); " +

                "}" +
                "function pick() {" +
                "    var latBox = document.getElementById('" + uxGpsLatitudeSelected.ClientID + "');" +
                "    var longBox = document.getElementById('" + uxGpsLongitudeSelected.ClientID + "');" +
                "    var latitide = latBox.value;" +
                "    var longitude = longBox.value;" +
                "    var window_opener = window.dialogArguments;" + //Fix for IE9: passed the window object in from where the modalform was created
                "    window_opener.RemoteFunction(latitide, longitude);" +
                "   window.close();" +
                "}" +                
            "</script>";
            Page.ClientScript.RegisterStartupScript(this.GetType(), "GoogleMaps", googleMapsScript);
        }

    }

    protected void uxContinue_Click(object sender, EventArgs e)
    {
        Session["Latitude"] = Latitude;
        Session["Longitude"] = Longitude;
        Response.Redirect(((PageBase)this.Page).ReturnToPage);


    //    Session["Latitude"] = Latitude;
    //    Session["Longitude"] = Longitude;
    //    string path = ((PageBase)this.Page).ReturnToPage;
    //    path = path.Replace("~", "http://www.hermesalert.com");
    //    Response.Redirect(path);
    //
    }

    #region Properties
    public double Latitude
    {
        get
        {
            return double.Parse(uxGpsLatitudeSelected.Text);
        }
        set
        {
            uxGpsLatitudeSelected.Text = value.ToString();
        }
    }
    public double Longitude
    {
        get
        {
            return double.Parse(uxGpsLongitudeSelected.Text);
        }
        set
        {
            uxGpsLongitudeSelected.Text = value.ToString();
        }
    }
    #endregion
}