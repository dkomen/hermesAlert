﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PageSectionHeading.ascx.cs" Inherits="WebControls_PageSectionHeading" %>
<%@ Register Src="~/WebControls/InfoButton.ascx" TagName="InfoButton" TagPrefix="uc" %>   

<div id="uxDivBefore" runat="server">   
</div>
<div id="Div1" runat="server" class="pageSectionHeading cornersPageSectionHeading">
    <asp:Table runat="server" width="100%" style="vertical-align:middle;position:relative;top:-2px;padding-left:5px;padding-right:5px">
        <asp:TableHeaderRow >
            <asp:TableCell VerticalAlign="Top">
                <asp:Label ID="uxText" runat="server" SkinID="PageSectionHeading" ></asp:Label>
            </asp:TableCell>
            <asp:TableCell align="right" VerticalAlign="Middle">
                <uc:InfoButton ID="uxInfoButton" runat="server" Text="help" Visible="false"/>
            </asp:TableCell>
        </asp:TableHeaderRow>
    </asp:Table>    
</div>
<div id="Div2" runat="server" class="corners" style="height:8px">   
</div>

