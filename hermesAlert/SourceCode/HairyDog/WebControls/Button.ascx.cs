﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public delegate void OnClickDelegate(object sender, EventArgs e);
public partial class WebControls_Button : System.Web.UI.UserControl
{
    #region Fields
    public event OnClickDelegate Clicked;
    private string _clientSideScript = string.Empty;
    #endregion

    #region Properties


    public string Text
    {
        get
        {
            return uxButton.Text;
        }
        set
        {
            if (value.Length <= 10)
            {
                int length = value.Length;
                int charsToAppend = (12 - value.Length)/2;
                string newValue = value;
                newValue = newValue.PadLeft(length + charsToAppend, ' ');
                newValue = newValue.PadRight(length + charsToAppend + charsToAppend, ' ');
                uxButton.Text = newValue;
            }
            else
            {
                uxButton.Text = " " + value + " ";
            }
        }
    }
    public string OnClientClick
    {
        set
        {
            _clientSideScript = value;           
        }
    }
    public bool LargeButton
    {
        get;
        set;
    }
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            if (_clientSideScript != string.Empty)
            {
                uxButton.Attributes.Remove("OnClick");
                uxButton.Attributes.Add("OnClick", _clientSideScript + "; " + (Clicked == null ? "return false;" : string.Empty));
            }

            uxButton.Attributes.Remove("onmouseover");
            uxButton.Attributes.Remove("onmouseout");
            uxButton.Attributes.Add("onmouseover", "this.style.backgroundColor='#e0f5e0', this.style.borderColor='#00ff00'");
            uxButton.Attributes.Add("onmouseout", "this.style.backgroundColor='#f4efe5', this.style.borderColor='#ba9944'");
        }
    }
    public void uxButton_Click(object sender, EventArgs e)
    {
        if (Clicked != null)
        {
            Clicked(sender, e);
        }
    }
}