﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GoogleMultiMap.ascx.cs" Inherits="WebControls_GoogleMultiMap" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<br />
<asp:Table id="Table1" runat="server" SkinID="NormalStandOutMore">
    <asp:TableRow>
        <asp:TableCell>    
            <div style="position:relative; top:2px">    
                <asp:Table ID="uxMarkerOptions" runat="server" style="width:100%">
                    <asp:TableRow>
                        <asp:TableCell ColumnSpan="3">
                            <asp:Label ID="Label1" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Your current area selection is : </asp:Label>&nbsp;&nbsp;
                            <asp:Label ID="Label2" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Longitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLongitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;
                            <asp:Label ID="Label3" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Latitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLatitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;
                            <asp:Label ID="Label5" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Radius (Km)&nbsp;</asp:Label><asp:DropDownList ID="uxMarkerRadiusSelcted" runat="server" SkinID='Normal'></asp:DropDownList>&nbsp;&nbsp;                          
                            <uc:Button ID="uxUpdateMarker" runat="server" Text="Update" OnClientClick="javascript:ReCreateMarker();" />&nbsp;
                            <uc:Button ID="uxDeleteCurrentMarker" runat="server" Text="Delete" OnClientClick="javascript:Delete();" />
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <uc:Button ID="uxCreateNewMarker" runat="server" Text="Create new area" OnClientClick="javascript:CreateMarker();" AutoPostBack="false" />&nbsp;&nbsp;                                                                                                                                                                                                        
                            <uc:Button ID="uxSaveAndContinue" runat="server" Text="Save" OnClientClick="javascript:SetAreaDataToHiddenField();" OnClicked="uxSaveAndContinue_Clicked" />
                            <uc:Button ID="Button1" runat="server" Text="Return..."  OnClicked="uxReturn_Clicked" />&nbsp;    
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>                                    
            </div>
            <div style="position:relative; top:6px" class="corners">
                <asp:Table id="d" runat="server" SkinID="NormalStandOutMore">
                    <asp:TableRow>
                        <asp:TableCell>                            
                            <div id="uxFilterCriteria" runat="server" class="filterCriteria corners">
                                <asp:Label SkinID="NormalNoWidthGiven" runat="server">Incident type filter</asp:Label>&nbsp;&nbsp;
                                <asp:DropDownList ID="uxIncidentTypes" runat="server" SkinID="Normal" OnSelectedIndexChanged="uxIncidentTypes_SelectedIndexChanged" AutoPostBack="true" />&nbsp;
                                <asp:Label ID="uxIncidentsDateRangeShown" runat="server" Text="Showing incidents for the last 14 days" SkinID="SmallNoWidthGiven" />
                            </div>
                            <div runat="server" id="uxMapCanvas" />
                        </asp:TableCell>
                    </asp:TableRow>                    
                </asp:Table>
            </div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow ID="uxLegendRowShow">
        <asp:TableCell>            
            <div id='uxLegendData' runat='server'>
                <br />
                <asp:Label ID="Label4" runat="server" Text="" SkinId='NormalStandOut'><asp:Image ID="Image1" runat="server" Width="25px" Height="20px" ImageUrl="~/Images/GlobeSpotter.png" />- Drag the selected marker on the map to where you want to select a gps coordinate</asp:Label>
            </div>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow ID="uxLegendRowHide" Height="6px">
        <asp:TableCell>            
            
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>                

<asp:HiddenField ID="uxSelectedMarkerHidden" runat="server" />
<asp:HiddenField ID="uxSelectedMarkerEventHidden" runat="server" />
<asp:HiddenField ID="uxHiddenIncidentStatisticsAreas" runat="server" />

<script type='text/javascript'>
    var geocoder = new google.maps.Geocoder();
    var _map;
    var _marker;
    var _listOfMarkers = new Array();
    var _listOfMarkerRadiuses = new Array();
    var _fieldSeperator = "^;";
                    
    //Custom Marker Icon
    var _imageArea = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/GlobeSpotter.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaEvent = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/GlobeSpotterRed.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentH = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconHijack.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentA = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconAccident.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentPV = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconPV.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentPA = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconPA.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentM = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconMurder.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentO = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconOther.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentR = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconRape.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentG = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconMugging.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentX = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconRobbery.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentPA = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconPoliceActivity.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentF = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconFire.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentD = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconDrugs.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentPRM = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconPRM.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentFA = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconFA.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentTA = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconTA.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentPRS = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconPRS.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentRW = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconRW.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentCON = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconCON.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);
    var _imageAreaIncidentMSP = new google.maps.MarkerImage('<%=SessionVariables.RootPath(true) %>Images/IncidentIconMSP.png',
	new google.maps.Size(40,35),
	null,
	new google.maps.Point(20,35)
	);

    initialize();
    PopulatePredefinedAreas();
    PopulatePredefinedEventMarkers();
    PopulateIncidentStatisticsAreas();

    //////////////////////////////////////////////////////////////////
    // Initialise the google map
    //////////////////////////////////////////////////////////////////
    function initialize() 
    {
        var latlng = new google.maps.LatLng(<% =StartLatitude()%>, <% =StartLongitude()%>);
        var mapOptions = {
            zoom: <% =StartZoomLevel()%>,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.<%= GetMapType()%>
        };
        _map = new google.maps.Map(document.getElementById('<%=uxMapCanvas.ClientID %>'), mapOptions);                 



//        var pyrmont = new google.maps.LatLng(-33.8665433,151.1956316);

//        _map = new google.maps.Map(document.getElementById('<%=uxMapCanvas.ClientID %>'), {
//          mapTypeId: google.maps.MapTypeId.ROADMAP,
//          center: pyrmont,
//          zoom: 15
//        });

//      var request = {
//        location: pyrmont,
//        radius: '300',
//        types: ['store']
//      };

//      service = new google.maps.places.PlacesService(_map);
//      service.search(request, callbackrr);
//      //service.search(request, callbackrr);

    }


//    function callbackrr(results, status) {
//        alert(status);
//        if (status == google.maps.places.PlacesServiceStatus.OK) {
//            for (var i = 0; i < results.length; i++) {
//                var place = results[i];
//                createMarker(results[i]); 
//            }
//        }
//    }

    //////////////////////////////////////////////////////////////////
    //Delete current marker and recreate it with new parameters
    //////////////////////////////////////////////////////////////////
    function ReCreateMarker() {
        //Get the id of the selected marker
        markerId = document.getElementById('<% =uxSelectedMarkerHidden.ClientID%>').value;

        //Get the properties from the Ui
        var newMarkerRadius = parseInt(document.getElementById('<% =uxMarkerRadiusSelcted.ClientID %>').value,10);
        var longitude = parseFloat(document.getElementById('<% =uxGpsLongitudeSelected.ClientID %>').value);
        var latitude = parseFloat(document.getElementById('<% =uxGpsLatitudeSelected.ClientID %>').value);

        //Look for the correct marker in the array
        for (var markerCounter = 0; markerCounter < _listOfMarkers.length; markerCounter++) {
            markerInfo = _listOfMarkers[markerCounter];
            if (markerInfo.Marker.title == markerId.toString()) {                
                //Create a new marker with the new properties               
                CreateMarker(markerId, longitude, latitude, newMarkerRadius);
                //Delete the old marker
                markerInfo.Marker.setMap(null);
                markerInfo.Marker=null;
                markerInfo.Circle.setMap(null);
                markerInfo.Circle = null;
                _listOfMarkers.splice(markerCounter, 1);
                UpdateUiWithSelectedMarker(markerId)

                break;
            }
        }        
    }

    //The MarkerInfo UDT
    function MarkerInfo(long, lat, markerRadius, marker, circle) {
        this.Long = long;
        this.Lat = lat;
        this.MarkerRadius = markerRadius;
        this.Marker = marker;
        this.Circle = circle;
    }

    /////////////////////////////////////////////
    // Create a new marker
    /////////////////////////////////////////////
    function CreateMarker(markerId, long, lat, markerRadius, isAreaMarker, markerDescription, urlToInfoPicture, date, markerType) {                
        if(isAreaMarker==null)
        {
            isAreaMarker=true;            
        }

        //If this is a brand new marker then give it some default properties
        if (markerId == null) {
            long = _map.getCenter().lng();
            lat = _map.getCenter().lat();
            if(markerRadius===undefined || markerRadius==0) //No marker radius was supplied so default it
            {
                markerRadius = 5000;
            }
            markerId = _listOfMarkers.length;
        }
        var latlng = new google.maps.LatLng(lat, long);


      //Are we adding a new Zone marker
        if('<%=GetUserMode() %>'=='CreateZones')
        {           
            //Create the Zone marker            
            var marker = new google.maps.Marker
            (
                {
                    position: latlng,
                    draggable: true,
                    map: _map,
                    title: markerId.toString(),
                    icon: _imageArea
                }
            );                                        
  
            //Add an on_click listener to marker to show the info window if clicked (DeanK - Maybe move this into the if(!IsAreaMarker) block)
            var infoWindow = new google.maps.InfoWindow();            
            google.maps.event.addListener(marker, 'click', function () {                
                UpdateUiWithSelectedMarker(marker.title);                              
            });


            //---Adding a new area marker
            if(isAreaMarker)
            {
                //Add a radius around the area marker        
                circle = new google.maps.Circle({
                    map: _map,
                    radius: markerRadius,
                    fillColor: '#BBBBFF',
                    strokeColor: '#FF7777',
                    strokeWeight: 1
                });
                circle.bindTo('center', marker, 'position');
                
                //Add the marker to the array
                
                var markerInfo = new MarkerInfo(long, lat, markerRadius, marker, circle);
                _listOfMarkers.push(markerInfo);                
                //Add a drag listener to marker
                google.maps.event.addListener(marker, 'dragend', function (event) {
                    if (event.latLng != null) {
                        var latitude = event.latLng.lat();
                        var longitude = event.latLng.lng();
                        _listOfMarkers[markerId].Long = longitude;
                        _listOfMarkers[markerId].Lat = latitude;                        
                        _listOfMarkers[markerId].MarkerRadius = markerRadius;
                        UpdateUiWithSelectedMarker(markerId);
                    }
                    }
                );      
                UpdateUiWithSelectedMarker(markerId);                          
            }            
        }
        else //Not a zone marker
        {        
            //--Adding a new missing\stolen item marker (DeanK - is it?)
            if(!isAreaMarker)
            {             
                var marker;   
                //Create the marker       
                if(markerType.substring(0,1)=="E")
                {                                      
                    marker = new google.maps.Marker
                    (
                        {
                            position: latlng,
                            map: _map,
                            title: markerId.toString()
                            ,icon: _imageAreaEvent
                        }
                    );
                }   
                else
                {
                    marker = new google.maps.Marker
                    (
                        {
                            position: latlng,
                            map: _map,
                            title: markerId.toString()
                            ,icon: GetIncidentIcon(markerType)
                        }
                    );
                }  
                marker.setDraggable(false);         
                //Add an on_click listener to marker
                var infoWindow = new google.maps.InfoWindow();            
                google.maps.event.addListener(marker, 'click', function () {
                    var dateDescription = "Incident date : " + date;
                    var imageInfo = "";
                    if(urlToInfoPicture!="")
                    {
                        imageInfo = '<table width="380px">'+
                            '<p><tr>'+ 
                                '<td >'+
                                    '<img  style="border-color:#444444;" border="2px" class="cornersButton" width="100%" src="<%=SessionVariables.RootPath(true)%>Uploads/Resized/' + urlToInfoPicture + '" alt="No picture available"/>'+
                                '</td>' +
                             '</tr>'+
                            '</table></p>' ;
                            dateDescription = "Last seen : " + date;
                    }                    


                    var descriptionArray = markerDescription.split("--$");
                    var displayString = markerDescription;
                    if(markerDescription.substr(0,3)== "--$")
                    {
                        displayString = "<b><font size='4' color='#9922aa'>" + descriptionArray[0] + "</b></font><br />" +
                        "<font size='3'>"
                            + descriptionArray[1] + "<br />" + descriptionArray[2] + "<br />" + descriptionArray[3] + "<br />" + descriptionArray[4] + "<br />" + descriptionArray[5] + "<br />" + descriptionArray[6] +  
                        "</font>"
                        ;
                        
                    }
                    var contentString = '<div id="content" style="width:420px">'+
                        '<div id="bodyContent">'+
                        '<p><font color="#2222aa" size="4"><b>' + displayString + '</b></font><br /><font size="2">' + dateDescription+ '</font></p>'+
                         imageInfo +                                                                         
                        '</div>'+
                        '</div>';                    
                        infoWindow.setContent(contentString);
                        infoWindow.open(_map, marker);
                });
            }
            else
            {

                //Add a radius around the marker        
                circle = new google.maps.Circle({
                    map: _map,
                    radius: markerRadius,
                    fillColor: '#DfDfFF',
                    strokeColor: '#FF7777',
                    strokeWeight: 1,
                    center: latlng
                });
            }
        }
    }

    function ExtractIdFromTitle(title) {
        return title;
    }

    ///////////////////////////////////////////////////////////////////////////
    // Search markerInfo array and return the markerInfo with id=markerId
    ///////////////////////////////////////////////////////////////////////////
    function GetMarkerInfoFromArray(markerIdToRetreive) {
        for (var markerCounter = 0; markerCounter < _listOfMarkers.length; markerCounter++) {
            markerInfo = _listOfMarkers[markerCounter];
            if (markerInfo.Marker.title == markerIdToRetreive.toString()) {
                return markerInfo;                
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Update the UI with the info of the currently selected marker
    ///////////////////////////////////////////////////////////////////////////
    function UpdateUiWithSelectedMarker(markerIdToSelect) {
        markerInfo = GetMarkerInfoFromArray(markerIdToSelect);
        var selectedMarkerBox = document.getElementById('<% =uxSelectedMarkerHidden.ClientID%>');
        var latBox = document.getElementById('<% =uxGpsLatitudeSelected.ClientID %>');
        var longBox = document.getElementById('<% =uxGpsLongitudeSelected.ClientID %>');
        var radiusBox = document.getElementById('<% =uxMarkerRadiusSelcted.ClientID %>');
        selectedMarkerBox.value = markerIdToSelect;
        latBox.value = markerInfo.Lat;
        longBox.value = markerInfo.Long;
        radiusBox.value = markerInfo.MarkerRadius;        
    }

    function SetAreaDataToHiddenField() {
        var selectedMarkerBox = document.getElementById('<% =uxSelectedMarkerHidden.ClientID%>');
        var areaData = '';        
        for (var markerCounter = 0; markerCounter < _listOfMarkers.length; markerCounter++) 
        {
            markerInfo = _listOfMarkers[markerCounter];
            areaData+=markerInfo.Long + _fieldSeperator + markerInfo.Lat + _fieldSeperator + markerInfo.MarkerRadius + '|';        
        }
        selectedMarkerBox.value = areaData;
    }

    function Delete() {
        if(confirm('Are you sure you want to delete this area marker?'))
        {
            markerId = document.getElementById('<% =uxSelectedMarkerHidden.ClientID%>').value;        
            for (var markerCounter = 0; markerCounter < _listOfMarkers.length; markerCounter++) 
            {
                markerInfo = _listOfMarkers[markerCounter];
                if (markerInfo.Marker.title == markerId.toString()) 
                {                
                    markerInfo.Marker.setMap(null);
                    markerInfo.Marker = null;
                    markerInfo.Circle.setMap(null);
                    markerInfo.Circle = null;
                    _listOfMarkers.splice(markerCounter, 1);
                    break;
                }
            }
        } 
    }

    function pick() {
        var latBox = document.getElementById('" + uxGpsLatitudeSelected.ClientID + "');
        var longBox = document.getElementById('" + uxGpsLongitudeSelected.ClientID + "');
        var latitide = latBox.value;
        var longitude = longBox.value;
        window.opener.RemoteFunction(latitide, longitude);
        window.close();
    }

    function PopulatePredefinedAreas() {
        var selectedMarkerBox = document.getElementById('<% =uxSelectedMarkerHidden.ClientID%>');

        listOfAreas = selectedMarkerBox.value.split("|");        
        for (var markerCounter = 0; markerCounter < listOfAreas.length-1; markerCounter++) {
            areaProperties = listOfAreas[markerCounter].split(_fieldSeperator);
            CreateMarker(markerCounter, areaProperties[0], areaProperties[1], parseInt(areaProperties[2]), true, markerCounter, areaProperties[3]);           
        }
    }
    function PopulatePredefinedEventMarkers() {
        var selectedMarkerBox = document.getElementById('<% =uxSelectedMarkerEventHidden.ClientID%>');
        listOfAreas = selectedMarkerBox.value.split("|");      
        for (var markerCounter = 0; markerCounter < listOfAreas.length-1; markerCounter++) {            
            areaProperties = listOfAreas[markerCounter].split(_fieldSeperator)
            CreateMarker(markerCounter, areaProperties[0], areaProperties[1], 1, false, areaProperties[2], areaProperties[3], areaProperties[4],areaProperties[5]);    
        }
    }
    function PopulateIncidentStatisticsAreas() {
        var selectedMarkerBox = document.getElementById('<% =uxHiddenIncidentStatisticsAreas.ClientID%>');

        listOfAreas = selectedMarkerBox.value.split("|");        
        for (var markerCounter = 0; markerCounter < listOfAreas.length-1; markerCounter++) {
            areaProperties = listOfAreas[markerCounter].split(_fieldSeperator);
            CreateMarker(markerCounter, areaProperties[0], areaProperties[1], parseInt(areaProperties[2]), true, markerCounter, areaProperties[3]);           
        }
    }

    function GetIncidentIcon(markerType)
    {
        if(markerType.length==1)
        {
            return _imageAreaEvent;
        }
        else
        {                                
            switch(markerType.substring(1,4))
            {
                case "M  ":
                    return _imageAreaIncidentM;
                case "R  ":
                    return _imageAreaIncidentR;
                case "A  ":
                    return _imageAreaIncidentA;
                case "H ":
                    return _imageAreaIncidentH;
                case "V  ":
                    return _imageAreaIncidentPV;
                case "P  ":
                    return _imageAreaIncidentPA;             
                case "O  ":
                    return _imageAreaIncidentO;
                case "G  ":
                    return _imageAreaIncidentG;
                case "X  ":
                    return _imageAreaIncidentX;
                case "L  ":
                    return _imageAreaIncidentPA;
                case "F  ":
                    return _imageAreaIncidentF;          
                case "D  ":
                    return _imageAreaIncidentD;     
                case "PRM":
                    return _imageAreaIncidentPRM;
                case "FA ":
                    return _imageAreaIncidentFA;
                case "TA ":
                    return _imageAreaIncidentTA;
                case "PRS":
                    return _imageAreaIncidentPRS;
                case "RW ":
                    return _imageAreaIncidentRW;
                 case "CON":
                    return _imageAreaIncidentCON;
                ///Missing person
                case "MSP":
                    return _imageAreaIncidentMSP;
                default:
                    iconId = "O  ";
                    break;     
            }
        }
    }
</script>