﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebControls_GoogleMultiMap : System.Web.UI.UserControl
{
    #region Fields
    private int _zoomLevel = 11;
    private string _mapType = "ROADMAP";
    private double _longutide = 0;//28.3;
    private double _latitude = 0;//-25.801;
    private string _fieldSeperator = "^;";
    private long _showForLast12HoursId = 999901;
    private long _showForTodayId = 999902;
    private long _showForlast72HoursId = 999903;
    private long _showForlastWeekId = 999904;
    private long _showMissingPersonsFor12MonthsId = 999910;
    public enum DisplayMode
    {
        CreateZones = 1,
        DisplayZoneHits = 2
    }
    private DisplayMode _displayMode = DisplayMode.CreateZones;
    #endregion

    #region Properties
    public long PropertyTypeToShow
    {
        get
        {
            if (ViewState["PropertyTypeToShow"] == null)
            {
                return 0;
            }
            else
            {
                return long.Parse(ViewState["PropertyTypeToShow"].ToString());
            }
        }
        set
        {
            ViewState["PropertyTypeToShow"] = value;
            if (_displayMode == DisplayMode.DisplayZoneHits && IncidentTypeToShow != 0)
            {
                //SendAllEventsToUi(PropertyTypeToShow, IncidentTypeToShow);
            }
        }
    }
    public long IncidentTypeToShow
    {
        get
        {
            if (ViewState["IncidentTypeToShow"] == null)
            {
                return 0;
            }
            else
            {
                return long.Parse(ViewState["IncidentTypeToShow"].ToString());
            }
        }
        set
        {
            ViewState["IncidentTypeToShow"] = value;
            if (_displayMode == DisplayMode.DisplayZoneHits)
            {
                SendAllEventsToUi(PropertyTypeToShow, IncidentTypeToShow);
            }
            uxIncidentsDateRangeShown.Visible = value == 0;
        }
    }
    public double Longitude { get { return _longutide; } set { _longutide = value; } }
    public double Latitude { get { return _latitude; } set { _latitude = value; } }
    public int ZoomLevel { get { return _zoomLevel; } set { _zoomLevel = value; } }
    public string MapType { get { return _mapType; } set { _mapType = value; } }
    #endregion

    #region UI Functions
    protected void Page_Load(object sender, EventArgs e)
    {
        //StartLatitude = -25.801;
        //StartLongitude = 28.3;
        if (!IsPostBack)
        {
            uxIncidentsDateRangeShown.Visible = (_displayMode == DisplayMode.DisplayZoneHits);
            //Prevent postback on buttons when clicked client side.. only allow javascript to execute
            uxUpdateMarker.Attributes.Add("OnClick", "return false;");
            uxDeleteCurrentMarker.Attributes.Add("OnClick", "return false;");
            uxCreateNewMarker.Attributes.Add("OnClick", "return false;");

            Dictionary<long, string> values = new Dictionary<long, string>();
            values.Add(500, "0.5Km");
            values.Add(1000, "1Km");
            values.Add(2500, "2.5Km");
            values.Add(5000, "5Km");
            values.Add(7500, "7.5Km");
            values.Add(10000, "10Km");
            values.Add(15000, "15Km");
            values.Add(25000, "25Km");
            values.Add(35000, "35Km");
            HelperClassDataElements.PopulateDropdownList(uxMarkerRadiusSelcted, values);

            //Get property types
            IList<derotek.HairyDog.Metrics.IncidentType> propertyTypes;
            using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
            {
                propertyTypes = transaction.Retrieve<derotek.HairyDog.Metrics.IncidentType>(null);
            }
            derotek.HairyDog.Metrics.IncidentType showAllType = new derotek.HairyDog.Metrics.IncidentType();
            showAllType.Id = 0;
            showAllType.ExternalReferanceId = _showForTodayId - 1;
            showAllType.Description = "No Filter";
            propertyTypes.Insert(0, showAllType);

            derotek.HairyDog.Metrics.IncidentType showMissingPersonsFor12Months = new derotek.HairyDog.Metrics.IncidentType();
            showMissingPersonsFor12Months.Id = _showMissingPersonsFor12MonthsId;
            showMissingPersonsFor12Months.ExternalReferanceId = _showMissingPersonsFor12MonthsId;
            showMissingPersonsFor12Months.Description = "150 Most recent missing persons";
            propertyTypes.Insert(1, showMissingPersonsFor12Months);

            derotek.HairyDog.Metrics.IncidentType showAllOver12Hours = new derotek.HairyDog.Metrics.IncidentType();
            showAllOver12Hours.Id = _showForLast12HoursId;
            showAllOver12Hours.ExternalReferanceId = _showForLast12HoursId;
            showAllOver12Hours.Description = "Show all for last 12 hours";
            propertyTypes.Insert(1, showAllOver12Hours);

            derotek.HairyDog.Metrics.IncidentType showAllOver24Hours = new derotek.HairyDog.Metrics.IncidentType();
            showAllOver24Hours.Id = _showForTodayId;
            showAllOver24Hours.ExternalReferanceId = _showForTodayId;
            showAllOver24Hours.Description = "Show all for last 24 hours";
            propertyTypes.Insert(2, showAllOver24Hours);

            derotek.HairyDog.Metrics.IncidentType showAllOver72Hours = new derotek.HairyDog.Metrics.IncidentType();
            showAllOver72Hours.Id = _showForlast72HoursId;
            showAllOver72Hours.ExternalReferanceId = _showForlast72HoursId;
            showAllOver72Hours.Description = "Show all for last 72 hours";
            propertyTypes.Insert(3, showAllOver72Hours);

            derotek.HairyDog.Metrics.IncidentType showAllOverLastWeek = new derotek.HairyDog.Metrics.IncidentType();
            showAllOverLastWeek.Id = _showForlastWeekId;
            showAllOverLastWeek.ExternalReferanceId = _showForlast72HoursId;
            showAllOverLastWeek.Description = "Show all for last 7 days";
            propertyTypes.Insert(4, showAllOverLastWeek);

            HelperClassDataElements.PopulateDropdownList(uxIncidentTypes, propertyTypes.ToDictionary(x => x.Id, yield => yield.Description));
            IncidentTypeToShow = 0;
            SendAllAreasToUi();
            SetMapCanvasSize(CanvasWidth, CanvasHeight);
        }
    }
    protected void uxSaveAndContinue_Clicked(object sender, EventArgs e)
    {
        SaveAreaInfo(uxSelectedMarkerHidden.Value);
        uxReturn_Clicked(null, null);
    }
    protected void uxReturn_Clicked(object sender, EventArgs e)
    {
        Response.Redirect(((PageBase)Page).ReturnToPage);
    }
    protected void uxIncidentTypes_SelectedIndexChanged(object sender, EventArgs e)
    {
        IncidentTypeToShow = long.Parse(uxIncidentTypes.SelectedValue == "" ? "0" : uxIncidentTypes.SelectedValue);
    }
    #endregion

    public double StartLongitude()
    {
        return _longutide;
    }
    public double StartLatitude()
    {
        return _latitude;
    }
    public double StartZoomLevel()
    {
        return _zoomLevel;
    }
    public string GetMapType()
    {
        return _mapType;
    }    
    private void SendAllAreasToUi()
    {        

        IList<derotek.HairyDog.MonitoringLocation> areas;
        using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
        {
            areas = transaction.Retrieve<derotek.HairyDog.MonitoringLocation>("UserId", UserId, null);
        }

        string areaInfoString = string.Empty;

        foreach (derotek.HairyDog.MonitoringLocation area in areas)
        {
            areaInfoString += area.Longitude.ToString() + _fieldSeperator + area.Latitude.ToString() + _fieldSeperator + area.RadiusInMeters.ToString() + _fieldSeperator + "E  " + '|';
            //if (_longutide == 0)
            //{
            _longutide = area.Longitude;
            _latitude = area.Latitude;
            //}
        }
        uxSelectedMarkerHidden.Value = areaInfoString;
    }
    private void SendAllEventsToUi(long propertyTypeToShow, long incidentTypeToShow)
    {
        string areaInfoString = string.Empty;
        if (propertyTypeToShow != _showMissingPersonsFor12MonthsId)
        {
            #region StolenItems
            if (propertyTypeToShow != 0)
            {
                IList<derotek.HairyDog.RegisteredItem> stolenItems = derotek.Fnh.DataAccess.SPROC_Functions.GetStolenItemsInAreas(UserId, propertyTypeToShow, HelperClassDataElements.MaximumitemsPerAreaToRetrieve, HelperClassDataElements.MaximumAreasToRetrieve, System.DateTime.Now.AddDays(-90));
                if (stolenItems == null) { stolenItems = new List<derotek.HairyDog.RegisteredItem>(); }
                foreach (derotek.HairyDog.RegisteredItem stolenItem in stolenItems)
                {
                    string description = stolenItem.Description;
                    if (description.Length >= derotek.Configuration.FieldLengths.MediumDescription)
                    {
                        description = description.Substring(0, derotek.Configuration.FieldLengths.MediumDescription) + "...";
                    }
                    switch (stolenItem.LocationAccuracy.ExternalReferanceId)
                    {
                        case (int)derotek.HairyDog.Enumerations.LocationAccuracy.About350m:
                            break;
                    }

                    string imageUrl = string.Empty;
                    if (stolenItem.Attachments.Count > 0)
                    {
                        imageUrl = stolenItem.Attachments[0].Path;
                    }

                    double longitude;
                    double latitude;
                    derotek.HairyDog.CoordinateObfuscator.ObfuscateCoordinates(stolenItem.LocationAccuracy, stolenItem.Longitude, stolenItem.Latitude, out longitude, out latitude);

                    areaInfoString += longitude.ToString() + _fieldSeperator + latitude.ToString() + _fieldSeperator + description + _fieldSeperator + imageUrl + _fieldSeperator + stolenItem.DateWasStolen.ToString("dd MMM yyyy") + _fieldSeperator + "E  " + '|';
                    if (_longutide == 0)
                    {
                        _longutide = longitude;
                        _latitude = latitude;
                    }
                }
            }
            #endregion

            #region Incidents
            IList<derotek.HairyDog.Incidents.Incident> incidents = derotek.Fnh.DataAccess.SPROC_Functions.GetIncidents(null, null, 30, 10, System.DateTime.Now.AddDays(-14), System.DateTime.Now);
            if (incidents == null) { incidents = new List<derotek.HairyDog.Incidents.Incident>(); }
            foreach (derotek.HairyDog.Incidents.Incident incident in incidents)
            {
                if (incidentTypeToShow == 0
                    || (incident.IncidentType.Id == incidentTypeToShow)
                    || (incidentTypeToShow == _showForLast12HoursId && (incident.IncidentDateTime.AddHours(12) >= System.DateTime.Now))
                    || (incidentTypeToShow == _showForTodayId && (incident.IncidentDateTime.AddDays(1) >= System.DateTime.Now))
                    || (incidentTypeToShow == _showForlast72HoursId && (incident.IncidentDateTime.AddDays(3) >= System.DateTime.Now))
                    || (incidentTypeToShow == _showForlastWeekId && (incident.IncidentDateTime.AddDays(7) >= System.DateTime.Now))
                    )
                {
                    string description = incident.Description;

                    description = incident.IncidentType.Description + " : " + description;
                    string iconId = string.Empty;

                    switch (incident.IncidentType.ExternalReferanceId)
                    {
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Murder:
                        case (int)derotek.HairyDog.Enumerations.IncidentType.AttemptedMurder:
                            iconId = "M  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Rape:
                        case (int)derotek.HairyDog.Enumerations.IncidentType.AttemptedRape:
                            iconId = "R  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.VehicleHijack:
                        case (int)derotek.HairyDog.Enumerations.IncidentType.AttemptedVehicleHijack:
                            iconId = "H  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.TrafficAccident:
                            iconId = "A  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.PublicViolence:
                            iconId = "V  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.PhysicalAssualt:
                            iconId = "P  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Mugging:
                            iconId = "G  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Other:
                            iconId = "O  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Robbery:
                            iconId = "X  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.PoliceActivity:
                            iconId = "L  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Fire:
                            iconId = "F  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Drugs:
                            iconId = "D  ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.PoorRoadSurface:
                            iconId = "PRS";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.PoorRoadMarkingLighting:
                            iconId = "PRM";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.FarmAttack:
                            iconId = "FA ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.TrafficAttack:
                            iconId = "TA ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Roadworks:
                            iconId = "RW ";
                            break;
                        case (int)derotek.HairyDog.Enumerations.IncidentType.Construction:
                            iconId = "CON";
                            break;
                        default:
                            iconId = "O  ";
                            break;
                    }


                    areaInfoString += incident.Longitude.ToString() + _fieldSeperator + incident.Latitude.ToString() + _fieldSeperator + description + _fieldSeperator + _fieldSeperator + incident.IncidentDateTime.ToString("dddd HH:mm, dd MMM yyyy") + _fieldSeperator + "I" + iconId + '|';
                    if (_longutide == 0)
                    {
                        _longutide = incident.Longitude;
                        _latitude = incident.Latitude;
                    }
                }
            }
            #endregion
        }
        #region Missing Persons
        List<derotek.HairyDog.PersonAlert.PersonAlert> missingPersonAlrets = derotek.HairyDog.Model.MissingPersons.GetMissingPersons(150); 

        foreach (derotek.HairyDog.PersonAlert.PersonAlert missingPersonAlert in missingPersonAlrets)
        {
            if (missingPersonAlert.ApprovalStatus.ExternalReferanceId == (long)derotek.HairyDog.Enumerations.ApprovalStatus.Approved)
            {
                string iconId = string.Empty;
                string description = "--$Missing " + missingPersonAlert.Gender.Description +
                    "--$Name : " + missingPersonAlert.Firstname + " " + missingPersonAlert.Surname +
                    "--$Known as : " + missingPersonAlert.KnownAs +
                    "--$Eye colour : " + missingPersonAlert.EyeColour.Description +
                    "--$Hair colour : " + missingPersonAlert.HairColour.Description +
                    "--$Features : " + missingPersonAlert.DistinguishingFeatures +
                    "--$Notes : " + missingPersonAlert.Notes +
                    "--$Award : " + missingPersonAlert.AwardAmount;
                string imageUrl = missingPersonAlert.PathToImage;
                
                double longitude;
                double latitude;
                derotek.HairyDog.CoordinateObfuscator.ObfuscateCoordinates(missingPersonAlert.LocationAccuracy, missingPersonAlert.Longitude, missingPersonAlert.Latitude, out longitude, out latitude);
                
                areaInfoString += longitude.ToString() + _fieldSeperator + latitude.ToString() + _fieldSeperator + description + _fieldSeperator + imageUrl + _fieldSeperator + missingPersonAlert.DateLastSeen.ToString("dd MMM yyyy") + _fieldSeperator + "IMSP" + '|';
            }
        }
        #endregion        

        uxSelectedMarkerEventHidden.Value = areaInfoString;
    }
    private void SaveAreaInfo(string areaInfoString)
    {

        using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
        {
            transaction.Delete<derotek.HairyDog.MonitoringLocation>("UserId", UserId);
            transaction.Commit();
        }
        string[] areas = areaInfoString.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
        using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
        {
            foreach (string area in areas)
            {
                string[] areaSplit = area.Split(_fieldSeperator.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                derotek.HairyDog.MonitoringLocation monitoringLocation = new derotek.HairyDog.MonitoringLocation();
                monitoringLocation.UserId = UserId;
                monitoringLocation.Description = "No Description";
                monitoringLocation.Longitude = double.Parse(areaSplit[0]);
                monitoringLocation.Latitude = double.Parse(areaSplit[1]);
                monitoringLocation.RadiusInMeters = int.Parse(areaSplit[2]);
                transaction.AddOrUpdate(monitoringLocation);
                transaction.Commit();
            }
        }
    }
    public long UserId
    {
        get
        {
            if (ViewState["UserId"] != null)
            {
                return long.Parse(ViewState["UserId"].ToString());
            }
            else
            {
                return 0;
            }
        }
        set
        {
            ViewState["UserId"] = value;
        }
    }
    public string GetUserMode()
    {
        return _displayMode.ToString();
    }
    public DisplayMode UserMode
    {
        set
        {
            _displayMode = value;
            switch (value)
            {
                case DisplayMode.DisplayZoneHits:
                    uxMarkerOptions.Visible = false;
                    uxLegendData.Visible = false;
                    break;
                default:
                    break;
            }
        }
    }
    public int CanvasWidth { get; set; }
    public int CanvasHeight { get; set; }

    #region Private Functions
    private void SetMapCanvasSize(int width, int height)
    {
        if (width == 0 || height == 0)
        {
            width = 1166;
            height = 600;
        }

        uxMapCanvas.Style.Add("width", width.ToString() + "px");
        uxMapCanvas.Style.Add("height", height.ToString() + "px");
    }
    #endregion
}