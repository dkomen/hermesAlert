﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Attachments.ascx.cs" Inherits="WebControls_Attachments" %>
<%@ Register TagPrefix="uc" TagName="PageSectionHeading" Src="~/WebControls/PageSectionHeading.ascx" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

    <%--prettyPhoto--%>
    <%--<script src="<%=SessionVariables.RootPath(true) %>"Scripts/prettyPhoto/js/jquery-1.4.4.min.js" type="text/javascript"></script>	--%>
    <script type="text/javascript" src='<%= SessionVariables.RootPath(true) %>Scripts/jquery-1.6.1.min.js'></script>
    <link rel="stylesheet" href="<%=SessionVariables.RootPath(true) %>Scripts/prettyPhoto/css/prettyPhoto.css" type="text/css" media="screen" title="prettyPhoto main stylesheet" charset="utf-8" />
    <script src="<%=SessionVariables.RootPath(true) %>Scripts/prettyPhoto/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" charset="utf-8">
       
        $(document).ready(function () {
            $(".gallery:first a[rel^='prettyPhoto']").prettyPhoto({
                animation_speed: 'normal',
                show_title: true,
                theme: 'facebook',
                slideshow: 4000,                
                autoplay_slideshow: true });
        });
		</script>
    <style type="text/css" media="screen">
		* { margin: 0; padding: 0; }
			
		body {
			font: 62.5%/1.2 Arial, Verdana, Sans-Serif;
			padding: 0 20px;
		}
			
		h1 { font-family: Georgia; font-style: italic; margin-bottom: 10px; }
			
		h2 {
			font-family: Georgia;
			font-style: italic;
			margin: 25px 0 5px 0;
		}
			
		p { font-size: 1.2em; }
			
		ul li { display: inline; }
			
		.wide {
			border-bottom: 1px #000 solid;
			width: 4000px;
		}
			
		.fleft { float: left; margin: 0 20px 0 0; }
			
		.cboth { clear: both; }
			
		#main {
			background: #fff;
			margin: 0 auto;
			padding: 30px;
			width: 1000px;
		}
	</style>
    <%--End prettyPhoto--%>
	
    <script type="text/javascript">

        function toggleSet(rad, selectionType, selectedItemId) {

            var hiddenControl = document.getElementById('<%=GetHiddenControlId() %>');
            hiddenControl.value = selectedItemId;// rad.value;
            if (selectionType == 2) 
            {
                selectedItems = rad.value;
            }            
           // __doPostBack(hiddenControl.Id, '111');
        }

        $(document).ready(function () {
            var hiddenControl = document.getElementById('<%=GetHiddenControlId() %>');
            var selectedRadio = document.getElementById(hiddenControl.value);
            selectedRadio.checked = true;
        })
    </script>
           
    <asp:HiddenField ID="uxHiddenFieldForSelectedItems" runat="server" EnableViewState="true" />
    <div id="Div3" runat="server" class="Section">
        <uc:PageSectionHeading ID="PageSectionHeading1" runat="server" SectionHeadingId="ItemAttachments" BreakBefore="true"/>                       
        <div id="Div4" runat="server" style="width:1185px;position:relative;left:5px">            
            <asp:Table ID="Table1" runat="server" SkinID="Normal" >                                          
                <asp:TableRow>                    
                    <asp:TableCell>                        
                        <ul class="gallery clearfix" id='uxFF' runat="server" ></ul>                                                                             
                    </asp:TableCell>
                </asp:TableRow>      
            </asp:Table>
            <br />
            <uc:Button ID="uxAddNewAttachment" runat="server" Text="Add new attachment" OnClicked="uxAddNewAttachment_Click" />   
            <uc:Button ID="uxModifySelectedAttachment" runat="server" Text="Edit selected attachment" OnClicked="uxModifySelectedAttachment_Click" />   
            <Br/><Br/>
        </div>
    </div>