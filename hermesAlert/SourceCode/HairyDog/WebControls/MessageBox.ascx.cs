﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class WebControls_MessageBox : System.Web.UI.UserControl
{

    public bool isErrorMessage
    {
        set
        {
            if (value == true)
            {
                //uxMessageBox.SkinID = "MessageBoxError";
                //uxMessage.SkinID = "ErrorMessage";
            }
            else
            {
                //uxMessageBox.SkinID = "MessageBoxNormal";
                //uxMessage.SkinID = "NormalMessage";
            }
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        isErrorMessage = true;
    }

    public string Message
    {
        set
        {
            string[] splitLines = value.Split(System.Environment.NewLine.ToCharArray(),StringSplitOptions.RemoveEmptyEntries);
            List<string> messages = new List<string>(splitLines);
            uxMessageRepeater.DataSource = messages;
            uxMessageRepeater.DataBind();
        }
    }
    public List<string> Messages
    {        
        set
        {
            uxMessageRepeater.DataSource = value;
        }
    }
}