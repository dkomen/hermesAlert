﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebControls_Slideshow : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region Public Functions
    public string GetSlideshowItems()
    {
        List<derotek.HairyDog.PersonAlert.PersonAlert> personAlerts = derotek.HairyDog.Model.MissingPersons.GetMissingPersons(150);
        
        System.Text.StringBuilder builder = new System.Text.StringBuilder();
        foreach(derotek.HairyDog.PersonAlert.PersonAlert personAlert in personAlerts)
        {
            //DeanK : If this code displays incorrect on the UI it may be because of '\r\n' etc in the descriptions etc
            int maxFieldLength = 120;          
            string description = personAlert.DistinguishingFeatures.Trim() + (personAlert.DistinguishingFeatures.Trim().EndsWith(".")?"":". ") + personAlert.Notes.Trim();
            description = description.Replace("\r\n", "<br />");
            description = description.Replace("'", "");
            string contactDetails = string.Empty;
            if (personAlert.ContactDetails != null)
            {
                int len = 45;
                personAlert.ContactDetails = personAlert.ContactDetails.Replace("\r\n", "<br />");
                contactDetails = "contact: " + (personAlert.ContactDetails.Length > len ? personAlert.ContactDetails.Substring(0, len) + "... " : personAlert.ContactDetails + ". ");
            }
            string data = "name: " + personAlert.Firstname + " " + personAlert.Surname
                + "<br/>gender: " + personAlert.Gender.Description
                + "<br/>age: " + personAlert.ApproximateAge
                + (contactDetails!=string.Empty?"<br />" + contactDetails:"")
                + "<br/><br/>" + (description.Length > maxFieldLength ? description.Substring(0, maxFieldLength) + "... " : description + ". ");                
            builder.Append("new SlideshowItem('Uploads/Thumbs/" + personAlert.PathToImage + "', 'missing person', '" + data + "'),");
        }

        string returnString = string.Empty;
        if (builder.Length > 0)
        {
            returnString = builder.ToString();
            if (returnString.EndsWith(","))
            {
                returnString = returnString.Substring(0, returnString.Length - 1);
            }
        }
        return returnString;        
    }
    #endregion
}