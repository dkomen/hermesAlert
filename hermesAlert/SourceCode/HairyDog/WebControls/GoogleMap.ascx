﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="GoogleMap.ascx.cs" Inherits="WebControls_GoogleMap" %>
<%@ Register TagPrefix="uc" TagName="Button" Src="~/WebControls/Button.ascx" %>

<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>




<asp:Table id="Table1" runat="server" SkinID="NormalStandOutMore">
    <asp:TableRow>
        <asp:TableCell>    
            <div style="position:relative; top:2px">        
                <asp:Label ID="Label1" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Your gps coordinate selection is : </asp:Label>&nbsp;&nbsp;
                <asp:Label ID="Label2" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Longitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLongitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;
                <asp:Label ID="Label3" runat="server" Text="Label" SkinId='NormalNoWidthGiven'>Latitude&nbsp;</asp:Label><asp:TextBox ID="uxGpsLatitudeSelected" runat="server" SkinID='Normal'></asp:TextBox>&nbsp;&nbsp;
                <%--<uc:Button ID="uxContinue" runat="server" Text="Accept and continue" OnClientClick="javascript:pick();" />--%>           
                <uc:Button ID="uxContinue" runat="server" Text="Accept and continue" OnClicked="uxContinue_Click" />     
            </div>
            <div style="position:relative; top:6px">
                <asp:Table id="d" runat="server" SkinID="NormalStandOutMore">
                    <asp:TableRow>
                        <asp:TableCell>
                            <div id="map_canvas" style="width:1178px; height:520px;" />
                        </asp:TableCell>
                    </asp:TableRow>                    
                </asp:Table>
            </div>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
        <br />
            <asp:Label ID="Label4" runat="server" Text="" SkinId='NormalStandOut'><asp:Image ID="Image1" runat="server" Width="25px" Height="20px" ImageUrl="~/Images/GlobeSpotterRed.png" />- Drag the marker on the map to where you want to select the gps coordinate</asp:Label>            
        </asp:TableCell>
    </asp:TableRow>    
</asp:Table>
