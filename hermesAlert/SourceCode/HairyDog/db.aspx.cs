﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class db : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        derotek.Fnh.DataAccess.DormAccessManager.CreateDataStoreDDL();
        using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
        {            
            //ItemType
            derotek.HairyDog.Metrics.ItemType itemTypeMissingPerson = new derotek.HairyDog.Metrics.ItemType("Missing person", (int)derotek.HairyDog.Enumerations.ItemType.MissingPerson);
            transaction.AddOrUpdate(itemTypeMissingPerson);
            derotek.HairyDog.Metrics.ItemType itemTypeRegisteredItem = new derotek.HairyDog.Metrics.ItemType("Registered item", (int)derotek.HairyDog.Enumerations.ItemType.RegisteredItem);
            transaction.AddOrUpdate(itemTypeRegisteredItem);
            derotek.HairyDog.Metrics.ItemType itemTypeWantedPerson = new derotek.HairyDog.Metrics.ItemType("Wanted person", (int)derotek.HairyDog.Enumerations.ItemType.WantedPerson);
            transaction.AddOrUpdate(itemTypeWantedPerson);
            derotek.HairyDog.Metrics.ItemType itemTypeMissingPet = new derotek.HairyDog.Metrics.ItemType("Wanted person", (int)derotek.HairyDog.Enumerations.ItemType.MissingPet);
            transaction.AddOrUpdate(itemTypeMissingPet);
            derotek.HairyDog.Metrics.ItemType itemTypeIncident = new derotek.HairyDog.Metrics.ItemType("Incident", (int)derotek.HairyDog.Enumerations.ItemType.Incident);
            transaction.AddOrUpdate(itemTypeIncident);

            //ApprovalStatus
            derotek.HairyDog.Metrics.ApprovalStatus approvalStatusApproved = new derotek.HairyDog.Metrics.ApprovalStatus("Approved", (int)derotek.HairyDog.Enumerations.ApprovalStatus.Approved);
            transaction.AddOrUpdate(approvalStatusApproved);
            derotek.HairyDog.Metrics.ApprovalStatus approvalStatusDeclined = new derotek.HairyDog.Metrics.ApprovalStatus("Declined", (int)derotek.HairyDog.Enumerations.ApprovalStatus.Declined);
            transaction.AddOrUpdate(approvalStatusDeclined);
            derotek.HairyDog.Metrics.ApprovalStatus approvalStatusPending = new derotek.HairyDog.Metrics.ApprovalStatus("Pending", (int)derotek.HairyDog.Enumerations.ApprovalStatus.Pending);
            transaction.AddOrUpdate(approvalStatusPending);

            //Gender
            derotek.HairyDog.Metrics.Gender genderMale = new derotek.HairyDog.Metrics.Gender("Male", 1);
            transaction.AddOrUpdate(genderMale);
            derotek.HairyDog.Metrics.Gender genderFemale = new derotek.HairyDog.Metrics.Gender("Female", 1);
            transaction.AddOrUpdate(genderFemale);

            //EyeColour
            derotek.HairyDog.Metrics.EyeColour eyeColourAmber = new derotek.HairyDog.Metrics.EyeColour("Amber", 1);
            transaction.AddOrUpdate(eyeColourAmber);
            derotek.HairyDog.Metrics.EyeColour eyeColourBlue = new derotek.HairyDog.Metrics.EyeColour("Blue", 5);
            transaction.AddOrUpdate(eyeColourBlue);
            derotek.HairyDog.Metrics.EyeColour eyeColourBrown = new derotek.HairyDog.Metrics.EyeColour("Brown", 10);
            transaction.AddOrUpdate(eyeColourBrown);
            derotek.HairyDog.Metrics.EyeColour eyeColourGreen = new derotek.HairyDog.Metrics.EyeColour("Green", 15);
            transaction.AddOrUpdate(eyeColourGreen);
            derotek.HairyDog.Metrics.EyeColour eyeColourGrey = new derotek.HairyDog.Metrics.EyeColour("Grey", 20);
            transaction.AddOrUpdate(eyeColourGrey);
            derotek.HairyDog.Metrics.EyeColour eyeColourHazel = new derotek.HairyDog.Metrics.EyeColour("Hazel", 25);
            transaction.AddOrUpdate(eyeColourHazel);
            derotek.HairyDog.Metrics.EyeColour eyeColourViolet = new derotek.HairyDog.Metrics.EyeColour("Violet", 30);
            transaction.AddOrUpdate(eyeColourViolet);
            derotek.HairyDog.Metrics.EyeColour eyeColourOther = new derotek.HairyDog.Metrics.EyeColour("Other", 100);
            transaction.AddOrUpdate(eyeColourOther);

            //HairColour
            derotek.HairyDog.Metrics.HairColour hairColourBrown = new derotek.HairyDog.Metrics.HairColour("Brown", 1);
            transaction.AddOrUpdate(hairColourBrown);
            derotek.HairyDog.Metrics.HairColour hairColourBlack = new derotek.HairyDog.Metrics.HairColour("Black", 5);
            transaction.AddOrUpdate(hairColourBlack);
            derotek.HairyDog.Metrics.HairColour hairColourBlond = new derotek.HairyDog.Metrics.HairColour("Blond", 10);
            transaction.AddOrUpdate(hairColourBlond);
            derotek.HairyDog.Metrics.HairColour hairColourAuburn = new derotek.HairyDog.Metrics.HairColour("Auburn", 15);
            transaction.AddOrUpdate(hairColourAuburn);
            derotek.HairyDog.Metrics.HairColour hairColourChestnut = new derotek.HairyDog.Metrics.HairColour("Chestnut", 20);
            transaction.AddOrUpdate(hairColourChestnut);
            derotek.HairyDog.Metrics.HairColour hairColourRed = new derotek.HairyDog.Metrics.HairColour("Red", 25);
            transaction.AddOrUpdate(hairColourRed);
            derotek.HairyDog.Metrics.HairColour hairColourGreyOrWhite = new derotek.HairyDog.Metrics.HairColour("Grey or white", 30);
            transaction.AddOrUpdate(hairColourGreyOrWhite);



            derotek.HairyDog.Metrics.HairColour hairColourOther = new derotek.HairyDog.Metrics.HairColour("Other", 100);
            transaction.AddOrUpdate(hairColourOther);

            //IncidentType
            derotek.HairyDog.Metrics.IncidentType incidentTypeOther = new derotek.HairyDog.Metrics.IncidentType("Other", (int)derotek.HairyDog.Enumerations.IncidentType.Other);
            transaction.AddOrUpdate(incidentTypeOther);
            derotek.HairyDog.Metrics.IncidentType incidentTypePhysicalAssualt = new derotek.HairyDog.Metrics.IncidentType("Physical assualt", (int)derotek.HairyDog.Enumerations.IncidentType.PhysicalAssualt);
            transaction.AddOrUpdate(incidentTypePhysicalAssualt);
            derotek.HairyDog.Metrics.IncidentType incidentTypeMurder = new derotek.HairyDog.Metrics.IncidentType("Murder", (int)derotek.HairyDog.Enumerations.IncidentType.Murder);
            transaction.AddOrUpdate(incidentTypeMurder);
            derotek.HairyDog.Metrics.IncidentType incidentTypeAttemptedMurder = new derotek.HairyDog.Metrics.IncidentType("Attempted Murder", (int)derotek.HairyDog.Enumerations.IncidentType.AttemptedMurder);
            transaction.AddOrUpdate(incidentTypeAttemptedMurder);
            derotek.HairyDog.Metrics.IncidentType incidentTypeRape = new derotek.HairyDog.Metrics.IncidentType("Rape", (int)derotek.HairyDog.Enumerations.IncidentType.Rape);
            transaction.AddOrUpdate(incidentTypeRape);
            derotek.HairyDog.Metrics.IncidentType incidentTypeAttemptedRape = new derotek.HairyDog.Metrics.IncidentType("Attempted rape", (int)derotek.HairyDog.Enumerations.IncidentType.AttemptedRape);
            transaction.AddOrUpdate(incidentTypeAttemptedRape);
            derotek.HairyDog.Metrics.IncidentType incidentTypeVehicleHijack = new derotek.HairyDog.Metrics.IncidentType("Vehicle hijack", (int)derotek.HairyDog.Enumerations.IncidentType.VehicleHijack);
            transaction.AddOrUpdate(incidentTypeVehicleHijack);
            derotek.HairyDog.Metrics.IncidentType incidentTypeAttemptedVehicleHijack = new derotek.HairyDog.Metrics.IncidentType("Attempted vehicle hijack", (int)derotek.HairyDog.Enumerations.IncidentType.AttemptedVehicleHijack);
            transaction.AddOrUpdate(incidentTypeAttemptedVehicleHijack);
            derotek.HairyDog.Metrics.IncidentType incidentTypePublicViolence = new derotek.HairyDog.Metrics.IncidentType("Public violence", (int)derotek.HairyDog.Enumerations.IncidentType.PublicViolence);
            transaction.AddOrUpdate(incidentTypePublicViolence);
            derotek.HairyDog.Metrics.IncidentType incidentTypeTrafficObstruction = new derotek.HairyDog.Metrics.IncidentType("Traffic obstruction", (int)derotek.HairyDog.Enumerations.IncidentType.TrafficObstruction);
            transaction.AddOrUpdate(incidentTypeTrafficObstruction);
            derotek.HairyDog.Metrics.IncidentType incidentTypeTrafficAccident = new derotek.HairyDog.Metrics.IncidentType("Traffic accident", (int)derotek.HairyDog.Enumerations.IncidentType.TrafficAccident);
            transaction.AddOrUpdate(incidentTypeTrafficAccident);
            derotek.HairyDog.Metrics.IncidentType incidentTypeTrafficAttack = new derotek.HairyDog.Metrics.IncidentType("Traffic attack", (int)derotek.HairyDog.Enumerations.IncidentType.TrafficAttack);
            transaction.AddOrUpdate(incidentTypeTrafficAttack);
            derotek.HairyDog.Metrics.IncidentType incidentTypeMugging = new derotek.HairyDog.Metrics.IncidentType("Mugging", (int)derotek.HairyDog.Enumerations.IncidentType.Mugging);
            transaction.AddOrUpdate(incidentTypeMugging);
            derotek.HairyDog.Metrics.IncidentType incidentTypeRobbery = new derotek.HairyDog.Metrics.IncidentType("Robbery", (int)derotek.HairyDog.Enumerations.IncidentType.Robbery);
            transaction.AddOrUpdate(incidentTypeRobbery);
            derotek.HairyDog.Metrics.IncidentType incidentTypePoliceActivity = new derotek.HairyDog.Metrics.IncidentType("Policing activity", (int)derotek.HairyDog.Enumerations.IncidentType.PoliceActivity);
            transaction.AddOrUpdate(incidentTypePoliceActivity);
            derotek.HairyDog.Metrics.IncidentType incidentTypeFire = new derotek.HairyDog.Metrics.IncidentType("Fire", (int)derotek.HairyDog.Enumerations.IncidentType.Fire);
            transaction.AddOrUpdate(incidentTypeFire);
            derotek.HairyDog.Metrics.IncidentType incidentTypeDrugs = new derotek.HairyDog.Metrics.IncidentType("Drugs", (int)derotek.HairyDog.Enumerations.IncidentType.Drugs);
            transaction.AddOrUpdate(incidentTypeDrugs);
            derotek.HairyDog.Metrics.IncidentType incidentTypePoorRoadSurface = new derotek.HairyDog.Metrics.IncidentType("Poor road surface", (int)derotek.HairyDog.Enumerations.IncidentType.PoorRoadSurface);
            transaction.AddOrUpdate(incidentTypePoorRoadSurface);
            derotek.HairyDog.Metrics.IncidentType incidentTypePoorRoadMarkings = new derotek.HairyDog.Metrics.IncidentType("Poor road markinging\\ligthing", (int)derotek.HairyDog.Enumerations.IncidentType.PoorRoadMarkingLighting);
            transaction.AddOrUpdate(incidentTypePoorRoadMarkings);
            derotek.HairyDog.Metrics.IncidentType incidentTypeConstruction = new derotek.HairyDog.Metrics.IncidentType("Construction", (int)derotek.HairyDog.Enumerations.IncidentType.Construction);
            transaction.AddOrUpdate(incidentTypeConstruction);
            derotek.HairyDog.Metrics.IncidentType incidentTypeRoadWorks = new derotek.HairyDog.Metrics.IncidentType("Road-works", (int)derotek.HairyDog.Enumerations.IncidentType.Roadworks);
            transaction.AddOrUpdate(incidentTypeRoadWorks);
            derotek.HairyDog.Metrics.IncidentType incidentTypeFarmAttack = new derotek.HairyDog.Metrics.IncidentType("Farm attack", (int)derotek.HairyDog.Enumerations.IncidentType.FarmAttack);
            transaction.AddOrUpdate(incidentTypeFarmAttack);

            //IdentificationNumberType
            derotek.HairyDog.Metrics.IdentificationNumberType idTypeUnknown = new derotek.HairyDog.Metrics.IdentificationNumberType("Unknown type", (int)derotek.HairyDog.Enumerations.IdentificationNumberType.Unknown);
            transaction.AddOrUpdate(idTypeUnknown);
            derotek.HairyDog.Metrics.IdentificationNumberType idTypeSerialNumber = new derotek.HairyDog.Metrics.IdentificationNumberType("Serial number", (int)derotek.HairyDog.Enumerations.IdentificationNumberType.SerialNumber);
            transaction.AddOrUpdate(idTypeSerialNumber);
            derotek.HairyDog.Metrics.IdentificationNumberType idTypeModelNumber = new derotek.HairyDog.Metrics.IdentificationNumberType("Model number", (int)derotek.HairyDog.Enumerations.IdentificationNumberType.ModelNumber);
            transaction.AddOrUpdate(idTypeModelNumber);
            derotek.HairyDog.Metrics.IdentificationNumberType idTypeRegistrationNumber = new derotek.HairyDog.Metrics.IdentificationNumberType("Registration number", (int)derotek.HairyDog.Enumerations.IdentificationNumberType.RegistrationNumber);
            transaction.AddOrUpdate(idTypeRegistrationNumber);
            derotek.HairyDog.Metrics.IdentificationNumberType idTypePersonalIdNumber = new derotek.HairyDog.Metrics.IdentificationNumberType("Personal identification number", (int)derotek.HairyDog.Enumerations.IdentificationNumberType.PersonsIdentityNumber);
            transaction.AddOrUpdate(idTypePersonalIdNumber);
            derotek.HairyDog.Metrics.IdentificationNumberType idTypeCellPhoneIMEINumber = new derotek.HairyDog.Metrics.IdentificationNumberType("Cell phone IMEI number", (int)derotek.HairyDog.Enumerations.IdentificationNumberType.CellPhoneImeiNumber);
            transaction.AddOrUpdate(idTypeCellPhoneIMEINumber);
            derotek.HairyDog.Metrics.IdentificationNumberType idTypeEngineNumber = new derotek.HairyDog.Metrics.IdentificationNumberType("Engine number", (int)derotek.HairyDog.Enumerations.IdentificationNumberType.EngineNumber);
            transaction.AddOrUpdate(idTypeEngineNumber);

            //Country
            derotek.HairyDog.Country countrySouthAfrica = new derotek.HairyDog.Country(27, "South Africa");
            transaction.AddOrUpdate(countrySouthAfrica);

            //Provinces
            derotek.HairyDog.Province provinceGauteng = new derotek.HairyDog.Province(countrySouthAfrica, "Gauteng");
            transaction.AddOrUpdate(provinceGauteng);
            derotek.HairyDog.Province provinceFreeState = new derotek.HairyDog.Province(countrySouthAfrica, "Free-State");
            transaction.AddOrUpdate(provinceFreeState);
            derotek.HairyDog.Province provinceNatal = new derotek.HairyDog.Province(countrySouthAfrica, "Natal");
            transaction.AddOrUpdate(provinceNatal);
            derotek.HairyDog.Province provinceWesternCape = new derotek.HairyDog.Province(countrySouthAfrica, "Western Cape");
            transaction.AddOrUpdate(provinceWesternCape);
            //Towns
            derotek.HairyDog.Town townPretoria = new derotek.HairyDog.Town(provinceGauteng, "Pretoria");
            transaction.AddOrUpdate(townPretoria);
            derotek.HairyDog.Town townJohannesburg = new derotek.HairyDog.Town(provinceGauteng, "Johannesburg");
            transaction.AddOrUpdate(townJohannesburg);
            derotek.HairyDog.Town townCapeTown = new derotek.HairyDog.Town(provinceWesternCape, "CapeTown");
            transaction.AddOrUpdate(townCapeTown);

            //Delivery Method
            derotek.HairyDog.Metrics.DeliveryMethod deliveryMethodEMail = new derotek.HairyDog.Metrics.DeliveryMethod("eMail", (int)derotek.HairyDog.Enumerations.DeliveryMethod.eMail);
            transaction.AddOrUpdate(deliveryMethodEMail);

            //Receive notifications Method
            derotek.HairyDog.Metrics.ReceiveNotificationsFrequency receiveNotificationsMethodMethodImmediate = new derotek.HairyDog.Metrics.ReceiveNotificationsFrequency("Immediate", (int)derotek.HairyDog.Enumerations.ReceiveNotificationsFrequency.Immediate);
            transaction.AddOrUpdate(receiveNotificationsMethodMethodImmediate);
            derotek.HairyDog.Metrics.ReceiveNotificationsFrequency receiveNotificationsMethodMethodNever = new derotek.HairyDog.Metrics.ReceiveNotificationsFrequency("Never", (int)derotek.HairyDog.Enumerations.ReceiveNotificationsFrequency.Never);
            transaction.AddOrUpdate(receiveNotificationsMethodMethodNever);

            //Basic user
            derotek.HairyDog.Users.User user = new derotek.HairyDog.Users.User();
            user.EMailAddress = "dkomen@gmail.com";
            user.Name = "Dean";
            user.Surname = "Komen";
            user.ContactNumber = "1";
            user.Password = "x";
            user.VerifyPassword = "x";
            user.LastLoginDate = System.DateTime.Now;
            user.Province = provinceGauteng;
            user.SecurityQuestion = "Where was I born?";
            user.SecurityQuestionAnswer = "Durban";
            user.Town = townPretoria;
            user.UserRating = 250;
            user.ReceiveNotificationsFrequency = receiveNotificationsMethodMethodImmediate;
            user.NotificationsDeliveryMethod = deliveryMethodEMail;
            transaction.AddOrUpdate(user);

            //Attachment Types
            derotek.HairyDog.Metrics.AttachmentType attachTypeNone = new derotek.HairyDog.Metrics.AttachmentType("None", (long)derotek.HairyDog.Enumerations.AttachmentType.None);
            transaction.AddOrUpdate(attachTypeNone);
            derotek.HairyDog.Metrics.AttachmentType attachTypeAudio = new derotek.HairyDog.Metrics.AttachmentType("Audio", (long)derotek.HairyDog.Enumerations.AttachmentType.Audio);
            transaction.AddOrUpdate(attachTypeAudio);
            derotek.HairyDog.Metrics.AttachmentType attachTypeDocument = new derotek.HairyDog.Metrics.AttachmentType("Document", (long)derotek.HairyDog.Enumerations.AttachmentType.Document);
            transaction.AddOrUpdate(attachTypeDocument);
            derotek.HairyDog.Metrics.AttachmentType attachTypePicture = new derotek.HairyDog.Metrics.AttachmentType("Picture", (long)derotek.HairyDog.Enumerations.AttachmentType.Picture);
            transaction.AddOrUpdate(attachTypePicture);
            derotek.HairyDog.Metrics.AttachmentType attachTypeVideo = new derotek.HairyDog.Metrics.AttachmentType("Video", (long)derotek.HairyDog.Enumerations.AttachmentType.Video);
            transaction.AddOrUpdate(attachTypeVideo);

            //Property Types
            derotek.HairyDog.Metrics.PropertyType propertyTypePassengerVehicle = new derotek.HairyDog.Metrics.PropertyType("Passenger vehicle and SUV", (long)derotek.HairyDog.Enumerations.PropertyType.PassengerVehicleSuv);
            transaction.AddOrUpdate(propertyTypePassengerVehicle);
            derotek.HairyDog.Metrics.PropertyType propertyTypeSuv = new derotek.HairyDog.Metrics.PropertyType("Sport equipment(SCUBA, Sky-diving)", (long)derotek.HairyDog.Enumerations.PropertyType.Sport);
            transaction.AddOrUpdate(propertyTypeSuv);
            derotek.HairyDog.Metrics.PropertyType propertyTypeMpv = new derotek.HairyDog.Metrics.PropertyType("Multipurpose vehicle (MPV, Pickup, bakkie etc)", (long)derotek.HairyDog.Enumerations.PropertyType.MPV);
            transaction.AddOrUpdate(propertyTypeMpv);
            derotek.HairyDog.Metrics.PropertyType propertyTypeSmallTruck = new derotek.HairyDog.Metrics.PropertyType("Home robbery", (long)derotek.HairyDog.Enumerations.PropertyType.HomeRobbery);
            transaction.AddOrUpdate(propertyTypeSmallTruck);
            derotek.HairyDog.Metrics.PropertyType propertyTypelargeTruck = new derotek.HairyDog.Metrics.PropertyType("Truck (18-Wheeler, 10-Ton, 1-Ton)", (long)derotek.HairyDog.Enumerations.PropertyType.Truck);
            transaction.AddOrUpdate(propertyTypelargeTruck);
            derotek.HairyDog.Metrics.PropertyType propertyTypeHeavyIndustrial = new derotek.HairyDog.Metrics.PropertyType("Heavy industrial (Earth mover, mine truck, crane)", (long)derotek.HairyDog.Enumerations.PropertyType.HeavyIndustrialVehicle);
            transaction.AddOrUpdate(propertyTypeHeavyIndustrial);
            derotek.HairyDog.Metrics.PropertyType propertyTypePerformanceCar = new derotek.HairyDog.Metrics.PropertyType("Performance car (Modified street car, super\\sports car)", (long)derotek.HairyDog.Enumerations.PropertyType.PerformanceVehicle);
            transaction.AddOrUpdate(propertyTypePerformanceCar);
            derotek.HairyDog.Metrics.PropertyType propertyTypeRacingCar = new derotek.HairyDog.Metrics.PropertyType("Racing car (Track car, racing go-cart)", (long)derotek.HairyDog.Enumerations.PropertyType.RacingCar);
            transaction.AddOrUpdate(propertyTypeRacingCar);
            derotek.HairyDog.Metrics.PropertyType propertyTypeOffroadVehicle = new derotek.HairyDog.Metrics.PropertyType("Offroad vehicle (Sandmaster, rally car)", (long)derotek.HairyDog.Enumerations.PropertyType.OffroadVehicle);
            transaction.AddOrUpdate(propertyTypeOffroadVehicle);
            derotek.HairyDog.Metrics.PropertyType propertyTypeMotorcycleRoad = new derotek.HairyDog.Metrics.PropertyType("Cell phone", (long)derotek.HairyDog.Enumerations.PropertyType.CellPhone);
            transaction.AddOrUpdate(propertyTypeMotorcycleRoad);
            derotek.HairyDog.Metrics.PropertyType propertyTypeMotorcycleOffRoad = new derotek.HairyDog.Metrics.PropertyType("Motorcycle", (long)derotek.HairyDog.Enumerations.PropertyType.Motorcycle);
            transaction.AddOrUpdate(propertyTypeMotorcycleOffRoad);
            derotek.HairyDog.Metrics.PropertyType propertyTypeBicycle = new derotek.HairyDog.Metrics.PropertyType("Bicycle (MTB, Road racing, BMX)", (long)derotek.HairyDog.Enumerations.PropertyType.Bicycle);
            transaction.AddOrUpdate(propertyTypeBicycle);
            derotek.HairyDog.Metrics.PropertyType propertyTypeElectronicEqipment = new derotek.HairyDog.Metrics.PropertyType("Electronics (TV, Hifi, SatNav, Computer)", (long)derotek.HairyDog.Enumerations.PropertyType.ElectronicEquipment);
            transaction.AddOrUpdate(propertyTypeElectronicEqipment);
            derotek.HairyDog.Metrics.PropertyType propertyTypeClothingAndLinen = new derotek.HairyDog.Metrics.PropertyType("Clothing and Linen (Normal clothing, blanket, towel)", (long)derotek.HairyDog.Enumerations.PropertyType.ClothingAndLinen);
            transaction.AddOrUpdate(propertyTypeClothingAndLinen);
            derotek.HairyDog.Metrics.PropertyType propertyTypeGardening = new derotek.HairyDog.Metrics.PropertyType("Landscaping (Lawnmower, garden dustbin)", (long)derotek.HairyDog.Enumerations.PropertyType.GardeningEquipment);
            transaction.AddOrUpdate(propertyTypeGardening);
            derotek.HairyDog.Metrics.PropertyType propertyTypeAviation = new derotek.HairyDog.Metrics.PropertyType("Aviation (Helicopter, aeroplane, microlight)", (long)derotek.HairyDog.Enumerations.PropertyType.Aviation);
            transaction.AddOrUpdate(propertyTypeAviation);
            derotek.HairyDog.Metrics.PropertyType propertyTypeBuildingMaterials = new derotek.HairyDog.Metrics.PropertyType("Building and materials (Wheelbarrow, bricks, Impact hammer)", (long)derotek.HairyDog.Enumerations.PropertyType.BuildingMaterials);
            transaction.AddOrUpdate(propertyTypeBuildingMaterials);
            derotek.HairyDog.Metrics.PropertyType propertyTypeArt = new derotek.HairyDog.Metrics.PropertyType("Art works (Fine art, scupltures, masks)", (long)derotek.HairyDog.Enumerations.PropertyType.Art);
            transaction.AddOrUpdate(propertyTypeArt);
            derotek.HairyDog.Metrics.PropertyType propertyTypOther = new derotek.HairyDog.Metrics.PropertyType("Other", (long)derotek.HairyDog.Enumerations.PropertyType.Other);
            transaction.AddOrUpdate(propertyTypOther);
            derotek.HairyDog.Metrics.PropertyType propertyTypPhotographicEquipment = new derotek.HairyDog.Metrics.PropertyType("Photographic equipment", (long)derotek.HairyDog.Enumerations.PropertyType.PhotographicEquipment);
            transaction.AddOrUpdate(propertyTypPhotographicEquipment);
            derotek.HairyDog.Metrics.PropertyType propertyTypFirearmsExplosives = new derotek.HairyDog.Metrics.PropertyType("Firearms and explosives", (long)derotek.HairyDog.Enumerations.PropertyType.FirearmsExplosives);
            transaction.AddOrUpdate(propertyTypFirearmsExplosives);

            //LocationAccuracy
            derotek.HairyDog.Metrics.LocationAccuracy locationAccuracyExact = new derotek.HairyDog.Metrics.LocationAccuracy("Exact coordinates", (long)derotek.HairyDog.Enumerations.LocationAccuracy.Exact);
            transaction.AddOrUpdate(locationAccuracyExact);
            derotek.HairyDog.Metrics.LocationAccuracy locationAccuracyAbout350m = new derotek.HairyDog.Metrics.LocationAccuracy("Upto 350meters way", (long)derotek.HairyDog.Enumerations.LocationAccuracy.About350m);
            transaction.AddOrUpdate(locationAccuracyAbout350m);
            transaction.Commit();
        }        
    }
    protected void uxAddTestData_Click(object sender, EventArgs e)
    {
        using (DORM.Interfaces.ITransaction transaction = derotek.Fnh.DataAccess.DormAccessManager.GetFnhDataAccessor().TransactionCreate(true))
        {
            derotek.HairyDog.Province province = transaction.RetrieveFirst<derotek.HairyDog.Province>("Description", "Gauteng");
            derotek.HairyDog.Metrics.IdentificationNumberType idNumberType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.IdentificationNumberType>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.IdentificationNumberType.SerialNumber);
            derotek.HairyDog.Metrics.LocationAccuracy locationAccruracy = transaction.RetrieveFirst<derotek.HairyDog.Metrics.LocationAccuracy>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.LocationAccuracy.About350m);
            derotek.HairyDog.Metrics.PropertyType propertyType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.PropertyType>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.PropertyType.Bicycle);
            derotek.HairyDog.Metrics.AttachmentType attachmentType = transaction.RetrieveFirst<derotek.HairyDog.Metrics.AttachmentType>("ExternalReferanceId", (long)derotek.HairyDog.Enumerations.AttachmentType.Picture);

            for (int i = 0; i <= 1000; i++)
            {
                derotek.HairyDog.Users.User user = new derotek.HairyDog.Users.User();
                user.EMailAddress = i.ToString() + "@gmail.com";
                user.Name = "Dean" + i.ToString();
                user.Surname = "Komen";
                user.ContactNumber = i.ToString();
                user.Password = "x";
                user.VerifyPassword = "x";
                user.Province = province;
                transaction.AddOrUpdate(user);

                for (int itemLoop = 0; itemLoop < 5; itemLoop++)
                {
                    derotek.HairyDog.RegisteredItem item = new derotek.HairyDog.RegisteredItem();
                    item.UserId = user.Id;
                    item.PropertyType = propertyType;
                    item.Description = "Description " + i.ToString() + "_" + itemLoop.ToString();
                    //item.IdentificationNumberType = idNumberType;
                    //item.IdentificationNumber = "1234567890abcdef";
                    item.IsStolen = true;
                    item.IfIsStolenMustNotifyCommunity = false;
                    item.DateWasStolen = System.DateTime.Now;
                    item.LocationAccuracy = locationAccruracy;
                    double longitude = 28.186703491210892;
                    double latitude = -25.747204870414322;
                    derotek.HairyDog.CoordinateObfuscator.ObfuscateCoordinates(locationAccruracy, longitude, latitude, out longitude, out latitude);
                    item.Longitude = longitude;
                    item.Latitude = latitude;
                    item.Notes = "aaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaaxaaaaaaaaax";
                    transaction.AddOrUpdate(item);

                    for (int attachmentLoop = 0; attachmentLoop < 2; attachmentLoop++)
                    {
                        derotek.HairyDog.Attachment attachment = new derotek.HairyDog.Attachment();
                        attachment.AttachmentType = attachmentType;
                        attachment.UserId = item.Id;
                        attachment.Description = "Attachment description " + i.ToString() + "_" + itemLoop.ToString() + "_" + attachmentLoop.ToString();
                        attachment.PictureShowIdNumberType = idNumberType;
                        transaction.AddOrUpdate(attachment);
                    }

                }
            }
            transaction.Commit();
        }
    }
}
