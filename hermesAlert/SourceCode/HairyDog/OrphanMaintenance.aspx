﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrphanMaintenance.aspx.cs" Inherits="OrphanMaintenance" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Label ID="Label1" runat="server" Text="Label" SkinID="Normal">Original files deleted : </asp:Label>&nbsp;
        <asp:Label ID="uxOrphanedOriginalsCount" runat="server" Text="Label" SkinID="Normal"></asp:Label>&nbsp;&nbsp;&nbsp;
        <asp:Label ID="uxOrphanedOriginalsSize" runat="server" Text="Label" SkinID="Normal"></asp:Label>&nbsp;<asp:Label ID="Label2" runat="server" Text="Label" SkinID="Normal">Mb</asp:Label><br />

        <asp:Label ID="Label3" runat="server" Text="Label" SkinID="Normal">Thumb files deleted : </asp:Label>&nbsp;
        <asp:Label ID="uxOrphanedThumbsCount" runat="server" Text="Label" SkinID="Normal"></asp:Label>&nbsp;&nbsp;&nbsp;
        <asp:Label ID="uxOrphanedThumbsSize" runat="server" Text="Label" SkinID="Normal"></asp:Label>&nbsp;<asp:Label ID="Label6" runat="server" Text="Label" SkinID="Normal">Mb</asp:Label><br />

        <asp:Label ID="Label7" runat="server" Text="Label" SkinID="Normal">Resized files deleted : </asp:Label>&nbsp;
        <asp:Label ID="uxOrphanedResizedCount" runat="server" Text="Label" SkinID="Normal"></asp:Label>&nbsp;&nbsp;&nbsp;
        <asp:Label ID="uxOrphanedResizedSize" runat="server" Text="Label" SkinID="Normal"></asp:Label>&nbsp;<asp:Label ID="Label10" runat="server" Text="Label" SkinID="Normal">Mb</asp:Label><br />

        <br /><br /> <br />
        <asp:Label ID="uxMessage" runat="server" Text="Label" SkinID="NormalNoWidthGiven"></asp:Label>
    </div>
    </form>
</body>
</html>
