﻿<%@ WebHandler Language="C#" Class="Upload" %>

using System;
using System.Web;

public class Upload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string pathToSaveTo = System.IO.Path.Combine(context.Request.PhysicalApplicationPath, "Uploads");
            HttpPostedFile uploadFileData = context.Request.Files["filedata"];
            
            string uploadFileName = derotek.HairyDog.Graphics.ImageManipulation.GetNewSystemFileName(uploadFileData.FileName);
            
            uploadFileName = System.IO.Path.Combine(pathToSaveTo, uploadFileName);
            uploadFileData.SaveAs(uploadFileName);           
            uploadFileData.InputStream.Close();
            System.Threading.Thread.Sleep(50);
            context.Response.Write(System.IO.Path.GetFileName(uploadFileName));
        }
        catch (Exception ex)
        {
            context.Response.Write("0");
        }
    }    
    
    public bool IsReusable
    {
        get { throw new NotImplementedException(); }
    }
}