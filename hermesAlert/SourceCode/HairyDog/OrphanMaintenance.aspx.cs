﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OrphanMaintenance : System.Web.UI.Page , derotek.BikeCasa.Mvp.Orphans.IOrphansView
{
    protected void Page_Load(object sender, EventArgs e)
    {
        derotek.BikeCasa.Mvp.Orphans.OrphansPresenter presenter = new derotek.BikeCasa.Mvp.Orphans.OrphansPresenter(this);
        presenter.DeleteOrphans(SessionVariables.RootPath(false) + "Uploads",SessionVariables.RootPath(false) + "Uploads\\Thumbs",SessionVariables.RootPath(false) + "Uploads\\Resized");
    }
    public KeyValuePair<int, float> OrphanAttachmentsThumbsDeleted
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxOrphanedThumbsCount.Text = value.Key.ToString();
            uxOrphanedThumbsSize.Text = (value.Value/1024/1024).ToString();
        }
    }

    public KeyValuePair<int, float> OrphanAttachmentsResizedDeleted
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxOrphanedResizedCount.Text = value.Key.ToString();
            uxOrphanedResizedSize.Text = (value.Value/1024/1024).ToString();
        }
    }

    public KeyValuePair<int, float> OrphanAttachmentsOriginalsDeleted
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxOrphanedOriginalsCount.Text = value.Key.ToString();
            uxOrphanedOriginalsSize.Text = (value.Value/1024/1024).ToString();
        }
    }

    public string Message
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxMessage.Text = value;
        }
    }
}