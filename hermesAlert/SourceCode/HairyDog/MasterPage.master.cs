﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage, derotek.BikeCasa.Mvp.IViewMasterPage
{
    #region Fields
    public derotek.BikeCasa.Mvp.ViewMasterPagePresenter _presenter;
    #endregion

    #region Form Events
    protected void Page_Load(object sender, EventArgs e)
    {
        _presenter = new derotek.BikeCasa.Mvp.ViewMasterPagePresenter(this);
        if (!IsPostBack)
        {
            _presenter.PrepareView(SessionVariables.RootPath(true));
        }
        
    }

    //protected void uxRedirectToUserMaintenance_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/Pages_User/RegisterUser.aspx");
    //}
    //protected void uxRedirectToUserHome_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/Pages_User/UserRegisteredItems.aspx");
    //}
    //protected void uxRedirectToItemsMonitoring_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/Pages_Monitoring/ItemsMonitoring.aspx");
    //}
    //protected void uxRedirectToSearch_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/Pages_Search/Search.aspx");
    //}
    //protected void uxRedirectToQuickReport_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/Pages_Reporting/QuickReporting.aspx");
    //}
    //protected void uxRedirectToMissingPersons_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/Pages_Reporting/MissingPersons.aspx");
    //}
    //protected void uxRedirectToFaq_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/Pages_Main/Faq.aspx");
    //}
    //protected void uxRedirectToHome_Click(object sender, EventArgs e)
    //{
    //    _presenter.RedirectView("~/default.aspx");
    //}
   

    protected void uxLogout_Click(object sender, EventArgs e)
    {
        SessionVariables.CurrentUserSet(Session,null);
        //_presenter.RedirectView("~/Pages_User/Logon.aspx");
        _presenter.RedirectView("~/default.aspx");
    }
    #endregion

    public bool ShowRedirectionOptions
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxRedirectToUserMaintenance.Visible = value;
            uxRedirectToUserHome.Visible = value;
            uxLogout.Visible = value;
            uxRedirectToItemsMonitoring.Visible = value;
            uxRedirectToSearchDatabase.Visible = value;
            uxRedirectToQuickReport.Visible = value;
            uxRedirectToHome.Visible = !value;
            uxRedirectToMissingPersons.Visible = value;
            uxRedirectToGroups.Visible = false;
        }
    }

    public long CurrentUserId
    {
        get
        {
            return SessionVariables.CurrentUserGet(this.Session).Id;
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public void DisplayMessage(string message, bool isErrorMessage)
    {
        throw new NotImplementedException();
    }
    public void RedirectView(string path)
    {
        Response.Redirect(path);
    }

    public string PageFooter
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            uxPageFooter.Text = value;
        }
    }

    public long ActiveObjectId
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            throw new NotImplementedException();
        }
    }

    public derotek.HairyDog.Users.User LoggedInUserDetails
    {
        get
        {
            throw new NotImplementedException();
        }
        set
        {
            if (value.Id != 0)
            {
                uxLoggedInUser.Text = value.Name + " " + value.Surname + "(" + value.UserRating + " activity points)";
            }
        }
    }

    public derotek.HairyDog.Users.User CurrentUser
    {
        get
        {
            return SessionVariables.CurrentUserGet(Session);
        }
        set
        {
            SessionVariables.CurrentUserSet(Session, value);
        }
    }

    public string ReturnToPage
    {
        get
        {
            return ((PageBase)Page).ReturnToPage;
        }
        set
        {
            ((PageBase)Page).ReturnToPage = value;
        }
    }
}
