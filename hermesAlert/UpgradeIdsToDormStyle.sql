sp_RENAME 'Attachment.[AttachmentType_id]' , 'AttachmentTypeId', 'COLUMN'
GO
sp_RENAME 'Attachment.[PictureShowIdNumberType_id]' , 'PictureShowIdNumberTypeId', 'COLUMN'
GO
sp_RENAME 'Incident.[LocationAccuracy_id]' , 'LocationAccuracyId', 'COLUMN'
GO
sp_RENAME 'Incident.[IncidentType_id]' , 'IncidentTypeId', 'COLUMN'
GO
sp_RENAME 'ItemDetection.[LocationAccuracy_id]' , 'LocationAccuracyId', 'COLUMN'
GO
sp_RENAME 'ItemReport.[ItemType_id]' , 'ItemTypeId', 'COLUMN'
GO
sp_RENAME 'PersonAlert.[ItemType_id]' , 'ItemTypeId', 'COLUMN'
GO
sp_RENAME 'PersonAlert.[ApprovalStatus_id]' , 'ApprovalStatusId', 'COLUMN'
GO
sp_RENAME 'PersonAlert.[LocationAccuracy_id]' , 'LocationAccuracyId', 'COLUMN'
GO
sp_RENAME 'PersonAlert.[EyeColour_id]' , 'EyeColourId', 'COLUMN'
GO
sp_RENAME 'PersonAlert.[HairColour_id]' , 'HairColourId', 'COLUMN'
GO
sp_RENAME 'PersonAlert.[Gender_id]' , 'GenderId', 'COLUMN'
GO
sp_RENAME 'Province.[Country_id]' , 'CountryId', 'COLUMN'
GO
sp_RENAME 'Town.[Province_id]' , 'ProvinceId', 'COLUMN'
GO
sp_RENAME 'RegisteredItem.[PropertyType_id]' , 'PropertyTypeId', 'COLUMN'
GO
sp_RENAME 'RegisteredItem.[LocationAccuracy_id]' , 'LocationAccuracyId', 'COLUMN'
GO
sp_RENAME 'User.[Province_id]' , 'ProvinceId', 'COLUMN'
GO
sp_RENAME 'User.[Town_id]' , 'TownId', 'COLUMN'
GO
sp_RENAME 'UserRole.[Role_id]' , 'RoleId', 'COLUMN'
GO
sp_RENAME 'Incident.[LocationAccuracyId]' , 'LocationAccuracyId', 'COLUMN'
GO
sp_RENAME 'Incident.[IncidentTypeId]' , 'IncidentTypeId', 'COLUMN'
GO


sp_RENAME 'Attachment.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'Group.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'Incident.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'ItemDetection.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'ItemReport.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'MonitoringLocation.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'PersonAlert.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'RegisteredItem.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'UploadedFile.[OwnerId]' , 'UserId', 'COLUMN'
GO
sp_RENAME 'UserRole.[OwnerId]' , 'UserId', 'COLUMN'
GO

