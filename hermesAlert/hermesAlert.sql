USE [hermesAlert]
GO
/****** Object:  StoredProcedure [dbo].[SearchForStolenIdentificationNumber]    Script Date: 05/30/2013 09:25:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchForStolenIdentificationNumber]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SearchForStolenIdentificationNumber] 
	@SearchTerm varchar(50),
	@MaximumRecords int
AS
BEGIN
	SELECT TOP(@MaximumRecords) a.UserId as Id 
	FROM Attachment a
	JOIN RegisteredItem ri ON ri.Id = a.UserId
	WHERE 
		ri.IsStolen=1
		AND ri.IfIsStolenMustNotifyCommunity=1
		AND a.Description LIKE(''%'' + @SearchTerm + ''%'')
END

' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[LocationsBeingMonitored]    Script Date: 05/30/2013 09:25:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LocationsBeingMonitored]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[LocationsBeingMonitored]
(	
	-- Add the parameters for the function here
	@UserId bigint
)
RETURNS TABLE 
AS
RETURN 
(
	SELECT Id, Longitude, Latitude, RadiusInMeters FROM MonitoringLocation
	WHERE UserId = COALESCE(@UserId, UserId)
)

' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetUsersMonitoringGpsLocation]    Script Date: 05/30/2013 09:25:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetUsersMonitoringGpsLocation]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Dean Komen
-- Create date: 29 Oct 2011
-- Description:	Return a list of userIs''s of users that are monitoring the area in in which the supplied gps coordinate lies
-- =============================================
CREATE PROCEDURE [dbo].[GetUsersMonitoringGpsLocation]
	@SqlGeographyPoint Geography
AS
BEGIN
	SET NOCOUNT ON; 

	SELECT 
		DISTINCT(u.Id) as ''Id'', u.*
	FROM 
		MonitoringLocation as ml
	LEFT JOIN [dbo].[User] as u ON u.Id = ml.UserId
	WHERE 
		(CONVERT(Geography, SqlGeographyPoint).STDistance(@SqlGeographyPoint)) <= RadiusInMeters	
		--Point.STDistance(@SqlGeographyPoint) <= 1000000		
	
END 
' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetStolenItemsInAreas]    Script Date: 05/30/2013 09:25:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStolenItemsInAreas]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[GetStolenItemsInAreas]
	-- Add the parameters for the stored procedure here
	@UserId bigint, 
	@PropertyTypeToGet bigint,
	@MaximumRecordsPerLocation int,
	@MaximumLocationsBeingMonitored int,
	@DateWasStolenStart DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF(@UserId=0)
	BEGIN 
		SET @UserId = NULL
	END
	Select Identity(int, 1,1) AS PK, Id, Longitude, Latitude, RadiusInMeters
	Into   #LocationsBeingMonitored
	From   LocationsBeingMonitored(@UserId)

	DECLARE @TempTable TABLE
    ( 
		Id bigint
    ) 

	DECLARE @RecordsMonitoring int
    DECLARE @Record int
    DECLARE @Long float
    DECLARE @Lat float
    DECLARE @RadiusInMeters int
    
    SELECT @RecordsMonitoring = MAX(PK) From #LocationsBeingMonitored    
    SET @Record = 1;
    
    WHILE @Record <= @RecordsMonitoring --For each location being monitored
    BEGIN
		
		SET @Long = (SELECT Longitude FROM #LocationsBeingMonitored WHERE PK = @Record)
		SET @Lat = (SELECT Latitude FROM #LocationsBeingMonitored WHERE PK = @Record)
		SET @RadiusInMeters = (SELECT RadiusInMeters FROM #LocationsBeingMonitored WHERE PK = @Record)
		if(@PropertyTypeToGet=0)
		BEGIN
			INSERT INTO @TempTable SELECT top(@MaximumRecordsPerLocation) Id 
			FROM RegisteredItem 
			WHERE 
				(CONVERT(Geography, SqlGeographyPoint).STDistance(CONVERT(Geography, ''POINT ('' + CONVERT(char,@Long) + '' '' + CONVERT(char,@Lat) + '')''))) <= @RadiusInMeters  
				AND IsStolen=1 
				AND IfIsStolenMustNotifyCommunity=1
				AND DateWasStolen >= @DateWasStolenStart
				--ORDER BY DateWasStolen
		END
		ELSE
		BEGIN
			INSERT INTO @TempTable SELECT top(@MaximumRecordsPerLocation) Id 
			FROM RegisteredItem 
			WHERE 
				(CONVERT(Geography, SqlGeographyPoint).STDistance(CONVERT(Geography, ''POINT ('' + CONVERT(char,@Long) + '' '' + CONVERT(char,@Lat) + '')''))) <= @RadiusInMeters  
				AND IsStolen=1 
				AND IfIsStolenMustNotifyCommunity=1
				AND PropertyTypeId = @PropertyTypeToGet
				AND DateWasStolen >= @DateWasStolenStart
		END
			SET @Record = @Record + 1
    END
	SELECT * FROM @TempTable	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[GetEvents]    Script Date: 05/30/2013 09:25:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEvents]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		Dean Komen
-- Create date: 11 Aug 2011
-- Description:	Get all the incident in the monitoring areas for a user within a date range
-- =============================================
-- EXEC [dbo].[GetEvents] null, null, 200, ''1 May 2013'', ''30 May 2013''
CREATE PROCEDURE [dbo].[GetEvents]
	-- Add the parameters for the stored procedure here
	@UserId bigint = NULL, 
	@EventTypeToGet bigint = NULL,
	@MaximumRecordsPerLocation int,
	@DateStart DateTime,
	@DateEnd DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TempTable TABLE
	( 
		Id bigint
	) 

	if(@UserId IS NOT NULL)
	BEGIN
		-- ==========================
		-- Get all the incidents in the monitoring areas for a specific user within the date range
		-- ==========================
	
		--Get all area monitored by the user=@UserId
		Select Identity(int, 1,1) AS PK, Id, Longitude, Latitude, RadiusInMeters
		Into   #LocationsBeingMonitored
		From   LocationsBeingMonitored(@UserId)

		DECLARE @RecordsMonitoring int
		DECLARE @Record int
		DECLARE @Long float
		DECLARE @Lat float
		DECLARE @RadiusInMeters int
	    
		SELECT @RecordsMonitoring = MAX(PK) From #LocationsBeingMonitored    
		SET @Record = 1;
	    
		WHILE @Record <= @RecordsMonitoring --For each location being monitored
		BEGIN
			SET @Long = (SELECT Longitude FROM #LocationsBeingMonitored WHERE PK = @Record)
			SET @Lat = (SELECT Latitude FROM #LocationsBeingMonitored WHERE PK = @Record)
			SET @RadiusInMeters = (SELECT RadiusInMeters FROM #LocationsBeingMonitored WHERE PK = @Record)
			
			INSERT INTO @TempTable SELECT top(@MaximumRecordsPerLocation) Id 
			FROM Incident 
			WHERE 
				(CONVERT(Geography, SqlGeographyPoint).STDistance(CONVERT(Geography, ''POINT ('' + CONVERT(char,@Long) + '' '' + CONVERT(char,@Lat) + '')''))) <= @RadiusInMeters  
				AND Id = COALESCE(@EventTypeToGet,Id)
				AND IncidentDateTime >= @DateStart AND IncidentDateTime <= @DateEnd
				ORDER BY IncidentDateTime DESC
			SET @Record = @Record + 1
		END	
	END
	ELSE
	BEGIN	
		-- ==========================
		-- Get all the incidents within the date range
		-- ==========================
		INSERT INTO @TempTable SELECT Id 
		FROM Incident 
		WHERE 
			Id = COALESCE(@EventTypeToGet,Id)
			AND IncidentDateTime >= @DateStart AND IncidentDateTime <= @DateEnd
			ORDER BY IncidentDateTime DESC
	END
	
	SELECT * FROM @TempTable
END

' 
END
GO
