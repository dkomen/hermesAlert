# hermesAlert
A system for reporting many types of 'incidents' (theft, murder, fires, pot holes etc etc) on google maps (uses current GPS coordinates for maps initial location). 
These incident reports are then mailed to other members of the site that may have elected to monitor the area(s) in which 
the incident was reported. You can also privately register your personal items of interest(camera, car, gun, works of art etc) 
and also add multiple photos to a particular registered item as taken from the included android application. 
Missing persons may also be reported... with a photo of them, police case numbers, contact numbers, description, google map of where person was last seen etc. All people monitoring the area(s) where the missing person was last sighted will be notified by email.

ASP.Net, C#, android, google maps, MS Sql Server 
